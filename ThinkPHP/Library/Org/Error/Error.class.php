<?php
namespace Org\Error;
class Error {
	const SUCCESS_OK                    = 0;
	const ERROR_GENERAL                 = -1;
	const ERROR_WECHATMENU_SYSTEMBUSY   = 10001;
	const ERROR_NOT_MODULE_AUTHORITY    = 10002;
	const ERROR_NOT_ACTION_AUTHORITY    = 10003;
	const ERROR_NOT_LOGIN               = 10004;
	const ERROR_WRONG_LOGIN_INFO        = 10005;
	const ERROR_WRONG_VERIFY            = 10006;
	const ERROR_COMPANY_NOTEXIST        = 10007;
	const ERROR_COMPANY_SERVICE_OVERDUE = 10008;
	const ERROR_COMPANY_NOT_BIND_ACCESS = 10009;
	const ERROR_COMPANY_WRONG_ACCESS    = 10010;
	const ERROR_DB_VOID_MENU            = 10011;
	const ERROR_DB_VOID_INDEX           = 10012;
	const ERROR_PARAMS_VOID             = 10013;
	const ERROR_PARAMS_ERR              = 10014;
	const ERROR_ADD_HANDLE_ERR          = 10015;
	const ERROR_EDIT_HANDLE_ERR         = 10016;
	const ERROR_DELETE_HANDLE_ERR       = 10017;
	const ERROR_DB_VOID_PRODUCT         = 10018;
	const ERROR_COMPANY_BIND_ACCESS     = 10019;
	const ERROR_TIMESTAMP_NULL          = 10020;
	const ERROR_TOKEN_NULL              = 10021;
	const ERROR_TOKEN_WRONG             = 10022;
	const ERROR_EXCHANGE_UNLUCK         = 10023;
	const ERROR_EXCHANGE_REPERT         = 10024;
	const ERROR_MENU_ROOT_COUNT         = 10025;
	const ERROR_MENU_SUB_COUNT          = 10026;
	const ERROR_MENU_TOKEN_EMPTY        = 10027;
	const ERROR_MENU_ROOT_LONG          = 10028;
	const ERROR_MENU_SUB_LONG           = 10029;
	const ERROR_KEYWORD_EXIST           = 10030;
	const ERROR_FW_NAME_EXIST           = 10031;
	const ERROR_BRAND_TAGS_NAME_EXIST   = 10032;
	const ERROR_USER_PWD_ERROR          = 10033;
	const ERROR_DEALER_NAME_EXIST       = 10034;
	const ERROR_FW_BIND_EXIST           = 10035;
	const ERROR_LUCKY_LUCKYNUM_OVER     = 10036;
	const ERROR_LUCKY_LUCKYNUM_NULL     = 10037;
	const ERROR_LUCKY_TIME_REPERT       = 10038;
	const ERROR_LUCKY_NAME_EXIST        = 10039;
	const ERROR_NEWS_NAME_EXIST         = 10040;
	const ERROR_MENU_REPLY_EMPTY        = 10041;
	const ERROR_LUCKY_PRIZE_TYPE_EXIST  = 10042;
	const ERROR_LUCKY_PRIZE_NAME_EXIST  = 10043;
	const ERROR_PRODUCT_NAME_EXIST      = 10044;
	const ERROR_QR_FILE_COLUMN_ERROR    = 10045;
	const ERROR_FW_THEMENAME_EXIST      = 10046;
	const ERROR_FW_LIBNAME_EXIST      	= 10047;
	const ERROR_AUTHORIZE_NAME_EXIST    = 10048;
	const ERROR_REGISTRATION_EXIST      = 10049;
	const ERROR_DEALER_BIND_EXIST       = 10050;
	const ERROR_LAF_AUTHORIZE_ERROR     = 10051;
	const ERROR_PRODUCT_SCENEID_EMPTY   = 10052;
	const ERROR_SAVE					= 10053;
	const ERROR_DEALER_BIND_INFO_NULL   = 10054;
	const ERROR_product_SAVE			= 10055;
	const ERROR_LOGIN_NAME_EXIST        = 10056;	
	const ERROR_USER_DEPARTNAME_EXIST   = 10057;
	const ERROR_MENU_NAME_EXIST			= 10058;
	const ERROR_STARTTIME_SET			= 10059;
	const ERROR_TITLE_EXIST      	    = 10060;
	const ERROR_ADNAME_EXIST      	    = 10061;
	const ERROR_ADVALUE_EXIST      	    = 10062;
	const ERROR_BRANDANDPRODUCT_SET     = 10063;
	const ERROR_ACTIVITYADD_ADD         = 10064;

	
	//签到模块错误码
	const ERROR_SIGNIN_HAS_SCAN         = 20001;
	const ERROR_ACTIVITY_INFO_EMPTY     = 20002;
	const ERROR_SIGNIN_HAS_BIND         = 20003;
	const ERROR_SIGNIN_USER_NOT_EXIST   = 20004;
	const ERROR_SIGNIN_GENERAL          = 20005;
	const ERROR_SIGNIN_FALSE            = 20006;
	const ERROR_CONTENT_FALSE			= 20007;

	//品牌管理模块错误码
	const ERROR_WLCODE_IS_UNBIND        = 30001;
	const ERROR_WLCODE_IS_OUT           = 30002;
	
	
	const ERROR_WECHATMENU_INVALID_CREDENTIAL = 40001;
	const ERROR_WECHATMENU_INVALID_APPID      = 40013;

	//会员模块错误码
	const ERROR_USER_IS_BIND            = 50001;

	//接口错误码
	const ERROR_API_DISTRUST_USER       = 90001;
	const ERROR_API_DISTRUST_REQUEST    = 90002;
	const ERROR_API_CUSTON_ADD_ERROR    = 90003;
	const ERROR_API_DEALER_BIND_ERROR   = 90004;

	public static function getErrMsg( $errorCode ) {
		$msg = '';
		switch ( $errorCode ) {
		case self::SUCCESS_OK:
			$msg='操作成功';
			break;
		case self::ERROR_GENERAL:
			$msg='一般错误';
			break;
		case self::ERROR_WECHATMENU_SYSTEMBUSY:
			$msg = '微信公众平台系统繁忙';
			break;
		case self::ERROR_NOT_MODULE_AUTHORITY:
			$msg = '没有对应Module操作权限';
			break;
		case self::ERROR_NOT_LOGIN:
			$msg = '您还未登录';
			break;
		case self::ERROR_WRONG_LOGIN_INFO:
			$msg = '错误的用户名或密码';
			break;
		case self::ERROR_LOGIN_NAME_EXIST:
			$msg = '账号名已经存在';
			break;
		case self::ERROR_WRONG_VERIFY:
			$msg = '错误的验证码';
			break;
		case self::ERROR_NOT_ACTION_AUTHORITY:
			$msg = '没有对应Action操作权限';
			break;
		case self::ERROR_WECHATMENU_INVALID_CREDENTIAL:
			$msg = '微信公众平台验证失败';
			break;
		case self::ERROR_WECHATMENU_INVALID_APPID:
			$msg = '微信公众帐台AppId错误';
			break;
		case self::ERROR_DB_VOID_MENU:
			$msg = '数据库无菜单记录';
			break;
		case self::ERROR_DB_VOID_INDEX:
			$msg = '数据库索引未找到';
			break;
		case self::ERROR_PARAMS_VOID:
			$msg = '传入的参数为空';
			break;
		case self::ERROR_PARAMS_ERR:
			$msg = '传入的参数错误';
			break;
		case self::ERROR_COMPANY_NOTEXIST:
			$msg = '企业不存在';
			break;
		case self::ERROR_COMPANY_SERVICE_OVERDUE:
			$msg = '企业服务已过期';
			break;
		case self::ERROR_COMPANY_BIND_ACCESS:
			$msg = '企业服务已过期';
			break;
		case self::ERROR_COMPANY_NOT_BIND_ACCESS:
			$msg = '企业未绑定微信帐号';
			break;
		case self::ERROR_COMPANY_WRONG_ACCESS:
			$msg = '微信帐号不是绑定的帐号';
			break;
		case self::ERROR_ADD_HANDLE_ERR:
			$msg = '添加失败';
			break;
		case self::ERROR_EDIT_HANDLE_ERR:
			$msg = '修改失败';
			break;
		case self::ERROR_DELETE_HANDLE_ERR:
			$msg = '删除失败';
			break;
		case self::ERROR_DB_VOID_PRODUCT:
			$msg = '企业产品为空';
			break;
		case self::ERROR_TIMESTAMP_NULL:
			$msg = '找不到时间戳';
			break;
		case self::ERROR_TOKEN_NULL:
			$msg = '找不到Token';
			break;
		case self::ERROR_TOKEN_WRONG:
			$msg = 'Token错误';
			break;
		case self::ERROR_EXCHANGE_UNLUCK:
			$msg = '防伪码未中奖';
			break;
		case self::ERROR_EXCHANGE_REPERT:
			$msg = '防伪码已兑奖';
			break;
		case self::ERROR_MENU_ROOT_COUNT:
			$msg = '一级菜单数量超出限制';
			break;
		case self::ERROR_MENU_SUB_COUNT:
			$msg = '二级菜单数量超出限制';
			break;
		case self::ERROR_MENU_TOKEN_EMPTY:
			$msg = '接口信息不完整，请先填写接口信息！';
			break;
		case self::ERROR_MENU_ROOT_LONG:
			$msg = '一级菜单名称字数超出限制';
			break;
		case self::ERROR_MENU_SUB_LONG:
			$msg = '二级菜单名称字数超出限制';
			break;
		case self::ERROR_KEYWORD_EXIST:
			$msg = '关键词已存在！';
			break;
		case self::ERROR_FW_NAME_EXIST:
			$msg = '规则名已存在';
			break;
		case self::ERROR_FW_THEMENAME_EXIST:
			$msg = '主题名已存在';
			break;
		case self::ERROR_FW_LIBNAME_EXIST:
			$msg = '目录名已存在';
			break;
		case self::ERROR_BRAND_TAGS_NAME_EXIST:
			$msg = '标签名已存在';
			break;
		case self::ERROR_USER_PWD_ERROR:
			$msg = '用户密码错误';
			break;
		case self::ERROR_DEALER_NAME_EXIST:
			$msg = '经销商名称已存在';
			break;
		case self::ERROR_FW_BIND_EXIST:
			$msg = '品牌或产品已绑定';
			break;
		case self::ERROR_LUCKY_LUCKYNUM_OVER:
			$msg = '中奖号码小于已查询人数';
			break;
		case self::ERROR_LUCKY_LUCKYNUM_NULL:
			$msg = '中奖号码或幸运位数不能为0';
			break;
		case self::ERROR_LUCKY_TIME_REPERT:
			$msg = '同一时间不能存在两个活动';
			break;
		case self::ERROR_LUCKY_NAME_EXIST:
			$msg = '活动名称已存在';
			break;
		case self::ERROR_NEWS_NAME_EXIST:
			$msg = '素材名称已存在';
			break;
		case self::ERROR_MENU_REPLY_EMPTY:
			$msg = '文本回复内容为空';
			break;
		case self::ERROR_LUCKY_PRIZE_TYPE_EXIST:
			$msg = '奖品类型已存在';
			break;
		case self::ERROR_LUCKY_PRIZE_NAME_EXIST:
			$msg = '奖品名称已存在';
			break;
		case self::ERROR_PRODUCT_NAME_EXIST:
			$msg = '产品名称已存在';
			break;
		case self::ERROR_QR_FILE_COLUMN_ERROR:
			$msg = '上传文件列名不正确';
			break;
		case self::ERROR_SIGNIN_HAS_SCAN:
			$msg = '用户已存在签到记录';
			break;
		case self::ERROR_ACTIVITY_INFO_EMPTY:
			$msg = '活动信息不存在';
			break;
		case self::ERROR_SIGNIN_HAS_BIND:
			$msg = '用户已绑定真实信息';
			break;
		case self::ERROR_AUTHORIZE_NAME_EXIST:
			$msg = '经销商或产品已绑定授权书';
			break;
		case self::ERROR_REGISTRATION_EXIST:
			$msg = '您已经登记过';
			break;
		case self::ERROR_USER_DEPARTNAME_EXIST:
			$msg = '部门已存在';
			break;
		case self::ERROR_SIGNIN_USER_NOT_EXIST:
			$msg = '用户信息不存在';
			break;
		case self::ERROR_SIGNIN_GENERAL:
			$msg = '活动不存在';
			break;
		case self::ERROR_SIGNIN_FALSE:
			$msg = '您不能参加此次会议';
			break;
		case self::ERROR_DEALER_BIND_EXIST:
			$msg = '该经销商已经进行过出库操作';
			break;
		case self::ERROR_LAF_AUTHORIZE_ERROR:
			$msg = '授权书编号不存在';
			break;
		case self::ERROR_PRODUCT_SCENEID_EMPTY:
			$msg = '产品未绑定二维码';
			break;
		case self::ERROR_SAVE:
			$msg = '品牌名已存在';
			break;
		case self::ERROR_product_SAVE:
			$msg = '产品名已存在';
			break;
		case self::ERROR_TITLE_EXIST:
			$msg = '标题已存在';
			break;
		case self::ERROR_ADNAME_EXIST:
			$msg = '广告名已存在';
			break;
		case self::ERROR_ADVALUE_EXIST:
			$msg = '广告位存在';
			break;
		case self::ERROR_DEALER_BIND_INFO_NULL:
			$msg = '经销商未获得该品牌或产品授权';
			break;
		case self::ERROR_API_DISTRUST_USER:
			$msg = '错误的授权信息';
			break;
		case self::ERROR_API_DISTRUST_REQUEST:
			$msg = '错误的请求方式';
			break;
		case self::ERROR_WLCODE_IS_UNBIND:
			$msg = '该物流码没有进行绑定';
			break;
		case self::ERROR_WLCODE_IS_OUT:
			$msg = '该物流码已经出库';
			break;
		case self::ERROR_CONTENT_FALSE:
			$msg = '内容重复';
			break;
		case self::ERROR_MENU_NAME_EXIST:
			$msg = '菜单名已存在';
			break;
		case self::ERROR_STARTTIME_SET:
			$msg = '开始时间错误';
			break;
		case self::ERROR_USER_IS_BIND:
			$msg = '会员已绑定';
			break;
		case self::ERROR_API_CUSTON_ADD_ERROR:
			$msg = '客户添加至经销商失败';
			break;
		case self::ERROR_API_DEALER_BIND_ERROR:
			$msg = '经销商绑定证书失败';
			break;
		default:
			$msg = '未知错误';
			break;
		case self::ERROR_BRANDANDPRODUCT_SET:
			$msg = '请选择品牌';
		case self::ERROR_ACTIVITYADD_ADD:
			$msg = '只能进行一次推送';
			break;
		}

		$msg .= '('.$errorCode.')';
		return $msg;
	}
}
?>
