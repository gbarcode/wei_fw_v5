<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Org\Mxt;
class WechatFwClient extends FwClient {
	const CACHE_NAME = 'WECHAT_FWCHECK';
	public function fwCheck($fwCode, $openid){
		$cacheName = self::CACHE_NAME."_{$fwCode}_{$this->ecid}_{$openid}";

		$result = S($cacheName);
		if(!$result){
			//调用Webservice接口查询
			$result = $this->FwCheckWebservice($fwCode);
			S($cacheName, $result, 86400);
		}
		$this->log($result);
		return $result;
	}

	protected function log($openid, $result){
		
	}
}