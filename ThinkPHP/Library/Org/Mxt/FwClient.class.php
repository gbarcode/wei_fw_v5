<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Org\Mxt;
class FwClient {
	const MXT_WEBSERVICES_URL = 'http://www.sz12365.net/mxt/service1.asmx?wsdl';
	const MXT_ADMIN_ECID = '816';

	protected $ecid;//厂商编号
    private $apiParams; //调用参数

    private $webSoap;      //WebService链接对象
    private $randCode;     //随机码

    protected $ipAddress;		//用户查询的IP地址
    protected $fwCode;        //防伪码
    protected $fwEcid;        //防伪码的厂商ID
    protected $fwBrand;       //品牌名
    protected $fwBrandId;     //品牌ID
    protected $fwProductId;   //产品ID
    protected $fwProduct;     //产品名
    protected $company;       //企业名称
    protected $wlCode;        //物流码
    protected $checkCount;    //查询次数
    protected $checkLog;      //查询历史
    protected $dealerId;      //经销商ID   
    protected $dealerName;    //经销商名称
    protected $ckTime;        //出库时间

    public function __construct( $options ) {
        $this->ecid     = isset($options['ecid'])?$options['ecid']:'';
        $this->uid      = isset($options['api_user'])?$options['api_user']:'';
        $this->pwd      = isset($options['api_psw'])?$options['api_psw']:'';

        $this->initMxtWebservice($options['ecid'], $options['api_user'], $options['api_psw']);
    }
    
    protected function FwCheckWebservice($fwcode) {
        if ( $this->webSoap == null )
            return false;

        $this->fwCode = $fwcode;

        //参数设定
        $fwParams = array_merge($this->apiParams, array(
        	'fwcode' => $fwcode
        	));

        //调用防伪查询接口
        $result = $this->webSoap->call( 'FwCheck', $fwParams );
        $fwResult = $result['FwCheckResult'];

        $this->checkCount = $fwResult['Code'];

        if ( $this->checkCount >= 0 ) {
            //提取参数，获得产品信息
            $this->updateProduct( $fwResult['Message'] );
        }else{
            $this->fwEcid = $this->ecid;
        }

        if($this->getFwMessage()){
            $result = array(
				"fwCode"       => $this->fwCode,
				'ecid'         => $this->fwEcid,
				"productName"  => $this->fwProduct ,    //产品名称
				"productId"    => $this->fwProductId ,  //产品ID
				"brandName"    => $this->fwBrand ,      //品牌名称
				"brandId"      => $this->fwBrandId ,    //品牌ID
				"replyContent" => $this->replyContent , //回复内容
				"replyImg"     => $this->replyImg,
				"wlcode"       => $this->wlCode ,       //物流码
				"checkCount"   => $this->checkCount,    //查询次数
				"dealerId"     => $this->dealerId,      //经销商ID
				"dealerName"   => $this->dealerName     //经销商名称
            );

            return $result;
        }

        return false;
    }

    /**
     * 对调用的接口帐号
     * @param  [type] $psw [description]
     * @return [type]      [description]
     */
    private function initMxtWebservice($ecid, $user, $password){
        $psw_cache_name = 'Mxt_params'.$ecid;
        if($rs = S($psw_cache_name)){
            $this->apiParams = $rs;
        }
        else{
        	$randCode = $this->getRandCode(10);
        	$pwd = strtoupper( md5( $user . "\0\0\0\0\0\0\0\0\0\0" . $randCode . $password ) );

        	$this->apiParams = array(
        		'ecId'     => $ecid,      //厂商ID
	            'uId'      => $this->uid,        //接口用户名
	            'pwd'      => $pwd,          //接口密码
	            'randcode' => $randCode
        		);

            S($psw_cache_name, $this->apiParams, 86400);
        }

        $this->createSoap();    //创建Webservice对象
    }

    /************************************************************************
    *函数名：createSoap
    *参数：
    *作用：创建WebService链接
    *返回值：
    *创建人：李少年                         时间：06/06/13 09:20:14
    *修改记录：
    ************************************************************************/
    private function createSoap() {
        Vendor( 'NuSoap.nusoap' );
        $this->webSoap = new \nusoap_client( self::MXT_WEBSERVICES_URL, true, false, false, false, false, 0, 30 );
        $this->webSoap->soap_defencoding = 'UTF-8';
        $this->webSoap->decode_utf8 = false;
        $this->webSoap->xml_encoding = 'utf-8';
    }

    /****************************************************
    getRandCode  函数作用：生成随机字符串函数
    参数：int $StrLength     数组长度
    返回值：Array $output
    ******************************************************/
    private function getRandCode( $StrLength ) {
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
            'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!',
            '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_',
            '[', ']', '{', '}', '<', '>', '~', '`', '+', '=', ',',
            '.', ';', ':', '/', '?', '|'
        );
        $charsLen = count( $chars ) - 1;

        shuffle( $chars );    // 将数组打乱

        $output = "";
        for ( $i=0; $i<$StrLength; $i++ ) {
            $output .= $chars[mt_rand( 0, $charsLen )];
        }

        return $output;
    }

    /*
     * 函数名：updateProduct
     * @Param $message  码信通返回的防伪参数
     * 作用：通过码信通返回参数提取产品信息
     */
    private function updateProduct( $message ) {
        $arrPram = explode( '#_#', $message );

        $this->company   = $arrPram[0];
        $this->fwBrand   = $arrPram[1];
        $this->fwProduct = $arrPram[2];
        $this->wlCode    = $arrPram[3];
        
        $this->fwEcid    = ($this->ecid == 816)?$this->getEcidByCompanyName():$this->ecid;

        //需要在下面添加通过物流码更新产品信息的操作
        //
        if(!$this->updateProductInfoFromWl($this->wlCode, $this->fwEcid))
            $this->getProductIndex();   #获取产品ID
    }

    private function updateProductInfoFromWl($wlCode, $ecid){
        //获取品牌、产品ID
        if($ecid == '200035'){
            $result = M()->query("
                SELECT
                sz12365_fw_company_{$ecid}_wl_info.id AS id,
                sz12365_fw_company_{$ecid}_wl_info.productId AS productId,
                sz12365_fw_company_product.brandId AS brandId,
                sz12365_fw_company_product.`name` AS productName,
                sz12365_fw_company_brand.`name` AS brandName,
                sz12365_fw_company_{$ecid}_wl_info.wlCode AS wlCode,
                sz12365_fw_company_{$ecid}_wl_info.SellerId AS dealerId,
                sz12365_fw_company_dealers.`name` AS dealerName,
                sz12365_fw_company_{$ecid}_wl_info.cqDate AS ckTime
                FROM sz12365_fw_company_{$ecid}_wl_info 
                LEFT OUTER JOIN sz12365_fw_company_dealers ON 
                sz12365_fw_company_{$ecid}_wl_info.SellerId = sz12365_fw_company_dealers.id
                LEFT OUTER JOIN sz12365_fw_company_product ON
                sz12365_fw_company_{$ecid}_wl_info.productId = sz12365_fw_company_product.id
                LEFT  JOIN sz12365_fw_company_brand ON
                sz12365_fw_company_product.brandId = sz12365_fw_company_brand.id
                WHERE sz12365_fw_company_{$ecid}_wl_info.wlCode = '{$this->fwCode}'
                ");
        }else{
            $result = M()->query("
                SELECT
                sz12365_fw_company_{$ecid}_wl_info.id AS id,
                sz12365_fw_company_{$ecid}_wl_info.productId AS productId,
                sz12365_fw_company_product.brandId AS brandId,
                sz12365_fw_company_product.`name` AS productName,
                sz12365_fw_company_brand.`name` AS brandName,
                sz12365_fw_company_{$ecid}_wl_info.wlCode AS wlCode,
                sz12365_fw_company_{$ecid}_wl_info.SellerId AS dealerId,
                sz12365_fw_company_dealers.`name` AS dealerName,
                sz12365_fw_company_{$ecid}_wl_info.cqDate AS ckTime
                FROM sz12365_fw_company_{$ecid}_wl_info 
                LEFT OUTER JOIN sz12365_fw_company_dealers ON 
                sz12365_fw_company_{$ecid}_wl_info.SellerId = sz12365_fw_company_dealers.id
                LEFT OUTER JOIN sz12365_fw_company_product ON
                sz12365_fw_company_{$ecid}_wl_info.productId = sz12365_fw_company_product.id
                LEFT  JOIN sz12365_fw_company_brand ON
                sz12365_fw_company_product.brandId = sz12365_fw_company_brand.id
                WHERE sz12365_fw_company_{$ecid}_wl_info.wlCode = '{$wlCode}'
                ");
        }

        if($result){
            if($result[0]['productId'] == ''){
                $this->getProductIndex();
            }else{
                $this->fwProduct   = $result[0]['productName'];
                $this->fwProductId = $result[0]['productId'];
                $this->fwBrand     = $result[0]['brandName'];
                $this->fwBrandId   = $result[0]['brandId'];
            }
            
            $this->dealerId    = $result[0]['dealerId'];
            $this->dealerName  = $result[0]['dealerName'];
            $this->ckTime      = $result[0]['ckTime'];

            return true;
        }
        else
            return false;
    }

    private function getProductIndex(){
        //获取品牌、产品ID
        $m = M('View_company_product');

        $opt['brandName'] = $this->fwBrand;
        $opt['productName'] = $this->fwProduct;

        $result = $m->where($opt)->find();
        if($result){
            $this->fwBrandId = $result['brandId'];
            $this->fwProductId = $result['productId'];
        }
    }

    /*
     * 函数名：getFwMessage
     * 作用：更新防伪提示语中的参数
     * @Prama  $code 查询次数
     * 返回：防伪提示语
     */
    private function getFwMessage() {
        $hasReply = false;
        //修复问题单11
        if($this->checkCount >= 0){
            $opt['ecid'] = $this->fwEcid;
            $opt['productId'] = $this->fwProductId;
            $hasReply = $this->setReplyFromDb($opt);

            if(!$hasReply){
                unset($opt);
                $opt['ecid'] = $this->fwEcid;
                $opt['brandId'] = $this->fwBrandId;
                $opt['productId'] = -1;
                $hasReply = $this->setReplyFromDb($opt);
            }
        }

        if(!$hasReply){
            unset($opt);
            $opt['ecid'] = $this->fwEcid;
            $opt['brandId'] = -1;
            $opt['productId'] = -1;

            $hasReply = $this->setReplyFromDb($opt);
        }

        if(!$hasReply){
            unset($opt);
            $opt['ecid'] = 816;
            $opt['brandId'] = -1;
            $opt['productId'] = -1;

            $hasReply = $this->setReplyFromDb($opt);
        }

        return $hasReply;
    }

    private function setReplyFromDb($opt){
        $m = M("Company_fw_reply");
        $result = $m->where($opt)->find();

        if($result){
            if($this->checkCount == 0){
                $this->replyContent = $result['rightReply'];
                $this->replyImg = $result['rightImg'];
            }
            else if($this->checkCount > 0){
                if(!$this->isJewelryCheck){
                    $this->replyContent = $result['repertReply'];
                    $this->replyImg = $result['repertImg'];
                }else{
                    $this->replyContent = $result['rightReply'];
                    $this->replyImg = $result['rightImg'];
                }
            }
            else{
                $this->replyContent = $result['errorReply'];
                $this->replyImg = $result['errorImg'];
            }

            $this->replaceParams();
            return true;
        }

        return false;
    }

    private function replaceParams(){
        $this->replyContent = str_replace( '{-厂商名称-}', $this->company, $this->replyContent);
        $this->replyContent = str_replace( '{-品牌名称-}', $this->fwBrand, $this->replyContent);
        $this->replyContent = str_replace( '{-产品名称-}', $this->fwProduct, $this->replyContent);
        $this->replyContent = str_replace( '{-物流码-}', $this->wlCode, $this->replyContent);
        $this->replyContent = str_replace( '{-防伪码-}', $this->fwCode, $this->replyContent);
        $this->replyContent = str_replace( '{-经销商回复-}', $this->getDealerInfo(), $this->replyContent);
        $this->replyContent = str_replace( '{-查询次数-}', $this->checkCount, $this->replyContent);
        //修复问题单5
        $this->replyContent = str_replace( '{-首次时间-}', $this->checkLog['FirstTime'], $this->replyContent);
    }

    private function getDealerInfo(){
        if($this->dealerId != ''){
            $m = M('Company_ck_set');

            $option['ecid'] = $this->fwEcid;

            $result = $m->where($option)->find();

            if($result){
                $reply = $result['dealer_reply'];

                $reply = str_replace( '{-经销商名称-}', $this->dealerName, $reply);
                $reply = str_replace( '{-出库时间-}', $this->ckTime, $reply);

                return $reply;
            }
            else
                return '';
        }
        else
            return '';
    }

    private function getEcidByCompanyName(){
        if($this->company == '')
            return;

        $m = M('Company_info');
        $opt['company_name'] =  $this->company;

        $result = $m->where($opt)->find();

        if($result){
            return $result['company_ecid'];
        }
        else{
            return $this->ecid;
        }
    }
}