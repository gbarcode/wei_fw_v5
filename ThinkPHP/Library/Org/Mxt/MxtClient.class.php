<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Org\Mxt;
class MxtClient {
    private $userID;  //微信ID
    private $ecid;//厂商编号
    private $uid;//接口帐号
    private $pwd;//密码
    private $WebServiceURL = "http://www.sz12365.net/mxt/service1.asmx?wsdl"; //WebService服务器地址
    private $webSoap;      //WebService链接对象
    private $randCode;     //随机码

    private $type;          //验证类型：web或wechat
    private $fwCode;        //防伪码
    private $fwEcid;        //防伪码的厂商ID
    private $fwBrand;       //品牌名
    private $fwBrandId;     //品牌ID
    private $fwProductId;   //产品ID
    private $fwProduct;     //产品名
    private $company;       //企业名称
    private $wlCode;        //物流码
    private $checkCount;    //查询次数
    private $checkLog;      //查询历史
    private $dealerId;      //经销商ID   
    private $dealerName;    //经销商名称
    private $ckTime;        //出库时间
    private $isJewelryCheck;//是否为证书验证
    
    private $replyImg;      //回复图片
    private $replyContent;  //回复信息

    public function __construct( $options ) {
        $this->ecid     = isset($options['ecid'])?$options['ecid']:'';
        $this->uid      = isset($options['api_user'])?$options['api_user']:'';
        $this->pwd      = isset($options['api_psw'])?$options['api_psw']:'';
        $this->fwCode   = isset($options['fwCode'])?$options['fwCode']:'';
        $this->userID   = isset($options['OpenId'])?$options['OpenId']:'';
        $this->type     = isset($options['type'])?$options['type']:'';
        $this->isJewelryCheck = isset($options['isJewelryCheck'])?$options['isJewelryCheck']:false;

        $this->encryptionPsw($this->pwd);

        if ( $this->uid != '' && $this->pwd != '' )
            $this->createSoap();    //创建Webservice链接
    }

    public function getProudct() {
        return $this->fwProduct;
    }

    public function getWlcode() {
        return $this->wlCode;
    }

    public function getJewelryCheck(){
        return $this->isJewelryCheck;
    }

    public function getFwParams(){
        return array(
            'Code'       => $this->fwCode,
            'Brand'      => $this->fwBrand,
            'Product'    => $this->fwProduct,
            'Company'    => $this->company,
            'CheckCount' => $this->checkCount,
            'BrandId'    => $this->fwBrandId,
            'ProductId'  => $this->fwProductId);
    }

    private function encryptionPsw($psw){
        $psw_cache_name = 'Mxt_psw'.$psw;
        if($rs = S($psw_cache_name)){
            $this->randCode = $rs['rand'];
            $this->pwd      = $rs['pwd'];
        }
        else{
            $this->randCode = $this->getRandCode(10);
            $this->pwd      = $this->md5Pwd();

            $rs = array(
                'rand' => $this->randCode,
                'pwd'  => $this->pwd);

            S($psw_cache_name, $rs, 86400);
        }
    }

    /************************************************************************
    *函数名：createSoap
    *参数：
    *作用：创建WebService链接
    *返回值：
    *创建人：李少年                         时间：06/06/13 09:20:14
    *修改记录：
    ************************************************************************/
    private function createSoap() {
        Vendor( 'NuSoap.nusoap' );
        $this->webSoap = new \nusoap_client( $this->WebServiceURL, true, false, false, false, false, 0, 30 );
        $this->webSoap->soap_defencoding = 'UTF-8';
        $this->webSoap->decode_utf8 = false;
        $this->webSoap->xml_encoding = 'utf-8';
    }

    /****************************************************
    getRandCode  函数作用：生成随机字符串函数
    参数：int $StrLength     数组长度
    返回值：Array $output
    ******************************************************/
    private function getRandCode( $StrLength ) {
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
            'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!',
            '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_',
            '[', ']', '{', '}', '<', '>', '~', '`', '+', '=', ',',
            '.', ';', ':', '/', '?', '|'
        );
        $charsLen = count( $chars ) - 1;

        shuffle( $chars );    // 将数组打乱

        $output = "";
        for ( $i=0; $i<$StrLength; $i++ ) {
            $output .= $chars[mt_rand( 0, $charsLen )];
        }

        return $output;
    }

    /****************************************************
    GetMD5  函数作用：MD5加密函数
    参数：Array $this->randCode     随机码
    返回值：Array
    ******************************************************/
    private function md5Pwd() {
        return strtoupper( md5( $this->uid . "\0\0\0\0\0\0\0\0\0\0" . $this->randCode . $this->pwd ) );
    }

    /****************************************************
     FwCheck  函数作用：防伪码验证
    参数：$fwcode     防伪码
          $fwResult   MXT防伪验证结果，当此参数存在时不会再去MXT验证防伪码
    返回值：Array  $result    防伪码查询结果
    ******************************************************/
    public function FwCheck( $fwcode = '', $fwResult = '' ) {
        if ( $this->webSoap == null )
            return;

        $this->fwCode = $fwcode?$fwcode:$this->fwCode;

        if($fwResult == ''){
            //参数设定
            $Parms = array( 
                'ecId'     => $this->ecid,      //厂商ID
                'uId'      => $this->uid,        //接口用户名
                'pwd'      => $this->pwd,          //接口密码
                'randcode' => $this->randCode,     //8位随机码
                'fwcode'   => $this->fwCode );       //防伪码

            //调用防伪查询接口
            $result = $this->webSoap->call( 'FwCheck', $Parms );
            $fwResult = $result['FwCheckResult'];
        }

        if($this->ecid == 816){
            $this->isAdminCheckJewelry($fwResult['Message']);
        }

        $this->checkCount = $fwResult['Code'];
        if(!$this->isJewelryCheck){
            if ( $this->checkCount >= 0 ) {
                //提取参数，获得产品信息
                $this->updateProduct( $fwResult['Message'] );
            }else{
                $this->fwEcid = $this->ecid;
            }

            if($this->getFwMessage()){
                $this->log();
                if($this->type == 'wechat')
                    return $this->getWechatReply();
                else{
                    $result = array(
                        "productName"  => $this->fwProduct ,    //产品名称
                        "productId"    => $this->fwProductId ,  //产品ID
                        "brandName"    => $this->fwBrand ,      //品牌名称
                        "brandId"      => $this->fwBrandId ,    //品牌ID
                        "replyContent" => $this->replyContent , //回复内容
                        "wlcode"       => $this->wlCode ,       //物流码
                        "checkCount"   => $this->checkCount,    //查询次数
                        "dealerId"     => $this->dealerId,      //经销商ID
                        "dealerName"   => $this->dealerName     //经销商名称
                        );
                }
            }
        }else{
            $result = $this->getJewelryCheckMessage();
        }

        return $result;
    }

    private function isAdminCheckJewelry($message){
        $arrPram = explode( '#_#', $message );
        $this->company   = $arrPram[0];
        $this->fwBrand   = $arrPram[1];
        $this->fwProduct = $arrPram[2];
        $this->wlCode    = $arrPram[3];
        $this->fwEcid = $this->getEcidByCompanyName();

        $companyArr = C('JEWELRY_COMPANY_ECID');
        for($i = 0;$i<count($companyArr);$i++){
            if($this->fwEcid == $companyArr[$i]){
                $this->isJewelryCheck = true;
                return;
            }
        }

        $this->isJewelryCheck = false;
    }

    /**
     * 记录防伪验证
     */
    private function getJewelryCheckMessage(){
        $reply['type'] = 'news';
        $m = M('Company_fw_reply');
        $opt['ecid'] = 816;

        $title = "微信防伪证书验证结果";
        if ( $this->checkCount >= 0) {
            $title .= "——验证通过";
            $this->replyImg = $m->where($opt)->getField('rightImg');
        }else{
            $title .= "——防伪码错误";
            $this->replyImg = $m->where($opt)->getField('errorImg');

            if(substr($this->replyImg,0,4) != 'http')
                $img = \Weixin\Response\WechatConst::SERVER_DOMAIN . $this->replyImg;
            else
                $img = $this->replyImg;

            $reply['content'] = array(
                array(
                    'Title'=>$title,
                    'Description'=>"",
                    'PicUrl'=> $img));

            return $reply;
        }

        if(substr($this->replyImg,0,4) != 'http')
            $img = \Weixin\Response\WechatConst::SERVER_DOMAIN . $this->replyImg;
        else
            $img = $this->replyImg;

        $orderArr = M($this->ecid.C('JEWELRY_CHECK_TABLE_NAME'),'sz12365_fw_',C('JEWELRY_CHECK_DB_LINK'))->where('fwCode = '.$this->fwCode)->find();
        $webUrl = C('CLIENT_WEB_NAME.'.$this->ecid);

        if($orderArr){
            $reply['content'] = array(
                array(
                    'Title'=>$title,
                    'Description'=>"",
                    'PicUrl'=> $img,
                    'Url'=> $webUrl.'/?fw='.$this->fwCode ),
                array(
                    'Title'=>'证书编号：'.$orderArr['testNo'].'，点击查看证书详情',
                    'PicUrl'=> \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Public/Image/logo.jpg',
                    'Url'=> $webUrl.'/?fw='.$this->fwCode ) );
        }else{
            $orderArr = M("Exdata",'sz12365_fw_',C('JEWELRY_CHECK_DB_LINK'))->where("fwcode = '".$this->fwCode."'")->find();

            if($orderArr){
                $reply['content'] = array(
                    array(
                        'Title'=>'微防伪证书验证结果——验证通过',
                        'Description'=>"",
                        'PicUrl'=> $img,
                        'Url'=> 'http://zg.fw.msa12365.com/index.php/Home/Zg?no='.$orderArr['test_no'] ),
                    array(
                        'Title'=>'证书编号：'.$orderArr['test_no'].'，点击查看证书详情',
                        'PicUrl'=> \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Public/Image/logo.jpg',
                        'Url'=> 'http://zg.fw.msa12365.com/index.php/Home/Zg?no='.$orderArr['test_no'] ) );
            }else{
                $reply['content'] = "证书不存在！";
            }
        }

        return $reply;
    }

    /**
     * 记录防伪验证
     */
    private function log() {
        $m = M( 'Company_fw_log' );
        $time = date( "Y-m-d H:i:s" );

        $data['fromEcid']  = $this->ecid;
        $data['type']      = $this->type;
        $data['openId']    = $this->userID;
        $data['fwCode']    = $this->fwCode;
        $data['wlCode']    = $this->wlCode;
        $data['product']   = $this->fwProduct;
        $data['productId'] = $this->fwProductId;
        $data['brand']     = $this->fwBrand;
        $data['brandId']   = $this->fwBrandId;
        $data['ecid']      = $this->fwEcid;
        $data['dealerId']   = $this->dealerId;
        $data['dealerName'] = $this->dealerName;
        $data['time']      = $time;

        if($this->checkCount == 0)
            $data['fwType'] = 'first';
        else if($this->checkCount > 0)
            $data['fwType'] = 'repert';
        else
            $data['fwType'] = 'error';

        $m->add( $data );
    }

    public function cloudFwCheck( $fwResult ) {
        $this->checkCount = $fwResult['Code'];

        //提取参数，获得产品信息
        $this->updateProduct( $fwResult['Message'] );

        $this->getFwMessage();

        $result = array(
            "productName" => $this->fwProduct ,
            "productId" => $this->fwProductId ,
            "brandName" => $this->fwBrand ,
            "brandId" => $this->fwBrandId , 
            "replyContent" => $this->replyContent ,
            "wlcode" => $this->wlCode
            );
        
        return $result;
    }

    /*
     * 函数名：updateProduct
     * @Param $message  码信通返回的防伪参数
     * 作用：通过码信通返回参数提取产品信息
     */
    private function updateProduct( $message ) {
        $arrPram = explode( '#_#', $message );

        $this->company   = $arrPram[0];
        $this->fwBrand   = $arrPram[1];
        $this->fwProduct = $arrPram[2];
        $this->wlCode    = $arrPram[3];
        
        $this->fwEcid    = ($this->ecid == 816)?$this->getEcidByCompanyName():$this->ecid;

        //需要在下面添加通过物流码更新产品信息的操作
        //
        if(!$this->updateProductInfoFromWl($this->wlCode, $this->fwEcid))
            $this->getProductIndex();   #获取产品ID
    }

    private function updateProductInfoFromWl($wlCode, $ecid){
        //获取品牌、产品ID
        if($ecid == '200035'){
            $result = M()->query("
                SELECT
                sz12365_fw_company_{$ecid}_wl_info.id AS id,
                sz12365_fw_company_{$ecid}_wl_info.productId AS productId,
                sz12365_fw_company_product.brandId AS brandId,
                sz12365_fw_company_product.`name` AS productName,
                sz12365_fw_company_brand.`name` AS brandName,
                sz12365_fw_company_{$ecid}_wl_info.wlCode AS wlCode,
                sz12365_fw_company_{$ecid}_wl_info.SellerId AS dealerId,
                sz12365_fw_company_dealers.`name` AS dealerName,
                sz12365_fw_company_{$ecid}_wl_info.cqDate AS ckTime
                FROM sz12365_fw_company_{$ecid}_wl_info 
                LEFT OUTER JOIN sz12365_fw_company_dealers ON 
                sz12365_fw_company_{$ecid}_wl_info.SellerId = sz12365_fw_company_dealers.id
                LEFT OUTER JOIN sz12365_fw_company_product ON
                sz12365_fw_company_{$ecid}_wl_info.productId = sz12365_fw_company_product.id
                LEFT  JOIN sz12365_fw_company_brand ON
                sz12365_fw_company_product.brandId = sz12365_fw_company_brand.id
                WHERE sz12365_fw_company_{$ecid}_wl_info.wlCode = '{$this->fwCode}'
                ");
        }else{
            $result = M()->query("
                SELECT
                sz12365_fw_company_{$ecid}_wl_info.id AS id,
                sz12365_fw_company_{$ecid}_wl_info.productId AS productId,
                sz12365_fw_company_product.brandId AS brandId,
                sz12365_fw_company_product.`name` AS productName,
                sz12365_fw_company_brand.`name` AS brandName,
                sz12365_fw_company_{$ecid}_wl_info.wlCode AS wlCode,
                sz12365_fw_company_{$ecid}_wl_info.SellerId AS dealerId,
                sz12365_fw_company_dealers.`name` AS dealerName,
                sz12365_fw_company_{$ecid}_wl_info.cqDate AS ckTime
                FROM sz12365_fw_company_{$ecid}_wl_info 
                LEFT OUTER JOIN sz12365_fw_company_dealers ON 
                sz12365_fw_company_{$ecid}_wl_info.SellerId = sz12365_fw_company_dealers.id
                LEFT OUTER JOIN sz12365_fw_company_product ON
                sz12365_fw_company_{$ecid}_wl_info.productId = sz12365_fw_company_product.id
                LEFT  JOIN sz12365_fw_company_brand ON
                sz12365_fw_company_product.brandId = sz12365_fw_company_brand.id
                WHERE sz12365_fw_company_{$ecid}_wl_info.wlCode = '{$wlCode}'
                ");
        }

        if($result){
            if($result[0]['productId'] == ''){
                $this->getProductIndex();
            }else{
                $this->fwProduct   = $result[0]['productName'];
                $this->fwProductId = $result[0]['productId'];
                $this->fwBrand     = $result[0]['brandName'];
                $this->fwBrandId   = $result[0]['brandId'];
            }
            
            $this->dealerId    = $result[0]['dealerId'];
            $this->dealerName  = $result[0]['dealerName'];
            $this->ckTime      = $result[0]['ckTime'];

            return true;
        }
        else
            return false;
    }

    private function getProductIndex(){
        //获取品牌、产品ID
        $m = M('View_company_product');

        $opt['brandName'] = $this->fwBrand;
        $opt['productName'] = $this->fwProduct;

        $result = $m->where($opt)->find();
        if($result){
            $this->fwBrandId = $result['brandId'];
            $this->fwProductId = $result['productId'];
        }
    }

    /*
     * 函数名：getFwMessage
     * 作用：更新防伪提示语中的参数
     * @Prama  $code 查询次数
     * 返回：防伪提示语
     */
    private function getFwMessage() {
        $hasReply = false;
        //修复问题单11
        if($this->checkCount >= 0){
            $opt['ecid'] = $this->fwEcid;
            $opt['productId'] = $this->fwProductId;
            $hasReply = $this->setReplyFromDb($opt);

            if(!$hasReply){
                unset($opt);
                $opt['ecid'] = $this->fwEcid;
                $opt['brandId'] = $this->fwBrandId;
                $opt['productId'] = -1;
                $hasReply = $this->setReplyFromDb($opt);
            }
        }

        if(!$hasReply){
            unset($opt);
            $opt['ecid'] = $this->fwEcid;
            $opt['brandId'] = -1;
            $opt['productId'] = -1;

            $hasReply = $this->setReplyFromDb($opt);
        }

        if(!$hasReply){
            unset($opt);
            $opt['ecid'] = 816;
            $opt['brandId'] = -1;
            $opt['productId'] = -1;

            $hasReply = $this->setReplyFromDb($opt);
        }

        return $hasReply;
    }

    private function setReplyFromDb($opt){
        $m = M("Company_fw_reply");
        $result = $m->where($opt)->find();
        // $reply_cache_name = 'mxt_reply_c'. $opt['ecid'] . 'b' . $this->fwBrandId . 'p' .$this->fwProductId;

        // $result = S($reply_cache_name);
        // if(!$result){
        //     $m = M("Company_fw_reply");
        //     $result = $m->where($opt)->find();

        //     if($result){
        //         S($reply_cache_name, $result);

        //         $cache_list = 'mxt_reply_cache_list' . $opt['ecid'];
        //         $reply_list = S($cache_list);
        //         if($reply_list)
        //             S($cache_list, $reply_list. ',' . $reply_cache_name);
        //         else
        //             S($cache_list, $reply_cache_name);
        //     }
        // }

        if($result){
            if($this->checkCount == 0){
                $this->replyContent = $result['rightReply'];
                $this->replyImg = $result['rightImg'];
            }
            else if($this->checkCount > 0){
                if(!$this->isJewelryCheck){
                    $this->replyContent = $result['repertReply'];
                    $this->replyImg = $result['repertImg'];
                }else{
                    $this->replyContent = $result['rightReply'];
                    $this->replyImg = $result['rightImg'];
                }

                //重复查询时添加首次记录
                $this->getFwCheckLog();
                //$this->replyContent .= "\n--------------\n首次查询时间:\n" . $this->checkLog['FirstTime'] ."\n首次查询方式：\n" . $this->checkLog['FirstIndex'];
            }
            else{
                $this->replyContent = $result['errorReply'];
                $this->replyImg = $result['errorImg'];
            }

            $this->replaceParams();
            return true;
        }

        return false;
    }

    private function replaceParams(){
        $this->replyContent = str_replace( '{-厂商名称-}', $this->company, $this->replyContent);
        $this->replyContent = str_replace( '{-品牌名称-}', $this->fwBrand, $this->replyContent);
        $this->replyContent = str_replace( '{-产品名称-}', $this->fwProduct, $this->replyContent);
        $this->replyContent = str_replace( '{-物流码-}', $this->wlCode, $this->replyContent);
        $this->replyContent = str_replace( '{-防伪码-}', $this->fwCode, $this->replyContent);
        $this->replyContent = str_replace( '{-经销商回复-}', $this->getDealerInfo(), $this->replyContent);
        $this->replyContent = str_replace( '{-查询次数-}', $this->checkCount, $this->replyContent);
        //修复问题单5
        $this->replyContent = str_replace( '{-首次时间-}', $this->checkLog['FirstTime'], $this->replyContent);
    }

    private function getDealerInfo(){
        if($this->dealerId != ''){
            $m = M('Company_ck_set');

            $option['ecid'] = $this->fwEcid;

            $result = $m->where($option)->find();

            if($result){
                $reply = $result['dealer_reply'];

                $reply = str_replace( '{-经销商名称-}', $this->dealerName, $reply);
                $reply = str_replace( '{-出库时间-}', $this->ckTime, $reply);

                return $reply;
            }
            else
                return '';
        }
        else
            return '';
    }

    private function getEcidByCompanyName(){
        if($this->company == '')
            return;

        $m = M('Company_info');
        $opt['company_name'] =  $this->company;

        $result = $m->where($opt)->find();

        if($result){
            return $result['company_ecid'];
        }
        else{
            return $this->ecid;
        }
    }

    /*
     * 函数：getWechatReply
     * 作用：获取微信返回所需的参数数组
     * @Param   $type:获回类型
     *          $conten:获回的文本
     *          $$this->checkCount:验证次数
     */
    private function getWechatReply() {
        $reply['type'] = 'news';

        $title = "微信防伪验证结果";
        if ( $this->checkCount == 0) {
            $title .= "——验证通过";
        }
        //添加else if ，解决问题单编号：3
        else if ( $this->checkCount > 0) {
            $title .= "——重复验证";
        }
        else{
            $title .= "——防伪码错误";
        }

        if(substr($this->replyImg,0,4) != 'http')
            $img = \Weixin\Response\WechatConst::SERVER_DOMAIN . $this->replyImg;
        else
            $img = $this->replyImg;
        
        $reply['content'] = array(
            array(
                'Title'=>$title,
                'Description'=>"",
                'PicUrl'=> $img,
                'Url'=>'http://www.sz12365.net/mb/?from=weixin&fw='.$this->fwCode ),
            array(
                'Title'=>$this->replyContent,
                'PicUrl'=> \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Public/Image/logo.jpg',
                'Url'=>'http://www.sz12365.net/mb/?from=weixin&fw='.$this->fwCode ) );

        return $reply;
    }


    /****************************************************
    getFwCheckLog  函数作用：获取防伪查询历史
    参数：$fwcode     防伪码
    返回值：Array  $result   防伪码查询历史记录
    ******************************************************/
    public function getFwCheckLog() {
        //参数设定
        $Parms = array( 'ecId'=> $this->ecid,      //厂商ID
            'uId'=> $this->uid,        //接口用户名
            'pwd'=> $this->pwd,          //接口密码
            'randcode'=> $this->randCode,     //8位随机码
            'fwcode'=> $this->fwCode );       //防伪码

        //调用防伪查询接口
        $result = $this->webSoap->call( 'FwHisDatas', $Parms );

        $logArr = $result['FwHisDatasResult']['Row']['InterSearchDetai'];

        $logArr = $this->multi_array_sort($logArr, 'CxDate', SORT_ASC);

        $CheckLog['FirstTime'] = $logArr[0]['CxDate'];
        $CheckLog['FirstIndex'] = $logArr[0]['HaoLx'];

        $CheckLog['FirstTime'] = str_replace( 'T', '  ', $CheckLog['FirstTime'] );
        $tempIndex = strpos( $CheckLog['FirstTime'], '.' );

        $CheckLog['FirstTime'] = substr( $CheckLog['FirstTime'], 0, $tempIndex );

        switch($CheckLog['FirstIndex']){
            case 0;
                $CheckLog['FirstIndex'] = "中国移动手机短信查询。";
                break;
            case 1:
                $CheckLog['FirstIndex'] = "中国联通手机短信查询。";
                break;
            case 2:
                $CheckLog['FirstIndex'] = "中国电信手机短信查询。";
                break;
            case 50:
                $CheckLog['FirstIndex'] = "4008899936语音电话查询。";
                break;
            case 51:
                $CheckLog['FirstIndex'] = "网站、二维码或微信查询。";
                break;
            default:
                $CheckLog['FirstIndex'] = "未知查询方式。";
                break;
        }

        $this->checkLog =  $CheckLog;
    }

    private function multi_array_sort(&$multi_array,$sort_key,$sort=SORT_DESC){
        if(is_array($multi_array)){
            foreach ($multi_array as $row_array){
                if(is_array($row_array)){
                    //把要排序的字段放入一个数组中，
                    $key_array[] = $row_array[$sort_key];
                }
                else{
                    return false;
                }
            }
        }
        else{
            return false;
        }
        //对多个数组或多维数组进行排序
        array_multisort($key_array,$sort,$multi_array);
        return $multi_array;
    }

    /************************************************************************
    *函数名：wlCheck
    *参数：wlCode    物流码
    *作用：物流码验证
    *返回值：
    *创建人：李少年                         时间：06/06/13 09:16:23
    *修改记录：
    ************************************************************************/
    public function wlCheck( $wlCode ) {
        //参数设定
        $Parms = array( 
            'ecId'     => $this->ecid,      //厂商ID
            'uId'      => $this->uid,        //接口用户名
            'pwd'      => $this->pwd,          //接口密码
            'randcode' => $this->randCode,     //8位随机码
            'wlcode'   => $wlCode 
            );

        //调用防伪查询接口
        $result = $this->webSoap->call( 'WlCheck', $Parms );

        return $result;
    }

    /************************************************************************
    *函数名：CkLibrary
    *参数：$wlCode 物流码, $sellerID 物流区域ID, $areaID 销售区域ID
    *作用：物流码出库
    *返回值：
    *创建人：范小宝                         时间：06/06/13 09:16:23
    *修改记录：
    ************************************************************************/
    public function CkLibrary( $wlCode , $sellerID , $areaID ) {
        //参数设定
        $Parms = array( 'ecId '=> $this->ecid,      //厂商ID
            'uId' => $this->uid,        //接口用户名
            'pwd' => $this->pwd,          //接口密码
            'randcode' => $this->randCode,     //8位随机码
            'wlcode' => $wlCode,          //物流码
            'dealType' =>"O",
            'sellerId' => $sellerID ,
            'areaId' => $areaID );

        //调用防伪查询接口
        $result = $this->webSoap->call( 'CkLibrary', $Parms );

        return $result;
    }
}
?>
