<?php

namespace Org\Weibo;

class Weibo {

    const MSGTYPE_TEXT = 'text';
    const MSGTYPE_IMAGE = 'image';
    const MSGTYPE_LOCATION = 'location';
    const MSGTYPE_LINK = 'link';
    const MSGTYPE_EVENT = 'event';
    const MSGTYPE_MUSIC = 'music';
    const MSGTYPE_NEWS = 'news';
    const MSGTYPE_VOICE = 'voice';
    const MSGTYPE_VIDEO = 'video';
    const API_URL_PREFIX = 'https://m.api.weibo.com/2';
    const AUTH_URL = '/token?grant_type=client_credential&';
    const MENU_CREATE_URL = '/messages/menu/create.json?';
    const MENU_GET_URL = '/messages/menu/show.json?';
    const MENU_DELETE_URL = '/messages/menu/delete.json?';
    const MEDIA_GET_URL = '/media/get?';
    const CALLBACKSERVER_GET_URL = '/getcallbackip?';
    const QRCODE_CREATE_URL = '/qrcode/create?';
    const QR_SCENE = 0;
    const QR_LIMIT_SCENE = 1;
    const QRCODE_IMG_URL = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=';
    const SHORT_URL = '/shorturl?';
    const USER_GET_URL = '/user/get?';
    const USER_INFO_URL = '/user/info?';
    const USER_UPDATEREMARK_URL = '/user/info/updateremark?';
    const GROUP_GET_URL = '/groups/get?';
    const USER_GROUP_URL = '/groups/getid?';
    const GROUP_CREATE_URL = '/groups/create?';
    const GROUP_UPDATE_URL = '/groups/update?';
    const GROUP_MEMBER_UPDATE_URL = '/groups/members/update?';
    const CUSTOM_SEND_URL = '/message/custom/send?';
    const MEDIA_UPLOADNEWS_URL = '/media/uploadnews?';
    const MASS_SEND_URL = '/message/mass/send?';
    const TEMPLATE_SEND_URL = '/message/template/send?';
    const MASS_SEND_GROUP_URL = '/message/mass/sendall?';
    const MASS_DELETE_URL = '/message/mass/delete?';
    const UPLOAD_MEDIA_URL = 'http://file.api.weixin.qq.com/cgi-bin';
    const MEDIA_UPLOAD = '/media/upload?';
    const OAUTH_PREFIX = 'https://open.weixin.qq.com/connect/oauth2';
    const OAUTH_AUTHORIZE_URL = '/authorize?';
    const OAUTH_TOKEN_PREFIX = 'https://api.weixin.qq.com/sns/oauth2';
    const OAUTH_TOKEN_URL = '/access_token?';
    const OAUTH_REFRESH_URL = '/refresh_token?';
    const OAUTH_USERINFO_URL = 'https://api.weixin.qq.com/sns/userinfo?';
    const OAUTH_AUTH_URL = 'https://api.weixin.qq.com/sns/auth?';
    const PAY_DELIVERNOTIFY = 'https://api.weixin.qq.com/pay/delivernotify?';
    const PAY_ORDERQUERY = 'https://api.weixin.qq.com/pay/orderquery?';
    const CUSTOM_SERVICE_GET_RECORD = '/customservice/getrecord?';
    const CUSTOM_SERVICE_GET_KFLIST = '/customservice/getkflist?';
    const CUSTOM_SERVICE_GET_ONLINEKFLIST = '/customservice/getkflist?';
    const SEMANTIC_API_URL = 'https://api.weixin.qq.com/semantic/semproxy/search?';

    private $token;
    private $appid;
    private $appsecret;
    private $access_token;
    private $user_token;
    private $partnerid;
    private $partnerkey;
    private $paysignkey;
    private $postxml;
    private $_msg;
    private $_receive;
    private $_text_filter = true;
    public $debug = false;
    public $errCode = 40001;
    public $errMsg = "no access";
    private $_logcallback;

    public function __construct($options) {
        $this->token = isset($options['token']) ? $options['token'] : '';
        $this->appid = isset($options['appid']) ? $options['appid'] : '';
        $this->appsecret = isset($options['appsecret']) ? $options['appsecret'] : '';
        $this->access_token = isset($options['accesstoken']) ? $options['accesstoken'] : '';
        $this->partnerid = isset($options['partnerid']) ? $options['partnerid'] : '';
        $this->partnerkey = isset($options['partnerkey']) ? $options['partnerkey'] : '';
        $this->paysignkey = isset($options['paysignkey']) ? $options['paysignkey'] : '';
        $this->debug = isset($options['debug']) ? $options['debug'] : false;
        $this->_logcallback = isset($options['logcallback']) ? $options['logcallback'] : false;
    }

    /**
     * For weixin server validation 
     * @param bool $return 是否返回
     */
    public function valid($return = false) {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $postStr = file_get_contents("php://input");
            $this->postxml = $postStr;
        } elseif (isset($_GET["echostr"])) {
            $echoStr = $_GET["echostr"];
            if ($return) {
                if ($this->checkSignature())
                    return $echoStr;
                else
                    return false;
            } else {
                if ($this->checkSignature())
                    die($echoStr);
                else
                    die('no access');
            }
        }
        if (!$this->checkSignature($encryptStr)) {
            if ($return)
                return false;
            else
                die('no access');
        }
        return true;
    }

    /**

     * 验证签名

     * @param $signature

     * @param $timestamp

     * @param $nonce

     * @return bool

     */
    function checkSignature() {
        $signature = isset($_GET["signature"]) ? $_GET["signature"] : '';
        $timestamp = isset($_GET["timestamp"]) ? $_GET["timestamp"] : '';
        $nonce = isset($_GET["nonce"]) ? $_GET["nonce"] : '';

        $token = $this->appsecret;
        $tmpArr = array($token, $timestamp, $nonce);

        sort($tmpArr, SORT_STRING);

        $tmpStr = sha1(implode($tmpArr));

        if ($tmpStr == $signature) {

            return true;
        } else {

            return false;
        }
    }

    /**
     * 获取微信服务器发来的信息
     * @param arrry $wechatMsg 本地测试时模拟接收微博数据
     */
    public function getRev($wechatMsg) {
        if ($wechatMsg) {
            $this->_receive = $wechatMsg;
            return $this;
        } else {
            if ($this->_receive)
                return $this;
            $postStr = !empty($this->postxml) ? $this->postxml : file_get_contents("php://input");
            if (!empty($postStr)) {
                $weiboObj = json_decode($GLOBALS['HTTP_RAW_POST_DATA'], true);
                $this->_receive = $this->changeToWechat($weiboObj);
            }
            return $this;
        }
    }

    /**
     * 获取微信服务器发来的信息
     */
    public function getRevData() {
        return $this->_receive;
    }

    /**
     * 获取消息发送者
     */
    public function getRevFrom() {
        if (isset($this->_receive['FromUserName']))
            return $this->_receive['FromUserName'];
        else
            return false;
    }

    /**
     * 获取消息接受者
     */
    public function getRevTo() {
        if (isset($this->_receive['ToUserName']))
            return $this->_receive['ToUserName'];
        else
            return false;
    }

    /**
     * 获取接收消息的类型
     */
    public function getRevType() {
        if (isset($this->_receive['MsgType']))
            return $this->_receive['MsgType'];
        else
            return false;
    }

    /**
     * 获取消息ID
     */
    public function getRevID() {
        if (isset($this->_receive['MsgId']))
            return $this->_receive['MsgId'];
        else
            return false;
    }

    /**
     * 获取消息发送时间
     */
    public function getRevCtime() {
        if (isset($this->_receive['CreateTime']))
            return $this->_receive['CreateTime'];
        else
            return false;
    }

    /**
     * 获取接收消息内容正文
     */
    public function getRevContent() {
        if (isset($this->_receive['Content']))
            return $this->_receive['Content'];
        else if (isset($this->_receive['Recognition'])) //获取语音识别文字内容，需申请开通
            return $this->_receive['Recognition'];
        else
            return false;
    }

    /**
     * 把微博信息格式转成微信格式
     * @param array $weiboObj
     */
    private function changeToWechat($weiboObj) {
        $wechatObj;
        $type = $weiboObj['type'];
        $data = $weiboObj['data'];

        $wechatObj["ToUserName"] = $weiboObj['receiver_id'];
        $wechatObj["FromUserName"] = $weiboObj['sender_id'];
        $wechatObj["CreateTime"] = $weiboObj['created_at'];
        $wechatObj["MsgType"] = $type;

        if ($type == 'text') {
            $wechatObj["Content"] = $weiboObj['text'];
            $wechatObj["MsgId"] = "";
        } else if ($type == 'position') {
            
        } else if ($type == 'vioce') {
//            $wechatObj["MediaId"] = $data['tovfid'];
//            $wechatObj["Format"] = '';
        } else if ($type == 'image') {
            
        } else if ($type == 'event') {
            if ($data['subtype'] == 'follow') {
                $wechatObj["Event"] = 'subscribe';
            } else if ($data['subtype'] == 'unfollow') {
                $wechatObj["Event"] = 'unsubscribe';
            } else if ($data['subtype'] == 'scan_follow') {
//                $wechatObj["EventKey"] = '';
//                $wechatObj["Ticket"] = '';
            } else if ($data['subtype'] == 'click' || $data['subtype'] == 'view') {
                $wechatObj["Event"] = "CLICK";
                $wechatObj["EventKey"] = $data['key'];
            }else if ($data['subtype'] == 'view') {
                $wechatObj["Event"] = "VIEW";
                $wechatObj["EventKey"] = $data['key'];
            }
        }
        return $wechatObj;
    }

    /**
     * 把微信信息格式还原微博格式
     * @param array $wechatObj
     */
    private function changeToWeibo($wechatObj) {
        
    }

    /**
     * 把微信新闻格式还原微博新闻格式
     * @param array $news
     */
    private function toWeiboNews($news) {
        $count = count($news);
        for ($i = 0; $i < $count; $i++) {
            $data[$i]['display_name'] = $news[$i]['Title'] ? $news[$i]['Title'] : "";
            $data[$i]['summary'] = $news[$i]['Description'] ? $news[$i]['Description'] : "1";
            $data[$i]['image'] = $news[$i]['PicUrl'] ? $news[$i]['PicUrl'] : "";
            $data[$i]['url'] = $news[$i]['Url'] ? $news[$i]['Url'] : "";
        }
        return $data;
    }

    /**
     * 过滤文字回复\r\n换行符
     * @param string $text
     * @return string|mixed
     */
    private function _auto_text_filter($text) {
        if (!$this->_text_filter)
            return $text;
        return str_replace("\r\n", "\n", $text);
    }

    /**
     * 设置回复消息
     * Example: $obj->text('hello')->reply();
     * @param string $text
     */
    public function text($text = '') {
        $data = $this->textData($text);
        $msg = array(
            'receiver_id' => $this->getRevFrom(),
            'sender_id' => $this->getRevTo(),
            'type' => self::MSGTYPE_TEXT,
            'data' => urlencode(json_encode($data))
        );
        $this->Message($msg);
        return $this;
    }

    /**
     * 设置回复图文
     * @param array $newsData 
     * 数组结构:
     *  array(
     *  	"0"=>array(
     *  		'Title'=>'msg title',
     *  		'Description'=>'summary text',
     *  		'PicUrl'=>'http://www.domain.com/1.jpg',
     *  		'Url'=>'http://www.domain.com/1.html'
     *  	),
     *  	"1"=>....
     *  )
     */
    public function news($newsData = array()) {
        $news = $this->toWeiboNews($newsData);
        $data = $this->articleData($news);
        $msg = array(
            'result' => TRUE,
            'receiver_id' => $this->getRevFrom(),
            'sender_id' => $this->getRevTo(),
            'type' => 'articles',
            'data' => urlencode(json_encode($data))
        );
        $this->Message($msg);
        return $this;
    }

    /**
     * 设置发送消息
     * @param array $msg 消息数组
     * @param bool $append 是否在原消息数组追加
     */
    public function Message($msg = '', $append = false) {
        if (is_null($msg)) {
            $this->_msg = array();
        } elseif (is_array($msg)) {
            if ($append)
                $this->_msg = array_merge($this->_msg, $msg);
            else
                $this->_msg = $msg;
            return $this->_msg;
        } else {
            return $this->_msg;
        }
    }

    /**
     * 
     * 回复微信服务器, 此函数支持链式操作
     * Example: $this->text('msg tips')->reply();
     * @param string $msg 要发送的信息, 默认取$this->_msg
     * @param bool $return 是否返回信息而不抛出到浏览器 默认:否
     */
    public function reply($msg = array(), $return = false) {
        if (empty($msg))
            $msg = $this->_msg;
        $xmldata = json_encode($msg);
        if ($return)
            return $xmldata;
        else
            echo $xmldata;
    }

    /**

     * 组装返回数据

     * @param $receiver_id

     * @param $sender_id

     * @param $data

     * @param $type

     * @return array

     */
    function buildReplyMsg($receiver_id, $sender_id, $data, $type) {

        return $msg = array(
            "sender_id" => $sender_id,
            "receiver_id" => $receiver_id,
            "type" => $type,
            //data字段需要进行urlencode编码
            "data" => urlencode(json_encode($data))
        );
    }

    /**

     * 生成text类型的回复消息内容

     * @param $text

     * @return array

     */
    function textData($text) {

        return $data = array("text" => $text);
    }

    /**

     * 生成article类型的回复消息内容

     * @param $article

     * @return array

     */
    function articleData($articles) {

        return $data = array(
            'articles' => $articles
        );
    }

    /**

     * 生成position类型的回复消息内容

     * @param $longitude

     * @param $latitude

     * @return array

     */
    function positionData($longitude, $latitude) {

        return $data = array(
            "longitude" => $longitude,
            "latitude" => $latitude
        );
    }

    /**
     * 创建菜单
     * @param array $data 菜单数组数据
     * example:
     * 	array (
     * 	    'button' => array (
     * 	      0 => array (
     * 	        'name' => '扫码',
     * 	        'sub_button' => array (
     * 	            0 => array (
     * 	              'type' => 'scancode_waitmsg',
     * 	              'name' => '扫码带提示',
     * 	              'key' => 'rselfmenu_0_0',
     * 	            ),
     * 	            1 => array (
     * 	              'type' => 'scancode_push',
     * 	              'name' => '扫码推事件',
     * 	              'key' => 'rselfmenu_0_1',
     * 	            ),
     * 	        ),
     * 	      ),
     * 	      1 => array (
     * 	        'name' => '发图',
     * 	        'sub_button' => array (
     * 	            0 => array (
     * 	              'type' => 'pic_sysphoto',
     * 	              'name' => '系统拍照发图',
     * 	              'key' => 'rselfmenu_1_0',
     * 	            ),
     * 	            1 => array (
     * 	              'type' => 'pic_photo_or_album',
     * 	              'name' => '拍照或者相册发图',
     * 	              'key' => 'rselfmenu_1_1',
     * 	            )
     * 	        ),
     * 	      ),
     * 	      2 => array (
     * 	        'type' => 'location_select',
     * 	        'name' => '发送位置',
     * 	        'key' => 'rselfmenu_2_0'
     * 	      ),
     * 	    ),
     * 	)
     * type可以选择为以下几种，其中5-8除了收到菜单事件以外，还会单独收到对应类型的信息。
     * 1、click：点击推事件
     * 2、view：跳转URL
     * 3、scancode_push：扫码推事件
     * 4、scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框
     * 5、pic_sysphoto：弹出系统拍照发图
     * 6、pic_photo_or_album：弹出拍照或者相册发图
     * 7、pic_weixin：弹出微信相册发图器
     * 8、location_select：弹出地理位置选择器
     */
    public function createMenu($data) {
        if (!$this->access_token) {
            return false;
        }
        $result = $this->http_post(self::API_URL_PREFIX.self::MENU_CREATE_URL.'access_token=' . $this->access_token, self::json_encode($data));
        if ($result) {
            $json = json_decode($result, true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return true;
        }
        return false;
    }

    	/**
	 * 微信api不支持中文转义的json结构
	 * @param array $arr
	 */
	static function json_encode($arr) {
		$parts = array ();
		$is_list = false;
		//Find out if the given array is a numerical array
		$keys = array_keys ( $arr );
		$max_length = count ( $arr ) - 1;
		if (($keys [0] === 0) && ($keys [$max_length] === $max_length )) { //See if the first key is 0 and last key is length - 1
			$is_list = true;
			for($i = 0; $i < count ( $keys ); $i ++) { //See if each key correspondes to its position
				if ($i != $keys [$i]) { //A key fails at position check.
					$is_list = false; //It is an associative array.
					break;
				}
			}
		}
		foreach ( $arr as $key => $value ) {
			if (is_array ( $value )) { //Custom handling for arrays
				if ($is_list)
					$parts [] = self::json_encode ( $value ); /* :RECURSION: */
				else
					$parts [] = '"' . $key . '":' . self::json_encode ( $value ); /* :RECURSION: */
			} else {
				$str = '';
				if (! $is_list)
					$str = '"' . $key . '":';
				//Custom handling for multiple data types
				if (is_numeric ( $value ) && $value<2000000000)
					$str .= $value; //Numbers
				elseif ($value === false)
				$str .= 'false'; //The booleans
				elseif ($value === true)
				$str .= 'true';
				else
					$str .= '"' . addslashes ( $value ) . '"'; //All other things
				// :TODO: Is there any more datatype we should be in the lookout for? (Object?)
				$parts [] = $str;
			}
		}
		$json = implode ( ',', $parts );
		if ($is_list)
			return '[' . $json . ']'; //Return numerical JSON
		return '{' . $json . '}'; //Return associative JSON
	}
    
    /**
     * 获取菜单
     * @return array('menu'=>array(....s))
     */
    public function getMenu() {
        if (!$this->access_token) {
            return false;
        }
        $result = $this->http_get(self::API_URL_PREFIX . self::MENU_GET_URL . 'access_token=' . $this->access_token);
        if ($result) {
            $json = json_decode($result, true);
            if (!$json || isset($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return $json;
        }
        return false;
    }

    /**
     * 删除菜单
     * @return boolean
     */
    public function deleteMenu() {
        if (!$this->access_token) {
            return false;
        }
        $result = $this->http_post(self::API_URL_PREFIX . self::MENU_DELETE_URL . 'access_token=' . $this->access_token);
        if ($result) {
            $json = json_decode($result, true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return true;
        }
        return false;
    }

    function getUserInfo($uid) {
        $access_token = '2.00i6J7FD5gcOREc6cb64bea58tFEdD';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.weibo.com/2/eps/user/info.json?access_token=" . $access_token . "&uid=" . $uid);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //值为false curl_exec($ch)直接输出结果
        $tmpInfo = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return json_decode($tmpInfo, true);
    }

    /**
     * 获取关注者详细信息
     * @param string $openid
     * @return array {subscribe,openid,nickname,sex,city,province,country,language,headimgurl,subscribe_time,[unionid]}
     * 注意：unionid字段 只有在用户将公众号绑定到微信开放平台账号后，才会出现。建议调用前用isset()检测一下
     */
//	public function getUserInfo($openid){
//		if (!$this->access_token && !$this->checkAuth()) return false;
//		$result = $this->http_get(self::API_URL_PREFIX.self::USER_INFO_URL.'access_token='.$this->access_token.'&openid='.$openid);
//		if ($result)
//		{
//			$json = json_decode($result,true);
//			if (isset($json['errcode'])) {
//				$this->errCode = $json['errcode'];
//				$this->errMsg = $json['errmsg'];
//				return false;
//			}
//			return $json;
//		}
//		return false;
//	} 

    /**
     * GET 请求
     * @param string $url
     */
    private function http_get($url) {
        $oCurl = curl_init();
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        if (intval($aStatus["http_code"]) == 200) {
            return $sContent;
        } else {
            return false;
        }
    }

    /**
     * POST 请求
     * @param string $url
     * @param array $param
     * @param boolean $post_file 是否文件上传
     * @return string content
     */
    private function http_post($url, $param, $post_file = false) {
        $oCurl = curl_init();
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }
        if (is_string($param) || $post_file) {
            $strPOST = $param;
        } else {
            $aPOST = array();
            foreach ($param as $key => $val) {
                $aPOST[] = $key . "=" . urlencode($val);
            }
            $strPOST = join("&", $aPOST);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCurl, CURLOPT_POST, true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS, $strPOST);
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        if (intval($aStatus["http_code"]) == 200) {
            return $sContent;
        } else {
            return false;
        }
    }

}

?>