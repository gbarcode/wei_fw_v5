<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Examples</title>
<style type="text/css">
        html,body {
            margin:0;
            height:100%;
        }
    </style>
<script src="/CreateControl.js" type="text/javascript"></script>

<script type="text/javascript">
//在网页初始加载时向报表提供数据
	function window_onload() {
		var Report = ReportViewer.Report;

	    ReportViewer.Stop();
	    
	    //用代码给参数赋值
	    Report.ParameterByName("code").AsString = 'LAF<?php echo $_GET["fwcode"] ?>';
	    Report.ParameterByName("qr").AsString = 'http://www.laf.hk/dealer?c=LAF<?php echo $_GET["fwcode"] ?>';
	    Report.ParameterByName("name").AsString = '<?php echo $_GET["name"] ?>';
	    Report.ParameterByName("url").AsString = '<?php echo $_GET["url"] ?>';
	    Report.ParameterByName("startDay").AsString = '<?php echo $_GET["startDay"] ?>';
	    Report.ParameterByName("endDay").AsString = '<?php echo $_GET["endDay"] ?>';
	    
	    ReportViewer.Start();
	}
</script>
</head>
<body onload="window_onload()">
<script type="text/javascript">
<?php
if($_GET['productID'] == -1){
	echo "CreatePrintViewerEx('100%', '100%', '/chart/".$_GET['brandID'].".grf', '', false, '<param name=BorderStyle value=1>')";
}else{
	echo "CreatePrintViewerEx('100%', '100%', '/chart/".$_GET['brandID']. "-" .$_GET['productID']. ".grf', '', false, '<param name=BorderStyle value=1>')";
}
?>
</script>
</body>
</html>