<?php
namespace Web\Controller;
use Think\Controller;
class NewsController extends Controller {
	//解决方案首页
    public function index(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();

        $Data = M('Company_news');

        $opt['ecid']   = $companyInfo['company_ecid'];
        $opt['synWeb'] = 1;//同步到网站的数据

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 6);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($opt)->order('top desc,modifyTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        $latest = $this->getTopNews();
        $arr=count($result);
        $id=$result[$arr-1]['id'];
        $this->assign('num',$id);
        $this->assign("newsList" , $result);
        $this->assign("latest" , $latest[0]);
        $this->assign( 'page', $show );// 赋值分页输出

        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    //解决方案详细介绍页
    public function info(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile    = \Web\General\Device::isMobile();
        $m           = M('Company_news');
        if(I('get.id')){
            $opt['id']   = I('get.id'); 
       }else{
            $opt['id']   = I('post.id');
       }
        

        $result = $m->where("id=".$opt['id'])->find();

        $this->assign('news',$result);
        $this->assign('recom',$this->getTopNews());

        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('info_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    /**
    * 函数名：getTopNews
    * 获取前8条推荐新闻
    * @access public
    * @param  id       新闻id
    * @return $data    读取的数据
    * @author 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-04-07 创建函数
    */
    private function getTopNews(){
        $m     = M('Company_news');
        $recom = $m->where("top = 1")->order("modifyTime desc")->limit(8)->select();
        return $recom;
    }

    //追加数据
    public function getnews(){
        $opt['id']=array('gt',I('get.id'));
        $opt['ecid']   = S('Ecid');
        $opt['synWeb'] = 1;
        
        $m=M('Company_news');
        $res=$m->where($opt)->limit(6)->select();
        $num=$res[count($res)-1]['id'];
        if($num){
            $data["status"] = $num;
            $data["info"] = $res;
          }else{
            $data["status"] = -1;
            $data["info"] = 'error';
          }
        $this->ajaxReturn($data ,"JSON" );
    }
}