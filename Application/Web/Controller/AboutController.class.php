<?php
namespace Web\Controller;
use Think\Controller;
class AboutController extends Controller {
	//解决方案首页
    public function index(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();

        $m           = M('Company_introduction');
        $opt['ecid'] = $companyInfo['company_ecid'];
        
        $result      = $m->where($opt)->select();
        for($i=0;$i<count($result);$i++){
           $result[$i]['content'] = htmlspecialchars_decode($result[$i]['content']); 
        } 

        $this->assign('info',$result);
        $this->assign('count',count($result));
        $this->assign('firstId',$result[0]['id']);
        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    /**
     * 公司介绍详情信息页
     * @author 蒋东芸
     * 2015/4/9
     */
    public function profile(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile    = \Web\General\Device::isMobile();

        $m           = M('Company_introduction');
        $opt['id'] = I('get.id');
        
        $result      = $m->where($opt)->find();
        $result['content'] = htmlspecialchars_decode($result['content']);                

        $this->assign('info',$result);
        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    public function info(){
        $m           = M('Company_introduction');
        $opt['id'] = I('get.id');
        
        $result      = $m->where($opt)->find();
        $result['content'] = htmlspecialchars_decode($result['content']);                

        if($result){
            $data["status"] = 0;
            $data["info"] = $result;
          }else{
            $data["status"] = -1;
            $data["info"] = 'error';
          }
        $this->ajaxReturn($data ,"JSON" );
    }

}