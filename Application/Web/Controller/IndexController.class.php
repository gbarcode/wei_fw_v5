<?php
namespace Web\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();

        $m           = M('Company_news');//获取新闻素材信息
        $opt['ecid'] = $option['ecid'] = $companyInfo['company_ecid'];
        $opt['top']  = 1;
        $res         = $m->where($opt)->order('modifyTime desc')->limit(8)->select();

        $this->assign('info',$this->getIntroduction());//获取企业介绍信息
        $this->assign('customer',$this->getCooperation());//获取合作企业
        $this->assign('recom',$res);
        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    /**
    * 函数名：getIntroduction
    * 获取合作企业
    * @access private
    * @param 
    * @return 企业介绍
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-13 创建函数
    */
    private function getIntroduction(){
        $m = M('Company_introduction');

        $result      = $m->find();
        $result['content'] = htmlspecialchars_decode($result['content']); 

        return $result;
    }

    /**
    * 函数名：getCooperation
    * 获取合作企业
    * @access private
    * @param 
    * @return 最新6个合作企业
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-13 创建函数
    */
    private function getCooperation(){
        $m = M('Company_cooperation');

        $result = $m->order('id desc')->limit(6)->select();

        return $result;
    }
}