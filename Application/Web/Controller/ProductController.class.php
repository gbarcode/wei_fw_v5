<?php
namespace Web\Controller;
use Think\Controller;
class ProductController extends Controller {
	public function index(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();
        S('Ecid',$companyInfo['company_ecid']);
        $m=M('company_column');
        $opt['ecid']=$companyInfo['company_ecid'];
        $opt['group_id']=0;
        //查找一级栏目
        $res=$m->where($opt)->select();
        //根据一级栏目查找二级栏目
        for($i=0;$i<count($res);$i++){
            $result[$i]['arr']=$this->getcolumn($res[$i]['id']);
            $result[$i]['name']=$res[$i]['name'];
            $result[$i]['id']=$res[$i]['id'];
        }
        $this->assign('info',$result);
        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    private function getcolumn($id){
        $m=M('Company_column');
        $opt['group_id']=$id;
        $res=$m->where($opt)->select();

        return $res;
    }

    public function column(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();
        //根据ecid找到品牌id
        $brand=M('Company_brand');
        $brandid=$brand->where(' ecid = '.S('Ecid'))->select();
        $id=array();
        for($i=0;$i<count($brandid);$i++){
            $id[$i]=$brandid[$i]['id'];
        }
        $getbrand['brandId']=array('in',$id);
        $m=M('Company_product');
        if(I('get.id')){
            $option=I('get.id');
        }else{//找到第一级栏目下,第一个第二级栏目的id
            $opt['group_id']=0;
            $opt['ecid']=S('Ecid');
            $res=M('Company_column')->where($opt)->find();
            $option=M('Company_column')->where('group_id = '.$res['id'])->getField('id');
        }
        if(I('post.id')){
            $option=I('post.id');
        }
        
        $obj=array();
        //根据品牌id找到所有的产品
        $result=$m->where($getbrand)->select();
        //根据栏目id找到所有符合条件的产品id
        for($i=0;$i<count($result);$i++){
            $arr=explode(",", $result[$i]['columnId']);
            if(in_array($option,$arr)){
                $obj[$i]=$result[$i]['id'];
            }
        }
        //找出所有符合条件的产品
        $productid['id']=array('in',$obj);
        $count      = $m->where($productid)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page($count, 8 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        $array      = $m->where($productid)->limit( $page->firstRow.','.$page->listRows )->select();
        
        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign('column',$array);
        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('column_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    //产品详细介绍页
    public function info(){
    	$companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();
        
        if(I('post.id')){
            $opt['id']=I('post.id');
        }else{
            $opt['id']=I('get.id');  
        }
        $m=M('Company_product');
        
        
        $res=$m->where($opt)->find();
        $res['modifyTime']=explode(" ",$res['modifyTime'])[0];
        $res['info']=htmlspecialchars_decode($res['info'], ENT_QUOTES);
        $res['content']=htmlspecialchars_decode($res['content'], ENT_QUOTES);
        $this->assign('info',$res);
        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('info_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }
}