<?php
namespace Web\Controller;
use Think\Controller;
use Org\Mxt\MxtClient;
class FwController extends Controller {
	//防伪查询页
    public function index(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();

        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    /**
    * 函数名：fwCheckResult
    * 防伪码查询
    * @access public
    * @param $fwCode 防伪码
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-13 创建函数
    */
    public function fwCheckResult(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();

        $fwCode = I('post.fwCode');
        $options = array(
            'ecid'     => $companyInfo['company_ecid'],
            'api_user' => $companyInfo['mxt_api'],
            'api_psw'  => $companyInfo['mxt_psw'],
            'fwCode'   => $fwCode,
            'type'     => 'web');

        $mxt = new MxtClient( $options );
        $result = $mxt->FwCheck( $fwCode );

        //返回查询类型
        if($result['checkCount']==0){
            $result['type'] = 'right';
        }elseif($result['checkCount']<0){
            $result['type'] = 'error';
        }else{
            $result['type'] = 'repeat';
        }
        
        $this->assign('fwCheck',$result);

        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    public function getfw(){
        $companyInfo = \Web\General\Company::companyInfo();
        $fwCode = I('get.fwCode');
        $options = array(
            'ecid'     => $companyInfo['company_ecid'],
            'api_user' => $companyInfo['mxt_api'],
            'api_psw'  => $companyInfo['mxt_psw'],
            'fwCode'   => $fwCode,
            'type'     => 'web');
        
        $mxt = new MxtClient( $options );
        $result = $mxt->FwCheck( $fwCode );

        //返回查询类型
        if($result['checkCount']==0){
            $result['type'] = 'right';
        }elseif($result['checkCount']<0){
            $result['type'] = 'error';
        }else{
            $result['type'] = 'repeat';
        }
        
        if($result){
            $data["status"] = 0;
            $data["info"] = $result;
          }else{
            $data["status"] = -1;
            $data["info"] = 'error';
          }
        $this->ajaxReturn($data ,"JSON" );
    }

    public function fwrepeat(){
        $this->assign('replyContent',I('get.replyContent'));
        $this->theme($companyInfo['webTheme'])->display('fwrepeat_mobile'); 
    }

    public function fwerror(){
        $this->assign('replyContent',I('get.replyContent'));
        $this->theme($companyInfo['webTheme'])->display('fwerror_mobile'); 
    }

    public function fwright(){
        $this->assign('replyContent',I('get.replyContent'));
        $this->theme($companyInfo['webTheme'])->display('fwright_mobile'); 
    }
}