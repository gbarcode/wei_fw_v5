<?php
namespace Web\Controller;
use Think\Controller;
class SolutionController extends Controller {
	//解决方案首页
    public function index(){
        $companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();
        
        $Data = M('Web_solution');

        $opt['ecid']   = $companyInfo['company_ecid'];

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 6);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        // 进行分页数据查询
        $result = $Data->where($opt)->order('modifyTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        $arr=count($result);
        $id=$result[0]['id'];
        $this->assign('num',$id);
        $this->assign("solution" , $result);
        $this->assign( 'page', $show );// 赋值分页输出

        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('index_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    //解决方案详细介绍页
    public function info(){
    	$companyInfo = \Web\General\Company::companyInfo();
        $isMobile = \Web\General\Device::isMobile();
        $m           = M('Web_solution');
        $opt['id']   = I('get.id');

        $result = $m->where("id=".$opt['id'])->find();

        $solution=$m->limit(8)->select();
        $this->assign('webSolution',$result);
        $this->assign('solution', $solution);

        if($isMobile)
            $this->theme($companyInfo['webTheme'])->display('info_mobile');
        else
            $this->theme($companyInfo['webTheme'])->display();
    }

    //追加数据
    public function getsolution(){
        $opt['id']=array('lt',I('get.id'));
        $opt['ecid']   = S('Ecid');
        
        $m=M('Web_solution');
        $res=$m->where($opt)->order('modifyTime desc' )->limit(3)->select();
        $num=$res[count($res)-1]['id'];
        if($num){
            $data["status"] = $num;
            $data["info"] = $res;
          }else{
            $data["status"] = -1;
            $data["info"] = 'error';
          }
        $this->ajaxReturn($data ,"JSON" );
    }
}