<?php
namespace Web\General;
class Company{
	public static function companyInfo(){
		$ecid = I('get.ecid');

		if(!$ecid)
			return false;

		if($companyInfo = S('companyInfo'.$ecid)){
			return $companyInfo;
		}

		$m = M('Company_info');

		$companyInfo = $m->find($ecid);

		if($companyInfo)
			S('companyInfo'.$ecid, $companyInfo);

		return $companyInfo; 
	}
}
?>