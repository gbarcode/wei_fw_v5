<?php
namespace Mobile\Wechat;
use Org\Weixin\Wechat;
use Org\Weixin\TPWechat;
class WechatPublic {
    public static function getOpenid(){
        $appid = C('WECHAT_APPID');
        $appsecret = C('WECHAT_SECRET');

        $options = array(
            'appid'=>$appid, //填写高级调用功能的app id
            'appsecret'=>$appsecret, //填写高级调用功能的密钥
        );

        $weixin = new TPWechat($options);

        $userInfo = $weixin->getOauthAccessToken();

        if($userInfo){
            return $userInfo['openid'];
        }

        // if(I('get.redirct') == 1)
        //     return false;

        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];

        $url = urlencode($url);

        $redirctUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$appid}&redirect_uri={$url}&response_type=code&scope=snsapi_base&state=0#wechat_redirect";

        header("Location: {$redirctUrl}");
    }

    public static function getJsSdkSign($appid,$appsecret){
        // $appid = C('WECHAT_APPID');
        // $appsecret = C('WECHAT_SECRET');

        $options = array(
            'appid'=>$appid, //填写高级调用功能的app id
            'appsecret'=>$appsecret, //填写高级调用功能的密钥
        );
        $weObj = new TPWechat($options);

        $auth = $weObj->checkAuth();
        $api_ticket = $weObj->getJsTicket();

        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        return $weObj->getJsSign($url);
    }

    public static function getMedia($mediaId){
        $appid = C('WECHAT_APPID');
        $appsecret = C('WECHAT_SECRET');

        $options = array(
            'appid'=>$appid, //填写高级调用功能的app id
            'appsecret'=>$appsecret, //填写高级调用功能的密钥
        );

        $weixin = new TPWechat($options);

        return $weixin->getMedia($mediaId);
    }
    
}