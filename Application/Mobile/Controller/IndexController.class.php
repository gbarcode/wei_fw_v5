<?php
namespace Mobile\Controller;
use Think\Controller;
use Org\Mxt\MxtClient;
use Mobile\Wechat\WechatPublic;
use Org\Weixin\Wechat;
use Org\Weixin\TPWechat;
class IndexController extends Controller {
    public function index(){
        $this->display();
    }
    public function fwCheck(){
    	$this->display();
    }
    public function fwCheckResult(){
        $ecid = I('get.ecid');
        $fwCode = I('post.fwCode');
        $m = M('Company_info');
        $result = $m->where('ecid = '.$ecid)->select();
        $mxt_api = $result['mxt_api'];
        $mxt_pwd = $result['mxt_pwd'];
        $fwCode = I('post.fwCode');
        $options = array(
            'ecid'     => $ecid,
            'api_user' => $mxt_api,
            'api_psw'  => $mxt_pwd,
            'fwCode'   => $fwCode,
            'type'     => 'mobile');

        $mxt = new MxtClient( $options );
        $result = $mxt->FwCheck( $fwCode );
        $this->assign('fwCode',$fwCode);
        $this->display();
    }
    public function News(){
    	$this->display();
    }
    public function dmfw(){
    	$this->display();
    }
    public function qyzx(){
    	$m = M('Company_news');
    	$result = $m->where('ecid = 816')->select();
    	$this->assign('news',$result);
    	$this->display();
    }
    public function product(){
    	$ecid = I('get.ecid');
        
    	$opt['field'] = 'product.name,product.minImg,product.info';
    	$opt['table'] = array('sz12365_fw_company_brand'=>'brand','sz12365_fw_company_product'=>'product');
    	$opt['option'] = "brand.ecid = {$ecid} AND brand.id = product.brandId";
    	$Model= new \Think\Model();//实例化空模型
    	$count = $Model->field($opt['field'])->table($opt['table'])
            ->where($opt['option'])->count();//计算行数
        
        $page       = new \Think\Page( $count , 8 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
    	$result = $Model->field($opt['field'])->table($opt['table'])
            ->where($opt['option'])->select();
            
    	$this->assign('product',$result);
    	$this->display();
    }
    public function aboutAs(){
    	$this->display();
    }

    public function shareTest(){
        $ecid=I('get.ecid');
        $share = M("Company_fw_share")->where("ecid = '".$ecid."' AND isDefault=1")->find();
        $companyInfo = M("Company_info")->where("company_ecid = '".$ecid."'")->find();
        $this->assign('share', $share);
        $this->assign('js_sign', WechatPublic::getJsSdkSign($companyInfo['weixin_AppId'],$companyInfo['weixin_AppSecret']));
        $this->display();
    }

}