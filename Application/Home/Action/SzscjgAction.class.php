<?php
namespace Home\Action;
use Think\Action;
use Org\Szscjg\SzscjgClient;
class SzscjgAction extends Action {
    public function CBuItemInfo(){
        $this->is_weixin();
        $this->display();
    }

    public function is_weixin(){ 
        if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') == false ) {
            echo "ERROR 404--NOT FOUND";die;
        }
    }
    
    public function CEModify(){
        $this->is_weixin();
        $this->display();
    }
    
    public function CKnowledge(){
        $this->is_weixin();
        $szscjg = new SzscjgClient();

        $result = $szscjg->GetCKnowledge_CategoryList();
        for($i = 0;$i<count($result['业务知识库类别信息']);$i++){
            $result['业务知识库类别信息'][$i] = $this->std_class_object_to_array($result['业务知识库类别信息'][$i]);
        }
        
        $this->assign('result' , $result['业务知识库类别信息']);
        $this->display();
    }
    
    public function CKnowledgeInfo(){
        $this->is_weixin();
        $id = I('get.id');

        $szscjg = new SzscjgClient();
        $data = $szscjg->GetCKnowledgeInfo($id);
        $result = $this->std_class_object_to_array($data['业务知识库详细信息']);
        
        $this->assign('result' , $result);
        $this->display();
    }
    
    public function CKnowledgeList(){
        $this->is_weixin();
        $id = I('get.id');
        $keyword = I('get.keyword');
        $szscjg = new SzscjgClient();

        //知识库类别信息
        $info = $szscjg->GetCKnowledge_CategoryList();
        for($i = 0;$i<count($info['业务知识库类别信息']);$i++){
            $info['业务知识库类别信息'][$i] = $this->std_class_object_to_array($info['业务知识库类别信息'][$i]);
        }

        $result = $szscjg->GetCKnowledge_List($keyword , $id);
        
        for($i = 0;$i<count($result['业务知识库列表']);$i++){
            $result['业务知识库列表'][$i] = $this->std_class_object_to_array($result['业务知识库列表'][$i]);
        }

        $this->assign('info' , $info['业务知识库类别信息']);
        $this->assign('result' , $result['业务知识库列表']);
        $this->display();
    }

    public function std_class_object_to_array($stdclassobject)
    {
        $this->is_weixin();
        $_array = is_object($stdclassobject) ? get_object_vars($stdclassobject) : $stdclassobject;
     
        foreach ($_array as $key => $value) {
            $value = (is_array($value) || is_object($value)) ? $this->std_class_object_to_array($value) : $value;
            $array[$key] = $value;
        }
     
        return $array;
    }
    
    public function cList(){
        $this->is_weixin();
        $this->display();
    }
    
    public function DCDYInfo(){
        $this->is_weixin();
        $this->display();
    }
    
    public function EnterpriseInfo(){
        $this->is_weixin();
        $this->display();
    }
    
    public function FYDJInfo(){
        $this->is_weixin();
        $this->display();
    }
    
    public function GQZYInfo(){
        $this->is_weixin();
        $this->display();
    }
    
    public function XZXKList(){
        $this->is_weixin();
        $this->display();
    }
    
    public function YLMLInfo(){
        $this->is_weixin();
        $this->display();
    }

    public function getCompanyListHandle(){
        $this->is_weixin();
        $keyword = I('post.keyword');

        $szscjg = new SzscjgClient();

        $result = $szscjg->GetEnterpriseListByNameKey($keyword);

        $this->ajaxReturn($result,'JSON',0);
    }

    public function GetCBuItemInfoByCode(){
        $this->is_weixin();
        $keyword = I('post.keyword');

        $szscjg = new SzscjgClient();

        $result = $szscjg->GetCBuItemInfoByCode($keyword);

        $this->ajaxReturn($result,'JSON',0);
    }

    public function getCKnowledgeList(){
        $this->is_weixin();
        $data = I('post.');

        $szscjg = new SzscjgClient();

        $result = $szscjg->GetCKnowledge_List($data['keyword'] , $data['id']);

        $this->ajaxReturn($result,'JSON',0);
    }
    // public function test(){
    //     $keyword = '华为';

    //     $szscjg = new SzscjgClient();

    //     $result = $szscjg->GetEnterpriseListByNameKey($keyword);

        
    // }
}
?>