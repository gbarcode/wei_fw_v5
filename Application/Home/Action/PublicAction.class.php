<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Home\Action;
use Think\Action;
class PublicAction extends Action{
    public function verify(){
    	$config =    array(
			    'fontSize'    =>    15,    // 验证码字体大小
			    'length'      =>    4,     // 验证码位数
			    'useNoise'    =>    false, // 关闭验证码杂点
			);

        $Verify = new \Think\Verify($config);
		$Verify->entry();
    }
}
?>