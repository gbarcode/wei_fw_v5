<?php
namespace Home\Action;
use Org\Error\Error;
use Org\Weixin\Wechat;
use Think\Action;
class UserAction extends Action {
    public function activity() {
        $ecid=I('get.ecid');
        $condtion['openId']=I('get.openId');
        $condtion['activityId']=I('get.activityId');
        $m=M('Company_'.$ecid.'_activity_user');
        $result=$m->where($condtion)->find();
        if($result){
            $this->display('signed');
        }else{
       //注册活动信息
        $this->assign('activityItem', $this->getActivityInfo(I('get.activityId')));
        //注册用户信息
        $this->assign('userInfo', $this->getUserInfo(I('get.openId'), I('get.ecid')));

        $this->assign('ecid', I('get.ecid'));

        $this->display(); 
        }         
    }

    private function addActivityUser($ecid, $userData){
        $m = M('Company_'.$ecid.'_activity_user');

        return $m->add($userData);
    }

    private function isRightUser($ecid, $data){
        $m = M('Company_'.$ecid.'_activity_user');
        
        //判断Company_'.$ecid.'_activity_user表中是否有数据
        $res=$m->where($data)->find();

        return $res;
    }

    private function userSign($ecid, $activityUserId, $openid){
        $m = M('Company_'.$ecid.'_activity_user');
        $data['signTime']=date('Y-m-d H:i:s');
        $data['openId'] = $openid;
        $data['id'] = $activityUserId;

        $saveLine = $m->save($data);
        $res=$m->where($data)->find();

        return $saveLine;
    }

    public function checkUser() {
        
        $ecid=I('post.ecid');

        $userId = '';

        //获取活动信息
        $activityInfo = $this->getActivityInfo(I('post.activityId'));
        

        if($activityInfo){
        //存在此活动
            //判断是否为开放活动
            if($activityInfo['anonymous']==1){
                $data['user_name']  =I('post.name');
                $data['tel']        =I('post.tel');
                $data['activityId'] =I('post.activityId');
                $data['openId']     =I('post.openid');
                $data['signTime']   =date('Y-m-d H:i:s');
                //添加用户
                $userId = $this->addActivityUser($ecid, $data);
            }
            else{ 
                $condition['user_name']  = I('post.name');
                $condition['tel']        = I('post.tel');
                $condition['activityId'] = I('post.activityId');
                
                //判断是否为可签到用户
                $userInfo = $this->isRightUser($ecid, $condition);
                if($userInfo){
                    //登录签到信息
                    $saveLine = $this->userSign($ecid, $userInfo['id'], I('post.openid'));
                    $userId = $userInfo['id'];
                }
                else{
                    $ajaxResult['status'] = Error::ERROR_SIGNIN_FALSE;
                    $ajaxResult['msg'] = Error::getErrMsg($ajaxResult['status']);  
                }
            }
        }
        else{
            $ajaxResult['status'] = Error::ERROR_SIGNIN_GENERAL;
            $ajaxResult['msg'] = Error::getErrMsg($ajaxResult['status']);  
        }

        //用户正确时记录用户信息
        if($userId > 0){
            $m = M('Company_'.$ecid.'_activity_user');
            $userInfo = $m->find($userId);

            $msg = "{$userInfo['user_name']},您好！您已成功完成活动《{I('post.activityName')}》人员信息绑定操作。";
            $this->sendCustomMessage($msg, I('post.openid'), I('post.ecid')); 
            //添加微信用户与活动ID缓存
            $session = array("openid"=>I('post.openid') , "activityId"=>I('post.activityId'));
            S("sign_".I('post.openid') , $session , 86400);

            $ajaxResult['status'] = Error::SUCCESS_OK; 
            $ajaxResult['userInfo'] = $userInfo;
        }
            
        $this->ajaxReturn($ajaxResult , "JSON");              
    }

    private function getActivityInfo($activityId){
        $m = M('Company_activity');
        $result = $m->find($activityId);
        return $result;
    }

    private function getUserInfo($openId, $ecid){
        $m = M('Company_'.$ecid.'_user_info');
        $opt['openId'] = $openId;
        $result = $m->where($opt)->find();
        return $result;
    }

    public function test(){
        $openid = I('get.openId');
        $this->sendCustomMessage('test', $openid, '816');
    }

    private function sendCustomMessage($msg, $openId, $ecid){
        $ecid_cache = 'company_info'.$ecid;
        $companyInfo = S($ecid_cache);

        $msgArr = array(
            'touser' => $openId,
            'msgtype' => 'text',
            'text' => array(
                'content' => $msg));

        
        $weObj = new Wechat();
        if($weObj->checkAuth($companyInfo['weixin_AppId'], $companyInfo['weixin_AppSecret'])){
            $weObj->sendCustomMessage($msgArr);
        }
    }
}
?>