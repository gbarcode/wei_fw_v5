<?php
namespace Home\Action;
use Think\Action;
use Org\Error\Error;
class ActivityAction extends Action{
	public function index(){
		$m=M('Company_activity');

		$result = $m->find(I('get.activityId'));
		$con = json_decode($result['configure'],true);

		$this->assign('openId',I('get.openId'));
        $this->assign('activityInfo',$result);
        $this->assign('need',$con['need']);
        $companyName = M('Company_info')->where("company_ecid = ".$result['ecid'])->getField('company_name');
        $this->assign('companyName',$companyName);

        $info=M('Company_'.$result['ecid'].'_user_info');

        $opt['openId']=I('get.openId');
        

        $res=$info->where($opt)->find();
        $exdata = json_decode($res['exdata'],true);
		
        if($res['realName']){
        	$this->assign('exchangeCode' , $this->getExchageCode(I('get.activityId') , I('get.openId')));
			$this->assign('userinfo',$res);
		}
		$this->assign('exdata',$exdata);
		$this->assign('ecid' , $result['ecid']);
		$this->display();
	}
	

	public function ParticiInfo(){
		$data = I('post.');

		$m=M('Company_'.$data['ecid'].'_user_info');
		$option['openId'] = $data['openId'];
		$option['realName']=I('post.realName');
		$option['tel']=I('post.tel');
		$option['address']=I('post.address');
		$option['exdata']=json_encode(I('post.exdata'));//转成json数组格式

		if($data['activityId'] == 251){
			$exchangeCode = $this->getExchageCode($data['activityId'] , $data['openId']);
			$result['exchangeCode'] = $exchangeCode;
		}

		if($result=$m->where($option)->find()){
			$result['status'] = Error::ERROR_REGISTRATION_EXIST;
			$result['info'] = Error::getErrMsg(Error::ERROR_REGISTRATION_EXIST);
		}
		else{
			
			if($m->where("openId='".$data['openId']."'")->save($option)){
				$result['status'] = Error::SUCCESS_OK;
				$result['exchangeCode'] = $exchangeCode;
			}else{
				$result['status'] = Error::ERROR_GENERAL;
	            $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
			}
		}
		$this->ajaxReturn($result,"JSON");
	}

	private function getExchageCode($activityId , $openId){
		$m = M('Company_lucky_user');
		$opt['activityId'] = $activityId;
		$opt['userId'] = $openId;
		return $m->where($opt)->getField('exchangeCode');
	}

	public function meeting(){
		$id = I('get.id');
		$openId = I('get.openId');
		$m = M('Company_activity');
		$opt['id'] = $id;
		$activityInfo = $m->where($opt)->find();

		//查询是否已经报名
		if($row = M('Company_activity_signup')->where("openId = '".$openId."'")->find()){
			$this->assign('userInfo' , $row);
		}

		$this->assign('activityInfo' , $activityInfo);
		$this->assign('openId' , $openId);
		$this->assign('activityId' , $id);
		$this->display();
	}

	public function meetingSignUp(){
		$opt = I('post.');
		$opt['addTime'] = date('Y-m-d H:i:s');
		$m = M('Company_activity_signup');

		if(!$this->checkVerify($opt['verify'])){
			$result['status'] = -1;
			$result['message'] = '验证码不正确！';
			$this->ajaxReturn($result , 'json');
		}

		unset($opt['verify']);
		//判断是否已经报名
		if($row = $m->where("activityId = ".$opt['activityId']." and openId = '".$opt['openId']."'")->getField('id')){
			$m->where('id = '.$row['id'])->save($opt);
			$result['status'] = 0;
		}else{
			if($m->add($opt)){
				$result['status'] = 0;
			}else{
				$result['status'] = -1;
				$result['message'] = '提交失败';
			}
		}

		$this->ajaxReturn($result , 'json');
	}

	/**
     * 检查验证码是否正确
     * @return [int] [错误码]
     */
    private function checkVerify($code, $id='') {
      $verify = new \Think\Verify();
      return $verify->check($code, $id);
    }
}
?>