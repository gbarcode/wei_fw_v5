<?php
namespace Home\Action;
use Think\Action;
class MobileShowAction extends Action {
    public function index() {
        $this->display();
    }

    public function brand(){
    	$m = M("Company_brand");
        $opt['id'] = I('get.id');
        $result = $m->where($opt)->find();
        $result['info'] = htmlspecialchars_decode($result['info']);

        $this->assign("company" , $this->getCompanyInfo($result['ecid']));
        $this->assign("brand" , $result);
    	$this->display();
    }

    public function product(){
    	$m = M("Company_product");
        $opt['id'] = I('get.id');
        $result = $m->where($opt)->find();
        $result['info'] = htmlspecialchars_decode($result['info']);

        $this->assign("company" , $this->getCompanyInfo(M('Company_brand')->where('id = '.$result['brandId'])->getField('ecid')));
        $this->assign("product" , $result);
        $this->display();
    }

    public function label(){
        $m = M("Company_fw_label");
        $opt['id'] = I('get.id');
        $result = $m->where($opt)->find();
        $result['info'] = htmlspecialchars_decode($result['info']);

        $this->assign("company" , $this->getCompanyInfo($result['ecid']));
        $this->assign("label" , $result);
        $this->display();
    }

    public function employee(){
        $opt['id']=I('get.id');
        $m=M('Company_employees');
        $result=$m->where($opt)->find();
        $this->assign("company" , $this->getCompanyInfo($result['ecid']));
        $this->assign('employeeInfo',$result);
        $this->display();
    }

    public function company(){
        $result=$this->getCompanyInfo(I('get.ecid'));
        $this->assign('companyInfo',$result);     
        $this->display();
    }

    public function dealer(){
        $opt['id'] = $_GET['id'];
        $m = M("Company_dealers");
        $result = $m->where($opt)->find();
        $result['info'] = htmlspecialchars_decode($result['info']);

        $this->assign("company" , $this->getCompanyInfo($result['ecid']));
        $this->assign('dealerInfo',$result);     
        $this->display();
    }

    public function vipInfo(){
        //获取web接口链接
        $url = M('Company_info')->where('company_ecid = '.I('get.ecid'))->getField('mall_domain');
        $WebServiceURL = $url . "/weixin/webservice.php?wsdl";
        Vendor( 'NuSoap.nusoap' );
        $webSoap = new \nusoap_client( $WebServiceURL, true, false, false, false, false, 0, 30 );
        $webSoap->soap_defencoding = 'UTF-8';
        $webSoap->decode_utf8 = false;
        $webSoap->xml_encoding = 'utf-8';

        $opt['wxId'] = I('get.id');
        $opt['timestamp'] = time();
        $opt['token'] = $this->getShopToken($opt['wxId'] , $opt['timestamp']); 

        $result = $webSoap->call('getUserInfo' , $opt);

        if($result['Items'][0]['sex'] == 0){
            $result['Items'][0]['sex'] = "女";
        }else{
            $result['Items'][0]['sex'] = "男";
        }

        $this->assign('orderArr' , $result['Items'][0]);
        $this->display();
    }

    public function vipBind(){
        $result=$this->getCompanyInfo(I('get.ecid'));
        $this->assign('companyInfo',$result); 
        $this->assign('wxId' , I('get.id'));
        $this->assign('ecid' , I('get.ecid'));
        $this->display();
    }

    public function vipBindHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('vipBind'));

        $m = M('Company_'.I('post.ecid').'_user_info');
        $userArr['openId'] = I('post.wxId');
        $userArr['ecuid'] = 0;

        if($m->where($userArr)->find()){
            $opt = I('post.');
            $opt['timestamp'] = time();
            $opt['token'] = sha1('sz12365'.'\0\0\0\0\0\0'.$opt['wxId'].$opt['timestamp']);

            //获取web接口链接
            $url = M('Company_info')->where('company_ecid = '.$opt['ecid'])->getField('mall_domain');
            $WebServiceURL = $url . "/weixin/webservice.php?wsdl";
            Vendor( 'NuSoap.nusoap' );
            $webSoap = new \nusoap_client( $WebServiceURL, true, false, false, false, false, 0, 30 );
            $webSoap->soap_defencoding = 'UTF-8';
            $webSoap->decode_utf8 = false;
            $webSoap->xml_encoding = 'utf-8';

            $result = $webSoap->call('userBindHandle' , $opt);

            if($result['Code'] > 0){
                $save['ecuid'] = $result['Code'];
                $m->where("openId = '".$opt['wxId']."'")->save($save);
            }
            
        }else{
            $result['Code'] = Error::ERROR_USER_IS_BIND;
            $result['Message'] = Error::getErrMsg(Error::ERROR_USER_IS_BIND);
        }
        
        $this->ajaxReturn($result , 'JSON');
    }

    private function getShopToken($wxId , $timestamp){
        return sha1('sz12365'.'\0\0\0\0\0\0'.$wxId.$timestamp);
    }

    private function getCompanyInfo($ecid){
        $opt['company_ecid']=$ecid;
        return M('Company_info')->where($opt)->find();
    }

    public function authorizCheck(){
        $Model= new \Think\Model();//实例化空模型
        $fwCode = I('get.fwCode');
        $result = $Model->field('authorize.fwCode,authorize.productId,authorize.startTime,authorize.endTime,info.company_name,info.company_logo,dealer.name as dealername,dealer.address,dealer.info,brand.name as brandname')->table(array('sz12365_fw_company_authorize'=>'authorize','sz12365_fw_company_info'=>'info','sz12365_fw_company_dealers'=>'dealer','sz12365_fw_company_brand'=>'brand'))
                ->where("authorize.ecid = info.company_ecid AND authorize.fwCode = '{$fwCode}' AND authorize.brandId= brand.id  AND authorize.dealerId = dealer.id" )->find();
        
        if($result['productId']!='-1'){
            $opt['id'] = $result['productId'];
            $result['productname']=M('Company_product')->where($opt)->getField('name');
        }
        $opt['dealerId'] = $result['dealerId'];

        $result['url']=M('Company_dealers_electricity')->where($opt)->getField('url');
        
        
        if(M('Company_authorize')->where('fwCode = "'.$fwCode.'"')->find()){
            $result['status'] = 0;
        }else{
            $result['status'] = 1;
            $fw = M('Company_info')->where('company_ecid = 816')->find();
            $result['company_logo'] = $fw['company_logo'];
            $result['company_name'] = $fw['company_name'];
        }
        $this->assign('data',$result);
        $this->display();
    }

    public function questionActivity(){
        if(S('question_get_params')){
            //获取缓存信息
            $sessionArr = S('question_get_params');
        }else{
            $this->error ( '请在微信客户端打开链接' );
        }

        //获取活动信息
        $result = $this->getActivityInfo(I('get.activityId'));
        $result['configure'] = json_decode($result['configure'] , true);

        //判断活动时间
        if(strtotime(date('Y-m-d')) >= strtotime($result['configure']['endDate'])&&strtotime(date('H:i:s')) > strtotime($result['configure']['endTime'])){
            $this->assign('isActivityTime' , 0);
        }else{
            if(strtotime(date('Y-m-d')) >= strtotime($result['configure']['startDate']) && strtotime(date('Y-m-d')) <= strtotime($result['configure']['endDate'])){
                if( strtotime(date('H:i:s')) >= strtotime($result['configure']['startTime']) && strtotime(date('H:i:s')) <= strtotime($result['configure']['endTime']) ){
                    $this->assign('isActivityTime' , true);
                }else{
                    $this->assign('isActivityTime' , false);
                }
            }else{
                $this->assign('isActivityTime' , false);
            }
        }

        //if($result['configure']['timesType'] == 'once'){
            //获取当前用户信息
            $res = $this->getUserId($result['ecid'],$sessionArr['openId']);
            $opt['userId'] = $res['id'];
            $opt['activityId'] = I('get.activityId');
            if(M('Company_activity_join')->where($opt)->find()){
                $this->assign('joined',"您已参加过此活动！");
            }
        //}

        //获取问答活动信息
        $options['activityId'] = I('get.activityId');
        $Data = M('Company_activity_question');
        $questionInfo = $Data->where($options)->find();

        $this->assign('activityInfo',$result);
        $this->assign('activityTime',json_decode($result['configure'],true));
        $this->assign('question',$questionInfo['question']);
        $this->assign('id',I('get.activityId'));
        $this->assign('openId',$sessionArr['openId']);
        $this->assign('ecid' , $result['ecid']);
        $this->display();
    }

    //获取活动信息
    private function getActivityInfo($id){
        $opt['id'] = $id;        
        $m = M('View_company_activity');
        $result = $m->where($opt)->find();
        return $result;
    }

    //获取当前用户id
    private function getUserId($ecid,$openId){
        $info=M('Company_'.$ecid.'_user_info');
        $options['openId']=$openId;
        $res = $info->where($options)->find();
        return $res;
    }

    public function questionHandle(){

        if(I('post.end') == true){
            //获取活动信息
            $result = $this->getActivityInfo(I('post.id'));                
            //获取当前用户信息
            $res = $this->getUserId($result['ecid'],I('post.openId'));
            $condition['userId'] = $res['id'];
            $condition['ecid'] = $result['ecid'];
            $condition['joinTime'] = date('Y-m-d H:i:s');
            $condition['activityId'] = I('post.id');
            $condition['hasLuckyChange'] = I('post.pass');

            M('Company_activity_join')->add($condition);
        }        
    }

    //申诉举报页面
    public function userReport(){
        $this->assign('openId' , I('get.openId'));
        $this->assign('ecid' , I('get.ecid'));
        $this->display();
    }

    public function sendReportHandle(){
        $postArr = I('post.');

        if(!$this->checkVerify($postArr['verify'])){
            $result['status'] = -1;
            $result['message'] = '验证码不正确！';
            $this->ajaxReturn($result , 'json');
        }

        $body = '申诉人：'.$postArr['name'].'<br />联系电话：'.$postArr['tel'].'<br />内容：'.$postArr['content'];
        $this->sendMail('dajiabaoyou@163.com','【'.date('Y-m-d H:i:s').'】申诉举报',$body);

        $result['status'] = 0;
        $this->ajaxReturn($result , 'json');
    }

    /**
     * 检查验证码是否正确
     * @return [int] [错误码]
     */
    private function checkVerify($code, $id='') {
      $verify = new \Think\Verify();
      return $verify->check($code, $id);
    }

    /**
    * 定义函数
    * sendMail
    * 函数功能描述
    * 发送邮件
    * @access protected
    * @param string $to            收件人
    * @param string $subject       邮件主题
    * @param string $content       邮件内容
    * @return null
    * @auth 范小宝<fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-10-09 创建函数
    */
    protected function sendMail($to, $subject, $content) {
        Vendor('PHPMailer.class#phpmailer');
         
        $mail = new \PHPMailer(); //实例化
        $mail->IsSMTP(); // 启用SMTP
        $mail->Host=C('MAIL_SMTP'); //smtp服务器的名称
        $mail->SMTPAuth = true; //启用smtp认证
        $mail->Username = C('MAIL_LOGINNAME'); //你的邮箱名
        $mail->Password = C('MAIL_PASSWORD') ; //邮箱密码
        $mail->From = C('MAIL_LOGINNAME'); //发件人地址（也就是你的邮箱地址）
        $mail->FromName = '深圳市市场监管防伪服务中心'; //发件人姓名
        $mail->AddAddress($to,'');//发送邮件
        $mail->WordWrap = 50; //设置每行字符长度
        $mail->IsHTML(true); // 是否HTML格式邮件
        $mail->CharSet='utf-8'; //设置邮件编码
        $mail->Subject =$subject; //邮件主题
        $mail->Body = $content; //邮件内容
        $mail->Send();
    }
}
?>