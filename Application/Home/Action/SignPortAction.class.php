<?php
namespace Home\Action;
use Think\Action;
class SignPortAction extends Action {
    public function login() {
    	$option['user_name']=$_POST['name'];
    	$option['user_password']=md5($_POST['password']);
    	$m=M('User_info');
    	$result=$m->where($option)->find();
        if($result){
            $ajaxReturn['code'] = 0;
            $ajaxReturn['result'] = $result;
        }
        else
            $ajaxReturn['code'] = -1;
        
    	$this->ajaxReturn($ajaxReturn , "JSON");
    }

    public function getActivityList(){
        $opt['ecid']=$_POST['ecid'];
        $m=M('Company_activity');
        $result=$m->where($opt)->select();
    	$this->ajaxReturn($result , "JSON");
    }

    public function getSceneid(){
        import( 'ORG.Weixin.Wechat' );    //引用微信SDK
        import( 'ORG.Error.Error' );
        $opt['activityId']=$_POST['activityId'];
        $sceneId=M('Company_qr_type')->where($opt)->getField('scene_id');

        $m = M("Company_info");
        $ecid['company_ecid']=$_POST['ecid'];
        $token = $m->where($ecid)->find();
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            $result["status"] = Error::ERROR_WECHATMENU_INVALID_APPID;
            $result["info"] = Error::getErrMsg(Error::ERROR_WECHATMENU_INVALID_APPID);
            $this->ajaxReturn($result,"JSON");
        }

        $weObj = new Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
          $qrCode = $weObj->getQRCode($sceneId, 1);
            if($qrCode){
              $result["status"] = Error::SUCCESS_OK;
              $result["info"] = $weObj->getQRUrl($qrCode['ticket']);
            }
        }
        else{
            $result["status"] = Error::ERROR_COMPANY_WRONG_ACCESS;
            $result["info"] = Error::getErrMsg(Error::ERROR_COMPANY_WRONG_ACCESS);
        }
        $this->ajaxReturn($result,"JSON");
    }
    
    public function getActivityid(){
         $opt['id']=$_POST['activityId'];
        $m=M('Company_activity');
        $result=$m->where($opt)->find();
        $this->ajaxReturn($result,"JSON");
    }
    
 }
?>