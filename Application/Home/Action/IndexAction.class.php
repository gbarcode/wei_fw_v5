<?php
namespace Home\Action;
use Think\Action;
use Org\Net\Http;
class IndexAction extends Action {
    public function index() {
      if(I('get.ecid')){
        $opt['company_ecid'] = I('get.ecid');
        $m = M('Company_info');
        $companyName = $m->where($opt)->getField('company_name');
        $this->assign('name' , $companyName);
      }else{
        $this->assign('name' , '深圳市市场监管防伪网');
      }
      $result = M('Web_config')->where("ecid = 816")->select();

      for($i=0;$i<count($result);$i++){
        //循环输出
        $this->assign('title' , $result[$i]['title']);
        $this->assign('keyword',$result[$i]['keyword']);
        $this->assign('description',$result[$i]['description']);

      }
      //获取广告位最大限制数
      $options['value'] = 'index_ad';
      $maxNum = M('Company_ad')->where($options)->getField('maxNum');
      //获取首页图片
      $picture = M('Company_ad_set')->limit($maxNum)->order('modifyTime desc')->select();
      $this->assign('picture',$picture);

      $this->display("newIndex");
    }

    public function Login(){
      if(I('get.ecid') == 200113)
        $this->assign('iszg' , true);

      $this->display('new-login');
    }

    public function setUseInfo(){
      $opt = I('get.');
      
      if($opt['sex'] == 'male')
        $opt['sex'] = 1;
      else
        $opt['sex'] = 0;

      $opt['ip'] = $_SERVER["REMOTE_ADDR"];
      $opt['visitTime'] = date("Y-m-d H:i:s");
      $m = M('Visit_info');
      $m->add($opt);
      $this->display();
    }

    public function test(){
      $rand = rand(1,1000);

      $keyword = '';
      switch($rand%10){
        case 0:
          $keyword = '深圳防伪';
          break;
        case 1:
          $keyword = '通用条码';
          break;
        case 2:
          $keyword = '深圳标准院';
          break;
        case 3:
          $keyword = '我是帅哥';
          break;
        case 4:
          $keyword = '奔跑吧兄弟';
          break;
        case 5:
          $keyword = '我是歌手';
          break;
        case 6:
          $keyword = '二维码应用';
          break;
        case 7:
          $keyword = '处女座';
          break;
        case 8:
          $keyword = 'DOTA';
          break;
        case 9:
          $keyword = 'AngelBaby';
          break;
      }
      $this->assign('url', "http://www.baidu.com/s?wd={$keyword}&rsv_spt=1&issp=1&f=8&rsv_bp=0&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_enter=1&rsv_sug3=5&rsv_sug4=882&rsv_sug6=6&rsv_sug1=3&rsv_sug2=0&inputT=35003&rsv_sug=2");
      $this->display();
    }

    public function test1(){
      $rand = rand(1,1000);

      $url = '';
      switch($rand%10){
        case 0:
          $url = "http://www.baidu.com/s?wd=深圳防伪&rsv_spt=1&issp=1&f=8&rsv_bp=0&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_enter=1&rsv_sug3=5&rsv_sug4=882&rsv_sug6=6&rsv_sug1=3&rsv_sug2=0&inputT=35003&rsv_sug=2";
          break;
        case 1:
          $url = 'http://q.name.im/TrocknfcCkaN019234';
          break;
        case 2:
          $url = 'http://item.jd.com/1219311.html';
          break;
        case 3:
          $url = 'http://www.sz12365.net/mb/?fw=16614211800900750786';
          break;
        case 4:
          $url = 'http://www.gbarcode.com';
          break;
        case 5:
          $url = 'http://www.printhoo.com';
          break;
        case 6:
          $url = 'http://www.qq.com';
          break;
        case 7:
          $url = 'http://www.baidu.com';
          break;
        case 8:
          $url = 'http://www.163.com';
          break;
        case 9:
          $url = 'http://www.csdn.net';
          break;
      }
      $this->assign('url', $url);
      $this->display('test');
    }
    public function newIndex(){
      $this->display();
    }

    public function news() {
        $data = M( "Company_news" )->where( "id = " . $_GET["id"] )->find();
          $data['content'] = htmlspecialchars_decode($data['content']);
  
        $params = I('get.');
        $this->saveRead($params);

        $this->assign( 'newsArr' , $data );
        //$this->assign( 'params', $session);
        $this->display();
       
        // if(S('news_view_'.$params['r'])){
        //   $this->assign( 'newsArr' , $data );
        //   $this->assign( 'params', $session);
        //   $this->display();
        // }else{
        //   $url = \Weixin\Response\WechatConst::REDIRECT_DOMAIN . "ecid={$params['ecid']}&type=news&fromOpenId={$params['openid']}&from_type=1&newsId={$params['id']}";

        //   $base_url = $this->getOauthUrl($url, $params['ecid']);

        //   header("Location: {$base_url}");
        // }
    }

    private function saveRead($params){
      if($params['from'] == 'timeline'){
        $m = M('News_view_log');

        $opt['fromOpenId'] = $params['openid'];
        $opt['type'] = 2;
        $opt['ecid'] = $params['ecid'];
        $opt['newsId'] = $params['id'];
        $opt['time'] = date('Y-m-d H:i:s');

        $m->add($opt);
      }
    }

    private function getOauthUrl($url, $ecid){
      $companyInfo = $this->getCompanyInfo($ecid);

      if($companyInfo){
        $base_url = \Weixin\Response\WechatConst::OAUTH_REDIRECT_URL;

        $base_url = str_replace("<url>", urlencode($url), $base_url);
        $base_url = str_replace("<appid>", $companyInfo['weixin_AppId'], $base_url);

        return $base_url;
      }

      else 
        return $url;
    }

    private function getCompanyInfo($ecid){
      $m = M('Company_info');

      return $m->find($ecid);
    }

    public function activityNews(){
        $opt['id'] = $_GET['id'];
        $m = M('View_company_activity');
        $result = $m->where($opt)->find();
        $result['content'] = htmlspecialchars_decode($result['content']);
        $companyInfo = M("Company_info")->where("company_ecid = ".$result['ecid'])->find();
        $this->assign('company' , $companyInfo);
        $this->assign('activity',$result);
        $this->display();
    }

 
    public function doLogin() {
        //验证验证码是否正确

        // if (!$this->checkVerify($_POST['verify'])) {
        //     $this->error ( \Org\Error\Error::getErrMsg( \Org\Error\Error::ERROR_WRONG_VERIFY ) );
        // }

        //检查数据库是否存在用户
        $code = $this->checkUser();

        if ( $code != \Org\Error\Error::SUCCESS_OK ) {
            $this->error( \Org\Error\Error::getErrMsg( $code ) );
        }

        $ecid = session('ecid');
        $role = session('role');
        if($ecid == 200113 && $role == 'dealer'){
          redirect( U('Admin/Location/zgDealer') );
        }else{
          redirect( U('Admin/Index/index') );
        }
    }

    /**
     * 移动端登陆页面
     */
    public function MobileLogin(){
      if(I('get.ecid')){
        $opt['company_ecid'] = I('get.ecid');
        $m = M('Company_info');
        $companyName = $m->where($opt)->getField('company_name');
        $this->assign('name' , $companyName);
      }else{
        $this->assign('name' , '深圳市市场监管防伪网');
      }
      
      $this->display();
    }

    /**
     * 退出操作
     * @return [type] [description]
     */
    public function doQuit(){
        session(null);
        $this->success('退出成功！',U('Home/Index/index') );
    }

    /**
     * 检查验证码是否正确
     * @return [int] [错误码]
     */
    private function checkVerify($code, $id='') {
      $verify = new \Think\Verify();
      return $verify->check($code, $id);
    }

    /**
     * 检查用户名、密码是否正确
     * @return [int] [错误码]
     */
    private function checkUser() {
        //检查数据库是否存在用户
        $ecid = I('post.ecid',0);
        $user = I('post.user_name','-1');
        $password = md5(I('post.user_password'));

        if(substr($user,0,1) == '@'){
          $ecid = 816;
          $user = substr($user, 1);
        }
        $m = M( "User_info" );
        $condition['company_ecid'] = $ecid;
        $condition['user_name'] = $user;
        $condition['user_password'] = $password;
        $condition['roleId'] = array(array('EQ' , 'admin') , array('EQ' , 'dealer') , array('EQ' , 'Dprint') , array('EQ' , 'Dreview'), array('EQ' , 'Dadd') , 'or');
        $result = $m->where( $condition )->find();

        //用户正确时记录用户信息
        if ( $result != '' ) {
            $userCfg = C( 'LOGIN_USER' );

            session( $userCfg['NAME'], $user );
            session( $userCfg['ECID'] , I('post.ecid') );
            session( $userCfg['UID'] , $result["id"] );
            session( $userCfg['ROLE'] , $result["roleId"]);

            $info = M('Company_info')->field('company_name,company_logo')->where("company_ecid=".$ecid)->find();

            session('companyName',$info['company_name']);
            session('companyLogo',$info['company_logo']);

            if($result['roleId'] == 'dealer'){
              session( $userCfg['DEALERID'] , $result["dealerId"] );
            }

            return \Org\Error\Error::SUCCESS_OK;
        }
        else {
            return \Org\Error\Error::ERROR_WRONG_LOGIN_INFO;
        }
    }

  /**
   * downUserInfoFile  下载人员信息格式表
   * @return [array]
   * @author 范小宝
   */
  public function downUserInfoFile(){
    Http::download($_SERVER[DOCUMENT_ROOT] . __ROOT__ . "/Public/File/userInfo.xls");
  }


    public function downEmployeeInfoFile(){
    Http::download($_SERVER[DOCUMENT_ROOT] . __ROOT__ . "/Public/File/EmployeeInfo.xls");
  }

    /**
   * downAll  下载活动所有人员信息表
   * @return [array]
   * @author 范小宝
   */
  public function downAllUserFile(){
        import("@.PHPExcel.PHPExcel");
        import("@.PHPExcel.PHPExcel.IOFactory");
        import("@.PHPExcel.PHPExcel.Reader.Excel5");

        $objPHPExcel = new \PHPExcel();

                   // Set document properties

                   $objPHPExcel->getProperties()->setCreator("admin")//创建者

                   ->setLastModifiedBy("admin")//最后修改者

                   ->setTitle("所有人员信息")//标题

                   ->setSubject("所有人员信息")//主题

                   ->setDescription("所有人员信息")//备注

                   ->setKeywords("所有人员信息")//关键字

                   ->setCategory("所有人员信息");//分类

                   // Add some data

                   $objPHPExcel->setActiveSheetIndex(0)

                   //设置表的名称标题

                   ->setCellValue('A1',"姓名")

                   ->setCellValue('B1',"性别")

                   ->setCellValue('C1',"公司")

                   ->setCellValue('D1',"电话");

                   //获取人员信息
                   $info = M("Company_".$_GET['ecid']."_activity_user")->where("activityId = ".$_GET["id"])->select();

                   foreach($info as $k => $v)
                   {

                        $num=$k+2;

                        switch ($v['sex']) {
                            case 1:
                                $v['sex'] = "男";
                                break;
                            
                            case 0:
                                $v['sex'] = "女";
                                break;
                        }

                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$num, $v["user_name"])

                            ->setCellValue('B'.$num, $v["sex"])

                            ->setCellValue('C'.$num, $v["company"])

                            ->setCellValue('D'.$num, $v["tel"]);
                   }
                   

                   // Rename worksheet

                   $objPHPExcel->getActiveSheet()->setTitle('所有人员信息');

                   // Set active sheet index to the first sheet, so Excel opens this as the first sheet

                   $objPHPExcel->setActiveSheetIndex(0);

                   // Redirect output to a client’s web browser (Excel5)

                   header('Content-Type: application/vnd.ms-excel;charset=utf-8');

                   header('Content-Disposition: attachment;filename="所有人员信息.xls"');

                   header('Cache-Control: max-age=0');

                   $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

                   $objWriter->save('php://output');
    }

  /**
   * downSignedFile  下载已签到人员信息表
   * @return [array]
   * @author 范小宝
   */
  public function downSignedFile(){
        import("@.PHPExcel.PHPExcel");
        import("@.PHPExcel.PHPExcel.IOFactory");
        import("@.PHPExcel.PHPExcel.Reader.Excel5");

        $objPHPExcel = new \PHPExcel();

                   // Set document properties

                   $objPHPExcel->getProperties()->setCreator("admin")//创建者

                   ->setLastModifiedBy("admin")//最后修改者

                   ->setTitle("签到人员信息")//标题

                   ->setSubject("签到人员信息")//主题

                   ->setDescription("签到人员信息")//备注

                   ->setKeywords("签到人员信息")//关键字

                   ->setCategory("签到人员信息");//分类

                   // Add some data

                   $objPHPExcel->setActiveSheetIndex(0)

                   //设置表的名称标题

                   ->setCellValue('A1',"姓名")

                   ->setCellValue('B1',"性别")

                   ->setCellValue('C1',"公司")

                   ->setCellValue('D1',"电话")

                   ->setCellValue('E1',"签到时间");

                  //获取签到人员信息
                  $opt['activityId'] = $_GET["id"];
                  $opt['signTime'] = array("NEQ" , '');
                  $info = M("Company_816_activity_user")->where($opt)->select();

                   foreach($info as $k => $v)
                   {

                        $num=$k+2;

                        switch ($v['sex']) {
                            case 1:
                                $v['sex'] = "男";
                                break;
                            
                            case 0:
                                $v['sex'] = "女";
                                break;
                        }

                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$num, $v["user_name"])

                            ->setCellValue('B'.$num, $v["sex"])

                            ->setCellValue('C'.$num, $v["company"])

                            ->setCellValue('D'.$num, $v["tel"])

                            ->setCellValue('E'.$num, $v["signTime"]);
                            $num++;
                   }
                   

                   // Rename worksheet

                   $objPHPExcel->getActiveSheet()->setTitle('签到人员信息');

                   // Set active sheet index to the first sheet, so Excel opens this as the first sheet

                   $objPHPExcel->setActiveSheetIndex(0);

                   // Redirect output to a client’s web browser (Excel5)

                   header('Content-Type: application/vnd.ms-excel;charset=utf-8');

                   header('Content-Disposition: attachment;filename="签到人员信息"');

                   header('Cache-Control: max-age=0');

                   $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

                   $objWriter->save('php://output');
                   // $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV')->setDelimiter(',' )  ->setEnclosure('"' )    //设置包围符  
                   //    ->setLineEnding("\r\n" )//设置行分隔符  
                   //    ->setSheetIndex(0)      //设置活动表  
                   //    ->save(str_replace('.php' , '.csv' , __FILE__)); 

    }

  /**
   * downSignedFile  下载未签到人员信息表
   * @return [array]
   * @author 范小宝
   */
  public function downUnSignedFile(){
    $userTB = M("sz12365_fw_company_qr_sign_user_info_".$_GET["id"]);
    $signTB = M("sz12365_fw_company_qr_sign_user_openid_".$_GET["id"]);

        import("@.PHPExcel.PHPExcel");
        import("@.PHPExcel.PHPExcel.IOFactory");
        import("@.PHPExcel.PHPExcel.Reader.Excel5");

        $objPHPExcel = new \PHPExcel();

                   // Set document properties

                   $objPHPExcel->getProperties()->setCreator("admin")//创建者

                   ->setLastModifiedBy("admin")//最后修改者

                   ->setTitle("未签到人员信息")//标题

                   ->setSubject("未签到人员信息")//主题

                   ->setDescription("未签到人员信息")//备注

                   ->setKeywords("未签到人员信息")//关键字

                   ->setCategory("未签到人员信息");//分类

                   // Add some data

                   $objPHPExcel->setActiveSheetIndex(0)

                   //设置表的名称标题

                   ->setCellValue('A1',"姓名")

                   ->setCellValue('B1',"性别")

                   ->setCellValue('C1',"公司")

                   ->setCellValue('D1',"电话");

                  //获取未签到人员信息
                  $opt['activityId'] = $_GET["id"];
                  $info = M("Company_".$_GET['ecid']."_activity_user")->where($opt)->select();

                  $num = 2;//初始化数据写入位置，从第2行开始写入
                  foreach($info as $k => $v)
                  {
                    if($v['signTime'] == null){
                      switch ($v['sex']) {
                        case 1:
                            $v['sex'] = "男";
                            break;
                        
                        case 0:
                            $v['sex'] = "女";
                            break;
                      }

                      $objPHPExcel->setActiveSheetIndex(0)
                          ->setCellValue('A'.$num, $v["user_name"])

                          ->setCellValue('B'.$num, $v["sex"])

                          ->setCellValue('C'.$num, $v["company"])

                          ->setCellValue('D'.$num, $v["tel"]);

                      $num++;
                    }
                  }

                   // Rename worksheet

                   $objPHPExcel->getActiveSheet()->setTitle('未签到人员信息');

                   // Set active sheet index to the first sheet, so Excel opens this as the first sheet

                   $objPHPExcel->setActiveSheetIndex(0);

                   // Redirect output to a client’s web browser (Excel5)

                   header('Content-Type: application/vnd.ms-excel;charset=utf-8');

                   header('Content-Disposition: attachment;filename="未签到人员信息"');

                   header('Cache-Control: max-age=0');

                   $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

                   $objWriter->save('php://output');
    }
}
