<?php
namespace Home\Action;
use Think\Action;
use Org\Mxt\MxtClient;
class FwAction extends Action {
    public function index() {
        $option = array(
            "ecid" => $_POST['ecid'] ,
            "fwCode" => $_POST['fwCode'] ,
            "type" => "web" ,
            "api_user" => $_POST['api_user'] ,
            "api_psw" => $_POST['api_psw'] ,
            );
        $result = array(
            "Code" => $_POST['Code'] ,
            "Message" => $_POST['Message'] ,
            );
        $mxt = new MxtClient($option);
        $result = $mxt->cloudFwCheck($result);
        $this->ajaxReturn($result , "JSON");
    }

    public function indexTest() {
        $option = array(
            "ecid" => '200019' ,
            "fwCode" => '18442470100405630096' ,
            "type" => "web" ,
            "api_user" => 'lafapi' ,
            "api_psw" => '1D71B3899165F4287096641150C64F5E',
            );
        $result = array(
            "Code" => '0' ,
            "Message" => '立方国际有限公司#_#立方国际#_#立方国际#_#100109975016705803',
            );
        $mxt = new MxtClient($option);
        $result = $mxt->cloudFwCheck($result);
        dump($result);
        $this->ajaxReturn($result , "JSON");
    }

    public function wlCheck(){
        $mxt = new MxtClient($_POST['ecid'] , "");
        $result['fwMessage'] = $mxt->FwCheck($_POST['fwcode'] , false);
        $result['wlcode'] = $mxt->getWLCode();
        $result['productId'] = $mxt->getProudct();
        $result['fwCheckCode'] = $mxt->getCheckCode();
        $wlResult = $mxt->wlCheck($result['wlcode']);
        $result['wlMessage'] = $wlResult['WlCheckResult']['Message'];
        $this->ajaxReturn($result , "JSON");
    }

    public function dealerCheck(){
        $Model = new Model(); // 实例化一个model对象 没有对应任何数据表
        $result = $Model->query("
            SELECT
            sz12365_fw_company_{$_POST['ecid']}_authorize.id,
            sz12365_fw_company_{$_POST['ecid']}_authorize.brandId,
            sz12365_fw_company_{$_POST['ecid']}_authorize.productId,
            sz12365_fw_company_{$_POST['ecid']}_authorize.dealerId,
            sz12365_fw_company_{$_POST['ecid']}_authorize.startTime,
            sz12365_fw_company_{$_POST['ecid']}_authorize.endTime,
            sz12365_fw_company_{$_POST['ecid']}_authorize.fwCode,
            sz12365_fw_company_dealers.`name`,
            sz12365_fw_company_dealers_electricity.url
            FROM
            sz12365_fw_company_{$_POST['ecid']}_authorize ,
            sz12365_fw_company_dealers,
            sz12365_fw_company_dealers_electricity
            WHERE
            fwCode = '{$_POST['code']}' AND
            sz12365_fw_company_{$_POST['ecid']}_authorize.dealerId = sz12365_fw_company_dealers.id AND
            sz12365_fw_company_200019_authorize.dealerId = sz12365_fw_company_dealers_electricity.dealerId
            ");

        $this->ajaxReturn($result , "JSON");
    }

    public function yoobao(){
        $this->display();
    }

    public function angel(){
        $this->display();
    }

    public function FwCodeCheck(){
        $fwcode = I('post.fwcode');
        $ecid = I('post.ecid');
        $api = $this->getCompanyApi($ecid);

        $opt = array(
            'ecid' => $ecid ,
            'api_user' => $api['mxt_api'] ,
            'api_psw' => $api['mxt_psw'] ,
            'fwCode' => $fwcode ,
            'type' => 'web' ,
            'isJewelryCheck' => false
            );

        //获取防伪提示
        $mxt = new MxtClient( $opt );
        $result = $mxt->FwCheck( $fwcode );
        $this->ajaxReturn($result);
    }

    private function getCompanyApi($ecid){
        $m = M('Company_info');
        $opt['company_ecid'] = $ecid;
        return $m->where($opt)->find();
    }
}
?>