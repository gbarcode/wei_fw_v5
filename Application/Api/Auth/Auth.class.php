<?php
namespace Api\Auth;
use Org\Error\Error;
class Auth{
	private $ecid;
	private $grant_type;
	private $appid;
	private $appsecret;

	public function __construct($opt){
		$this->ecid       = $opt['ecid'];
		$this->grant_type = $opt['grant_type'];
		$this->appid      = $opt['appid'];
		$this->appsecret  = $opt['appsecret'];
	}

	public function token(){
		if($this->checkUser()){
			$time= time();//返回当前时间
			$token = sha1($this->ecid . $this->appid. $this->appsecret.$time);//计算字符串的 SHA-1 散列

			$req = array(
				'access_token' => $token,
				'expires_in'   => 7200 
				);

			S('api_token_'.$this->ecid,$token,7200);//缓存

			return $token;
		}
		else{
			$req = array(
				'errcode' => Error::ERROR_API_DISTRUST_USER,
				'errmsg' => Error::getErrMsg(Error::ERROR_API_DISTRUST_USER)
				);

			return $req;
		}
	}

	public static function checkToken($token, $ecid){
		if($token == S("api_token_{$ecid}"))
			return true;
		else
			return false;
	}

	private function checkUser(){
		$m = M('Company_info');
		
		$opt['company_ecid'] = $this->ecid;
		$opt['api_Id']       = $this->appid;
		$opt['api_Secret']   = $this->appsecret;
		
		$result = $m->where($opt)->find();

		return $result;
	}
}
?>