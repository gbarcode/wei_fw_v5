<?php
namespace Api\Controller;
use Think\Controller;
use Org\Mxt\MxtClient;
class ServiceController extends Controller {
    public function index(){
    	$ecid = $_POST["ecid"];
        $method = $_GET['method'];
    	if($this->checkToken($ecid , $_POST["token"])){
            if($method == 'fwcheck'){
                $jsonArr = $this->getFwResual($ecid, $_POST['fwcode']);
            }
            if($method == 'sign'){
                $jsonArr = array(
                "status" => 0,
                "token" => $this->sign($_GET['ecid'], $_GET['name'], $_GET['password'])
                );
            }
    	}
    	else{
            $jsonArr = array(
                            "status" => 2,
                            "message" => "认证失败！"
                            );
    	}
    	$this->ajaxReturn($jsonArr);
    }

    public function getScanFwcode(){
        $key = htmlspecialchars_decode(I('get.url'));

        Vendor( 'NuSoap.nusoap' );
        $webSoap = new \nusoap_client( "http://203.91.45.207/key.asmx?wsdl", true, false, false, false, false, 0, 30 );
        $webSoap->soap_defencoding = 'UTF-8';
        $webSoap->decode_utf8 = false;
        $webSoap->xml_encoding = 'utf-8';
        $Parms = array( "key" => $key );

        $data = $webSoap->call( 'getFwData', $Parms );
        $opt['company_ecid'] = $data['getFwDataResult']['ecid'];
        $companyInfo = M('Company_info')->where($opt)->find();
        $options = array(
            'ecid'     => $companyInfo['company_ecid'],
            'api_user' => $companyInfo['mxt_api'],
            'api_psw'  => $companyInfo['mxt_psw'],
            'OpenId'   => $wechatMsg['FromUserName'],
            'fwCode'   => $data['getFwDataResult']['fwCode'],
            'type'     => 'web',
            'isJewelryCheck' => false);

        $mxt = new MxtClient( $options );
        $result = $mxt->FwCheck( $data['getFwDataResult']['fwCode'] );
        $this->ajaxReturn($result);
    }

    public function test(){
        $ecid = $_GET["ecid"];
        $method = $_GET['method'];
        if($this->checkToken($ecid , $_GET["token"])){
            if($method == 'fwcheck'){
                $jsonArr = $this->getFwResual($ecid, $_GET['fwcode']);
            }
        }
        else{
            $jsonArr = array(
                            "status" => 2,
                            "message" => "认证失败！"
                            );
        }
        $this->ajaxReturn($jsonArr);
    }

    public function token(){
        $jsonArr = array(
                "status" => 0,
                "token" => $this->sign($_GET['ecid'], $_GET['name'], $_GET['password'])
                );
        $this->ajaxReturn($jsonArr);
    }
    private function getComName($ecid){
    	$companyName = M("Company_info")->where("company_ecid = " . $ecid)->find();
    	return $companyName["Name"];
    }
    
    private function checkToken($ecid , $token){
    	
    	$companyResult = M("Company_info")->where("company_ecid = " . $ecid)->find();
    	if($token == $this->sign($ecid, $companyResult["mxt_api"], $companyResult["mxt_psw"])){
            return true;
    	}
    	else{
            return false;
    	}
    }
    
    private function sign($ecid, $name, $password){
    	return md5($ecid . "\0\0\0\0\0\0\0\0\0\0" . $name . $password);
    }
    
    private function getFwResual($ecid, $fwCode){
    	$companyInfo = $this->getCompanyInfo($ecid);
    	$options = array(
            'ecid'     => $ecid,
            'api_user' => $companyInfo['mxt_api'],
            'api_psw'  => $companyInfo['mxt_psw'],
            'fwCode'   => $fwCode,
            'type'     => 'web',
            'isJewelryCheck' => false);

        $mxt = new MxtClient( $options );
        $result = $mxt->FwCheck( $fwCode );

        $fwParams = $mxt->getFwParams();
		
		$img = "";
		if(substr($companyInfo['company_logo'], 0 , 4) != "http"){
			$img = "http://v5.msa12365.com".$companyInfo['company_logo'];
		}

        $jsonArr = array(
                        "status" => 0 , 
                        "title" => "市场监管防伪验证",
                        "content" => $result["replyContent"],
                        "img" => $img,
                        "link" => "http://www.sz12365.net/mb/?from=weixin&fw=".$fwCode
                        );
        
        return $jsonArr;
    }

    private function getCompanyInfo($ecid){
    	$result = M('Company_info')->where('company_ecid = '.$ecid)->find();
    	return $result;
    }
    
    public function handle(){
        $this->ajaxReturn($_GET['c']);
    }
}
?>