<?php
namespace Api\Controller;
use Think\Controller;
use Org\Error\Error;
use Org\Weixin\Wechat;
use Api\Redirect\NewsRedirect;
use Api\Redirect\QuestionRedirect;
class RedirectController extends Controller {
    public function index(){
    	$params = I('get.');

    	if(!$params['ecid'])
    		return;

    	$companyInfo = $this->getCompanyInfo($params['ecid']);

	    $options = array(
			'token'=>'tokenaccesskey', //填写你设定的key
			'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
			'appsecret'=>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
		);

		$weixin = new Wechat($options);

		$userInfo = $weixin->getOauthAccessToken();

        switch ($params['type']) {
            case 'shop':
                $timestamp = time();
                $token = $this->getShopToken($timestamp, $userInfo['openid']);
                $getParams['token'] = $token;
                $getParams['t'] = $timestamp;
                $getParams['wxid'] = $userInfo['openid'];
                $getParams['url'] = $params['url']; 

                $url = $this->shopRedirectUrl($userInfo['openid'], $params['ecid'], $companyInfo['mall_domain'], http_build_query($getParams));

                header("Location: {$url}");
                break;
            case 'news':
                $params['openId'] = $userInfo['openid'];
                $newsRedirect = new NewsRedirect($params);
                $url = $newsRedirect->getRedirectUrl();
                header("Location: {$url}");
                break;
            case 'question':
                $params['openId'] = $userInfo['openid'];
                $questionRedirect = new QuestionRedirect($params);
                $url = $questionRedirect->getQuestionUrl();

                header("Location: {$url}");
                break;
            default:
                # code...
                break;
        }
    }

    private function getCompanyInfo($ecid){
    	$m = M('Company_info');

    	return $m->find($ecid);
    }

    /*
     * 函数：getShopToken
     * 作用：生成商城token
     * @Param:  
     */
	private function getShopToken($timestamp, $openId){
		return sha1('sz12365'.'\0\0\0\0\0\0'.$openId.$timestamp);
	}

    private function shopRedirectUrl($openId, $ecid, $mall_domain, $params){
    	$m = M("Company_{$ecid}_user_info");
    	$opt['openId'] = $openId;
    	$result = $m->where($opt)->find();

    	if($result){
    		$ecuid = $result['ecuid'];
    		$url = "{$mall_domain}/mobile/redirect.php?{$params}&ecuid={$ecuid}";
    		return $url;
    	}

    	return false;
    }
}