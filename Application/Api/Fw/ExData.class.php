<?php
namespace Api\Fw;
use Org\Error\Error;
class ExData{
	private $ecid;
	private $fwCode;
	private $table;
	private $dbPdo = 'mysql://gbarcode:tytm2011@gbarcode.mysql.rds.aliyuncs.com/client_data';

	public function __construct($ecid, $fwCode){
		$this->ecid = $ecid;
		$this->fwCode = $fwCode;

		if($ecid == '200113')
			$this->table = 'exdata';
		else	
			$this->table = $ecid . '_exdata';
	}

	public function saveExData($exData){
		$m = M($this->table, 'sz12365_fw_', $this->dbPdo);

		$data = $this->isDataExist();

		$exData['exdata'] = htmlspecialchars_decode(urldecode($exData['exdata']));
		if($data){
			$m->where("id = ".$data)->save($exData);
			return $data;
		}else{
			$result = $m->add($exData);

			return $result;
		}

		return false;

	}

	public function insertCustomer($arr){
		$m = M('customer_dealer' , 'sz12365_fw_' , $this->dbPdo);
		if($m->add($arr)){
			return true;
		}else{
			return false;
		}
	}

	private function isDataExist(){
		$m = M($this->table, 'sz12365_fw_', $this->dbPdo);

		$result = $m->where("fwcode = '{$this->fwCode}'")->getField('id');


		return $result;
	}

	public function getNum($date){
		$opt['date']=$date; 
		$m = M('Statistics', 'sz12365_fw_', $this->dbPdo);
		$res=$m->where($opt)->find();
		return $res['num'];
	}

	public function getquery($arr){
    	if(strlen($arr) == 20){
    		$opt['fwCode'] = $arr;
    	}
    	if(substr($arr,0,1)=='S'||substr($arr,0,1)=='s'){
    		$opt['testNo']=$arr;
    	}
    	$m = M($this->table, 'sz12365_fw_', $this->dbPdo);
    	$res = $m->where($opt)->find();
    	if($res){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function getJewelryResult(){
    	$m = M($this->table, 'sz12365_fw_', $this->dbPdo);
    	$opt['fwcode'] = $this->fwCode;
    	return $m->where($opt)->find();
    }
}
?>