<?php
namespace Fw\Controller;
use Think\Controller;
use Org\Mxt\FwClient;
use Org\Mxt\WechatFwClient;
class IndexController extends Controller {
    public function index(){
    	//不是合法的链接不可以显示
    	if(!$this->checkSignature(I('get.ecid'), I('get.token')))
    		return;

    	//获取微信用户信息
    	if(I('get.openid'))
    		$openid = I('get.openid');
    	else
    		$openid = \Fw\Gereral\User::getOpenId(I('get.ecid'));

    	if($openid)
    		$this->assign('openid', $openid);

    	$this->assign('ecid', I('get.ecid'));
        $this->display();
    }

    public function result(){
    	$Reply = M("Company_fw_reply"); // 实例化Reply对象
		 // 手动进行令牌验证
		 if (!$Reply->autoCheckToken($_POST)){
		 // 令牌验证错误
		 	return;
		 }

		 $fwResult = $this->fwCheck(I('post.fwCode'), I('post.ecid'), I('post.openid'));

		 $this->assign('result', $fwResult);
		 $this->display();
    }

    /**
     * 验证网址调用的合法性
     * @param  [int] $ecid  [厂商ID]
     * @param  [string] $token [token]
     * @return [boolean]        [是否合法]
     * @author 李少年 <lisn@gbarcode.com>
     */
    private function checkSignature($ecid, $token){
    	$rightToken = sha1($ecid.'sz12365');

    	if($rightToken == $token)
    		return true;

    	return false;
    }

    /**
     * 防伪验证
     *
     * @return Array
     */
    private function fwCheck($fwCode, $ecid, $openid) {
    	$companyInfo = \Fw\Gereral\User::getCompanyInfo($ecid);

        $options = array(
            'ecid'     => $ecid,
            'api_user' => $companyInfo['mxt_api'],
            'api_psw'  => $companyInfo['mxt_psw']);

        $mxt = new WechatFwClient( $options );
        $result = $mxt->fwCheck( $fwCode, $openid );

        return $result;
    }
}