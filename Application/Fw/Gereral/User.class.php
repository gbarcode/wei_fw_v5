<?php
namespace Fw\Gereral;
use Org\Weixin\Wechat;
class User {
    public static function getOpenId($ecid){
        if($userInfo = session('userInfo')){
            return $userInfo;
        }

        $wechat_config_admin = C('WECHAT_ADMIN_API');

        $options = array(
            'token'=>'tokenaccesskey', //填写你设定的key
            'appid'=>$wechat_config_admin['APPID'], //填写高级调用功能的app id
            'appsecret'=>$wechat_config_admin['APPSECRET'], //填写高级调用功能的密钥
        );

        $weixin = new Wechat($options);

        if($userInfo = $weixin->getOauthAccessToken()){
            return $userInfo['openid'];
        }

        if(I('get.redirct') == 1)
            return false;

        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'] . '&redirct=1';

        $url = urlencode($url);

        $redirctUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$wechat_config_admin['APPID']}&redirect_uri={$url}&response_type=code&scope=snsapi_base&state=0#wechat_redirect";

        header("Location: {$redirctUrl}");
    }

    public static function getUserInfo($openid, $ecid){
        $m = M('Company_{$ecid}_user_info');

        $opt['openId'] = $openid;

        return $m->where($opt)->find();
    }

    public static function getCompanyInfo($ecid){
        $m = M('Company_info');

        return $m->find($ecid);
    }
}
?>