<?php
namespace Admin\BaiduMap;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class BaiduMap{
    /**
     *许可密钥
     * @var string 
     */
    const BAIDUMAP_AK = '41193a426878963512af9f91de8b414f';
    
    /**
     *数据库ID
     * @var int 
     */
    const BAIDUMAP_DATABOX = 32406;    

    private $ecid;                             
    
    /**
     * 
     * 
     */
    public function __construct($ecid) {
        $this->ecid = $ecid;
    }
    
    /**
     * getAllLocation函数用于通过百度地图API获取所有POI信息，请注意不会返回EXT信息
     * @return 由百度地图API响应的类
     * @author 李少年
     * @version 2013-07-07
     */
    public function getAllLocation(){
        $m = M("Company_location_set");
        $data['ecid'] = $this->ecid;
        $result = $m->where($data)->select();

        return $result;
    }
    
    /**
     * getPoiInfo函数用于通过百度地图API获取指定POI的所有信息
     * @param int $PoiID 百度地图POI的ID编号
     * @return 由百度地图API响应的类
     * @author 李少年
     * @version 2013-07-07
     */
    public function getPoiInfo($PoiID){
        $m = M("Company_location_set");
        $data['poiID'] = $PoiID;
        $result = $m->where($data)->find();

        return $result;
    }
    
    /**
     * addPoi函数用于通过百度地图API添加POI记录，请注意不会添加POI的EXT信息。
     * @param array $arrPoiInfo POI信息数组，可用到的参数
     *              title   名称    必须
     *              address   地址
     *              tags      tags 
     *              zip_code  邮政编码
     *              latitude  纬度
     *              longitude  经度
     *              coord_type  坐标的类型  2：国测局加密；3：百度加密两个值；1：未加密的GPS坐标。
     *              ecid      厂商编号
     * @param array $arrDbInfo  用于保存数据库信息  
     *              info  地点介绍
     *              binImg   大图地址
     *              smallImg  小图地址
     * @return 由百度地图API响应的类
     * @author 李少年
     * @version 2013-07-07
     */
    public function addPoi($arrPoiInfo, $arrDbInfo){
        $url = "http://api.map.baidu.com/geodata/v3/poi/create";
        
        $arrBase = array("ak" => BaiduMap::BAIDUMAP_AK,
                         "coord_type" => 3,
                         "geotable_id" => BaiduMap::BAIDUMAP_DATABOX);
        
        Vendor('Unirest.Unirest');
        $response = \Unirest::post($url, array( "Accept" => "application/json" ),array_merge($arrBase, $arrPoiInfo));

        //如果返回的状态不为0，说明百度地图数据库创建地点不成功
        if($response->body->status != 0)
            return -1;

        //获取百度地图POI地点的ID
        $poiID = $response->body->id;
        
        //向数据库添加地点记录
        if($poiID != null){
            $data['longitude'] = $arrPoiInfo['longitude'];
            $data['latitude'] = $arrPoiInfo['latitude'];
            $data['address'] = $arrPoiInfo['address'];
            $arrDbInfo['poiId'] = $poiID;

            return $this->controlLocaitionTable(array_merge($data, $arrDbInfo), 'modify');
        }

        return -1;
    }


    /**
    * 操作数据库中的位置数据表
    * @param $arrParams 参数数组
    * @param $type 操作类型：  add    modify    del
    */
    private function controlLocaitionTable($arrParams, $type){
        $m = M('Company_dealers');

        if($type == 'modify')
            if($m->save($arrParams)){
                return 0;
            }
        if($type == 'del'){
            $result = $m->delete($arrParams);
            $info = explode( ",", $arrParams["poiId"] );
            for($i = 0;$i<count($info)-1;$i++){
                $result = $m->where("poiId = '".$info[$i]."'")->delete();
            }
        }

        return $result;
    }

    /**
     * modifyPoi函数用于通过百度地图API修改POI记录，请注意不会修改POI的EXT信息。
     * @param int $poi_id POI编号
     * @param array $arrPoiInfo POI信息数组，可用到的参数可在http://developer.baidu.com/map/lbs-geodata.htm#.poi.manage2.3查询
     * @return 由百度地图API响应的类
     * @author 李少年
     * @version 2013-07-07
     */
    public function modifyPoi($poi_id, $arrPoiInfo, $arrDbInfo){

        //如果POI参数不为空，修改百度地图的数据
        if($arrPoiInfo != ''){
            $url = "http://api.map.baidu.com/geodata/v3/poi/update";
            $arrBase = array(
                "ak"          => BaiduMap::BAIDUMAP_AK,
                "id"          => $poi_id,
                "geotable_id" => self::BAIDUMAP_DATABOX,
                "coord_type"  => 3
                    );
            Vendor('Unirest.Unirest');      
            $response = \Unirest::post($url, array( "Accept" => "application/json" ),array_merge($arrBase, $arrPoiInfo));
        }

        //如果数据表参数不为空，修改数据库记录
        if($arrDbInfo != ''){
            $arrDbInfo['poiId'] = $poi_id;

            $this->controlLocaitionTable($arrDbInfo, 'modify');
        }

        return $response->body->status;
    }

    private function std_class_object_to_array($stdclassobject)
    {
        $_array = is_object($stdclassobject) ? get_object_vars($stdclassobject) : $stdclassobject;

        foreach ($_array as $key => $value) {
            $value = (is_array($value) || is_object($value)) ? std_class_object_to_array($value) : $value;
            $array[$key] = $value;
        }

        return $array;
    }
    
    /**
     * delPoi函数用于通过百度地图API删除指定的POI。
     * @param string $poi_ids POI编号
     * @return 由百度地图API响应的类
     * @author 李少年
     * @version 2013-07-07
     */
    public function delPoi($poi_id){
        $url = "http://api.map.baidu.com/geodata/v3/poi/delete";
        $arrBase = array("ak" => self::BAIDUMAP_AK,
                         "ids" => $poi_id,
                         "geotable_id" => self::BAIDUMAP_DATABOX);
        Vendor('Unirest.Unirest');      
        $response = \Unirest::post($url, array( "Accept" => "application/json" ),$arrBase);

        if($response->body->status == 0){
            $params['poiID'] = $poi_id;

            return 0;
        }
        return -1;
    }
}
?>
