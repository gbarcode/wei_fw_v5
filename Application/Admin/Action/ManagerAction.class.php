<?php
namespace Admin\Action;
use Org\Error\Error;
class ManagerAction extends AdminAction{
    public function index(){
      if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function sn(){
      $Data = M('Company_device');

      if($_POST['ecid'] != ""){
        $opt['company_ecid'] = $_POST['ecid'];
      }

      $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
      $page       = new \Think\Page( $count , 30 );// 实例化分页类 传入总记录数
      $show       = $page->show();// 分页显示输出

      // 进行分页数据查询
      $sn = $Data->where($opt)->order( 'company_ecid desc' )->limit( $page->firstRow.','.$page->listRows )->select();

      for($i = 0;$i < count($sn);$i++){
        $company = M("Company_info")->where("company_ecid = '".$sn[$i]['company_ecid']."'")->find();
        $sn[$i]['company_name'] = $company['company_name'];
      }

      $this->assign( 'page', $show );// 赋值分页输出
      $this->assign("sn" , $sn);
      if(session('theme')){
          $this->theme('nifty')->display();
      }else{
          $this->display();
      }
    }

    public function snInfo(){
      if($_POST['id'] == "add"){
        $result = array(
          "company_ecid" => "" ,
          "sn" => "" ,
          "buyTime" => "" ,
          "handleType" => "add"
          );
      }else{
        $result = M("Company_device")->where("id = '".$_POST['id']."'")->find();
        $result['handleType'] = "edit";
      }

      $this->assign("sn" , $result);
      $this->setToken();
      $this->display();
    }

    public function item(){
      if($_GET['id'] == "add"){
        $result = array(
          "company_ecid" => "" ,
          "company_name" => "" ,
          "company_web" => "" ,
          "company_logo" => "" ,
          "weixin_AppId" => "" ,
          "weixin_AppSecret" => "" ,
          "handleType" => "add"
          );
      }else{
        $result = M("Company_info")->where("company_ecid = '".$_GET['id']."'")->find();
        $result['handleType'] = "edit";
      }

      $this->assign("user" , $result);
      $this->setToken();
      if(session('theme')){
          $this->theme('nifty')->display();
      }else{
          $this->display();
      }
    }

    public function service(){
      $result = M("Company_services_type")->select();
      
      $num = 0;
      for($i = 0;$i < count($result);$i++){
        if(!M("Company_services")->where("ecid = '".$_POST['ecid']."' AND serviceId = '".$result[$i]['id']."'")->find()){
          $service[$num] = array("id"=>$result[$i]['id'],"name"=>$result[$i]['name'],"info"=>$result[$i]['info']);
          $num++;
        }
      }

      if(M("Company_services")->where("ecid = '".$_POST['ecid']."'")->find()){
        $this->assign("isServiceExist" , 1);
      }else{
        $this->assign("isServiceExist" , 0);
      }

      $this->assign("service" , $service);
      $this->assign("ecid" , $_POST['ecid']);
      $this->display();
    }

    public function serviceInfo(){
      $this->assign("url" , "http://www.msa12365.com/Weixin/Vip/sz12365/".$_POST['ecid']);
      $this->assign("token" , md5($_POST['ecid']."sz12365"));
      $this->display();
    }

    public function update(){
      $opt['ecid'] = I('post.ecid');
      $data = M("Company_services")->where($opt)->select();
      for($i = 0;$i < count($data);$i++){
        $row = M("Company_services_type")->where("id = '".$data[$i]['serviceId']."'")->find();
        $result[$i] = array("id"=>$row['id'] , "name"=>$row['name']);
      }
      $this->assign("ecid" , $_POST['ecid']);
      $this->assign("service" , $result);
      $this->display();
    }

    public function serviceList(){
      //获取服务类型
      $serviceType = M("Company_services_type")->select();
      $this->assign("type" , $serviceType);

      $type = 'all';
      if($_POST['select'] != ""){
        $type = $_POST['select'];
      }
      $this->assign("typeSelect" , $type);

      if($_POST['ecid'] != ""){
        //获取指定ID企业
        $this->getResultFromEcid($_POST['ecid']);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
        return;
      }

      if($_POST['select'] == 'all'){
        //获取所有企业
        $this->getAllResult();
      }else if($_POST['select'] != ''){
        //获取指定服务企业
        $this->getServiceResult($_POST['select']);
      }
      
      $this->assign("serviceType" , $_POST['select']);
      if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    /**通过服务ID获取服务时间
     * 范小宝
     * 2014.4.2
     */
    public function getServiceTime(){
      if(!IS_POST) _404 ('页面不存在' , U('index'));

      

      $m = M("Company_services");
      $opt['serviceId'] = $_POST['id'];
      $opt['ecid'] = $_POST['ecid'];
      $result = $m->where($opt)->find();

      $data['startTime'] = $result['startTime'];
      $data['endTime'] = $result['endTime'];
      $this->ajaxReturn($data , "JSON");
    }

    public function snHandle(){
      if(!IS_POST) _404 ('页面不存在' , U('index'));

      

      $m = M("Company_device");
      $opt = $_POST;
      $opt['modifyUserId'] = session($this->_userCfg['UID']);
      $opt['modifyTime'] = date("Y-m-d H:i:s");

      if($_POST['handleType'] == 'add'){
        if($m->add($opt)){
          $result["status"] = Error::SUCCESS_OK;
        }else{
          $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
          $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }
      }
      if($_POST['handleType'] == 'edit'){
        if($m->save($opt)){
          $result["status"] = Error::SUCCESS_OK;
        }else{
          $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
          $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
      }

      $this->ajaxReturn($result , "JSON");
    }

    private function getResultFromEcid($ecid){
      $result[0] = M("Company_info")->where("company_ecid = '".$ecid."'")->find();

      //判断是否开通服务
      if($row = M("Company_services")->where("ecid = '".$result[0]['company_ecid']."'")->select()){
        for($j = 0;$j < count($row) ;$j++){
          $service = M("Company_services_type")->where("id = '".$row[$j]['serviceId']."'")->find();

          if($j == count($row) - 1){
            $result[0]['service'] .= $service['name'];
          }else{
            $result[0]['service'] .= $service['name']."、";
          }

          $date = strtotime(date("Y-m-d"));
          $endTime = strtotime($row[$j]['endTime']);

          if(ceil(($endTime-$date)/86400) < 30){
            $timeStr = "<span style='margin-left:10px;'><a onclick=\"showService(".$row[$j]['ecid'].")\" href='javascript:void(0)'>续期</a></span>";
          }
        }
        
        $result[0]['service'] .= "<span style='margin-left:10px;'><a onclick=\"openService(".$result[0]['company_ecid'].")\" href='javascript:void(0)'>开通服务</a></span>";
        $result[0]['service'] .= $timeStr;
        
        $result[0]['isHasService'] = true;
      }else{
        $result[0]['service'] = "<span><a onclick=\"openService(".$result[0]['company_ecid'].")\" href='javascript:void(0)'>开通服务</a></span>";
        $result[0]['isHasService'] = false;
      }

      $this->assign( 'cId', $_POST['cId'] );
      $this->assign("company" , $result);
    }

    private function getAllResult(){
      $Data = M('Company_info');

      $count      = $Data->count();// 查询满足要求的总记录数 $map表示查询条件
      $page       = new \Think\Page( $count , 30 );// 实例化分页类 传入总记录数
      $show       = $page->show();// 分页显示输出

      // 进行分页数据查询
      $result = $Data->order( 'company_ecid desc' )->limit( $page->firstRow.','.$page->listRows )->select();
      for($i = 0;$i < count($result);$i++){
        //判断是否开通服务
        if($row = M("Company_services")->where("ecid = '".$result[$i]['company_ecid']."'")->select()){
          for($j = 0;$j < count($row) ;$j++){
            $service = M("Company_services_type")->where("id = '".$row[$j]['serviceId']."'")->find();

            if($j == count($row) - 1){
              $result[$i]['service'] .= $service['name'];
            }else{
              $result[$i]['service'] .= $service['name']."、";
            }

            $date = strtotime(date("Y-m-d"));
            $endTime = strtotime($row[$j]['endTime']);

            if(ceil(($endTime-$date)/86400) < 30){
              $timeStr = "<span style='margin-left:10px;'><a onclick=\"showService(".$row[$j]['ecid'].")\" href='javascript:void(0)'>续期</a></span>";
            }
          }
          
          $result[$i]['service'] .= "<span style='margin-left:10px;'><a onclick=\"openService(".$result[$i]['company_ecid'].")\" href='javascript:void(0)'>开通服务</a></span>";          
          $result[$i]['service'] .= $timeStr;
          $timeStr = "";
          $result[$i]['isHasService'] = true;
        }else{
          $result[$i]['service'] = "<span><a onclick=\"openService(".$result[$i]['company_ecid'].")\" href='javascript:void(0)'>开通服务</a></span>";
          $result[$i]['isHasService'] = false;
        }
      }

      $this->assign( 'cId', $_POST['cId'] );
      $this->assign( 'page', $show );// 赋值分页输出
      $this->assign("company" , $result);
    }

    private function getServiceResult($type){
      // 导入分页类

      $service = M("Company_services")->where("serviceId = '".$type."'")->select();

      $count      = count($service);// 查询满足要求的总记录数 $map表示查询条件
      $page       = new \Think\Page( $count , 30 );// 实例化分页类 传入总记录数
      $show       = $page->show();// 分页显示输出

      for($i = 0;$i < count($service);$i++){
        $result[$i] = M("Company_info")->where("company_ecid = '".$service[$i]['ecid']."'")->find();

        $row = M("Company_services_type")->where("id = '".$service[$i]['serviceId']."'")->find();

        $result[$i]['service'] = $row['name'];
        $date = strtotime(date("Y-m-d"));
        $endTime = strtotime($service[$i]['endTime']);

        if(ceil(($endTime-$date)/86400) < 30){
          $result[$i]['service'] .= "<span style='margin-left:10px;'><a onclick=\"showService(".$service[$i]['ecid'].")\" href='javascript:void(0)'>续期</a></span>";
        }
        $result[$i]['isHasService'] = true;
      }

      $this->assign( 'cId', $_POST['cId'] );
      $this->assign( 'page', $show );// 赋值分页输出
      $this->assign("company" , $result);
    }

    public function serviceHandle(){
      if(!IS_POST) _404 ('页面不存在' , U('index'));
      
      //清除企业缓存
      $ecid_cache = 'company_info'.session($this->_userCfg['ECID']);
      if(S($ecid_cache))
        S($ecid_cache, NULL);

      if(I('post.isServiceExist') == 0){
        //更新厂商信息
        $company['company_ecid'] = I('post.ecid');
        $company['weixin_access'] = I('post.weixin_access');
        $company['mxt_api'] = I('post.mxt_api');
        $company['mxt_psw'] = I('post.mxt_psw');
        M("Company_info")->save($company);

        //添加账号信息
        $user['user_name'] = I('post.user_name');
        $user['user_password'] = C( 'DEFAULT_PWD' );
        $user['company_ecid'] = I('post.ecid');
        $user['roleId'] = "admin";
        M("User_info")->add($user);
      }

      if(I('post.serviceId') == "3"){
        if(!M("Company_services")->where("ecid = '".I('post.ecid')."' AND serviceId = 3")->find()){
          $this->createWLTable(I('post.ecid')); 
        }
      }else{
        if(!M("Company_services")->where("ecid = '".I('post.ecid')."' AND serviceId != 3")->find()){
          //添加数据表
          $this->createTable(I('post.ecid'));
        }
      }

      //添加服务信息
      $m = M("Company_services");
      
      $opt['serviceId'] = I('post.serviceId');
      $opt['ecid'] = I('post.ecid');
      $opt['startTime'] = I('post.startTime');
      $opt['endTime'] = I('post.endTime');
      $opt['modifyTime'] = date("Y-m-d H:i:s");
      $opt['modifyUserId'] = session($this->_userCfg['UID']);

      if($m->add($opt)){
        $result["status"] = Error::SUCCESS_OK;
      }else{
        $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
      }

      $this->ajaxReturn($result , "JSON");
    }

    public function companyHandle(){
      if(!IS_POST) _404 ('页面不存在' , U('index'));

      $m = M("Company_info");
      $opt['company_ecid'] = I('post.company_ecid');
      $opt['company_name'] = I('post.company_name');
      $opt['company_web'] = I('post.company_web');
      $opt['company_logo'] = I('post.company_logo');
      $opt['weixin_AppId'] = I('post.weixin_AppId');
      $opt['weixin_AppSecret'] = I('post.weixin_AppSecret');
      $opt['modifyUserId'] = session($this->_userCfg['UID']);
      $opt['mxt_psw'] = I('post.mxt_psw');
      $opt['mxt_api'] = I('post.mxt_api');
      $opt['modifyTime'] = date("Y-m-d H:i:s");

      if(I('post.handleType') == "add"){
        //设置默认回复
        $this->setFwResponse($opt);
        //设置默认关注回复
        $this->setEventResponse($opt['company_ecid'] , $opt['company_name']);

        if($m->add($opt)){
          $result["status"] = Error::SUCCESS_OK;
        }else{
          $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
          $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }
      }else{
        if($m->save($opt)){
          $result["status"] = Error::SUCCESS_OK;

          //清除企业缓存
          $ecid_cache = 'company_info'.session($this->_userCfg['ECID']);
          if(S($ecid_cache))
            S($ecid_cache, NULL);
        }else{
          $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
          $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
      }

      $this->ajaxReturn($result , "JSON");
    }

    public function updateService(){
      if(!IS_POST) _404 ('页面不存在' , U('index'));

      

      $data = M("Company_services")->where("serviceId = '".$_POST['id']."' AND ecid = '".$_POST['ecid']."'")->find();

      $opt['id'] = $data['id'];
      $opt['endTime'] = date('Y-m-d' , strtotime($data['endTime'] . "+".$_POST['year']." year"));
      
      if(M("Company_services")->save($opt)){
        $result["status"] = Error::SUCCESS_OK;

        //清除企业缓存
        $ecid_cache = 'company_info'.session($this->_userCfg['ECID']);
        if(S($ecid_cache))
          S($ecid_cache, NULL);
      }else{
        $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
      }

      $this->ajaxReturn($result , "JSON");
    }
    
    private function setFwResponse($data){
      $img = M("Company_fw_reply")->where("ecid = '816'")->find();

      $data = array(
          "ecid" => $data['company_ecid'] , 
          "name" => $data['company_name'] , 
          "brandId" => -1 ,
          "productId" => -1 ,
          "rightReply" => "深圳市市场监管防伪网提醒您：您所购买的是{-厂商名称-}生产的{-品牌名称-}品牌商品，敬请放心购买使用！！！" , 
          "rightImg" => $img['rightImg'] ,
          "repertReply" => "深圳市市场监管防伪网提醒您：您所验证的防伪码已被查询{-查询次数-}次，如果首次验证并非本人操作，则此防伪码验证不通过，敬请留意！" , 
          "repertImg" => $img['repertImg'] ,
          "errorReply" => "深圳市市场监管防伪网提醒您：您所验证的防伪码不存在，请认真核对后再次验证，谨防假冒！" , 
          "errorImg" => $img['errorImg']
          
      );
      M("Company_fw_reply")->data($data)->add();
    }

    private function setEventResponse($ecid , $company){
      $data = array(
        "ecid" => $ecid ,
        "event" => "subscribe" ,
        "eventType" => "subscribe" ,
        "responseType" => "text" ,
        "responseText" => "您好，感谢您关注".$company."！"
        );

      M("Company_event_response")->data($data)->add();
    }

    private function createWLTable($ecid){
        //新建wl_info表
        M()->query("CREATE TABLE `sz12365_fw_company_".$ecid."_wl_info` (
          `id` int(11) unsigned NOT NULL auto_increment,
          `wlCode` varchar(20),
          `productId` int(11),
          `cqDate` datetime,
          `SellerId` int(11),
          `AreaId` varchar(255),
          `modifyUserId` int(11),
          `modifyTime` datetime,
          PRIMARY KEY (`id`)
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

        //新建库存表
        M()->query("CREATE TABLE `sz12365_fw_company_".$ecid."_stock` (
          `id` int(11) unsigned NOT NULL auto_increment,
          `dealerId` int(11),
          `productId` int(11),
          `wl_Code` varchar(18),
          `time` datetime,
          `mark` tinyint(4) NOT NULL,
          `outTime` datetime,
          PRIMARY KEY (`id`)
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8");
    }
    
    private function createTable($ecid){
        //新建user_info表
        M()->query("CREATE TABLE `sz12365_fw_company_".$ecid."_user_info` (
          `id` int(11) unsigned NOT NULL auto_increment,
          `userName` varchar(100),
          `userPassword` varchar(100),
          `openId` varchar(100),
          `ecuid` int(11),
          `nickname` varchar(100),
          `sex` tinyint(4),
          `language` varchar(100),
          `city` varchar(50),
          `province` varchar(50),
          `country` varchar(50),
          `headimgurl` varchar(255),
          `subscribe_time` datetime,
          `realName` varchar(50),
          `tel` varchar(50),
          `address` varchar(255),
          `zipCode` varchar(10),
          `scanQrScene` varchar(255),
          `longitude` double,
          `latitude` double,
          `userUpdate` datetime,
          `locationUpdate` datetime,
          `mark` tinyint(4) NOT NULL,
          PRIMARY KEY (`id`)
     ) ENGINE=MyISAM DEFAULT CHARSET=utf8");
        
        //新建wx_log表
        M()->query("CREATE TABLE `sz12365_fw_company_".$ecid."_wx_log` (
          `id` int(11) unsigned NOT NULL auto_increment,
          `FromUserName` varchar(100),
          `MsgType` varchar(20),
          `Content` text,
          `Event` varchar(20),
          `EventKey` varchar(20),
          `Longitude` double,
          `Latitude` double,
          `Recognition` varchar(255),
          `time` datetime,
          PRIMARY KEY (`id`)
     ) ENGINE=MyISAM DEFAULT CHARSET=utf8");
    }

    public function fwcode(){
      $m=M('Company_info');
      $name=$m->select();
      $this->assign('name',$name);
      if(session('theme')){
          $this->theme('nifty')->display();
      }else{
          $this->display();
      }
    }

    public function updatefwcode(){
        $ecid=I('post.ecid');
        $txtPath = "./Public/fwCode/".$ecid."/".$ecid.".txt";
        $tmpPath = "./Public/fwCode/".$ecid."/tmp_".$ecid.".txt";
        $uploadify = "./Public/fwCode/".$ecid."/upfile.txt";

        $upfile = fopen($uploadify, "r") or exit("Unable to open file!");
        $file = fopen($txtPath, "r") or exit("Unable to open file!");
        $f2  = fopen($tmpPath, 'w');//建立临时文件
        $line = '';

        while(!feof($file))
        {
          $str = fgets($file);//读取一行
        
          $line .= $str.'\r\n';

          if($str != "")
            fputs($f2,$str);//写入
          
        }

        while(!feof($upfile))
        {
          $strl = fgets($upfile);//读取一行
          $line .= $strl."\r\n";

          if($strl != "")
            fwrite($f2,$strl);//写入
          
        }
        
        fclose($file);
        fclose($f2);
        fclose($upfile);
        rename($tmpPath,$txtPath);
        unlink($uploadify);
        $this->ajaxReturn("成功！");
    }
}
?>
