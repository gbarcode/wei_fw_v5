<?php
namespace Admin\Action;
use Org\Error\Error;
class ShareAction extends AdminAction {
    public function index() {
        $share = M("Company_fw_share")->where("ecid = '".session( $this->_userCfg['ECID'] )."'")->find();
        if($share){
            $this->assign("share" , $share);
        }else{
            $o['ecid']=session($this->_userCfg['ECID']);
            $mod=M("Company_fw_share");
            $row=$mod->add($o);
            $this->assign("share" , $o);
        }

        $this->assign("ecid" ,$this->_userCfg['ECID']);

        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display("new-index");
        }    
    }

  /**
    *微信分享操作
    **/
  public function shareHandle(){
    if(!IS_POST) 
        _404 ('页面不存在' , U('index'));

    $opt['modifyUserId'] = session($this->_userCfg['UID']);
    $opt['modifyTime'] = date("Y-m-d H:i:s");
    $opt['ecid'] = $this->_userCfg['UID'];
    $opt['title']=I('post.title');
    $opt['url']=I('post.url');
    $opt['img']=I('post.img');
    $opt['description']=I('post.description');

    if(M("Company_fw_share")->where("ecid = '".session($this->_userCfg['ECID'])."'")->save($opt)){
        $result["status"] = Error::SUCCESS_OK;
    }else{
        $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
    }

    $this->ajaxReturn($result , "JSON");

}

public function shareAddHandle(){
    if(!IS_POST) 
        _404 ('页面不存在' , U('index'));

    $opt['modifyUserId'] = session($this->_userCfg['UID']);
    $opt['modifyTime'] = date("Y-m-d H:i:s");
    $opt['ecid'] = session($this->_userCfg['ECID']);
    $opt['title']=I('post.title');
    $opt['url']=I('post.url');
    $opt['img']=I('post.img');
    $opt['description']=I('post.description');
    if(M("Company_fw_share")->add($opt)){
        $result["status"] = Error::SUCCESS_OK;
    }else{
        $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
    }

    $this->ajaxReturn($result,"JSON");
}

public function shareItem(){
        // 导入分页类
    $Data = M('Company_fw_share');

        $count      = $Data->where("ecid = '".session($this->_userCfg['ECID'])."'")->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page($count , 5);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result=$Data->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        $this->assign('shareItem' , $result);
        $this->assign('page' , $show);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }  
    }

    public function shareEdit(){
        $m = M('Company_fw_share');
        $opt['id'] = $_GET['id'];
        $result = $m->where($opt)->find();
        
        $this->assign('shareView',$result);
        $res = $m->where($opt)->select();
        
        $this->assign('shareSave',$res);
        $this->assign('rand' , rand());
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }
    public function shareEditHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M('Company_fw_share');
            //获取所有post参数
        $opt = $_POST;
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $status = $m->save($opt);
        $data['status'] = Error::SUCCESS_OK;
        $data['info'] = Error::getErrMsg(Error::SUCCESS_OK);
        
        
        $this->ajaxReturn($data,"JSON");
    }

    public function shareView(){
        $m = M('Company_fw_share');

        $opt['id'] = $_GET['id'];
        $result = $m->where($opt)->find();


        $this->assign('shareItem',$result);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function shareAdd(){
        $this->assign('rand' , rand());
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
        
    }

    public function delShareHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
        $opt['id']=$_POST['id'];

        if(!assert($opt['id'])){
            $result['status'] = Error::ERROR_GENERAL;
            $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
            $this->ajaxReturn($result , 'JSON');
        }

        $m = M('Company_fw_share');

        if($m->where($opt)->delete()){
            $result['status'] = ERROR::SUCCESS_OK;
        }else{
           $result['status'] = Error::ERROR_GENERAL;
           $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
       }
       $this->ajaxReturn($result,"JSON");
   }

   public function setShareHandle(){
    if(!IS_POST) 
        _404 ('页面不存在' , U('index'));
    $opt['id']=$_POST['id'];

    $m = M('Company_fw_share');
    $set0['isDefault'] = 0;
    $oldDefault = $m->where("ecid = '".session($this->_userCfg['ECID'])."'AND isDefault=1")->save($set0);
    $set1['isDefault'] = 1;
    if($m->where($opt)->save($set1)){
        $result['status'] = ERROR::SUCCESS_OK;
    }else{
       $result['status'] = Error::ERROR_GENERAL;
       $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
   }
   $this->ajaxReturn($result,"JSON");

}

public function removeShareHandle(){
    if(!IS_POST) 
        _404 ('页面不存在' , U('index'));

    $m = M('Company_fw_share');
    $set0['isDefault'] = 0;
    $oldDefault = $m->where("ecid = '".session($this->_userCfg['ECID'])."'AND isDefault=1")->save($set0);
    if($oldDefault){
        $result['status'] = ERROR::SUCCESS_OK;
    }else{
       $result['status'] = Error::ERROR_GENERAL;
       $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
   }
   $this->ajaxReturn($result,"JSON");

}

}
?>
