<?php
class FwAjaxAction extends PublicAction {
    public function addMsg(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));
        
        
        $_POST["ecid"] = session("ecid");
        $_POST["type"] = "news";
        
        if(M("Company_fw_reply_review")->data($_POST)->add()){
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function deleteRule() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        if ( M( "Company_fw_reply" )->where( "id = '".$_POST["id"]."'" )->delete() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function exchangeHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $m = M( "Company_".session( "ecid" )."_lucky_exchange" );

        //判断防伪码是否为中奖号码
        if ( !$result = $m->where( "fwCode = '".$_POST["fwcode"]."'" )->find() ) {
            $data = array(
                "status" => Error::ERROR_EXCHANGE_UNLUCK ,
                "info" => Error::getErrMsg(Error::ERROR_EXCHANGE_UNLUCK)
            );
            $this->ajaxReturn( $data, 'JSON' );
        }

        //中奖号码已兑奖
        if ( $result["exchangeUser"] != null ) {
            $data = array(
                "status" => Error::ERROR_EXCHANGE_REPERT ,
                "info" => Error::getErrMsg(Error::ERROR_EXCHANGE_REPERT) ,
                "prize" => $result["prize"]
            );
        }else {
            $data = array(
                "status" => Error::SUCCESS_OK ,
                "info" => $result["prize"] ,
                "id" => $result["id"]
            );
        }

        $this->ajaxReturn( $data, 'JSON' );
    }

    public function exchangeLog() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $data = array(
            "exchangeUser" => session( 'user_name' ) ,
            "exchangeTime" => date( "Y-m-d H:i:s" )
        );
        M( "Company_".session( "ecid" )."_lucky_exchange" )->data( $data )->where( "id = '".$_POST["id"]."'" )->save();

        $data = array(
            "status" => Error::SUCCESS_OK ,
            "info" => "兑奖成功"
        );
        $this->ajaxReturn( $data, 'JSON' );
    }
    
    public function editRule(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));
        
        
        $_POST["replyid"] = $_POST["id"];
        $_POST["handleType"] = "edit";
        unset($_POST["id"]);
        
        $m = M("Company_fw_reply_review");
        if($m->where("replyid = '".$_POST["replyid"]."' AND reviewType = 0")->find()){
            if($m->data($_POST)->where("replyid = '".$_POST["replyid"]."'")->save()){
                $this->ajaxReturn(Error::SUCCESS_OK);
            }
        }else{
            if($m->data($_POST)->add()){
                $this->ajaxReturn(Error::SUCCESS_OK);
            }
        }

        $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        
        $this->ajaxReturn($result , "JSON");
    }
    
    public function editDefaultRule(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));
        
        
        $data = array(
            "replyid" => $_POST["id"] ,
            "ecid" => $_POST["ecId"],
            "name" => "默认",
            "product" => "default",
            "type" => "default",
            "rightContent" => $_POST["defaultRight"],
            "rightImg" => $_POST["defaultRightImg"],
            "repertContent" => $_POST["defaultRepert"],
            "repertImg" => $_POST["defaultRepertImg"],
            "defaultError" => $_POST["defaultError"],
            "defaultErrorImg" => $_POST["defaultErrorImg"],
            "handleType" => "default"
        );
        
        $m = M("Company_fw_reply_review");
        
        if($m->where("replyid = '".$data["replyid"]."' AND reviewType = 0")->find()){
            if($m->data($data)->where("replyid = '".$data["replyid"]."'")->save()){
                $this->ajaxReturn(Error::SUCCESS_OK);
            }
        }else{
            if($m->data($data)->add()){
                $this->ajaxReturn(Error::SUCCESS_OK);
            }
        }
        
        $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        
        $this->ajaxReturn($result , "JSON");
    }
    
    public function FwMsgHandle(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));
        
        
        $Arr = array(
            'replySort' => "news",
            'defaultRight' => $_POST['right'],
            'defaultRepert' => $_POST['repeat'],
            'defaultError' => $_POST['error'],
            'defaultRightImg' => $_POST['right-img'],
            'defaultRepertImg' => $_POST["repeat-img"],
            'defaultErrorImg' => $_POST["error-img"]
        );

        //更新数据
        if(M('company_fw_set')->data($Arr)->where('ecId = '.session('ecid').'')->save())
        {
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function editLucky() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        if ( M( "Company_".session( "ecid" )."_lucky" )->data( $_POST )->where( "id = '".$_GET["id"]."'" )->save() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function deleteRiviewRule(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));
        
        import("ORG.Error.Error");
        
        if(M("Company_fw_reply_review")->where("id = '".$_POST["id"]."'")->delete()){
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }
}
?>
