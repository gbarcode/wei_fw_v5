<?php
namespace Admin\Action;
use Org\Error\Error;
class AjaxAction extends AdminAction {
	public function textInput(){
		$this->assign('rand', rand(0,10000));
		$this->assign('param', $_POST['p']);
		$this->assign('defaultValue', $_POST['v']);

        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }   
	}

	public function newsChange(){
		$defaultID = $_POST['id'];

		if($defaultID != ''){
			$m = M('Company_material_group');

	        $opt['id'] = $defaultID;
	        $result = $m->where($opt)->find();

	        $material = explode( "," , $result['materialId'] );

	        $items = "";

	        for($i=0; $i<count($material); $i++){
	            $m = M('Company_news');

	            $opt['id'] = $material[$i];

	            $items[$i] = $m->where($opt)->find();
	        }

	        if(count($items) > 1)
                $multiple = 1;
            else
                $multiple = 0;

	        $this->assign('material',$items);
		}

		$this->assign('rand', rand(0,10000));
		$this->assign('param', I('post.p'));
		$this->assign('dafaultID', I('post.id'));
		$this->assign('classID', I('post.cId'));
		$this->assign('multiple', $multiple);
        $this->assign('ecid', I('post.ecid'));
        $this->assign('responseId', I('post.responseId'));
		
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }  
	}

	public function showAllMaterial(){
		//分页
        // 导入分页类
        $Data = M( "Company_material_group" );

        $opt['ecid'] = I('post.ecid');

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 9 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $groupResult = $Data->where($opt)->order( 'modifyTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $newsArr = null;     //记录数据库数据
        for ( $i = 0;$i<count( $groupResult );$i++ ) {
            $newsArr[$i]["groupId"] = $groupResult[$i]['id'];
            $newsArr[$i]['addTime'] = $groupResult[$i]['modifyTime'];
            $newsId = explode( ",", $groupResult[$i]["materialId"] );

            $newsRow = null;
            for ( $j = 0;$j<count( $newsId );$j++ ) {
                $newsResult = M( "company_news" )->where( 'id = ' . $newsId[$j] )->find();

                $newsRow[$j] = array(
                    'title' => $newsResult['title'],
                    'addTime' => $newsResult['modifyTime'],
                    'description' => $newsResult['description'],
                    'content' => $newsResult['content'],
                    'newsImg' => ( $j==0 )?$newsResult['bigImg']:$newsResult['minImg']
                );
            }
            if(count($newsRow) > 1)
                $newsArr[$i]['multiple'] = 1;
            else
                $newsArr[$i]['multiple'] = 0;
            $newsArr[$i]['Result'] = $newsRow;
        }

        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign( "newsArr" , $newsArr );//赋值素材数据
        $this->assign( 'rand' , rand(0,10000));
        $this->assign('ajaxClassID', $_GET['cId']);
        $this->assign('param', $_GET['param']);
        $this->assign('ecid',I('post.ecid'));
        $this->assign('responseId',I('post.responseId'));
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }   
	}

    public function activitySelect(){
        $m = M("View_company_activity");

        $opt['ecid'] = session($this->_userCfg['ECID']);
        $opt['type'] = array('not in',array('general','sign'));

        $result = $m->where($opt)->select();

        $res = $this->getActivityInfo(I('post.menuId'));
        for($i=0;$i<count($res);$i++){
            $activityIdArr[$i] = $res[$i]['id'];
            $activityName[$i] = $res[$i]['name'];
        }

        $this->assign('activityIdArr',$activityIdArr);
        $this->assign('activityName',$activityName);
        $this->assign('informKeyword',$res);
        $this->assign('activity', $result);
        $this->display();
    }

    private function getActivityInfo($menuId){
        $Data = M('Company_menu');

        $result = $Data->find($menuId);
        $result['informKeyword'] = explode(",",$result['informKeyword']);
        for($i=0;$i<count($result['informKeyword']);$i++){
             $res[$i] = M('View_company_activity')->find($result['informKeyword'][$i]);
        }
        return $res;
    }

    public function mallSelect(){
        $this->display();
    }

    public function vipSelect(){
        $this->display();
    }
    public function activityAllSelect(){
        $m = M("Company_activity");
        $opt['ecid'] = session($this->_userCfg['ECID']);
        $result = $m->where($opt)->select();
        $this->assign("activityItem" , $result);
        if(I('post.v')){
            $this->assign('defaultActivityId',I('post.v'));
        }        

        $this->assign('activityArr', I('post.p'));
        $this->display();
    }
}
?>