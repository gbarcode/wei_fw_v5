<?php
namespace Admin\Action;
use Org\Error\Error;
class KeywordAction extends AdminAction {
    public function index() {
        $this->assign('welcomeItem', $this->getWelcomeItems(session('ecid')));
        $this->setToken();
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }

    public function item(){
        $Data = M('Company_keyword_response');
        $opt['ecid'] = I('post.ecid');
       
        if(I('post.keyword') != ''){
            $opt['keyword']= array('LIKE','%'.I('post.keyword').'%');
        }

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        
        $result = $Data->where($opt)->order( 'modifyTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        if($result){
            for($i=0; $i<count($result); $i++){
                $result[$i]['keyword'] = explode( "," , $result[$i]['keyword'] );

                $result[$i]['TypeName'] = $this->getTypeName($result[$i]['responseType']);
            }

            $this->assign('keywordItem', $result);
            $this->assign( 'page', $show );// 赋值分页输出
            $this->assign( 'cId', I('post.cId'));
            $this->assign( 'ecid', I('post.ecid'));
        }else{
            if(I('post.keyword') != ''){
                $this->assign('searchNull',true); 
            }else{
               $this->assign('resultnull',true); 
           }            
        }
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }

    public function edit(){
        $m = M('Company_keyword_response');

        $opt['id'] = I('get.id');
        $result = $m->where($opt)->find();

        $result['name']=trim($result['name']);
        $result['keyword'] = explode( "," , $result['keyword'] );
        $this->assign('response',$result);
        $this->assign('ecid',$result['ecid']);
        $this->display();
    }

    public function add(){
        $this->assign('ecid',session( $this->_userCfg["ECID"] ));
        $this->display();
    }

    public function view(){
        $m = M('Company_keyword_response');

        $opt['id'] = I('get.id');
        $result = $m->where($opt)->find();

        $result['keyword'] = explode( "," , $result['keyword'] );
        $result['TypeName'] = $this->getTypeName($result['responseType']);

        $this->assign('item',$result);
        $this->display();
    }

    /**
     * addHandle用于处理添加规则时的ajax请求
     */
    public function addHandle(){
        
        //获取所有post参数
        $opt = I('post.');

        for($i=0; $i<Count($opt['keyword']); $i++){
            //添加对关键词重复的判断
            if($this->checkKeywordExist($opt['keyword'][$i], session('ecid'))){
                $data['data'] = Error::ERROR_KEYWORD_EXIST;
                $data['info'] = $opt['keyword'][$i]. Error::getErrMsg(Error::ERROR_KEYWORD_EXIST);
                $this->ajaxReturn($data,"JSON");
            } 

            if($i == 0)
                $keyword = $opt['keyword'][0];
            else
                $keyword .= ',' . $opt['keyword'][$i];
        }

        $opt['keyword'] = $keyword;

        //添加时间参数
        $opt['modifyTime'] = date('Y-m-d H:i:s');
        $opt['ecid'] = session('ecid');

        $m = M('Company_keyword_response');

        //判断规则名是否重复
        if(M("Company_keyword_response")->where("ecid = '".session($this->_userCfg['ECID'])."' AND name = '".$opt['name']."' AND id != '".$opt['id']."'")->find()){
            $data['data']= Error::ERROR_FW_NAME_EXIST;
            $data['info']= Error::getErrMsg(Error::ERROR_FW_NAME_EXIST);
        }
        else{
            if($m->add($opt))
                $data['data']= Error::SUCCESS_OK;
            else{
                $data['data']= Error::ERROR_GENERAL;
                $data['info']= Error::getErrMsg(Error::ERROR_GENERAL);                
            }
        }

            $this->ajaxReturn($data,"JSON");
    }

    public function editHandle(){
        //获取所有post参数
        $opt = I('post.');

        //将keyword合并
        for($i=0; $i<Count($opt['keyword']); $i++){
            if($this->checkKeywordExist($opt['keyword'][$i], session('ecid'),I('post.id'))){
                $data['data'] = Error::ERROR_KEYWORD_EXIST;
                $data['info'] = $opt['keyword'][$i]. Error::getErrMsg(Error::ERROR_KEYWORD_EXIST);
                $this->ajaxReturn($data,"JSON");
            } 

            if($i == 0)
                $keyword = $opt['keyword'][0];
            else
                $keyword .= ',' . $opt['keyword'][$i];
        }
        $opt['keyword'] = $keyword;

        //添加时间参数
        $opt['modifyTime'] = date('Y-m-d H:i:s');

        $m = M('Company_keyword_response');
        //判断规则名是否重复
        if(M("Company_keyword_response")->where("ecid = '".session($this->_userCfg['ECID'])."' AND name = '".$opt['name']."' AND id != '".$opt['id']."'")->find()){
            $result["data"] = Error::ERROR_FW_NAME_EXIST;
            $result["info"] = Error::getErrMsg(Error::ERROR_FW_NAME_EXIST);
        }else{
            if($m->save($opt)){
                $result["data"] = Error::SUCCESS_OK;
            }else{
                $result["data"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }
        
        $this->ajaxReturn($result,"JSON");
    }

    public function delHandle(){
        //获取所有post参数
        $opt = I('post.');

        if(!assert($opt['id']) || !assert($opt['ecid'])){
            $result["data"] = Error::ERROR_GENERAL;
                $result["info"] = Error::getErrMsg(Error::ERROR_GENERAL);
        }

        $m = M('Company_keyword_response');

        if($m->where($opt)->delete()){
                $result["data"] = Error::SUCCESS_OK;
            }else{
                $result["data"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        $this->ajaxReturn($result,"JSON");
    }

    public function welcomeHandle(){
        //获取所有post参数
        $opt = I('post.');
        unset($opt['TypeName']);
        $m = M('Company_event_response');
        $status = $m->save($opt);

        if($status){
            $subscribe_cache = 'subscribe' . $opt['ecid'];
            S($subscribe_cache, NULL);
        }
        $response  = S($subscribe_cache);

        $this->ajaxReturn($status,'info',0);
    }

    private function checkKeywordExist($keyword, $ecid, $id=''){
        $m = M('Company_keyword_response');

        $opt = "ecid = " . $ecid." AND (keyword = '".$keyword."' OR keyword like '".$keyword.",%' OR keyword like '%,".$keyword.",%' OR keyword like '%,".$keyword."')";
        
        if($id != '')
            $opt .= " AND id !=" .$id;

        $result = $m->where($opt)->find();

        if($result)
            return true;
        else
            return false;
    }

    private function getWelcomeItems($ecid){
        $m = M('Company_event_response');

        $opt['event'] = 'subscribe';
        $opt['eventType'] = 'subscribe';
        $opt['ecid'] = $ecid;
        $result = $m->where($opt)->find();

        $result['TypeName'] = $this->getTypeName($result['responseType']);

        return $result;
    }

    private function getTypeName($type){
        switch ($type) {
            case 'text':
                return '文本信息';
                break;
            case 'news':
                return '图文信息';
                break;
            case 'check_prize':
                return '查询中奖';
                break;
            case 'tmall':
                return '天猫直达';
                break;
            default:
                return '未知';
                break;
        }
    }
}
?>
