<?php
namespace Admin\Action;
use Org\Error\Error;

class UserAction extends AdminAction{
    public function index(){
        $result = M("Company_info")->where("company_ecid = '".session( $this->_userCfg['ECID'] )."'")->find();

        $this->assign("url" , "http://www.msa12365.com/Weixin/Vip/sz12365/".session( $this->_userCfg['ECID'] ));
        $this->assign("weiboUrl" , "http://www.msa12365.com/Weixin/Vip/sz12365/platform/weibo/ecid/".session( $this->_userCfg['ECID'] ));
        $this->assign("serviceToken" , md5(session( $this->_userCfg['ECID'] )."sz12365"));
        $this->assign("user" , $result);
        $this->assign('role',session( $this->_userCfg['ROLE'] ));
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function login(){
        // 导入分页类
        $Data = M('User_info');

        $opt['company_ecid'] = session($this->_userCfg['ECID']);

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 15);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        //区分员工性别
        for($i = 0;$i<count($result);$i++){
            if($result[$i]['sex'] == 0){
                $result[$i]['sexName'] = "女";
            }else{
                $result[$i]['sexName'] = "男";
            }
        }
        
        $this->assign("user" , $result);
        $this->assign( 'page', $show );// 赋值分页输出
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function userInfo(){
        if(I('post.id') == 'add'){
            $result = array(
                "user_name" => "" ,
                "name" => "" ,
                "sex" => "" ,
                "email" => "" ,
                "handleType" => "add"
                );
        }else{
            //获取员工信息
            $result = M("User_info")->where("id = '".I('post.id')."'")->find();
            $result['handleType'] = 'edit';
        }

        $this->assign("user" , $result);
        $this->display();
    }

    public function password(){
        $this->display();
    }

    public function defaultPwd(){
        $this->display();
    }

    /**
     * loginUserHandle企业物流码账号操作
     */
    public function loginUserHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("User_info");
        //获取post参数
        $opt['sex'] = I('post.sex');
        $opt['user_name'] = I('post.user_name');
        $opt['name'] = I('post.name');
        $opt['email'] = I('post.email');

        if(I('post.handleType') == 'add'){
            $checkName['user_name'] = I('post.user_name');

            if($m->where($checkName)->find()){
                $result['status'] = ERROR_LOGIN_NAME_EXIST;
                $result['info'] = Error::getErrMsg(Error::ERROR_LOGIN_NAME_EXIST);
                $this->ajaxReturn($result , "JSON");
            }
        }

        //判断操作类型
        if(I('post.handleType') == 'add'){
        //判断账号名是否重复[2014/5/8]
            if($m->where("user_name ='".I('post.user_name')."'")->find()){
                $result['status'] = ERROR_LOGIN_NAME_EXIST;
                $result['info'] = Error::getErrMsg(Error::ERROR_LOGIN_NAME_EXIST);
            }else{
                $opt['company_ecid'] = session($this->_userCfg['ECID']);
                $opt['user_password'] = C('DEFAULT_PWD');
                if($m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
                }
            }
        }else{
            $opt['id'] = I('post.id');
            if($m->save($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    /**
     * resetPWD重置账号密码
     */
    public function resetPWD(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt = array(
            "id" => I('post.id') ,
            "user_password" => C('DEFAULT_PWD')
            );

        if(M("User_info")->where($opt)->find($opt)){
            $result["status"] = Error::SUCCESS_OK;
        }
        else{
            if(M("User_info")->save($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    /**
     * userHandle企业员工操作
     */
    public function userHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['company_web'] = I('post.company_web');
        $opt['mall_domain'] = I('post.mall_domain');
        $opt['company_logo'] = I('post.company_logo');
        $opt['weixin_AppId'] = I('post.weixin_AppId');
        $opt['weixin_AppSecret'] = I('post.weixin_AppSecret');
        $opt['weibo_AppId'] = I('post.weibo_AppId');
        $opt['weibo_AppSecret'] = I('post.weibo_AppSecret');
        $opt['weibo_accessToken'] = I('post.weibo_accessToken');

        if(M("Company_info")->where("company_ecid = '".session($this->_userCfg['ECID'])."'")->save($opt)){
            $result["status"] = Error::SUCCESS_OK;

            //清除企业缓存
            $ecid_cache = 'company_info'.session($this->_userCfg['ECID']);
            if(S($ecid_cache))
                S($ecid_cache, NULL);
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }

        $this->ajaxReturn($result , "JSON");
    }

    /**
     * passwordHandle企业账号密码操作
     */
    public function passwordHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("User_info");
        $user = $m->where("id = " . session($this->_userCfg['UID']))->find();

        if($user['user_password'] != md5(I('post.oldPwd'))){
            $result["status"] = Error::ERROR_USER_PWD_ERROR;
            $result["info"] = Error::getErrMsg(Error::ERROR_USER_PWD_ERROR);
        }else{
            $opt['id'] = $user['id'];
            $opt['user_password'] = md5(I('post.user_password'));

            if($m->save($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }
    /**
     * 设置默认密码
     */
    public function defaultPwdHandle(){
        
        $m = M('Company_info');
        
        if(I('post.defaultPwd')){
            $opt['defaultPwd'] = md5(I('post.defaultPwd'));
        }
        else{
            $opt['defaultPwd'] = md5('tytm123');
        }

        if($m->where("company_ecid = '".session( $this->_userCfg['ECID'] )."'")->save($opt)){
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        $this->ajaxReturn($result , "JSON");
    }

    public function employees(){
        $this->assign('department',$this->getDepartment());
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }
    /**
     * 员工详情列表显示
     * 2014/5/9
     * 蒋东芸
     */
    public function employeesList(){
        $Data = M('Company_employees');
        $m = M('User_info');

        $opt['ecid'] = session($this->_userCfg['ECID']);

        if(I('post.name') != '')
            $opt['name'] = array('like','%'. I('post.name') .'%');//员工姓名模糊查询

        if(I('post.tel') !='')   
            $opt['tel'] = array('like','%'. I('post.tel') .'%');
        
        if(I('post.type') != 'all' && I('post.type') != ''){
            $opt['type'] = I('post.type');
        }
        if(I('post.departmentId')!= 'all'&& I('post.departmentId')!= ''){
            $opt['departmentId'] = I('post.departmentId');//部门选择查询
        }

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 15 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($opt)->order( 'Interdiction desc,id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
           
        for($i = 0;$i<count($result);$i++){

            $res['employeeId'] =$result[$i]['id']; 
            $department=$this->hasdeparmentName($result[$i]['departmentId']);
            $result[$i]['department'] = $department['department'];
            if($m->where("employeeId =".$result[$i]['id'])->find()){
                $result[$i]['account'] = 1;
            }
            if($this->hasEmployeeQrcode($res))
                $result[$i]['QrInfo']= '1';
            else{
                $result[$i]['QrInfo']= '0';
            }

            if($result[$i]['sex'] == 0){
                $result[$i]['sexName'] = "女";
            }else{
                $result[$i]['sexName'] = "男";
            }

        }
        
        $this->assign("employees" , $result);
        $this->assign("type" , $opt['type']);
        $this->assign("departmentId" , $opt['departmentId']);
        $this->assign( 'page', $show );// 赋值分页输出
        $this->display();
    }
/**
 * 获取部门名称
 * @param  [int] $departmentId 部门Id
 * @author 蒋东芸
 * 2014/4/14 
 */
    private function hasdeparmentName($departmentId){
        $opt['id'] = $departmentId;
        $result=M('Company_department')->where($opt)->find();

        return $result;
    }
    public function InterdictHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
        $opt['id']=I('post.id');
        $opt['Interdiction'] = I('post.Interdiction');
        $m = M('Company_employees');
        if($m->save($opt)){

                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        $this->ajaxReturn($result,"JSON");
    }
    /**
     * hasEmployeeQrcode判断员工二维码是否存在
     * $opt['employeeId']   员工id
     */
    private function hasEmployeeQrcode($opt){
        $result=M('Company_qr_type')->where($opt)->find();

        return $result;
    }
    /**
     * 获取该企业所有部门
     * 2014/5/5
     */
    private function getDepartment(){
        $m = M('Company_department');
        $ecid['ecid'] = session($this->_userCfg['ECID']);
        $res = $m->where($ecid)->select();
        return $res;
    }

    /**
     * 标识$result赋予employees
     * @author 蒋东芸
     */
    public function employeesInfo(){        
        if(I('post.id') == 'add'){
            $result = array(
                'name'=>"",
                'sex'=>"0",
                'img'=>"",
                'type'=>"0",
                'tel'=>"",
                'e-mail'=>'',
                'departmentId'=>'',
                'stopTime'=>'',
                'Interdiction'=>'1',
                "handleType" => "add"
            );
        }
        else{
            $result = M("Company_employees")->where("id = '".I('post.id')."'")->find();
            $result['handleType'] = 'edit';
        }
        $this->assign("systemdate",date('Y-m-d'));
        $this->assign("departName",$this->getDepartment());
        $this->assign("employees" , $result);
        $this->setToken();
        $this->display();
    }

    /**
     * 员工信息增删改处理
     * @author 蒋东芸
     */
    public function employeesHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
        
        //获取post参数
        $opt['name'] = I('post.name');
        $opt['sex'] = I('post.sex');
        $opt['img'] = I('post.img');
        $opt['type'] = I('post.type');
        $opt['tel'] = I('post.tel');
        $opt['stopTime'] = I('post.stopTime');
        $opt['position'] = I('post.position');
        $opt['departmentId'] = I('post.departmentId');
        $opt['e-mail'] = I('post.e-mail');
        $opt['Interdiction'] = I('post.Interdiction');

        $m=M('Company_employees');

        switch (I('post.handleType')) {
            case 'add':
                $opt['ecid'] = session($this->_userCfg['ECID']);
                $result =$m->add($opt);
                
                if($result){
                        $data["status"] = Error::SUCCESS_OK;
                    }else{
                        $data["status"] = Error::ERROR_ADD_HANDLE_ERR;
                        $data["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
                    }
                break;
            case 'edit':
                $opt['id'] = I('post.id');
                if($m->save($opt)){
                    if($opt['type']==1){
                        $stopTime['stopTime']='';
                        $m->where("id = '".I('post.id')."'")->save($stopTime);
                    }
                    $data["status"] = Error::SUCCESS_OK;
                }else{
                    $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $data["info"] = "未更改任意一项";
                }
                break;
            default:
                $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                break;
        }
            $this->ajaxReturn($data,"JSON");
    }

    /**
   * uploadFile  读取已上传EXCEL文件并导入数据库
   * 2014/4/21
   * @author 
   * 修改历史 ：1、2014/9/3 黄浩
   */
  public function uploadFile(){
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'User' ) );
        $ecid=session( $this->_userCfg["ECID"]);

        import("Home.PHPExcel.PHPExcel.IOFactory");
        import("@.PHPExcel.PHPExcel");
        import("@.PHPExcel.PHPExcel.IOFactory");
        import("@.PHPExcel.PHPExcel.Reader.Excel5");
        import("@.PHPExcel.PHPExcel.Reader.Excel2007");

        $objReader = \PHPExcel_IOFactory::createReader(I('post.fileType'));

        $objPHPExcel = $objReader->load($_SERVER[DOCUMENT_ROOT] . __ROOT__ . "/Public/uploads/".I('post.fileName'));
     
        $sheet = $objPHPExcel->getSheet(0);

        $highestRow = $sheet->getHighestRow(); // 取得总行数

        $highestColumn = $sheet->getHighestColumn(); // 取得总列数

        //判断列名是否正确
        if(trim($objPHPExcel->getActiveSheet()->getCell("A1")->getValue()) != "姓名" || trim($objPHPExcel->getActiveSheet()->getCell("B1")->getValue()) != "性别" ||
            trim($objPHPExcel->getActiveSheet()->getCell("C1")->getValue()) != "类型" || trim($objPHPExcel->getActiveSheet()->getCell("D1")->getValue()) != "所属部门"||
            trim($objPHPExcel->getActiveSheet()->getCell("E1")->getValue()) != "职务"|| trim($objPHPExcel->getActiveSheet()->getCell("F1")->getValue()) != "手机号码"||
            trim($objPHPExcel->getActiveSheet()->getCell("G1")->getValue()) != "个人邮箱"){
          $result["status"] = Error::ERROR_QR_FILE_COLUMN_ERROR;
          $result["info"] = Error::getErrMsg(Error::ERROR_QR_FILE_COLUMN_ERROR);
          $this->ajaxReturn($result , "JSON");
        }
        $telNum = 0;
        $emailNum = 0;

        //循环读取excel文件,读取一条,插入一条
        for($i=2;$i<=$highestRow;$i++)
        {
            $data = null;

            $sex = trim($objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue());

            switch ($sex) {
                case '男':
                    $sex = 1;
                    break;

                case '女':
                    $sex = 0;
                    break;
                
                default:
                    $sex = 1;
                    break;
            }

            $type = trim($objPHPExcel->getActiveSheet()->getCell("C".$i)->getValue());
            switch ($type) {
                case '临时员工':
                    $type= 0;
                    $stopTime = date("Y-m-d",strtotime("+3 month"));
                    break;

                case '固定员工':
                    $type = 1;
                    $stopTime = 0000-00-00;
                    break;
                
                default:
                    $type = 1;
                    break;
            }

            $tel = trim($objPHPExcel->getActiveSheet()->getCell("F".$i)->getValue());
           
            if(!is_numeric($tel)){//判断输入电话是否为数字
                $telNum++;
            }

            $email = trim($objPHPExcel->getActiveSheet()->getCell("G".$i)->getValue());
            
            if(!eregi("^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*$",$email)){//判断输入电话是否为email格式
                $emailNum++;
            }

            $department['department'] = trim($objPHPExcel->getActiveSheet()->getCell("D".$i)->getValue());
            
            $res = M('Company_department')->where($department)->find();

            if(!$res){
                $department['ecid'] = session($this->_userCfg['ECID']);
                $departmentId = M('Company_department')->add($department);
            }else{
                $departmentId = $res['id'];
            }
            
            $data = array(
                "name" => trim($objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue()) , 
                "sex" => $sex , 
                "type" =>$type,
                "departmentId" =>$departmentId,
                "position"=>trim($objPHPExcel->getActiveSheet()->getCell("E".$i)->getValue()) ,
                "tel" => $tel , 
                "e-mail" => trim($objPHPExcel->getActiveSheet()->getCell("G".$i)->getValue()), 
                "Interdiction"=>'1',
                "stopTime"=> $stopTime
                );

            if($data['name'] != '' && $telNum == 0 && $emailNum == 0){
               $data['ecid'] = session($this->_userCfg['ECID']);  
              M('Company_employees')->data($data)->add();
            }
        }
        $result['telNum'] = $telNum;
        $result['emailNum'] = $emailNum;
        $result["status"] = Error::SUCCESS_OK;
        $this->ajaxReturn($result , "JSON");
    }

    /**
     * 生成员工二维码
     * @author 蒋东芸
     */
    public function createEmployeeQr(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("Company_qr_type");
        $opt['ecid'] = session($this->_userCfg['ECID']);
        $opt['employeeId'] = I('post.id');
        $opt['type']="employees";
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['scene_id'] = $this->getSceneId("employees" , $opt['ecid']);

        if($m->add($opt)){
            $data['status'] = Error::SUCCESS_OK;
        }else{
            $data['status'] = ERROR_ADD_HANDLE_ERR;
            $data['info'] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }
        

        $this->ajaxReturn($data , "JSON");
    }

    /**
     * 点击添加员工动态框中的确定按钮，将相应的信息写入到表Company_qr_type中ecid、scene_id、employeeId、type字段中
     * @author 蒋东芸
     */
    private function getSceneId($type , $ecid){

        $m = M('Company_qr_type');

        //取出该类型sceneid范围
        $options['typeName'] = $type;
        $typeZone = M("Company_qr_zone")->where($options)->find();

        $opt['ecid'] = $ecid;
        $opt['scene_id'] = array('between' , $typeZone['MinScene'].','.$typeZone['MaxScene']);
        
        //查找该ecid最大sceneid值
        $maxSceneId = $m->where($opt)->max('scene_id');

        if($maxSceneId != ''){
          if($maxSceneId == $typeZone['MaxScene']){
            $sceneId = $typeZone['MinScene'];
          }else{
            $sceneId = $maxSceneId + 1;
          }
        }
        else
          $sceneId = $typeZone['MinScene'];


        return $sceneId;
    }

    /**
     * getQrHandle获取员工二维码
     */
    public function getQrHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        $m = M("Company_qr_type");
        $opt['employeeId'] = I('post.id');
        if($result = $m->where($opt)->find()){
            $data['status'] = Error::SUCCESS_OK;
            $data['info'] = $this->getQrPicUrl($result['ecid'] , $result['scene_id']);
        }
        else{
            $data['status'] = ERROR_PRODUCT_SCENEID_EMPTY;
            $data['info'] = Error::getErrMsg(Error::ERROR_PRODUCT_SCENEID_EMPTY);
        }
        $this->ajaxReturn($data,"JSON");
    }

    /**
     * getQrPicUrl获取二维码图片路径
     */
    private function getQrPicUrl($ecid , $sceneId){
        $token = $this->getAppToken($ecid);

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            return;
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
          $qrCode = $weObj->getQRCode($sceneId, 1);
            if($qrCode){
                return $weObj->getQRUrl($qrCode['ticket']);
            }
        }
    }

    /**
     * getAppToken获取企业微信token
     */
    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }

    public function department(){
        // 导入分页类

        $Data = M('Company_department');

        $opt['ecid'] = session($this->_userCfg['ECID']);

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 20 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($opt)->order( 'department asc' )->limit( $page->firstRow.','.$page->listRows )->select();
        
        $this->assign("department" , $result);
        $this->assign( 'page', $show );// 赋值分页输出

        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    public function departmentInfo(){
        if(I('post.id') == 'add'){
            $result = array(
                'department'=>"",
                'info'=>"",
                "handleType" => "add"
            );
        }
        else{
            $result = M("Company_department")->where("id = '".I('post.id')."'")->find();
            $result['handleType'] = 'edit';
        }
        $this->assign("department" , $result);
        $this->display();
    }

    public function DepartHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt['id'] = I('post.id');
        $opt['department'] = I('post.department');
        $opt['info'] = I('post.info');

        $m= M('Company_department');
        $Data=M('Company_employees');
        switch (I("post.handleType")) {
            case 'add':
                if($this->inputNameHandle(I('post.department'))){
                    $opt['ecid'] = session($this->_userCfg['ECID']);
                    $result =$m->add($opt);
                    if($result){
                            $data["status"] = Error::SUCCESS_OK;
                        }else{
                            $data["status"] = Error::ERROR_ADD_HANDLE_ERR;
                            $data["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
                        }
                }
                else{
                    $data['status'] = ERROR::ERROR_USER_DEPARTNAME_EXIST;
                    $data['info'] = Error::getErrMsg(Error::ERROR_USER_DEPARTNAME_EXIST);
                }

                break;
            case 'edit':
                if($m->save($opt)){
                    $data["status"] = Error::SUCCESS_OK;
                }else{
                    $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
                break;
            case 'delete':
                if($m->where("id = '".I('post.id')."'")->delete()){
                    
                    $departmentId['departmentId']='';
                    $Data->where("departmentId = '".I('post.id')."'")->save($departmentId);
                    $data["status"] = Error::SUCCESS_OK;
                }else{
                    $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
                break;
            
            default:
                $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                break;
        }
            $this->ajaxReturn($data,"JSON");
        
    }

    private function inputNameHandle($inputName){
        $m = M('Company_department');
        $opt['department'] = $inputName;
        if(!$m->where($opt)->find()){
            return true;
        }
        else{
            return false;
        }
    }

    /**
    * 函数名：signupShow
    * 添加员工权限账号页面
    * @access public
    * @param employeeId 员工id
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2014-12-30 创建函数
    */
    public function signupShow(){
        $this->assign('employeeId',I('post.employeeId'));
        $this->display();
    }    

    /**
    * 函数名：addAccountHandle
    * 添加员工权限账号
    * @access public
    * @param employeeId  员工id
    *        user_name   账号名
    *        roleId      角色
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2014-12-31 创建函数
    */
    public function addAccountHandle(){
        $m = M('User_info');
        $em= M('Company_employees');
        $opt = I('post.');

        $Info = $em->where('id='.$opt['employeeId'])->find();
        $opt['name'] = $Info['name'];
        $opt['company_ecid'] = $Info['ecid'];
        $opt['sex'] = $Info['sex'];
        $opt['email'] = $Info['e-mail'];
        $opt['user_password'] = C( 'DEFAULT_PWD' );
       
        if($m->add($opt)){
            $data['status'] = Error::SUCCESS_OK;
        }else{
            $data['status'] = ERROR_ADD_HANDLE_ERR;
            $data['info'] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        } 

        $this->ajaxReturn($data,"JSON");
    }
    /**
    * 函数名：introduction
    * 企业介绍页
    * @access public
    * @param 
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-11 创建函数
    */
    public function introduction(){
        $m           =M('Company_introduction');
        $opt['ecid'] = session( $this->_userCfg['ECID'] );        
        $result      = $m->where($opt)->select();
        
        $this->assign('info',$result);
        $this->assign('rand' , rand());
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    /**
    * 函数名：getContentFromID
    * 获取具体内容显示
    * @access public
    * @param 
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-11 创建函数
    */
    public function getContentFromID(){
        if(I('post.id') == "add"){
            $result = array(
                'name' => '' ,
                'bigImg' => '' ,
                'minImg' => '' ,
                'content' => '' ,
                'handleType' => 'add' 
                );
        }else{
            $result = M("Company_introduction")->where("id = '".I('post.id')."'")->find();

            $result['handleType'] = "";
            $result['content'] = htmlspecialchars_decode($result['content']);
        }

        $this->ajaxReturn($result , "JSON");
    }

    /**
    * 函数名：contentHandle
    * 企业介绍页显示内容添加、修改和删除处理
    * @access public
    * @param 
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-11 创建函数
    */
    public function contentHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("Company_introduction");

        $opt['name'] = I('post.name');
        $opt['id'] = array("neq" , I('post.id'));
        $opt['ecid'] = session( $this->_userCfg['ECID'] );    

        if($m->where($opt)->find()){
            $result["status"] = Error::ERROR_TITLE_EXIST;
            $result["info"] = Error::getErrMsg(Error::ERROR_TITLE_EXIST);
        }else{
            unset($opt['id']);
            $opt['content'] = I('post.content');
            $opt['bigImg'] = I('post.bigImg');
            $opt['minImg'] = I('post.minImg');
            
            if(I('post.handleType') == "add"){
                if($row = $m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                    $result['info'] = Error::getErrMsg(Error::SUCCESS_OK);
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }else if(I('post.handleType') == "edit"){
                if($row = $m->where('id = '.I('post.id'))->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                    $result['info'] = Error::getErrMsg(Error::SUCCESS_OK);
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        if(I('post.handleType') == "delete"){
            if($m->where('id = '.I('post.id'))->delete()){
                $result["status"] = Error::SUCCESS_OK;
                $result["info"] = Error::getErrMsg( Error::SUCCESS_OK);
            }else{
                $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }


    /**
    * 函数名：cooperation
    * 企业合作页
    * @access public
    * @param 
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-13 创建函数
    */
    public function cooperation(){
        $m           =M('Company_cooperation');
        $opt['ecid'] = session( $this->_userCfg['ECID'] );        
        $result      = $m->where($opt)->select();
        
        $this->assign('info',$result);
        $this->assign('rand' , rand());
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    /**
    * 函数名：getCooFromID
    * 获取具体内容显示
    * @access public
    * @param 
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-13 创建函数
    */
    public function getCooFromID(){
        if(I('post.id') == "add"){
            $result = array(
                'name' => '' ,
                'bigImg' => '' ,
                'minImg' => '' ,
                'content' => '' ,
                'handleType' => 'add' 
                );
        }else{
            $result = M("Company_cooperation")->where("id = '".I('post.id')."'")->find();

            $result['handleType'] = "";
            $result['content'] = htmlspecialchars_decode($result['content']);
        }

        $this->ajaxReturn($result , "JSON");
    }

    /**
    * 函数名：cooperationHandle
    * 企业介绍页显示内容添加、修改和删除处理
    * @access public
    * @param 
    * @return 
    * @auth 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-4-13 创建函数
    */
    public function cooperationHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("Company_cooperation");

        $opt['name'] = I('post.name');
        $opt['id'] = array("neq" , I('post.id'));
        $opt['ecid'] = session( $this->_userCfg['ECID'] );    

        if($m->where($opt)->find()){
            $result["status"] = Error::ERROR_TITLE_EXIST;
            $result["info"] = Error::getErrMsg(Error::ERROR_TITLE_EXIST);
        }else{
            unset($opt['id']);
            $opt['content'] = I('post.content');
            $opt['bigImg'] = I('post.bigImg');
            $opt['minImg'] = I('post.minImg');
            
            if(I('post.handleType') == "add"){
                if($row = $m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                    $result['info'] = Error::getErrMsg(Error::SUCCESS_OK);
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }else if(I('post.handleType') == "edit"){
                if($row = $m->where('id = '.I('post.id'))->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                    $result['info'] = Error::getErrMsg(Error::SUCCESS_OK);
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        if(I('post.handleType') == "delete"){
            if($m->where('id = '.I('post.id'))->delete()){
                $result["status"] = Error::SUCCESS_OK;
                $result["info"] = Error::getErrMsg( Error::SUCCESS_OK);
            }else{
                $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }
}
?>
