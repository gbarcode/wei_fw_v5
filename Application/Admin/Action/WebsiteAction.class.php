<?php
namespace Admin\Action;
use Org\Error\Error;
class WebsiteAction extends AdminAction {
    public function index(){ 
        if(I('get.tab') != ''){
            $this->assign('tab' , I('get.tab'));
        } 
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }   

    /**
     * @author 黄浩
     * 2014-07-09
     * 
     */
    public function website(){
        //查询web_cofig表中ecid为816的结果。
        $result = M("Web_config")->where("ecid=816")->find();
         $this->assign("info" , $result); // 赋值分页输出
       //页面显示
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

     /**
     * @author 黄浩
     * 2014-07-09
     *  网站信息操作
     */
    public function websiteHandle(){
        //判断是否以post方式提交。
        if(!IS_POST) _404 ('页面不存在' , U('index'));
            //实例化模块
            $m = M("Web_config");
            //post方式传输的数据赋值给opt数组。
            $opt['title'] = I('post.title');
            $opt['keyword'] = I('post.keyword');
            $opt['description'] = I('post.description');
            //缓存UID赋值发给opt数组中的modifyUserId。
            $opt['modifyUserId'] = session($this->_userCfg['UID']);
            //
            $opt['modifyTime'] = date("Y-m-d H:i:s");
            //缓存ECID赋值发给opt数组中的eid
            $opt['ecid'] = session($this->_userCfg['ECID']);
            /**
             * 判断操作方式是否编辑，如果是，判断以ecid为816为条件是否保存成功，
             * 如果成功，赋值操作成功提示信息。
             * 否则，赋值错误提示信息，调用错误信息提示语。
             */

        if(I('post.handleType')=='edit'){
            if($m->where("ecid=816")->save($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        /**
         *判断操作方式是否为添加，如果是，判断是否将opt数组添加到数据表中
         *如果成功，赋值给页面操作成功信息提示。
         * 如果不成功，赋值给页面操作失败信息提示，调用错误信息提示语
         */

        if(I('post.handleType')=='add'){
            if($m->add($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
            }
        }
        /**
         * 将数据以JSON格式返回给页面。
         */
        $this->ajaxReturn($result,"JSON");
    }

    /**
     * 绑定经销商品牌
     * @author 黄浩
     * 2014/7/17
     */
    public function getCompanyBrand(){
        $opt= I('post.');//赋值以post方式传过来的数据到opt 
        $m = M('Company_brand');
        $result = $m->where($opt)->select();//查找满足opt的数据
        $this->ajaxReturn($result,"JSON");//以JSON格式返回数据
    }

    /**
     * 绑定产品
     * @author 黄浩
     * 2014/7/17
     */
    public function getProduct(){
        $m = M('Company_product');
        if(I('post.brandId')!=-1){
            $opt['brandId'] = I('post.brandId');
        }
        $result = $m->where($opt)->select();//查找满足$opt返回数据
        $this->ajaxReturn($result,"JSON");//将数据以JSON格式返回
    }

    /**
     * 绑定经销商
     * @author 黄浩
     * 2014/7/17
     */
    public function getDealer(){
        $opt = I('post.');//将以post方式传递的值赋给opt数组
        $m = M('Company_dealers');
        $result = $m->where($opt)->select();//查找满足$opt返回数据
        $this->ajaxReturn($result,"JSON");//将数据以JSON给格式返回
    }


    private function getAdValue($adId){
        $m = M('Company_ad');
        $opt['id'] = $adId;
        $result=$m->where($opt)->find();
        return $result['value'];
    }

    /**
     *广告显示列表
     *@author 黄浩
     * 2014-7-22
     */
    public function advertList(){
        $m = M ('Company_ad_set');
        // $opt = M ('Company_ad');
        $count = $m->count();// 查询满足要求的总记录数 $map表示查询条件
        $page  = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show  = $page->show();// 分页显示输出
        //将查询的结果以id排序输出数据
        $result = $m->order( 'id desc' )->limit($page->firstRow.','.$page->listRows )->select();
        for($i=0; $i<count($result); $i++){
            $result[$i]['value'] = $this->getAdValue($result[$i]['adId']);
            $result[$i]['user_name'] = $this->getUseName($result[$i]['modifyUserId']);
        }

        $this->assign('adInfo',$result);//将变量result，赋值个adinfo页面
        $this->assign( 'page', $show );// 赋值分页输出
        $this->display();

    }
    /**
     *操作人显示
     *@author 蒋东芸
     *2014/7/24 
     */
    private function getUseName($id){
        $opt['id'] = $id;
        $result = M('User_info')->where($opt)->getField('user_name');
        return $result;
    }

    /**
     *广告管理显示
     *@author 黄浩
     *2014/7/16
     * 
     */
    public function advert(){
        $company = M("Company_info")->select();//查询company_in中数据
        $ad = M("Company_ad")->select();//查询数据表中的数据
        $dealerId = M("Company_dealers")->select();//查询Company_dealers中的数据
        //将以getSolution函数获取的值赋值给页面
        $this->assign('advertArr', json_encode($this->getSolution(I('get.id'))));
        $this->assign("company" , $company); //将变量company赋值给页面
        $this->assign("ad",$ad); //将变量ad的值赋给页面
        $this->assign('dealerId',$dealerId);//将变量dealerId赋值给页面
        $this->assign('adArr',$adArr);//将变量adArr赋值给页面
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    /**
     * getDealer 获取解决方案信息
     */
    private function getSolution($id){
        /**
         * 判断id是否为添加模式
         * 如果是建立result数组
         * 如果不是以id等于变量为条件，并将操作方式改为编辑。
         * 返回数据。
         */
        if($id == 'add'){
            $result = array(
                "title" => "" ,
                "company" => "" ,
                "description" => "" ,
                "img" => "" ,
                "keyword" => "" ,
                "handleType" => "add" , 
                "content" => "" 
                );
        }else{
            $result = M("Web_solution")->where("id = '".$id."'")->find();
            $result["handleType"] = "edit";
        }

        return $result;
    }

    public function advertInfo(){
        /**
         * 判断以post方式传过来的数据是否为添加
         * 如果是创建result数组
         * 否则查询id与所传值相等的数据，并将操作方式改为编辑方式
         * 
         */
        if(I('post.id') == 'add'){
            $result = array(
                'adId'=>"",
                'ecid'=>"",
                'brandId'=>"",
                'productId'=>"",
                'dealerId'=>"",
                'imgPath'=>'',
                'departmentId'=>'',
                'linkUrl'=>'',
                'tagId'=>'',
                'modifyTime' =>'',
                "handleType" => "add"
            );            
        }
        else{
            $result = M("Company_ad_set")->where("id = '".I('post.id')."'")->find();
            $result['handleType'] = 'edit';
            $imgRule = M("Company_ad")->where($result['adId'])->find();
            $this->assign('imgRule',$imgRule);
        }

        $company = M("Company_info")->select();//查询所有公司信息数据
        $ad = M("Company_ad")->select();//查询所有广告位数据
        $dealerId = M("Company_dealers")->select();//查询所有经销商数据
        $product = M("Company_product")->select();
        //将以getSolution函数获取的值赋值给页面
        $this->assign('advertArr', json_encode($this->getSolution(I('get.id'))));
        $this->assign("company" , $company);//将变量company赋值给页面
        $this->assign("ad",$ad); //将变量ad值给页面
        $this->assign('dealerId',$dealerId);//将变量dealerId赋值给页面
        
        $this->assign('adArr',$result);//将变量result赋值给页面
        $this->assign('product',$product);
        $this->display();
    }

    public function advertHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));//判断是否是post方式传值

        $m = M("Company_ad_set");
        //将以post方式传值赋值给opt数组
        $opt['adId'] = I('post.adId');
        $opt['ecid'] = I('post.ecid');
        $opt['brandId'] = I('post.brandId');
        $opt['productId'] = I('post.productId');
        $opt['dealerId'] = I('post.dealerId');
        $opt['linkUrl']  = I('post.linkUrl');
        // $opt['tagId'] = I('post.tagId');
        $opt['imgPath'] = I('post.imgPath');
        $opt['modifyUserId'] = session($this->_userCfg["UID"]);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        /**
         * 判断操作方式是否为添加，如果是判断是否添加成功，添加成功，提示操作成功信息
         * 不成功，提示不成功信息，调用不成功信息获取提示语。
         * 
         */
        if(I('post.handleType') == "add"){
            if($m->add($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        /**
         * 判断操作方式是否为删除，如果是判断是否添加成功，添加成功，提示操作成功信息
         * 不成功，提示不成功信息，调用不成功信息获取提示语。
         * 
         */
        if(I('post.handleType') == "delete"){
            if($m->where("id =".I('post.id'))->delete()){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
               
            }
        }

        if(I('post.handleType') == "edit"){
            if($m->where("id = '".I('post.id')."'")->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
        }  
       
        $this->ajaxReturn($result,"JSON");//以JSON格式返回数据。
    }
}



?>