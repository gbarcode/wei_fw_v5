<?php
namespace Admin\Action;
use Weixin\Response\WechatConst;
use Org\Error\Error;
use Think\Action;

class ActivityAction extends AdminAction {
  public function index(){
    if(session( $this->_userCfg["ECID"] ) == "816"){
          $isAdmin = true;
        }else{
          $isAdmin = false;
        }

        $this->assign('isAdmin' , $isAdmin);
    $this->assign('type',$this->getActivityType());
    $this->assign('ecid',session( $this->_userCfg["ECID"] ));
    
    if(session('theme')){
        $this->theme('nifty')->display();
    }else{
        $this->display();
    }    
  }

  public function addActivity(){
    $company = M("Company_info")->where('company_ecid != 816')->select();

    if(session( $this->_userCfg["ECID"] ) == "816"){
      $isAdmin = true;
    }else{
      $isAdmin = false;
    }
    $this->assign('isAdmin' , $isAdmin);

    $this->assign('company' , $company);
    $this->assign('type',$this->getActivityType());
    $this->assign('brand', $this->getBrand());
    $this->assign('cId', I('post.cId'));
    $this->setToken();
    $this->display();
  }

  public function prizeList(){
    $activityId = I('post.id');

    if($activityId == '')
      return;

    $m = M('Company_lucky_prize');
    $opt['activityId'] = $activityId;

    $result = $m->where($opt)->select();

    if($result)
      $this->assign('prizeList',$result);

    $this->display();
  }

  public function productListHandle(){
    $m = M('Company_product');
    $opt['brandId'] = I('post.id');
    $result = $m->where($opt)->select();

    if($result)
      $this->ajaxReturn($result,'JSON',0);
    else
      $this->ajaxReturn(0,'success',0);
  }

  public function addActivityHandle(){
    
     //判断公司活动表是否存在，不存在则创建
    $ecid=session($this->_userCfg["ECID"]);
      $m=M('Company_'.$ecid.'_activity_user');
        if(!$m->find()){
          M()->query("CREATE TABLE `sz12365_fw_company_".$ecid."_activity_user` (
          `id` int(11) unsigned NOT NULL auto_increment,
          `activityId` int(4),
          `openId` varchar(255),
          `user_name` varchar(10),
          `sex` int(1),
          `company` varchar(50),
          `tel` varchar(30),
          `checkCode` varchar(10),
          `signTime` datetime,
          PRIMARY KEY (`id`)
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8");
        }

    $opt = I('post.');
    $opt['configure'] = json_encode(I('post.configure'));
    $opt['modifyUserId'] = session($this->_userCfg['UID']);
    $opt['modifyTime'] = date('Y-m-d H:i:s');

    if($opt['type'] != 'general'&&$opt['type'] != 'sign'){
      $opt['userCount'] = 0;
      $opt['baseNumber'] = 50;
      $opt['luckyNum'] = 50;
    }

    $m = M('Company_activity');

    //判断活动时间是否合理
    if(I('post.timeType') == 'limit'){
      if(strtotime(I('post.startTime'))=='0000 00 00' || strtotime(I('post.startTime'))<strtotime(date("Y-m-d")) || strtotime(I('post.startTime'))>strtotime(I('post.endTime'))){
          $result["status"] = Error::ERROR_STARTTIME_SET;
          $result["info"] = Error::getErrMsg(Error::ERROR_STARTTIME_SET);
          $this->ajaxReturn($result , "JSON");
        }
    }

    $activity = M('Company_activity')->where("ecid = ".I('post.ecid')."  and (type = 'fwLucky' or type = 'general') and type ='".I('post.type')."'")->select();
    for($i = 0;$i<count($activity);$i++){
      if($activity){
        if($activity[$i]['timeType'] == 'unlimit'){
          $data['status'] = Error::ERROR_LUCKY_TIME_REPERT;
          $data['info'] = Error::getErrMsg(Error::ERROR_LUCKY_TIME_REPERT);
          $this->ajaxReturn($data , 'JSON');
        }else{
          if(date("Y-m-d") <= $activity[$i]['endTime'] && $opt['timeType']=='unlimit'){
            $data['status'] = Error::ERROR_LUCKY_TIME_REPERT;
            $data['info'] = Error::getErrMsg(Error::ERROR_LUCKY_TIME_REPERT);
            $this->ajaxReturn($data , 'JSON');
          }else{
            if(date("Y-m-d") <= $activity[$i]['endTime']){
              if($opt['type'] == 'general'||($opt['type'] == 'fwLucky' && $opt['bindBrandId'] =='-1')||($opt['bindBrandId'] != '-1' && $opt['bindProductId'] == '-1'&&($activity[$i]['bindBrandId']=='-1'||$activity[$i]['bindBrandId'] == $opt['bindBrandId']))||($opt['bindBrandId'] != '-1' && $opt['bindProductId'] != '-1'&&($activity[$i]['bindBrandId']=='-1'||$activity[$i]['bindProductId']=='-1'||($activity[$i]['bindBrandId']==$opt['bindBrandId']&&$activity[$i]['bindProductId']==$opt['bindProductId'])))){
                //普通活动处理时间冲突
                if((strtotime($opt['startTime'])) > strtotime($activity[$i]['endTime']) || strtotime($opt['endTime']) < strtotime($activity[$i]['startTime'])) {
                  continue;
                }else{
                  $data['status'] = Error::ERROR_LUCKY_TIME_REPERT;
                  $data['info'] = Error::getErrMsg(Error::ERROR_LUCKY_TIME_REPERT);
                  $this->ajaxReturn($data , 'JSON');          
                }
              }
            }                  
          }
        }
      }
        
    }
    
    
    $result = $m->add($opt);

    if($result){
      if($opt['type'] == 'scanLucky'){
        $this->addQrType($opt['type'] , $result , $opt['ecid']);
      }
      if($opt['type'] == 'sign'){
        $this->addQrType($opt['type'] , $result , $opt['ecid']);
      }
      $data['status'] = Error::SUCCESS_OK;
    }
    else{
      $data['status'] = Error::ERROR_GENERAL;
      $data['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
    }
      $this->ajaxReturn($data , 'JSON');
  }

  public function shareCreateQrHandle(){
      if(I('post.type') == 'share'){
        $this->addQrType(I('post.type') ,I('post.activityId'), I('post.ecid'));
        
        $data['status'] = Error::SUCCESS_OK;
      }
      else{
        $data['status'] = Error::ERROR_GENERAL;
        $data['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
      }
      $this->ajaxReturn($data , 'JSON');
  }

 private function createQrUserTable($activityId){
    M()->query("CREATE TABLE `sz12365_fw_company_qr_sign_user_info_".$activityId."` (
          `id` int(11) unsigned NOT NULL auto_increment,
          `activityId` int(4),
          `user_name` varchar(10),
          `sex` int(1),
          `company` varchar(50),
          `tel` varchar(30),
          `checkCode` varchar(10),
          PRIMARY KEY (`id`)
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

    M()->query("CREATE TABLE `sz12365_fw_company_qr_sign_user_openid_".$activityId."` (
          `id` int(11) unsigned NOT NULL auto_increment,
          `userId` int(4),
          `openid` varchar(255),
          `signTime` datetime,
          PRIMARY KEY (`id`)
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8");
  }

  //更新粉丝信息
  private function updateUserInfo($ecid , $openId){
    $token = $this->getAppToken($ecid);
    $weObj = new \Org\Weixin\Wechat();

    if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
        $option = $weObj->getUserInfo($openId);
        $saveArr['nickname'] = $option['nickname'];
        $saveArr['sex'] = $option['sex'];
        $saveArr['language'] = $option['language'];
        $saveArr['city'] = $option['city'];
        $saveArr['province'] = $option['province'];
        $saveArr['country'] = $option['country'];
        $saveArr['headimgurl'] = $option['headimgurl'];
        M("Company_".$ecid."_user_info")->where("openId = '".$openId."'")->save($saveArr);
    }

    return $saveArr['nickname'];
  }
  public function scanUserList(){
    $ecid = M('Company_qr_type')->where("activityId = '".I('post.activityId')."'")->getField('ecid');
    $opt = ("type.ecid = ".$ecid." AND type.activityId = ".I('post.activityId')." ");

    if(I('post.startTime')!=""||I('post.endTime')!=""){
      $startTime = I('post.startTime').' 00:00:00';
      $endTime =  I('post.endTime').' 23:59:59';
      $opt .="and scan.scanTime > '".$startTime."' and scan.scanTime < '".$endTime."' ";
     }
    
    $result = M()->query("select userInfo.nickname , userInfo.city , userInfo.sex ,scan.scanTime , userInfo.openId 
      from (sz12365_fw_company_qr_type as type INNER JOIN sz12365_fw_qr_scan_log as scan ON type.scene_id = scan.sceneId ) 
      INNER JOIN sz12365_fw_company_".$ecid."_user_info as userInfo ON userInfo.openId = scan.openid
      where ".$opt."");
    
    $page       = new \Think\Page(count($result), 10 );// 实例化分页类 传入总记录数
    $show       = $page->show();
  
    $result = M()->query("select userInfo.nickname , userInfo.city , userInfo.sex ,scan.scanTime , userInfo.openId 
      from (sz12365_fw_company_qr_type as type INNER JOIN sz12365_fw_qr_scan_log as scan ON type.scene_id = scan.sceneId ) 
      INNER JOIN sz12365_fw_company_".$ecid."_user_info as userInfo ON userInfo.openId = scan.openid
      where ".$opt." ORDER BY scan.scanTime LIMIT ".$page->firstRow.",".$page->listRows );

    $this->assign('page',$show);
    $this->assign('scanUser',$result);
    $this->assign('startTime',$startTime);
    $this->assign('endTime',$endTime);  
    $this->assign('activityId',I('post.activityId'));
    $this->display();
  }


  public function luckyUser(){

      $ecid = M('Company_activity')->where("id = '".I('post.id')."'")->getField('ecid');
    
      $opt = "activityId = '".I('post.id')."'";
      if(I('post.record') == 'recorded'){
        $opt .= " AND realName IS NOT NULL";
      }
      if(I('post.record') == 'unrecorded'){
        $opt .= " AND realName IS NULL";
      }
      $count = M("View_lucky_user_{$ecid}")->where($opt)->count();
      $page       = new \Think\Page($count, 10 );// 实例化分页类 传入总记录数
      $show       = $page->show();// 分页显示输出
      
      $result = M("View_lucky_user_{$ecid}")->distinct(true)->where($opt)->limit( $page->firstRow.','.$page->listRows )->select();

      $this->assign( 'record', I('post.record') );
      $this->assign( 'page', $show );// 赋值分页输出
      $this->assign("userList" , $result);
      $this->assign('activityId',I('post.id'));
      $this->display();
  }

  /**
   * 分配奖品页面
   * @author 黄浩
   * 修改时间：1、2014-08-18 创建
   */
  public function joinUser(){
      $ecid = M('Company_activity')->where("id = '".I('post.id')."'")->getField('ecid');
      $prize = M('Company_lucky_prize')->where("activityId = '".I('post.id')."'")->select();
      $prizeDate = M('Company_activity')->where("id = '".I('post.id')."'")->find();
      
      //查询是否满足条件 
      $opt = ("select activity.id , activity.activityId , activity.userId , activity.hasLuckyChange , activity.joinTime , userInfo.openId , userInfo.nickname , userInfo.realName , userInfo.tel
        from sz12365_fw_company_".$ecid."_user_info as userInfo INNER JOIN sz12365_fw_company_activity_join as activity ON activity.userId = userInfo.id
        where joinTime Like '".I('post.date')."%' AND activity.hasLuckyChange = 1 AND activity.activityId = '".I('post.id')."'");
       
      //查询是否满足条件 
      $obj = ("select activity.id , activity.activityId , activity.userId , activity.hasLuckyChange , activity.joinTime , userInfo.openId , userInfo.nickname , userInfo.realName , userInfo.tel
        from sz12365_fw_company_".$ecid."_user_info as userInfo INNER JOIN sz12365_fw_company_activity_join as activity ON activity.userId = userInfo.id
        where activity.hasLuckyChange = 1 AND activity.activityId = '".I('post.id')."'");
      
      if(I('post.date')!=''&&I('post.date')!=-1){
        $result = M()->query($opt);
      }else{
        $result = M()->query($obj);
      }
      $page       = new \Think\Page(count($result), 10 );// 实例化分页类 传入总记录数
      $show       = $page->show();// 分页显示输出
      if(I('post.date')!=''&&I('post.date')!=-1){
      $result = M()->query($opt .= "group by openId ORDER BY joinTime LIMIT ".$page->firstRow.",".$page->listRows);

      }else{
        $result = M()->query($obj .="group by openId ORDER BY joinTime LIMIT ".$page->firstRow.",".$page->listRows);
      }
      
      for($i = 0;$i<count($result);$i++){
        if($result[$i]['nickname'] == ''){
          $result[$i]['nickname'] = $this->updateUserInfo($ecid , $result[$i]['openId']);
        }

        $prizeData = M()->query("select * 
          from sz12365_fw_company_lucky_user as user 
          INNER JOIN sz12365_fw_company_lucky_prize as prize 
          ON user.prizeId = prize.id
          where user.userId = '".$result[$i]['openId']."' AND user.activityId = ".I('post.id'));

        $result[$i]['prizeType'] = $prizeData[0]['prizeType'];
        $result[$i]['prizeName'] = $prizeData[0]['prizeName'];
        $result[$i]['luckyTime'] = $prizeData[0]['luckyTime'];
      }

      $this->assign("joinUserList" , $result);
      $this->assign('type',I('post.type'));
      $this->assign('page', $show);
      $this->assign('prize', $prize);
      $this->assign('ecid',$ecid);
      $this->assign('activityId',I('post.id'));
      $this->assign("prizeDate",json_decode($prizeDate['configure']));
      $this->assign('date',I('post.date'));
      $this->display();
  }

  private function getAppToken($ecid){
    $m = M("Company_info");

    $opt['company_ecid'] = $ecid;

    $result = $m->where($opt)->find();

    return $result;
  }

  private function getImgUrl($imgFile){
    if(substr($imgFile,0,4) != 'http')
      return WechatConst::SERVER_DOMAIN . $imgFile;

    return $imgFile;
  }

  //发送客服消息
  private function sendCustomMessage($ecid , $activityId , $openId , $userId){
      $activityInfo = M('Company_activity')->where('id = '.$activityId)->find();
      $joinTime = M('Company_activity_join')->where('userId = '.$userId.' AND activityId = '.$activityId)->getField('joinTime');
      $luckyInfo = M()->query("select * 
from sz12365_fw_company_lucky_user as users
INNER JOIN sz12365_fw_company_lucky_prize as prize
ON users.prizeId = prize.id
where prize.activityId = ".$activityId." AND users.userId = '".$openId."'");

      if($activityInfo['luckyReply']){
        $activityInfo['luckyReply'] = str_replace( '{-活动名称-}', $activityInfo['name'], $activityInfo['luckyReply']);
        $activityInfo['luckyReply'] = str_replace( '{-奖品类型-}', $luckyInfo[0]['prizeType'], $activityInfo['luckyReply']);
        $activityInfo['luckyReply'] = str_replace( '{-奖品名称-}', $luckyInfo[0]['prizeName'], $activityInfo['luckyReply']);
      }else{
        $activityInfo['luckyReply'] = "感谢您于".$joinTime."参与本次活动，恭喜您获得《".$luckyInfo[0]['prizeType']."：".$luckyInfo[0]['prizeName']."》，点击输入兑奖信息";
      }

      $data['touser'] = $openId;
      $data['msgtype'] = 'news';
      $data['news']['articles'] = array(
        array(
           "title"=>$activityInfo['name'],
           "url"=>"http://www.msa12365.com/index.php/Home/Index/activityNews/id/".$activityId,
           "picurl"=>$this->getImgUrl($activityInfo['bigImg'])
          ) , 
        array(
           "title"=>$activityInfo['luckyReply'],
           "url"=> WechatConst::SERVER_DOMAIN . "/index.php/Activity/index/activityId/".$activityId."/openId/".$openId,
           "picurl"=>$this->getImgUrl($activityInfo['minImg'])
          )
        );

      $m = M('Send_crontab');

      $opt['ecid'] = $ecid;
      $opt['addTime'] = date('Y-m-d H:i:s');
      $opt['content'] = json_encode($data);

      if($m->add($opt)){
        return true;
      }else{
        return false;
      }
  }

  private function sendLuckyMeg($ecid , $activityId , $openId , $userId){
    $activityInfo = M('Company_activity')->where('id = '.$activityId)->find();
    $joinTime = M('Company_activity_join')->where('userId = '.$userId.' AND activityId = '.$activityId)->getField('joinTime');
    $luckyInfo = M()->query("select * 
from sz12365_fw_company_lucky_user as users
INNER JOIN sz12365_fw_company_lucky_prize as prize
ON users.prizeId = prize.id
where prize.activityId = ".$activityId." AND users.userId = '".$openId."'");

    if($activityInfo['luckyReply']){
      $activityInfo['luckyReply'] = str_replace( '{-活动名称-}', $activityInfo['name'], $activityInfo['luckyReply']);
      $activityInfo['luckyReply'] = str_replace( '{-奖品类型-}', $luckyInfo[0]['prizeType'], $activityInfo['luckyReply']);
      $activityInfo['luckyReply'] = str_replace( '{-奖品名称-}', $luckyInfo[0]['prizeName'], $activityInfo['luckyReply']);
    }else{
      $activityInfo['luckyReply'] = "感谢您于".$joinTime."参与本次活动，恭喜您获得《".$luckyInfo[0]['prizeType']."：".$luckyInfo[0]['prizeName']."》，点击输入兑奖信息";
    }
    $tmp = array(
      'touser' => $openId,
      'template_id' => 'OaLJ2rDvSPVlSCESunyO5QkgSSfd9kaQfyi0lENpKMk',
      'url' => WechatConst::SERVER_DOMAIN . "/index.php/Activity/index/activityId/".$activityId."/openId/".$openId,
      "topcolor" =>"#000000",
      "data" => array(
        'first' =>array(
          'value' =>$activityInfo['name'],
          "color"=>"#000000"
          ),
        'program' =>array(
          'value' =>$luckyInfo[0]['prizeType'],
          "color"=>"#173177"
          ),
        'result' =>array(
          'value' =>$luckyInfo[0]['prizeName'],
          "color"=>"#173177"
          ),
        'remark' =>array(
          'value' =>$activityInfo['luckyReply'],
          "color"=>"#FF0000"
          )
        )
      );

      $m = M('Send_crontab');

      $opt['ecid'] = $ecid;
      $opt['addTime'] = date('Y-m-d H:i:s');
      $opt['content'] = json_encode($tmp);
      $opt['type'] = 'templete';

      $m->add($opt);
  }

  public function questionPrizeHandle(){
    if(!IS_POST) _404 ('页面不存在' , U('index'));
      $m = M('Company_lucky_user');
      $ecid = I('post.ecid');
      $opt = I('post.');
      $opt['userId'] = M('Company_'.$ecid.'_user_info')->where("id = ".I('post.userId'))->getField('openId');
      $opt['luckyTime'] = date('Y-m-d H:i:s');
      $opt['exchangeCode'] = md5($opt['userId']);
      unset($opt['ecid']);
      if($m->add($opt)){
        $prize = M('Company_lucky_prize')->where('id = '.$opt['prizeId'])->find();
        $prizeSave['luckyCount'] = (int)($prize['luckyCount'])+1;
        M('Company_lucky_prize')->where('id = '.$opt['prizeId'])->save($prizeSave);
        $this->sendCustomMessage( $ecid , I('post.activityId') , $opt['userId'] , I('post.userId'));
        $data['status'] = Error::SUCCESS_OK;
      }
      else{
        $data['status'] = Error::ERROR_GENERAL;
        $data['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
      }
      
      $this->ajaxReturn($data , 'JSON');

  }

  public function updateActivityHandle(){
    if(!IS_POST) 
            _404 ('页面不存在' , U('index'));        

        $m = M("Company_activity");
        $opt["modifyTime"] = date("Y-m-d H:i:s");
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['content']=I('post.content');
        $opt['timeType']=I('post.timeType');
        $opt['type']=I('post.type');
        $opt['url']=I('post.url');
        $opt['name']=I('post.name');
        $opt['startTime']=I('post.startTime');
        $opt['endTime']=I('post.endTime');
        $opt['bigImg']=I('post.bigImg');
        $opt['minImg']=I('post.minImg');
        $opt['anonymous']=I('post.anonymous');
        $opt['configure']=json_encode(I('post.configure','',false));//转成json数组格式

        $id['id']=I('post.id');

        //判断活动时间是否合理
        if(I('post.timeType') == 'limit'){
          if(strtotime(I('post.startTime'))=='0000 00 00' || strtotime(I('post.startTime'))>strtotime(I('post.endTime'))){
            $result["status"] = Error::ERROR_STARTTIME_SET;
            $result["info"] = Error::getErrMsg(Error::ERROR_STARTTIME_SET);
            $this->ajaxReturn($result , "JSON");
          }
        }

        $activity = M('Company_activity')->where("ecid = ".I('post.ecid')."  and (type = 'fwLucky' or type = 'general') and type ='".I('post.type')."' and id !=".I('post.id'))->select();
        for($i = 0;$i<count($activity);$i++){

          if($activity){
            if($activity[$i]['timeType'] == 'unlimit'){
              $data['status'] = Error::ERROR_LUCKY_TIME_REPERT;
              $data['info'] = Error::getErrMsg(Error::ERROR_LUCKY_TIME_REPERT);
              $this->ajaxReturn($data , 'JSON');
            }else{
              if(date("Y-m-d") <= $activity[$i]['endTime'] && $opt['timeType']=='unlimit'){
                $data['status'] = Error::ERROR_LUCKY_TIME_REPERT;
                $data['info'] = Error::getErrMsg(Error::ERROR_LUCKY_TIME_REPERT);
                $this->ajaxReturn($data , 'JSON');
              }else{
                if(date("Y-m-d") <= $activity[$i]['endTime']){
                  if($opt['type'] == 'general'||($opt['type'] == 'fwLucky' && $opt['bindBrandId'] =='-1')||($opt['bindBrandId'] != '-1' && $opt['bindProductId'] == '-1'&&($activity[$i]['bindBrandId']=='-1'||$activity[$i]['bindBrandId'] == $opt['bindBrandId']))||($opt['bindBrandId'] != '-1' && $opt['bindProductId'] != '-1'&&($activity[$i]['bindBrandId']=='-1'||$activity[$i]['bindProductId']=='-1'||($activity[$i]['bindBrandId']==$opt['bindBrandId']&&$activity[$i]['bindProductId']==$opt['bindProductId'])))){
                    //普通活动处理时间冲突
                    if((strtotime($opt['startTime'])) > strtotime($activity[$i]['endTime']) || strtotime($opt['endTime']) < strtotime($activity[$i]['startTime'])) {
                      continue;
                    }else{
                      $data['status'] = Error::ERROR_LUCKY_TIME_REPERT;
                      $data['info'] = Error::getErrMsg(Error::ERROR_LUCKY_TIME_REPERT);
                      $this->ajaxReturn($data , 'JSON');          
                    }
                  }
                }                  
              }
            }
          }
            
        }
        if($m->where($id)->save($opt)){
          $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }

        $this->ajaxReturn($result , "JSON");
  }

  public function addPrizeHandle(){
    
    $opt['activityId'] = I('post.activityId');
    $opt['prizeType'] = I('post.prizeType');
    $opt['prizeName'] = I('post.prizeName');
    $opt['num'] = I('post.num');
    $opt['luckyCount'] = I('post.luckyCount');
    $opt['modifyUserId']=session($this->_userCfg['UID']);
    $opt['modifyTime']=date("Y-m-d H:i:s");
    $m = M('Company_lucky_prize');

    if(!$this->isDataExist('Company_lucky_prize' , "activityId = '".I('post.activityId')."' AND prizeType = '".I('post.prizeType')."'")){
      $result['data'] = Error::ERROR_LUCKY_PRIZE_TYPE_EXIST;
      $result['info'] = Error::getErrMsg(Error::ERROR_LUCKY_PRIZE_TYPE_EXIST);
      $this->ajaxReturn($result , "JSON");
    }

    if(!$this->isDataExist('Company_lucky_prize' , "activityId = '".I('post.activityId')."' AND prizeName = '".I('post.prizeName')."'")){
      $result['data'] = Error::ERROR_LUCKY_PRIZE_NAME_EXIST;
      $result['info'] = Error::getErrMsg(Error::ERROR_LUCKY_PRIZE_NAME_EXIST);
      $this->ajaxReturn($result , "JSON");
    }

    $id = $m->add($opt);

    if($opt['activityId'] == 264 || $opt['activityId'] == 261){
      $this->clearJoinUser($opt['activityId']);
    }

    if($id){
      $result['data'] = Error::SUCCESS_OK;
      $this->ajaxReturn($result , "JSON");
    }
    else{
      $result['data'] = Error::ERROR_GENERAL;
      $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
      $this->ajaxReturn($result , "JSON");
    }
      
  }

  private function clearJoinUser($id){
    $m = M('Company_activity_join');
    $opt['ecid'] = session($this->_userCfg['ECID']);
    $opt['activityId'] = $id;
    $opt['hasLuckyChange'] = 0;
    $m->where($opt)->delete();
  }

  private function isDataExist($table, $options){
        $m = M($table);

        if($m->where($options)->find()){
          return false;
        }else{
          return true;
        }
    }

  public function activityList(){
        $Data = M('View_company_activity');

        if(session( $this->_userCfg["ECID"] ) == "816"){
          $isAdmin = true;
        }else{
          $isAdmin = false;
        }

        if(!$isAdmin){
          $opt['ecid'] = session( $this->_userCfg["ECID"] );
        }
  
    if(I('post.type'))
      $opt['type']=I('post.type'); 

    if(I('post.ecid'))
      $opt['ecid'] = I('post.ecid');

    
    $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
    $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
    $show       = $page->show();// 分页显示输出
    // 进行分页数据查询
   
    $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
    
    //添加当前时间、问答活动开始时间、结束时间字段便于与之比较。
    for($i=0;$i<count($result);$i++){
      $result[$i]['date'] = date("Y-m-d");
      $obj[$i] = json_decode($result[$i]['configure'],true);
      $result[$i]['startDate'] = $obj[$i]['startDate']; 
      $result[$i]['endDate'] = $obj[$i]['endDate'];
    }

    $this->assign("isAdmin" , $isAdmin);
    $this->assign('activity',$result);
    $this->assign( 'page', $show );// 赋值分页输出
    $this->assign('type',I('post.type'));
    $this->assign( 'ecid', I('post.ecid'));
    $this->display();
  }

   public function activityDetails(){
    $opt['id'] = I('get.id');
    $m = M('View_company_activity');
    $result = $m->where($opt)->find();

    $brandid['brandId']=$result['bindBrandId'];

    $res['brandName'] = M('Company_brand')->where("id =".$result['bindBrandId'])->getField('name');
    $res['productName'] = M('Company_product')->where("id =".$result['bindProductId'])->getField('name');
    
    $con = json_decode($result['configure'],true);
    $ecid = $result['ecid'];

    $startTime=date("Y-m-d",strtotime("-1 week"));
    $endTime=date("Y-m-d",strtotime("-1 day"));
    $this->assign('startTime',$startTime);
    $this->assign('endTime',$endTime);
    
    $this->assign('brand',$res);
    $this->assign('ecid', $ecid);
     $this->assign('activityType', $result['type']);
    $this->assign('activity',$result); 
    $this->assign('need',$con['need']); 
    $this->assign('configure',$con); 
    
    $newsIdArr = $this->getnewsId($con['materialId']);

    for($i=0;$i<count($newsIdArr);$i++){
      $newsInfo[$i]['id'] = $newsIdArr[$i];
      $news = M('Company_news')->where("id = ".$newsIdArr[$i])->find();
      $newsInfo[$i]['title'] = $news['title'];
    }
    $this->assign('newsInfo',$newsInfo); 

    if($result['type'] == 'scanLucky' || $result['type'] == 'sign'|| $result['type'] == 'share'){
      $data = M('Company_qr_type');
      $ecid = M('Company_activity')->where("id = '".I('get.id')."'")->getField('ecid');
  
      $option['activityId'] = $result['id'];

      $res = $data->where($option)->find();
      if($result['type'] =='share'){
        if(!$res){
          $this->assign('exist',1);
        }
      }
      $this->assign('qrUrl', $this->getQrPicUrl($res['ecid'] , $res['scene_id']));
      $this->assign('ecid',$ecid);
    }
    
    if($result['type'] == 'question'){
      $prizeDate = M('Company_activity')->where($opt)->find();
      $this->assign("prizeDate",json_decode($prizeDate['configure'])); 
    }
    //如果是会议报名活动，就获取报名人员信息
    if($result['type'] == 'meeting'){
      $signupOpt['activityId'] = $opt['id'];
      $this->assign('signUpInfo' , M('Company_activity_signup')->where($signupOpt)->select());
    }
    if(I('get.materialtab') != ''){
            $this->assign('materialtab' , I('get.materialtab'));
        }
    if(I('get.tab') != ''){
            $this->assign('tab' , I('grt.tab'));
        }
    $this->assign('activityId',I('get.id'));
    $this->assign('prize',$this->getPrizeName(I('get.id')));
    $this->assign('count',$count);
    $this->assign('Listcount',$this->commentListcount(I('get.id')));
    $this->setToken();
    
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
  }
  /**
   * 获取奖品名称
   * @param $activityId 活动id
   * @return $result 该活动下的所有奖品名称
   * @author 蒋东芸
   * 修改历史 1、蒋东芸 2014/8/22 创建函数
   */
  private function getPrizeName($activityId){
    $m = M('Company_lucky_prize');
    $result = $m->where("activityId = ".$activityId)->select();
    return $result;
  }


  public function shareList(){
    $sharePrize = M('Company_lucky_prize')->where("activityId = '".I('post.activityId')."'")->select();

    $Model= new \Think\Model();//实例化空模型
    $ecid =  M('Company_activity')->where("id = '".I('post.activityId')."'")->getField('ecid');
    $newsId = I('post.newsId');
    $opt = " log.type = 2 AND log.newsId = {$newsId} AND users.nickname IS NOT NULL AND log.fromOpenId = users.openId AND log.newsId = news.id";
      
    if(I('post.startTime') != '' && I('post.endTime')!='' ){
      $startTime = I('post.startTime');
      $endTime =  date("Y-m-d H:i:s",strtotime(I('post.endTime')));
      $opt .= " AND (log.time >'" .$startTime."' and log.time <'".$endTime."')";
    }

    $count = $Model->query("
      SELECT COUNT(*) 
      FROM (
        SELECT count(*) as num,log.fromOpenId,users.nickname,users.id as userId 
        FROM `sz12365_fw_news_view_log` `log`,`sz12365_fw_company_news` `news`,`sz12365_fw_company_{$ecid}_user_info` `users` 
        WHERE ( {$opt} ) 
        GROUP BY log.fromOpenId
        ) 
      tp_count
    ");// 查询满足要求的总记录数 
    $page  = new \Think\Page( $count[0]['COUNT(*)'] , 10);// 实例化分页类 传入总记录数
    $show  = $page->show();// 分页显示输出
    
    // 进行分页数据查询
    $result = $Model->field('count(*) as num,log.fromOpenId,users.nickname,users.id as userId,log.time')->table(array('sz12365_fw_news_view_log'=>'log','sz12365_fw_company_news'=>'news','sz12365_fw_company_'.$ecid.'_user_info'=>'users'))
            ->where($opt)->order( 'num desc' )->group('log.fromOpenId')->limit( $page->firstRow.','.$page->listRows )->select();


    for($i = 0;$i<count($result);$i++){
      $option = "userId = '".$result[$i]['fromOpenId']."' AND activityId = ".I('post.activityId');

      $res = M('Company_lucky_user')->where($option)->find();
      if($res){
        
        $result[$i]['luckyTime'] = $res['luckyTime'];
      }
    }
    $this->assign('startTime',I('post.startTime'));
    $this->assign('endTime',I('post.endTime'));
    $this->assign('logInfo',$result);
    $this->assign('sharePrize',$sharePrize);
    $this->assign('activityId',I('post.activityId'));
    $this->assign('ecid',$ecid);
    $this->assign('newsId',I('post.newsId'));
    $this->assign( 'page', $show );// 赋值分页输出
    $this->display();
  }
  /**
   * 分配奖品后相关数据的存储
   * @return [type] [description]
   */
  public function sharePrizeHandle(){
    if(!IS_POST) _404 ('页面不存在' , U('index'));
      $m = M('Company_lucky_user');
      $ecid = I('post.ecid');
      $opt = I('post.');
      $opt['userId'] = M('Company_'.$ecid.'_user_info')->where("id = ".I('post.userId'))->getField('openId');
      $opt['luckyTime'] = date('Y-m-d H:i:s');
      $opt['exchangeCode'] = md5($opt['userId']);
      unset($opt['ecid']);
      if($m->add($opt)){
        $prize = M('Company_lucky_prize')->where('id = '.$opt['prizeId'])->find();
        $prizeSave['luckyCount'] = (int)($prize['luckyCount'])+1;
        M('Company_lucky_prize')->where('id = '.$opt['prizeId'])->save($prizeSave);
        $this->sendCustomMessage( $ecid , I('post.activityId') , $opt['userId'] , I('post.userId'));
        $data['status'] = Error::SUCCESS_OK;
      }
      else{
        $data['status'] = Error::ERROR_GENERAL;
        $data['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
      }
      
      $this->ajaxReturn($data , 'JSON');

  }
    /**
   * 分配奖品处理
   * @author 蒋东芸
   * 修改历史： 1、 蒋东芸 2014-08-22 创建函数
   */
  public function prizeDistributeHandle(){
    if(!IS_POST) _404 ('页面不存在' , U('index'));

      $m = M('Company_lucky_user');
      $ecid = I('post.ecid');
      $opt = I('post.');

      $luckyNum = I('post.luckyNum');

      $options['ecid'] = array('eq',$ecid);
      $options['activityId'] = array('eq',I('post.activityId'));

      $user =$this->getConditionLuckyUser(I('post.activityId'),I('post.prizeId'),$ecid);
      if($user){
        $options['userId'] = array('not in',$user);//排除已经中奖的人
      }
      $startTime =I('post.startTime');
      $endTime = date("Y-m-d H:i:s",strtotime(I('post.endTime'))+'86400');

      if(I('post.activityType') == 'share'){
        $result =  M()->distinct(true)->field('users.id as userId')->table(array('sz12365_fw_news_view_log'=>'log','sz12365_fw_company_200082_user_info'=>'users'))
            ->where("newsId = ".I('post.newsId')." and type=2 and ecid=".$ecid." and log.fromOpenId =users.openId and (log.time between '" .$startTime."' and '".$endTime."')")->select();
      }else{
        $options['joinTime'] = array('between',array($startTime,$endTime));
        $options['hasLuckyChange'] = array('eq',1);
        $result = M('Company_activity_join')->where($options)->select();
      }
      
      shuffle($result);
      unset($opt['newsId']);
      unset($opt['activityType']);
      if($luckyNum<=count($result)){
        for($i=0;$i<$luckyNum;$i++){
          $userId  = $result[$i]['userId'];
          $opt['userId'] = M('Company_'.$ecid.'_user_info')->where("id = ".$userId)->getField('openId');
          $opt['luckyTime'] = date('Y-m-d H:i:s');
          $opt['exchangeCode'] = md5($opt['userId']);
          unset($opt['ecid']);
          unset($opt['luckyNum']);
          unset($opt['startTime']);
          unset($opt['endTime']);
          if($m->add($opt)){
            $prize = M('Company_lucky_prize')->where('id = '.$opt['prizeId'])->find();
            $prizeSave['luckyCount'] = (int)($prize['luckyCount'])+1;
            M('Company_lucky_prize')->where('id = '.$opt['prizeId'])->save($prizeSave);
            $this->sendCustomMessage( $ecid , I('post.activityId') , $opt['userId'] ,$result[$i]['userId']);
            $data['status'] = Error::SUCCESS_OK;
          }
          else{
            $data['status'] = Error::ERROR_GENERAL;
            $data['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
          }
        }
      }else{
        if(count($result)==0){
          $data['status'] = Error::ERROR_GENERAL;
          $data['info'] = "没有满足条件的用户";
        }else{
          $data['status'] = Error::ERROR_GENERAL;
          $data['info'] = "请填写小于".count($result)."的数字";
        }          
      }
      $this->ajaxReturn($data , 'JSON');
  }

  /**
   * 获取满足条件下的中奖用户id数组
   * @return 获取满足条件中奖人id
   * @author 蒋东芸
   * 1、修改历史 2014-08-26创建
   */
  private function getConditionLuckyUser($activityId,$prizeId,$ecid){
    $opt['activityId'] = $activityId;
    $opt['prizeId'] = $prizeId;
    $result = M('Company_lucky_user')->where($opt)->select();
    for($i=0;$i<count($result);$i++){
      $user[$i] = M('Company_'.$ecid.'_user_info')->where("openId ='".$result[$i]['userId']."'")->getField('id'); 
    }
    return $user;
  }


  private function commentListcount($id){
    $m=M('Company_activity_msg');
    $option['activityId'] = $id;
    $option['comform']=0;
    
    $count=$m->where($option)->count();

    return $count;
  }
  /**
   * 分页显示以及将获取的字段内容赋予标识commentList
   * @author 蒋东芸
   */
  public function commentsList(){
    $activityId = I('post.activityId');//获取opst传递过来的activityId参数
    $ecid=session( $this->_userCfg["ECID"]);
    $Model= new \Think\Model();//实例化空模型
    $Comments = $Model->query("
      SELECT 
      company_activity_msg.msg,
      company_activity_msg.comform,
      company_activity_msg.openId,
      company_activity_msg.id,
      activity_user.user_name
      FROM sz12365_fw_company_{$ecid}_activity_user AS activity_user
       JOIN sz12365_fw_company_activity_msg AS company_activity_msg ON 
      activity_user.openId=company_activity_msg.openId AND company_activity_msg.activityId = {$activityId}
      ORDER BY comform
      "); //进行原生的SQL查询
   
    $this->assign('activityId',$activityId);
    $this->assign('commetsList',$Comments);//将得到的数据赋值给标识commetsList
    $this->assign('divName',I('post.divName'));//将post过来的divName参数赋值给标识divName
    $this->display();//系统默认输出对应模板
  }

  /**
   * 评论通过审核
   * @author 蒋东芸
   */
  public function passHandle(){
    $opt['id']=I('post.id');//获取opst传递过来的id参数
    $m=M('Company_activity_msg');//实例化Company_activity_msg对象

    $result = $m->where($opt)->find();

    if($result['comform']==0)//判断字段comform是否等于零
    {
      $result['comform']=1;//如果comform等于零将1赋给它
      if($m->where($opt)->save($result))//判断是否保存成功
      {
        //保存成功就返回‘操作成功’信息
        $result["status"] = Error::SUCCESS_OK;
        $result["info"] = Error::getErrMsg(Error::SUCCESS_OK);
        
      }else{
        //保存失败就返回‘一般错误’信息
        $result["status"] = Error::ERROR_GENERAL;
        $result["info"] = Error::getErrMsg(Error::ERROR_GENERAL);
        }
    }
    $this->ajaxReturn($result , "JSON");
  }
 
  /**
   * 删除评论
   * @author 蒋东芸
   */
  public function delHandle(){
    $opt['id']=I('post.id');//获取opst传递过来的id参数
    $m= M('Company_activity_msg');//实例化Company_activity_msg对象
    
    if($m->where($opt)->delete())//判断满足条件的数据是否删除
    {
        $result["status"] = Error::SUCCESS_OK;
        $result["info"] = Error::getErrMsg(Error::SUCCESS_OK);
        //保存成功就返回‘操作成功’信息
    }else{
        $result["status"] = Error::ERROR_GENERAL;
        $result["info"] = Error::getErrMsg(Error::ERROR_GENERAL);
        //保存失败就返回‘一般错误’信息
    }
    $this->ajaxReturn($result , "JSON");
  }
  public function userList(){
    //分页
        // 导入分页类
        $ecid=session( $this->_userCfg["ECID"]);
        $Data = M('Company_'.$ecid.'_activity_user');
        
      

        $option['activityId'] = I('post.id');

        $count      = $Data->where($option)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 15 );// 实例化分页类 传入总记录数
        
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($option)->order( 'id DESC' )->limit( $page->firstRow.','.$page->listRows )->select();

    for($i = 0;$i<count($result);$i++){
      switch ($result[$i]['sex']) {
                case 1:
                    $result[$i]['sex'] = '男';
                    break;

                case 0:
                    $result[$i]['sex'] = '女';
                    break;
            }
    }

    $this->assign('activityId' , I('post.id'));
    $this->assign( 'page', $show );// 赋值分页输出
    $this->assign('userItem' , $result);
    $this->display();
  }


  public function editUser(){
    $ecid=session( $this->_userCfg["ECID"]);
    if(I('post.id') == 'add'){
      $result = array(
        "activityId" => I('post.activityId') ,
        "user_name" => I('post.user_name') ,
        "sex" => I('post.sex') ,
        "company" => I('post.company') ,
        "handleType" => "add"
        );
    }else{
      $result = M('Company_'.$ecid.'_activity_user')->where('id='.I('post.id'))->find();
      $result['handleType'] = "edit";
    }

    $this->assign("userItem" , $result);
    $this->display();
  }

  public function delUser(){
    if(!IS_POST)_404 ('页面不存在' , U('index'));
    $opt['id'] = array('in',I('post.val'));
    $ecid = session( $this->_userCfg["ECID"]);

      $obj = M("Company_".$ecid."_activity_user")->where($opt)->delete();
      if($obj){
        $result["status"] = Error::SUCCESS_OK;
      }else{
        $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
      }
    $this->ajaxReturn($result ,"JSON" );
  }

  public function QrUserHandle(){
    if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
        $condition['user_name']=I('post.user_name');
        $opt['activityId'] = I('post.activityId');
        $opt['user_name']=I('post.user_name');
        $opt['sex']=I('post.sex');
        $opt['company']=I('post.company');
        $opt['tel']=I('post.tel');
        $ecid=session( $this->_userCfg["ECID"]);

        $m = M('Company_'.$ecid.'_activity_user');
        $res=$m->where($condition)->find();
        if($res){
          $result["status"] = Error::ERROR_CONTENT_FALSE;
          $result["info"] = Error::getErrMsg(Error::ERROR_CONTENT_FALSE);
          }
        if(I('post.handleType') == "add"){
          if($m->add($opt)){
            $result["status"] = Error::SUCCESS_OK;
          }else{
            $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
              $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
          }
        }else if(I('post.handleType') == "edit"){
          if($m->where('id='.I('post.id'))->save($opt)){
            $result["status"] = Error::SUCCESS_OK;
          }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
              $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
          }
        }else if(I('post.handleType') == "delete"){
          if($m->where("id = '".I('post.id')."'")->delete()){
            $result["status"] = Error::SUCCESS_OK;
          }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
              $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
          }
        }
      
        $this->ajaxReturn($result , "JSON");
  }

  /**
   * uploadFile  读取已上传EXCEL文件并导入数据库
   * @return [array]
   * @author 范小宝
   */
  public function uploadFile(){
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'User' ) );
        $ecid=session( $this->_userCfg["ECID"]);
        

        import("Home.PHPExcel.PHPExcel.IOFactory");
        import("@.PHPExcel.PHPExcel");
        import("@.PHPExcel.PHPExcel.IOFactory");
        import("@.PHPExcel.PHPExcel.Reader.Excel5");
        import("@.PHPExcel.PHPExcel.Reader.Excel2007");

        $objReader = \PHPExcel_IOFactory::createReader(I('post.fileType'));
        $objPHPExcel = $objReader->load($_SERVER[DOCUMENT_ROOT] . __ROOT__ . "/Public/uploads/".I('post.fileName'));

        $sheet = $objPHPExcel->getSheet(0);

        $highestRow = $sheet->getHighestRow(); // 取得总行数

        $highestColumn = $sheet->getHighestColumn(); // 取得总列数

        //判断列名是否正确
        if(trim($objPHPExcel->getActiveSheet()->getCell("A1")->getValue()) != "姓名" || trim($objPHPExcel->getActiveSheet()->getCell("B1")->getValue()) != "性别" || trim($objPHPExcel->getActiveSheet()->getCell("C1")->getValue()) != "公司名称" || trim($objPHPExcel->getActiveSheet()->getCell("D1")->getValue()) != "联系电话"){
          $result["status"] = Error::ERROR_QR_FILE_COLUMN_ERROR;
          $result["info"] = Error::getErrMsg(Error::ERROR_QR_FILE_COLUMN_ERROR);
          $this->ajaxReturn($result , "JSON");
        }
        
        $telNum = 0;
        //循环读取excel文件,读取一条,插入一条
        for($i=2;$i<=$highestRow;$i++)
        {
            $data = null;

            $sex = trim($objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue());

            switch ($sex) {
                case '男':
                    $sex = 1;
                    break;

                case '女':
                    $sex = 0;
                    break;
                
                default:
                    $sex = 0;
                    break;
            }

            $tel = trim($objPHPExcel->getActiveSheet()->getCell("D".$i)->getValue());
           
            if(!is_numeric($tel)){//判断输入电话是否为数字
                $telNum++;
            }

            $data = array(
                "activityId" => I('post.activityId') , 
                "user_name" => trim($objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue()) , 
                "sex" => $sex , 
                "company" => trim($objPHPExcel->getActiveSheet()->getCell("C".$i)->getValue()) , 
                "tel" => $tel, 
                );

            if($data['user_name'] != '' && $telNum == 0 ){
              M('Company_'.$ecid.'_activity_user')->data($data)->add();
            }
        }
        
        $result['telNum'] = $telNum;
        $result["status"] = Error::SUCCESS_OK;
        $this->ajaxReturn($result , "JSON");
    }

  public function luckyRule(){
    $result = M("View_company_activity")->where("id = '".I('post.id')."'")->find();

    $this->assign('item' , $result);
    $this->display();
  }

  public function luckyReply(){
    $this->assign("activity" , M("View_company_activity")->where("id = '".I('post.id')."'")->find());
    $this->display();
  }

  public function ruleHandle(){
    if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        //判断中奖号码是否合法
        $opt['id']=I('post.id');
        $opt['baseNumber']=I('post.baseNumber');
        $opt['luckyNum']=I('post.luckyNum');
        if($this->isLuckyNumOK($opt)){
      if(M("Company_activity")->save($opt)){
            $result["status"] = Error::SUCCESS_OK;
          }else{
              $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
              $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
          }
        }

        $this->ajaxReturn($result , "JSON");
  }

  public function replyHandle(){
    if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        
          $opt['id']=I('post.id');
          $opt['luckyReply']=I('post.luckyReply');
          $opt['unluckyReply']=I('post.unluckyReply');
        if(M("Company_activity")->save($opt)){
          $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }

        $this->ajaxReturn($result , "JSON");
  }

  private function isLuckyNumOK($data){
    

    if($data['luckyNum'] == 0 || $data['baseNumber'] == 0){
      $result["status"] = Error::ERROR_LUCKY_LUCKYNUM_NULL;
            $result["info"] = Error::getErrMsg(Error::ERROR_LUCKY_LUCKYNUM_NULL);
            $this->ajaxReturn($result , "JSON");
    }

    $activity = M("View_company_activity")->where("id = '".$data['id']."'")->find();

        if($data['luckyNum'] <= $activity['userCount']){
          $result["status"] = Error::ERROR_LUCKY_LUCKYNUM_OVER;
            $result["info"] = Error::getErrMsg(Error::ERROR_LUCKY_LUCKYNUM_OVER);
            $this->ajaxReturn($result , "JSON");
        }else{
          return true;
        }
  }

  private function getQrPicUrl($ecid , $sceneId){
        $token = $this->getAppToken($ecid);

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            return;
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
          $qrCode = $weObj->getQRCode($sceneId, 1);
            if($qrCode){
                return $weObj->getQRUrl($qrCode['ticket']);
            }
        }
  }

  private function addQrType($typeName , $activityId , $ecid){
    $m = M('Company_qr_type');

    $opt['typeName'] = $typeName;
    $typeZone = M("Company_qr_zone")->where($opt)->find();

    $opt = null;
    $opt['ecid'] = $ecid;
    $opt['scene_id'] = array('between' , $typeZone['MinScene'].','.$typeZone['MaxScene']);
    
    $maxSceneId = $m->where($opt)->max('scene_id');
    
    if($maxSceneId != ''){
      if($maxSceneId == $typeZone['MaxScene']){
        $opt['scene_id'] = $typeZone['MinScene'];
      }else{
        $opt['scene_id'] = $maxSceneId + 1;
      }
    }
    else
      $opt['scene_id'] = $typeZone['MinScene'];
    
    $opt['modifyUserId'] = session($this->_userCfg['UID']);
    $opt['modifyTime'] = date("Y-m-d H:i:s");

    if($typeName == 'question'){
      $opt['type'] = $typeName;
      $opt['activityId'] = $activityId;
    }else{
      $opt['type'] = 'activity';
      $opt['activityId'] = $activityId;
    }

    return $m->add($opt);
  }

  private function getActivityType(){
    $m = M('Company_activity_type');

    $result = $m->select();

    if($result)
      return $result;
  }
  private function getBrand(){
    $m = M('Company_brand');
    $opt['ecid'] = session('ecid');

    $result = $m->where($opt)->select();

    if($result)
      return $result;
  }

  public function material(){     
   $this->display();
  }

  public function AddMaterialHandle(){
    $option['text']=I('post.text');
    $option['activityId']=I('post.activityId');
    $option['type']='text';


    $m = M('Company_qr_sign_sendmessage');

    $opt['addUserId'] = session($this->_userCfg['UID']);
    $opt['addTime'] = date("Y-m-d H:i:s");
    $opt['isSend'] = '0';
    $opt['type']=I('post.type');
    $opt['activityId']=I('post.activityId');
    $opt['materialId']=I('post.materialId');
    $opt['text']=I('post.text');
  

  if(I('post.type')=='text'){
    $res=$m->where($option)->find();
    if(!$res){
      $status = $m->add($opt);
      if($status > 0){
        $result["data"] = Error::SUCCESS_OK;
        $result["info"] = Error::getErrMsg(Error::SUCCESS_OK);
      }
      else{
        $result["data"] = Error::ERROR_GENERAL;
        $result["info"] = Error::getErrMsg(Error::ERROR_GENERAL);
      }
    }else{
      $result["data"] = Error::ERROR_CONTENT_FALSE;
      $result["info"] = Error::getErrMsg(Error::ERROR_CONTENT_FALSE);
    }
  }else if(I('post.type')=='news')
  {
      $condition['addUserId'] = session($this->_userCfg['UID']);
      $condition['addTime'] = date("Y-m-d H:i:s");
      $condition['isSend'] = '0';
      $condition['type']=I('post.type');
      $condition['activityId']=I('post.activityId');
      $condition['materialId']=I('post.materialId');
      $condition['text']='';
    $status = $m->add($condition);
      if($status > 0){
        $result["data"] = Error::SUCCESS_OK;
        $result["info"] = Error::getErrMsg(Error::SUCCESS_OK);
      }
      else{
        $result["data"] = Error::ERROR_GENERAL;
        $result["info"] = Error::getErrMsg(Error::ERROR_GENERAL);
      }
  }

    
    $this->ajaxReturn($result, "JSON");
  }

  private function getTypeName($type){
    switch ($type) {
      case 'text':
        return '文本信息';
        break;
      case 'news':
        return '图文信息';
        break;
    }

  }

  /**
   * 问答活动问题管理界面
   *@author 黄浩
   * 1、修改历史 2014-08-01创建
   */
 public function question(){ 
    $m = M('Company_activity_question');
    $opt = $m->where('activityId = '.I('post.activityId'))->find();
    $obj = $opt['question'];
    $arr = str_replace("\"","'",$obj);//将双引号替换成单引号  
    $this->assign('arr',$arr);
    $this->assign('activityId',I('post.activityId'));
    $this->assign('id',$opt['id']);
    $this->assign('question',json_decode($obj,true));
    
    if(session('theme')){
        $this->theme('nifty')->display();
    }else{
        $this->display();
    }  
  }

  /**
   * 问答活动问题编辑显示界面
   * @access public
   * @author 黄浩
   * 1、修改历史 2014-08-06创建
   */
  public function editQuestion(){
    $result = I('post.');
    $this->assign('questionContent',$result['question']);
    $this->assign('num',$result['k']);
    $this->display();
  }

  /**
   * 问答活动问题添加
   * @author 黄浩
   * 1、修改历史 2014-08-06创建
   */
  public function questionHandle(){
    if(!IS_POST) _404 ('页面不存在' , U('index'));
      
      $m = M('Company_activity_question');
      $opt = I('post.','',false);

      if($opt['handleType'] =='add'){
        unset($opt['handleType']);//去除数据中的handleType字段
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $obj = $m->where('activityId ='.I('post.activityId'))->find();
        if($obj){
          if($m->where('activityId ='.I('post.activityId'))->save($opt)){
            $result["status"] = Error::SUCCESS_OK; 
          }else{
            $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
          }
        }else{
          if($m->add($opt)){
            $result["status"] = Error::SUCCESS_OK;
          }else{
            $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
          }
        }
       }    
    $this->ajaxReturn($result,"JSON");
  }


  /**
   * 发送登记信息
   * @author 黄浩
   * 修改历史 1、2014-08-19 创建
   */
  public function sendMessage(){
    $ecid = M('Company_activity')->where('id = '.I('post.id'))->getField('ecid');
    $opt = " activityId = '".I('post.id')."'";

    if(I('post.record') == 'recorded'){
      $opt .= " AND realName IS NOT NULL";
    }
    if(I('post.record') == 'unrecorded'){
      $opt .= " AND realName IS NULL";
    }

    $result = M("View_lucky_user_{$ecid}")->where($opt)->select();
    
    $num = 0;
    $data['code'] = $num;
    for($i=0;$i<count($result);$i++){
      $openId = $result[$i]['openId'];
      $userId = M("Company_{$ecid}_user_info")->where("openId = '".$openId."'")->getField('id');
      $nickname = M("Company_{$ecid}_user_info")->where("openId = '".$openId."'")->getField('nickname');
      $obj = $this->sendLuckyMeg($ecid , I('post.id') , $openId , $userId);
      if(!$obj){
        $data['item'][$num] = array(
          'openId'=>$openId ,
          'userId'=>$userId,
          'nicknmae'=>$nickname
          );
        $data['code'] = $num+1;
      }
    }   
    $this->ajaxReturn($data,"JSON");
  }

  /**
   * 通过模板消息发送登记信息
   * @author 李少年
   * 修改历史 1、2014-09-09 创建
   */
  public function sendMessageByTemplate(){
    $ecid = M('Company_activity')->where('id = '.I('post.id'))->getField('ecid');
    $opt = " activityId = '".I('post.id')."'";

    if(I('post.record') == 'recorded'){
      $opt .= " AND realName IS NOT NULL";
    }
    if(I('post.record') == 'unrecorded'){
      $opt .= " AND realName IS NULL";
    }

    $result = M("View_lucky_user_{$ecid}")->where($opt)->select();
    
    $num = 0;
    $data['code'] = $num;
    for($i=0;$i<count($result);$i++){
      $openId = $result[$i]['openId'];
      $userId = M("Company_{$ecid}_user_info")->where("openId = '".$openId."'")->getField('id');
      $nickname = M("Company_{$ecid}_user_info")->where("openId = '".$openId."'")->getField('nickname');
      $obj = $this->sendCustomMessage($ecid , I('post.id') , $openId , $userId);
      if(!$obj){
        $data['item'][$num] = array(
          'openId'=>$openId ,
          'userId'=>$userId,
          'nicknmae'=>$nickname
          );
        $data['code'] = $num+1;
      }
    }   
    $this->ajaxReturn($data,"JSON");
  }

  /**
   * 生成中奖人员CSV文件
   * @author 黄浩 2014-08-27 创建
   */
  public function exportLuckyUser(){
    $ecid = M('Company_activity')->where("id = ".I('get.activityId')."")->getField('ecid');
    $result = M("View_lucky_user_{$ecid}")->where("activityId = ".I('get.activityId')."")->select();
    $str = "微信号,中奖时间,奖项,奖品,姓名,地址,电话,身份证姓名,身份证号码,身份证地址\n";//首行内容
    $str = iconv("utf-8","gb2312",$str); //中文转码   
    $data = mysql_query("select * FROM sz12365_fw_View_lucky_user_{$ecid} where activityId = ".I('get.activityId')." group by openId");

    while($row = mysql_fetch_array($data)){//循环输入各字段值 
      $identify = json_decode($row['exdata'],true);//转换数据为数组格式
      $nickName = iconv('utf-8','gb2312',$row['nickname']); //中文转码   
      $luckyTime = iconv('utf-8','gb2312',$row['luckyTime']); 
      $prizeName =iconv('utf-8','gb2312',$row['prizeName']);
      $prizeType = iconv('utf-8','gb2312',$row['prizeType']);
      $realName = iconv('utf-8','gb2312',$row['realName']);
      $address = iconv('utf-8','gb2312',$row['address']);
      $identifyName = iconv('utf-8','gb2312',$identify['identifyName']);
      $identifyNum = iconv('utf-8','gb2312',$identify['identifyNum']);
      $identifyAddress = iconv('utf-8','gb2312',$identify['identifyAddress']);
      //循环每一行内容
      $str .= $nickName.",".$luckyTime.",".$prizeName.",".$prizeType.",".$realName.",".$address.",".$row['tel'].",".$identifyName.","."\t".$identifyNum.",".$identifyAddress."\n";  
      
    }

    $filename = date('Y-m-d')."中奖人员信息".'.csv'; //设置文件名  
    $this->export_csv($filename,$str);
  }

  /**
   * 下载中奖人员信息
   * @param   $filename 文件名
   * @param   $data     中奖信息
   * @return 下载页面   
   * @author 黄浩 2014-8-27 创建     
   */
  public  function export_csv($filename,$data) {
    header("Content-type:text/csv");    
    header("Content-Disposition:attachment;filename=".$filename); 
    header('Cache-Control:must-revalidate,post-check=0,pre-check=0'); 
    header('Expires:0'); 
    header('Pragma:public');
    echo $data;  
  }
  /**
   * 下载分享参与人员信息
   * @author 蒋东芸 2014-9-01 创建 
   */
  public function exportShareInfo(){
    $activityId = I('get.activityId');
    $ecid = M('Company_activity')->where("id = '".$activityId."'")->getField('ecid');
    $newsId = I('get.newsId');
    $str = "微信号,分享次数,中奖时间\n";//首行内容
    $str = iconv("utf-8","gb2312",$str); //中文转码   
    $data = mysql_query("
      SELECT count(*) as num,log.fromOpenId,users.nickname,users.id as userId,luckyUser.luckyTime 
        FROM `sz12365_fw_news_view_log` `log`
        JOIN`sz12365_fw_company_{$ecid}_user_info` `users` 
        ON( log.type = 2 AND log.newsId = {$newsId} AND users.nickname IS NOT NULL AND log.fromOpenId = users.openId )
        LEFT JOIN `sz12365_fw_company_lucky_user` `luckyUser` 
        ON log.fromOpenId=luckyUser.userId AND activityId = {$activityId}
        GROUP BY log.fromOpenId
        ORDER BY num desc;
        ");
    while($row = mysql_fetch_array($data)){//循环输入各字段值 
      $nickName = iconv('utf-8','GB18030',$row['nickname']); //中文转码   
      $num = iconv('utf-8','gb2312',$row['num']); 
      $luckyTime =iconv('utf-8','gb2312',$row['luckyTime']);
      //循环每一行内容
      $str .= $nickName.",".$num.",".$luckyTime."\n";  
      
    }

    $filename = date('Y-m-d')."分享活动参与人员信息".'.csv'; //设置文件名  
    $this->export_csv($filename,$str);
  }
  

  /**
   * 问答活动中问题编辑处理
   * @author 黄浩
   * 1、修改历史 2014-08-10创建
   */
  public function editHandle(){
    if(!IS_POST) _404 ('页面不存在' , U('index'));
    $opt['question'] = I('post.arr',"",false);//取消数据过滤。
    $opt['modifyUserId'] = session($this->_userCfg['UID']);
    $opt['modifyTime'] = date("Y-m-d H:i:s");
    $m = M('Company_activity_question');
    
      if($m->where('id ='.I('post.id'))->save($opt)){
        $result["status"] = Error::SUCCESS_OK;
      }else{
        $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
        $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
      }
  
    $this->ajaxReturn($result,"JSON");
  }


  public function messageList(){
    $opt['activityId']=I('activityId');
    //分页
    import('ORG.Util.Page');//导入分页类
    $Data =  M('Company_qr_sign_sendmessage');

    $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
    
    $page       = new \Think\Page( $count , 5);// 实例化分页类 传入总记录数
    
    $show       = $page->show();// 分页显示输出
    // 进行分页数据查询
    $result = $Data->where($opt)->order( 'addTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();

    if($result){
        for($i=0; $i<count($result); $i++){

            $result[$i]['TypeName'] = $this->getTypeName($result[$i]['type']);

            $newsId=$this->getnewsId($result[$i]['materialId']);

            $newsRow = null;
                for ( $j = 0;$j<count( $newsId );$j++ ) {
                    $newsResult = M( "Company_news" )->where( 'id = ' . $newsId[$j] )->find();

                    $newsRow[$j] = array(
                        'title' => $newsResult['title'],
                        'newsImg' => ( $j==0 )?$newsResult['bigImg']:$newsResult['minImg']
                    );
                }
                if(count($newsRow) > 1)
                    $result[$i]['multiple'] = 1;
                else
                    $result[$i]['multiple'] = 0;

            $result[$i]['Content'] = $newsRow;

        }
    }
   
   
    $this->assign('message', $result);
    $this->assign( 'page', $show );// 赋值分页输出
    $this->display();
  }
  private function getnewsId($newsId){

    $m= M('Company_material_group');

    $opt['id']=$newsId;

    $res=$m->where($opt)->find();

    $group_newsId= explode(",", $res['materialId']);

    return $group_newsId;

  }
  /**
   * 获取company_news素材表信息
   * @param  [type] $newsId [素材id]
   * @return [type]         [返回素材所有信息]
   * @author 蒋东芸 
   * @date:2014/8/12
   */
  private function getnewsInfo($newsId){
    $N = M("Company_news");
    $opt['id'] = $newsId;
    $result=$N->where($opt)->find();
    return $result;
  }

}

?>