<?php
namespace Admin\Action;
use Org\Error\Error;
class StatisticsAction extends AdminAction {
    public function index() {
        //设置默认时间为一星期
        $startTime=date("Y-m-d",strtotime("-1 week"));
        $endTime=date("Y-m-d",strtotime("-1 day"));

        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $ecid=session( $this->_userCfg['ECID'] );
        //显示品牌
        $m=M('Company_brand');
        $fw_log=M('Company_fw_log');

        $opt['fromEcid'] = $ecid;

        //显示类型和经销商
        $result=$fw_log->distinct(true)->field('dealerName')->where($opt)->select();
        $type=$fw_log->distinct(true)->field('fwType')->where($opt)->select();

        //英文转化为中文
        for($i=0;$i<count($type);$i++){
            $res[$i]['fwType']=$this->getfwType($type[$i]['fwType']);
        }

        $data['ecid'] = $opt['ecid'];
        $brandInfo=$m->where($data)->select();

        $this->assign('startTime',$startTime);
        $this->assign('endTime',$endTime);
        
        $this->assign('brandInfo',$brandInfo);
        $this->assign('fwtype',$res);
        $this->assign('dealername',$result);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }  
    }

    public function showProductSel(){
        $brandId = I('post.brandId');

        $m = M('Company_product');
        $opt['brandId'] = $brandId;

        $result = $m->where($opt)->select();

        $this->assign('productInfo', $result);
        $this->assign('val', I('post.val'));

        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    public function ProductHandle(){
        $ecid = session( $this->_userCfg['ECID'] );
        $startTime = I('post.startTime');
        
        $end = I('post.endTime');
        $endTime=date("Y-m-d",strtotime("+1 day",strtotime($end)));

        if(!$startTime || !$end){
            echo '<p>非法请求！</p>';
        }
            

        $m=M('Company_fw_log');

        $opt = array();
        $opt['fromEcid'] = $ecid;

        //如果有品牌ID，则设定品牌ID条件
        if(I('post.brandId') && I('post.brandId') != '-1')
            $opt['brandId'] = I('post.brandId');

        //如果有产品ID，则设定产品ID条件
        if(I('post.productId') && I('post.productId') != '-1')
            $opt['productId'] = I('post.productId');

        //如果有类型，则设类型为条件
        if(I('post.Type')&&I('post.Type')!='')
            $opt['fwType']=I('post.Type');

        //如果有经销商，则设经销商为条件
        if(I('post.dealerName')&&I('post.dealerName')!='')
            $opt['dealerName']=I('post.dealerName');
        
        if(I('post.Type')&&I('post.dealerName')&&I('post.brandId')&&I('post.productId')){
            $opt['fwType']=I('post.Type');
            $opt['dealerName']=I('post.dealerName');
            $opt['brandId'] = I('post.brandId');
            $opt['productId'] = I('post.productId');
            
        }

       
        $opt['time'] = array(array('EGT',$startTime),array('LT',$endTime));
        $count      = $m->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 20 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result = $m->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        
        for($i=0; $i<count($result); $i++){
            $result[$i]['fwType']=$this->getfwType($result[$i]['fwType']);
            $result[$i]['nickname']=$this->getNickname($result[$i]['openId']);
        }
        $this->assign('result',$result);
        $this->assign( 'page', $show );// 赋值分页输出
        $this->display();
    }

    //TO DO--要修改为从用户表取数据
    private function getNickname($openId){
        $Model= new \Think\Model();//实例化空模型
        $ecid = session( $this->_userCfg['ECID'] );

        $result = $Model->field('info.nickname')->table(array('sz12365_fw_company_'.$ecid.'_fw_log'=>'log','sz12365_fw_company_'.$ecid.'_user_info'=>'info'))
                ->where("log.openId = '{$openId}' AND log.openId = info.openId" )->find();

        return $result['nickname'];
    }
    
    //TO DO--要简化查询的表
    private function getDealer($wlCode){
        $Model= new \Think\Model();//实例化空模型

        $ecid = session( $this->_userCfg['ECID'] );
        $result = $Model->field('dealers.name')->table(array('sz12365_fw_company_'.$ecid.'_fw_log'=>'fw_log','sz12365_fw_company_wl_log'=>'log','sz12365_fw_company_wl_order'=>'order', "sz12365_fw_company_dealers" => 'dealers'))
                ->where("fw_log.wlCode = '{$wlCode}' AND fw_log.wlCode = log.wl_Code AND log.orderId = order.id AND order.sellerId = dealers.id" )->find();
        
        return $result['name'];

    } 

    private function getfwType($fwType){
        switch ($fwType) {
            case 'first':
                return '正确查询';
                break;
            case 'repert':
                return '重复查询';
                break;
            case 'error':
                return '错误查询';
                break;
            default :
                return '';
                break;
        }
    } 

    
    /**
     * 将获取到的值赋予标识
     * @return [type] [description]
     * @author 蒋东芸
     * 2014/5/15
     */
    public function qrCodeStatistics(){
        $m = M('Company_qr_zone');
        $result = $m->select();
        //将获取到的类型赋予标识
        for($i=0;$i<count($result);$i++){
            $result[$i]['type'] = $this->qrCodeTypeHandle($result[$i]['typeName']);
            $this->assign('qrCode',$result);
        }
        //将获取到的起始日期和截至日期赋予标识
        $startTime=date("Y-m-d",strtotime("-1 week"));
        $endTime=date("Y-m-d",strtotime("-1 day"));

        $this->assign('startTime',$startTime);
        $this->assign('endTime',$endTime);

        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }
    /**
     * 将获取到的类型转换成中文
     * @param  varchar $typeName 二维码类型
     * @author 蒋东芸
     * 2014/5/15
     */
    private function qrCodeTypeHandle($typeName){
        switch ($typeName) {
            case 'sign':
                return '签到类型';
                break;
            case 'fwLabel':
                return '防伪有奖';
                break;
            case 'employees':
                return '员工类型';
                break;
            case 'product':
                return '产品类型';
                break;
            case 'dealer':
                return '经销商';
                break;
            case 'scanLucky':
                return '扫描有奖';
                break;
            default :
                return '';
                break;
        }
        
    }
    /**
     * 获取二维码统计详情
     * @author 蒋东芸
     * 2014/5/15
     */
    public function qrCodeList(){
        $ecid= session( $this->_userCfg['ECID'] );//获取企业id
        $Model= new \Think\Model();//实例化空模型
        //获取起始日期和截至日期
        if($_POST['startTime']==''||$_POST['endTime']==''){
            $startDate = date("Y-m-d",strtotime("-1 week"));
            $endDate = date("Y-m-d",strtotime("-1 day"));

        }else{
            $startDate = I('post.startTime');
            $endDate = I('post.endTime');
        }
        $type = I('post.typeName');//获取post传递过来的类型值

        $opt = $this->qrCodeTypeDetail($type,$ecid,$startDate,$endDate);//将获取二维码类型名称的条件数组赋值

        $count = $Model->field($opt['field'])->table($opt['table'])
            ->where($opt['option'])->count();//计算行数
        
        $page       = new \Think\Page( $count , 15 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
         $result = $Model->field($opt['field'])->table($opt['table'])
            ->where($opt['option'])->limit( $page->firstRow.','.$page->listRows )->select();

        if($type == 'all'|| $type ==''){
            for($i=0;$i<count($result);$i++){
                $result[$i]['name'] = $this->qrCodeTypeHandle($result[$i]['type']);
            }
        }
        
        $this->assign('qrCodeList',$result);
        $this->assign('page',$show);
        $this->display();
    }
    /**
     * 获取二维码类型名称的条件数组
     * @param  varchar $type   扫描的二维码类型
     * @param  int $ecid      企业id
     * @param  date $startDate 起始时间
     * @param  date $endDate   截至时间
     * @return [type]            数组
     * @author 蒋东芸
     * 2014/5/15
     */
    private function qrCodeTypeDetail($type,$ecid,$startDate,$endDate){
        switch ($type) {
            case 'sign':
            case 'scanLucky':
                $result['table'] = array('sz12365_fw_qr_scan_log'=>'log','sz12365_fw_company_qr_type'=>'type', "sz12365_fw_company_{$ecid}_user_info" => 'user',"sz12365_fw_company_activity" => 'activity');
                $result['option'] = "log.ecid=type.ecid AND log.sceneId = type.scene_id AND log.ecid = {$ecid} AND log.openid = user.openId  AND type.activityId = activity.id  AND activity.type = '{$type}' AND log.scanTime >= '{$startDate}' AND log.scanTime <= '{$endDate}'" ;
                $result['field'] = 'user.nickname,log.scanTime,activity.name as name';
                break;
            case 'fwLabel':
                $result['table'] = array('sz12365_fw_qr_scan_log'=>'log','sz12365_fw_company_qr_type'=>'type', "sz12365_fw_company_{$ecid}_user_info" => 'user',"sz12365_fw_company_fw_label" => 'label');
                $result['option'] = "log.ecid=type.ecid AND log.sceneId = type.scene_id AND log.ecid = {$ecid} AND log.openid = user.openId AND type.labelId = label.id AND type.type = '{$type}' AND  log.scanTime >= '{$startDate}' AND log.scanTime <= '{$endDate}'" ;
                $result['field'] = 'user.nickname,log.scanTime,label.name as name';
                break;
            case 'employees':
                $result['table'] = array('sz12365_fw_qr_scan_log'=>'log','sz12365_fw_company_qr_type'=>'type', "sz12365_fw_company_{$ecid}_user_info" => 'user',"sz12365_fw_company_employees" => 'employees');
                $result['option'] = "log.ecid=type.ecid AND log.sceneId = type.scene_id AND log.ecid = {$ecid} AND log.openid = user.openId AND type.employeeId = employees.id AND type.type = '{$type}' AND log.scanTime >= '{$startDate}' AND log.scanTime <= '{$endDate}'" ;
                $result['field'] = 'user.nickname,log.scanTime,employees.name as name';
                break;
            case 'product':
                $result['table'] = array('sz12365_fw_qr_scan_log'=>'log','sz12365_fw_company_qr_type'=>'type', "sz12365_fw_company_{$ecid}_user_info" => 'user',"sz12365_fw_company_product" => 'product');
                $result['option'] = "log.ecid=type.ecid AND log.sceneId = type.scene_id AND log.ecid = {$ecid} AND log.openid = user.openId AND type.productId = product.id AND type.type = '{$type}' AND log.scanTime >= '{$startDate}' AND log.scanTime <= '{$endDate}'" ;
                $result['field'] = 'user.nickname,log.scanTime,product.name as name';
                break;
            case 'dealer':
                $result['table'] = array('sz12365_fw_qr_scan_log'=>'log','sz12365_fw_company_qr_type'=>'type', "sz12365_fw_company_{$ecid}_user_info" => 'user',"sz12365_fw_company_dealers" => 'dealer');
                $result['option'] = "log.ecid=type.ecid AND log.sceneId = type.scene_id AND log.ecid = {$ecid} AND log.openid = user.openId  AND type.dealerId = dealer.id AND type.type = '{$type}' AND log.scanTime >= '{$startDate}' AND log.scanTime <= '{$endDate}'" ;
                $result['field'] = 'user.nickname,log.scanTime,dealer.name as name';
                break;
            default:
                $result['table'] = array('sz12365_fw_qr_scan_log'=>'log','sz12365_fw_company_qr_type'=>'type', "sz12365_fw_company_{$ecid}_user_info" => 'user');
                $result['option'] = "log.ecid=type.ecid AND log.sceneId = type.scene_id AND log.ecid = {$ecid} AND log.openid = user.openId AND log.scanTime >= '{$startDate}' AND log.scanTime <= '{$endDate}'"  ;
                $result['field'] = 'user.nickname,log.scanTime,type.type';
                break;
        }
        return $result;
    }
}
?>