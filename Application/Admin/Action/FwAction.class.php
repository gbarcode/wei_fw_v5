<?php
namespace Admin\Action;
use Org\Error\Error;
class FwAction extends AdminAction {
    public function index() {
        if($this->isDataExist('Company_fw_reply_review', $opt))
            $this->assign('hasReview', 1);
        else
            $this->assign('hasReview', 0);

        if(I('get.ecid') != ""){
            if(session($this->_userCfg['ECID']) != '816')$this->Error("用户不存在查看权限");
            $ecid = I('get.ecid');
        }else{
            $ecid = session( $this->_userCfg['ECID'] );
        }

        $company = M("Company_info")->select();

        if(session($this->_userCfg['ECID']) == '816'){
            $this->assign("admin" , true);
            $this->assign("reviewArr" , $this->setResultFromDB("Company_fw_reply_review" , "review" , session( $this->_userCfg['ECID'] )));
        }

        if(I('get.tab') != ''){
            $this->assign('tab' , I('get.tab'));
        }
        //获取审核提示语条数
        $m=M('Company_fw_reply_review');
        $option['pass'] = 0;
    
        $count=$m->where($option)->count();

        $share = M("Company_fw_share")->where("ecid = '".session( $this->_userCfg['ECID'] )."'")->find();
        if($share){
            $this->assign("share" , $share);
        }else{
            $o['ecid']=session($this->_userCfg['ECID']);
            $mod=M("Company_fw_share");
            $row=$mod->add($o);
            $this->assign("share" , $o);
        }

        $this->assign("ecid" , $ecid);
        $this->assign("labelcount",$count);
        $this->assign("company" , $company);
        $this->assign("resultArr" , $this->setResultFromDB("Company_fw_reply" , "default" , $ecid));
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display("new-index");
        }    
    }

    public function fwDealer(){
        $ecid = $_POST['ecid'];
        if($ecid == ""){
            $ecid = session($this->_userCfg['ECID']);
        }

        $result = M("Company_ck_set")->where("ecid = '".$ecid."'")->find();

        if($result == null){
            $result = array(
                "ecid" => $ecid , 
                "dealer_reply" => ""
                );
        }

        $this->assign("dealer" , $result);
        $this->assign("ecid" , $ecid);
        $this->display();
    }

    public function label() {
        if(session($this->_userCfg['ECID']) == '816'){
            $this->assign("admin" , true);

            $opt = "";

            if($_POST['ecid'] != ''){
                $opt['ecid'] = $_POST['ecid'];
            }
        }else{
            $opt['ecid'] = session($this->_userCfg['ECID']);
        }

        $Data = M( "Company_fw_label" );
        
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 9 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign("ecid" , $_POST['ecid']);
        $this->assign("company" , M("Company_info")->select());
        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign("label" , $result);
        
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    public function labelInfo(){
        $ecid = session($this->_userCfg["ECID"]);

        if($_GET['id'] == 'add'){
            $result = array(
                "ecid" => $ecid ,
                "name" => "" ,
                "image" => "" ,
                "info" => "" ,
                "handleType" => "add"
            );
        }else{
            $m = M("Company_fw_label");
            $opt['id'] = I('get.id');
            $result = $m->where($opt)->find();
            $result['handleType'] = 'edit';
            $result['info'] = htmlspecialchars_decode($result['info']);//反编译

            $labelOpt['labelId'] = $result['id'];
            $labelOpt['ecid'] = $result['ecid'];
            $labelId = M("Company_qr_type")->where($labelOpt)->find();
            $result['QrUrl'] = $this->getQrPicUrl($result['ecid'] , $labelId['scene_id']);

            if($ecid == 816 && $result['ecid'] != 816){
                $labelOpt['ecid'] = 816;
                $labelId = M("Company_qr_type")->where($labelOpt)->find();
                $result['adminQrUrl'] = $this->getQrPicUrl($ecid , $labelId['scene_id']);
            }

        }

        if($ecid == '816'){
            $this->assign("admin" , true);
        }

        $this->assign("company" , M("Company_info")->select());
        $this->assign("rand" , rand());
        $this->assign("label" , $result);
        $this->setToken();
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    public function labelHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
        
        $m = M("Company_fw_label");
        $checkName['name']=I('post.name');

        if(I('post.handleType') == 'add'){
            if(!$m->where($checkName)->find()){
                $opt['ecid'] = I('post.ecid');
                $opt['name'] = I('post.name');
                $opt['image'] = I('post.image');
                $opt['info'] = I('post.info');
                $opt['modifyUserId'] = session( $this->_userCfg['UID'] );
                $opt['modifyTime'] = date("Y-m-d H:i:s");

                if($row = $m->add($opt)){
                    //保存sceneid
                    $this->addQrType('fwLabel' , $row , $opt['ecid'] );
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
                }
            }
            else{
                $result["status"] = Error::ERROR_BRAND_TAGS_NAME_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_BRAND_TAGS_NAME_EXIST);
            }
        }else if(I('post.handleType') == 'edit'){
            $opt['id'] = array('neq',I('post.id'));
            $opt['name']=I('post.name');

            if($m->where($opt)->find()){
                $result["status"] = Error::ERROR_BRAND_TAGS_NAME_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_BRAND_TAGS_NAME_EXIST);
            }else{
                $opt =null;
                $opt['name'] = I('post.name');
                $opt['image'] = I('post.image');
                $opt['info'] = I('post.info');
                $opt['modifyUserId'] = session($this->_userCfg['UID']);
                $opt['modifyTime'] = date("Y-m-d H:i:s");
                
                if($m->where('id = '.I('post.id'))->save($opt)){
                    $result['status'] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    private function addQrType($typeName , $labelId , $ecid){
        $m = M('Company_qr_type');

        $opt['ecid'] = $ecid;
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['scene_id'] = $this->getSceneId($typeName , $ecid);
        $opt['type'] = $typeName;
        $opt['labelId'] = $labelId;
        $m->add($opt);

        if($ecid != 816){
            $opt['ecid'] = 816;
            $opt['scene_id'] = $this->getSceneId($typeName , 816);
            $opt['type'] = $typeName;
            $opt['labelId'] = $labelId;
            $m->add($opt);
        }
    }

    private function getSceneId($type , $ecid){
        $m = M('Company_qr_type');

        //取出该类型sceneid范围
        $opt['typeName'] = $type;
        $typeZone = M("Company_qr_zone")->where($opt)->find();

        //查找该ecid最大sceneid值
        $opt = null;
        $opt['ecid'] = $ecid;
        $opt['scene_id'] = array('between' , $typeZone['MinScene'].','.$typeZone['MaxScene']);
        $maxSceneId = $m->where($opt)->max('scene_id');

        if($maxSceneId != ''){
            if($maxSceneId == $typeZone['MaxScene']){
                $sceneId = $typeZone['MinScene'];
            }else{
                $sceneId = $maxSceneId + 1;
            }
        }else{
            $sceneId = $typeZone['MinScene'];
        }

        return $sceneId;
    }

    public function dealerReplyUpdate(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        $data = $_POST;
        $m = M("Company_ck_set");
        if($m->where("ecid = '".$data['ecid']."'")->find()){
            if($m->where("ecid = '".$data['ecid']."'")->save($data)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }else{
            if($m->add($data)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    public function msgUpdate(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        if(session($this->_userCfg['ECID']) == '816'){
            $this->setAdminFwResult(I('post.'));
        }

        $options = I('post.');

        $options['rightReply'] = I('post.rightReply','','');
        $options['repertReply'] = I('post.repertReply','','');
        $options['errorReply'] = I('post.errorReply','','');
        $options['modifyId'] = $options['id'];
        $options['modifyTime'] = date('Y-m-d H:i:s');
        $options['modifyUserId'] = session( $this->_userCfg['UID']);
        
        $opt['ecid'] = $options['ecid'];
        $opt['name'] = $options['name'];

        if($options['handleType'] != 'default'){
            //判断是否重复绑定品牌或产品
            $bindOpt['brandId'] = $options['brandId'];
            $bindOpt['productId'] = $options['productId'];
            $bindOpt['ecid'] = $options['ecid'];
            if($options['handleType'] == "item")
                $bindOpt['id'] = array('neq' , $options['id']);

            //查找回复表
            $isBind = $this->isDataExist('Company_fw_reply', $bindOpt);
            $bindOpt['pass'] = 0;
            if($options['handleType'] == "item"){
                $bindOpt['modifyId'] = array('neq' , $options['id']);
                unset($bindOpt['id']);
            }

            //查找审核表
            $isReviewBind = $this->isDataExist('Company_fw_reply_review' , $bindOpt);

            if($isBind || $isReviewBind){
                $result["status"] = Error::ERROR_FW_BIND_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_FW_BIND_EXIST);
                $this->ajaxReturn($result , "JSON");
            }
        }

        //判断操作类型
        if($options['handleType'] == "new"){
            unset($options['handleType']);
            $db = $this->isDataExist('Company_fw_reply', $opt);

            if($db){
                //判断规则名是否重复
                if(M("Company_fw_reply")->where("ecid = '".session($this->_userCfg['ECID'])."' AND name = '".$options['name']."'")->find()){
                    $result["status"] = Error::ERROR_FW_NAME_EXIST;
                    $result["info"] = Error::getErrMsg(Error::ERROR_FW_NAME_EXIST);
                }
            }
        }else{
            unset($options['handleType']);
            unset($options['msg']);

            $opt['modifyId'] = $options['id'];
            $opt['pass'] = 0;
            $db = $this->isDataExist('Company_fw_reply_review', $opt);

            if($db){
                $options['id'] = $db['id'];

                //更新数据
                if(M('Company_fw_reply_review')->save($options))
                {
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        if(!$db){
            unset($options['id']);
            
            //更新数据
            if(M('Company_fw_reply_review')->add($options))
            {
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    public function msgReview(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $review = I('post.');
        $options = I('post.');
        unset($options['id']);
        unset($options['type']);
        unset($review['type']);
        $review['reviewTime'] = date("Y-m-d H:i:s");
        $review['reviewUserId'] = session( $this->_userCfg['UID']);
        $options['rightReply'] = I('post.rightReply','','');
        $options['repertReply'] = I('post.repertReply','','');
        $options['errorReply'] = I('post.errorReply','','');
        if($options['pass'] == "1"){
            $result = $this->reviewHandle($options);
            M('Company_fw_reply_review')->save($review);
        }else if($options['pass'] == "-1"){
            if(M('Company_fw_reply_review')->save($review)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }
        $result['id'] = $review['id'];

        $this->ajaxReturn($result , "JSON");
    }

    public function divFwResultHandle(){
        if(I('get.ecid') != ""){
            $ecid = I('get.ecid');  
        }else{
             $ecid =  session( $this->_userCfg['ECID'] );
        }

        if(I('post.type') == "new"){
            $arr = array(
                "id" => -1,
                "ecid" => $ecid , 
                "name" => "" , 
                "brandId" => -1,
                "productId" => -1 , 
                "rightReply"=> "" , 
                "rightImg" => "" , 
                "repertReply"=> "" , 
                "repertImg" => "" , 
                "errorReply"=> "" , 
                "errorImg" => "" , 
                "webTheme" => '',
                "moblieTheme" => '',
                "pass" => 0
                );

            $this->assign('handleType' , "new");
        }else{
            if(I('post.type') == "default"){
                $arr = $this->getReplyMsg($ecid , I('post.type'));
                $this->assign('handleType' , "default");
            }else if(I('post.type') == "item"){

                $arr = $this->getReplyMsg($ecid , I('post.type') , I('post.id'));

                if($arr['brandId'] != -1){
                    $brand = $this->getBrandFromID($arr["brandId"]);
                    $arr['msg'] = "已绑定品牌：".$brand['name'];
                }else if($arr['productId'] != -1){
                    $product = $this->getProductFromID($arr["productId"]);
                    $arr['msg'] = "已绑定产品：".$product['name'];
                }
                
                $this->assign('handleType' , "item");
            }
        }
        $this->assign('selectTheme',$this->getWebTheme());
        $this->assign('defaultArr' , $arr);
        $this->assign('index' , $_POST["index"]);
        $this->setToken();
        $this->assign('ecid' , $ecid);
        $this->assign('rand' , rand());
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }  
    }

    /**
 * 添加的主题名分页显示
 * @author:蒋东芸 
 * 2014.2.27
 */
    public function Themeitem(){
        if(session($this->_userCfg['ECID']) == '816'){
            $this->assign("admin" , true);
        }
        // 导入分页类
        $Data = M('Company_theme_type');
        $count      = $Data->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 5);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result=$Data->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        $this->assign('themeItem' , $result);
        $this->assign('page' , $show);
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }
     /**
 * 将得到的值赋给标识themeview
 * @author:蒋东芸
 */
public function themeEdit(){
    if(session($this->_userCfg['ECID']) == '816'){
            $this->assign("admin" , true);
        }
    $m = M('Company_theme_type');
        $opt['id'] = $_GET['id'];
        $result = $m->where($opt)->find();
        
        $this->assign('themeview',$result);
        $res = $m->where($opt)->select();
        
        $this->assign('themesave',$res);
        $this->assign('rand' , rand());
        $this->setToken();
        $this->display();
}
public function themeEditHandle(){
    if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
    
        $m = M('Company_theme_type');

        $opt['name'] = $_POST['name'];
        $opt['id'] = array('neq',$_POST['id']);
        //判断主题名是否重复
        if($m->where($opt)->find()){
            $data['status'] = ERROR::ERROR_FW_THEMENAME_EXIST;
            $data['info'] = Error::getErrMsg(Error::ERROR_FW_THEMENAME_EXIST);
        }
        else{
            //获取所有post参数
            $opt = $_POST;
            $opt['modifyUserId'] = session($this->_userCfg['UID']);
            $opt['modifyTime'] = date("Y-m-d H:i:s");
            $status = $m->save($opt);
                $data['status'] = Error::SUCCESS_OK;
                $data['info'] = Error::getErrMsg(Error::SUCCESS_OK);
            }
        
        $this->ajaxReturn($data,"JSON");
}
    /**
     * 将得到的值赋给标识themeitem
     * @author:蒋东芸
     */
    public function themeview(){
        $m = M('Company_theme_type');

        $opt['id'] = $_GET['id'];
        $result = $m->where($opt)->find();


        $this->assign('themeitem',$result);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    /**
 * 将得到的值赋给标识selectType
 * @author:蒋东芸
 */
    public function AddTheme(){
        if(session($this->_userCfg['ECID']) == '816'){
            $this->assign("admin" , true);
        }
        $this->assign('rand' , rand());
        $this->assign('selectType',$this->getWebType());
        $this->setToken();
        $this->display();
        
    }
 /**
 * 删除主题
 * @author:蒋东芸
 */
 public function delThemeHandle(){
    if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
    $opt['id']=$_POST['id'];
       
    if(!assert($opt['id'])){
        $result['status'] = Error::ERROR_GENERAL;
        $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
        $this->ajaxReturn($result , 'JSON');
    }

    $m = M('Company_theme_type');

    if($m->where($opt)->delete()){
        $result['status'] = ERROR::SUCCESS_OK;
    }else{
         $result['status'] = Error::ERROR_GENERAL;
         $result['info'] = Error::getErrMsg(Error::ERROR_GENERAL);
    }
    $this->ajaxReturn($result,"JSON");
 }
  /**
 * 判断主题名或目录名是否存在
 * @author:蒋东芸
 */
public function addThemeHandle(){
    if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
        

        if($this->inputThemeHandle($_POST['name'])){
            if($this->inputLibHandle($_POST['value'])){
                $m=M('Company_theme_type');

                $opt=$_POST;
                $opt['type']="web";
                if($m->add($opt)){
                    $result['status'] = ERROR::SUCCESS_OK;
                }else{
                    $result['status'] = ERROR::ERROR_FW_HANDLE_ERR;
                    $result['info'] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
                }
            }
            else{
            $result['status'] = ERROR::ERROR_FW_LIBNAME_EXIST;
            $result['info'] = Error::getErrMsg(Error::ERROR_FW_LIBNAME_EXIST);
            }
            
        }else{
            $result['status'] = ERROR::ERROR_FW_THEMENAME_EXIST;
            $result['info'] = Error::getErrMsg(Error::ERROR_FW_THEMENAME_EXIST);

        }
        
        $this->ajaxReturn($result,"JSON");
    }
    //判断主题名是否存在
    private function inputThemeHandle($themeName){
        
        $m=M('Company_theme_type');

        $opt['name']=$themeName;

        if(!$m->where($opt)->find())
        {
            return true;
        }
        else{
            return false;
        }
    }
    private function inputLibHandle($LibName){
        
        $m=M('Company_theme_type');

        $opt['value']=$_POST['value'];

        if(!$m->where($opt)->find())
        {
            return true;
        }
        else{
            return false;
        }
    }
        /**
 * 获取网站类型
 * @author:蒋东芸
 */
    public function getWebType(){
        $m = M('Company_theme_type');
        $arrType = $m->distinct(true)->field('type')->select();
        
        return $arrType;
    }
    /**
 * 获取图片路径
 * @author:蒋东芸
 *2014.2.27
 */
    public function getPreImg(){
        $Pre['id']=$_POST['id'];
        $themeTB = M("Company_theme_type");
        $PreImg=$themeTB->where($Pre)->getField('themeImg');
        $this->ajaxReturn($PreImg,"JSON");
    }
 /**
 * 获取网站主题
 * @author:蒋东芸
 */
    public function getWebTheme(){
        $m = M('Company_theme_type');
        $arrTheme = $m->where("type='web'")->select();

        return $arrTheme;
    }

    public function divFwReview(){
        $defaultArr = M("Company_fw_reply_review")->where("id = '".$_POST["id"]."'")->find();

        if($defaultArr['brandId'] == "-1" && $defaultArr['productId'] == -"1"){
            $defaultArr['type'] = "default";
        }

        $this->assign("defaultArr" , $defaultArr);
        $this->assign("index" , $_POST["index"]);
        $this->setToken();
        $this->assign('rand' , rand());
        $this->display();
    }

    public function brand(){
        $data = M("Company_brand")->where("ecid = '".session( $this->_userCfg['ECID'] )."'")->select();
        $this->ajaxReturn($data , "JSON");
    }

    public function product(){
        $data = M("Company_product")->where("brandId = '".$_POST["id"]."'")->select();
        $this->ajaxReturn($data , "JSON");
    }

    private function getDealerReply($ecid){
        return M("Company_ck_set")->where("ecid = '".$ecid."'")->find();
    }

    private function setAdminFwResult($options){
        unset($options['modifyId']);
        unset($options['pass']);
        unset($options['handleType']);        

        $options['modifyTime'] = date('Y-m-d H:i:s');
        $options['modifyUserId'] = session( $this->_userCfg['UID']);
        $options['rightReply'] = I('post.rightReply','','');
        $options['repertReply'] = I('post.repertReply','','');
        $options['errorReply'] = I('post.errorReply','','');
        $opt = null;
        
        $opt['ecid'] = $options['ecid'];
        $opt['id'] = $options['id'];

        $db = $this->isDataExist('Company_fw_reply', $opt);

        if($db){
            //更新数据
            if(M('Company_fw_reply')->save($options))
            {
                $result["status"] = Error::SUCCESS_OK;

                $this->clearCache($options['ecid']);
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }
        else{
            //更新数据
            if(M('Company_fw_reply')->add($options))
            {
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    private function reviewHandle($options){
        

        $m = M('Company_fw_reply');

        if($options["pass"] == "1"){
            unset($options['pass']);
            unset($options['errMsg']);
            unset($options['reviewTime']);
            unset($options['reviewUserId']);
            $modifyId = $options['modifyId'];
            unset($options['modifyId']);
            if($modifyId == '-1'){
                if($m->add($options)){
                    $result["status"] = Error::SUCCESS_OK;

                    $this->clearCache($options['ecid']);
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }else{
                if($m->where("id = ".$modifyId)->save($options)){
                    $result["status"] = Error::SUCCESS_OK;

                    $this->clearCache($options['ecid']);
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        return $result;
    }

    public function getBrand(){
        if(I('get.ecid') != ""){
            $ecid = I('get.ecid');
        }else{
            $ecid = session( $this->_userCfg['ECID'] );
        }

        if($data = M("Company_brand")->where("ecid = '".$ecid."'")->select()){
            $result['code'] = Error::SUCCESS_OK;
            $result['data'] = $data;
        }else{
            $result['code'] = Error::ERROR_EDIT_HANDLE_ERR;
        }

        $this->ajaxReturn($result);
    }

    public function getProduct(){

        if($data = M("Company_product")->where("brandId = '".$_POST["id"]."'")->select()){
            $result['code'] = Error::SUCCESS_OK;
            $result['data'] = $data;
        }else{
            $result['code'] = Error::ERROR_EDIT_HANDLE_ERR;
        }
        $this->ajaxReturn($result);
    }

    public function msgDelete(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        //审核表设置数据不通过
        $opt = I('post.');
        if($row = M("Company_fw_reply_review")->where("modifyId = ".$opt['id'])->find()){
            $newOpt['pass'] = -1;
            $newOpt['id'] = $row['id'];
            $newOpt['reviewUserId'] = session( $this->_userCfg['UID'] );
            $newOpt['reviewTime'] = date("Y-m-d H:i:s");
            M("Company_fw_reply_review")->save($newOpt);
        }

        //删除回复表信息
        if(M("Company_fw_reply")->where("id = '".$_POST["id"]."' AND ecid = '".$_POST['ecid']."'")->delete()){
            $data["status"] = Error::SUCCESS_OK;
        }else{
            $data["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $data["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }

        $this->ajaxReturn($data,"JSON");
    }

    private function clearCache($ecid){
        $cache_list = S('mxt_reply_cache_list' . $ecid);

        if($cache_list){
            $arrCache = explode(',' , $cache_list);

            foreach ($arrCache as $item)
            {
                S($item, NULL);
            }
        }

        S('mxt_reply_cache_list' . $ecid, NULL);
    }

    public function getBindInfo(){
        $id = $_POST['id'];
        $arr = M("Company_fw_reply")->where("id = '".$id."'")->find();

        if($arr['brandId'] == "-1" && $arr['productId'] == "-1"){
            $this->ajaxReturn("默认回复");
        }else if($arr['brandId'] != -1){
            $brand = $this->getBrandFromID($arr["brandId"]);
            $this->ajaxReturn("绑定品牌：".$brand['name']);
        }else if($arr['productId'] != -1){
            $product = $this->getProductFromID($arr["productId"]);
            $this->ajaxReturn("绑定产品：".$product['name']);
        }
    }

    private function setResultFromDB($table , $type , $ecid){
        if($type == "default"){
            $arr = M($table)->where("ecid = '".$ecid."' AND ( brandId > -1 OR productId > -1)")->select();
        }else if ($type == "review"){
            $arr = M($table)->where("pass = 0")->order('id DESC')->select();
        }

        for($i = 0;$i<count($arr);$i++){
            if($arr[$i]['brandId'] == "-1" && $arr[$i]['productId'] == "-1"){
                $arr[$i]['msg'] = "默认回复";
            }else if($arr[$i]['brandId'] != -1){
                $brand = $this->getBrandFromID($arr[$i]["brandId"]);
                $arr[$i]['msg'] = "绑定品牌：".$brand['name'];
            }else if($arr[$i]['productId'] != -1){
                $product = $this->getProductFromID($arr[$i]["productId"]);
                $arr[$i]['msg'] = "绑定产品：".$product['name'];
            }

            $company = M("Company_info")->where("company_ecid = '".$arr[$i]['ecid']."'")->find();
            $arr[$i]["companyName"] = $company['company_name'];
        }

        return $arr;
    }

    private function getBrandFromID($id){
        return M("Company_brand")->where("id = '".$id."'")->find();
    }

    private function getProductFromID($id){
        return M("Company_product")->where("id = '".$id."'")->find();
    }

    /**
     * 获取产商默认提示语
     * @param  [string] $ecid [厂商ID]
     * @return [array]       [提示语参数]
     */
    private function getReplyMsg($ecid , $type , $id){
        $m = m( "Company_fw_reply" );

        $options['ecid'] = $ecid;


        if($type == "default"){
            $options['brandId'] = -1;
            $options['productId'] = -1;

            //获取默认提示信息
            $defaultArr = M( "Company_fw_reply" )->where($options)->find();
            if($defaultArr == null){
                $company = M("Company_info")->where("company_ecid = '".$ecid."'")->find();
                
                $defaultArr = array(
                "ecid" => $ecid , 
                "name" => $company['company_name'] , 
                "brandId" => -1,
                "productId" => -1 , 
                "rightReply"=> "" , 
                "rightImg" => "" , 
                "repertReply"=> "" , 
                "repertImg" => "" , 
                "errorReply"=> "" , 
                "errorImg" => "" , 
                "pass" => 0
                );
            }
        }else if($type == "item"){
            $options['id'] = $id;
            $defaultArr = M( "Company_fw_reply" )->where($options)->find();
        }

        $defaultArr["modifyId"] = $defaultArr["id"];
        $defaultArr["pass"] = 0;

        return $defaultArr;
    }

    private function isDataExist($table, $options){
        $m = M($table);

        return $m->where($options)->find();
    }

    private function getQrPicUrl($ecid , $sceneId){
        $token = $this->getAppToken($ecid);

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            return;
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
          $qrCode = $weObj->getQRCode($sceneId, 1);
            if($qrCode){
                return $weObj->getQRUrl($qrCode['ticket']);
            }
        }
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }

    /**
    *微信分享操作
    **/
    public function shareHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['title']=I('post.title');
        $opt['url']=I('post.url');
        $opt['img']=I('post.img');
        $opt['description']=I('post.description');

        if(M("Company_fw_share")->where("ecid = '".session($this->_userCfg['ECID'])."'")->save($opt)){
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }

        $this->ajaxReturn($result , "JSON");

    }

    public function shareAddHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt['id'] = I('post.id');
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['ecid'] = session($this->_userCfg['ECID']);
        $opt['title']=I('post.title');
        $opt['url']=I('post.url');
        $opt['img']=I('post.img');
        $opt['replyId'] = I('post.replyId');
        $opt['description']=I('post.description');
        if(!$opt['id']){
            if(M("Company_fw_share")->add($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }else{
            if(M("Company_fw_share")->save($opt)){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }
        
        $this->ajaxReturn($result,"JSON");
    }

    public function shareDialog(){
        $replyId = I("post.replyId");
        if(!$replyId){
            $arr = M("Company_fw_reply")->where("brandId = -1 AND productId=-1 AND ecid='".session($this->_userCfg['ECID'])."'")->find();
            $replyId = $arr['id'];
        }
        
        $m = M("Company_fw_share");
        $share = $m->where("replyId='".$replyId."'")->find();

        $this->assign("share",$share);
        $this->assign("replyId",$replyId);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }
    public function delShare(){
        $replyId = I("post.replyId");
        if(!$replyId){
            $arr = M("Company_fw_reply")->where("brandId = -1 AND productId=-1 AND ecid='".session($this->_userCfg['ECID'])."'")->find();
            $replyId = $arr['id'];
        }
         $m = M("Company_fw_share");
         if($m->where("replyId='".$replyId."'")->delete()){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
            }
        $this->ajaxReturn($result,"JSON");
    }
}
?>
