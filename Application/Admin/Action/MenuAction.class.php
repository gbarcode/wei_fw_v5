<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Admin\Action;
use Org\Error\Error;
class MenuAction extends AdminAction {
    public function index() {
        $this->assign('appToken',$this->getAppToken(session('ecid')));
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }

    public function refreshMenu(){
        $this->assign('menuArr',$this->getMenuSet(session('ecid')));
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }

    /**
     * 拖动菜单栏目位置，保存菜单栏目修改
     * 2015/8/4
     * 蒋东芸
     */
    public function saveMenu(){
        $opt['rootId'] = I('post.rootId');
        $obj=I('post.obj','','');
        $str = '&'.$obj;

        if($opt['rootId'] == -1){
            $arr = explode('&root-menu[]=', $str);//若拖动的是父级菜单
        }else{
            $arr = explode('&menu-sub[]=', $str);//若拖动的是子级菜单
        }
        
        array_shift($arr);//删除第一个元素
        
        //首先根据menuId获取菜单数据并进行排序
        $m = M("Company_menu");
        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $result = $m->field('id,sort')->where($opt)->order('sort asc')->select();
        //替换时间戳
        for($i=0;$i<count($arr);$i++){
            $subMenu=array(
                'id' => $arr[$i],
                'sort' => $result[$i]['sort']
                );
            
            if($m->save($subMenu)){
                $data['data'] = Error::SUCCESS_OK;
                $data['info'] = Error::getErrMsg(Error::SUCCESS_OK);
            }
        }
        $this->ajaxReturn($data,'JSON');
    }

    public function showEditHtml(){
        $opt  = I('post.id');
        $ecid = session('ecid');

        $m = M('Company_menu');
        $result = $m->find($opt);
        if($result['view_attr']){
            $result['view_attr'] = json_decode($result['view_attr'],true);
            if($result['view_attr']['type'] == 'view_material'){
                $Data = M('Company_news');
                $newsTitle = $Data->where('id ='.$result['view_attr']['news'])->getField('title');
                $result['view_attr']['title'] = $newsTitle;
            }
        }
        $result['informKeyword'] = explode(",",$result['informKeyword']);

        $this->assign('menu',$result);
        $this->assign('menuId',$opt);
        $this->assign('ecid' , $ecid);
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }   
    }

    public function newsList(){
        $Data = M('Company_news');
        $opt['ecid'] = session('ecid');

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 6 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
   
        $news = $Data->field('id,title')->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign('ecid' , $opt['ecid']);
        $this->assign('news' , $news);
        $this->assign( 'page', $show );// 赋值分页输出
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }   
    }

    public function editTokenHandle(){
        $opt = I('post.');

        $m = M("Company_info");

        $code = $m->save($opt);

        $this->ajaxReturn($code,'info',0);
    }

    public function delMenuHandle(){
        
        $opt = I('post.');

        $m = M('Company_menu');
        if($opt['id'] != ''){
            if($opt['hasSub'] == 1){
                $m->where('rootId = '. $opt['id'])->delete();
            }

            $result = $m->where('id = '.$opt['id'])->find();
            if($m->where('rootId = ' . $result['rootId'] . ' AND id != ' . $result['id'])->select()){

            }else{
                $arr = array(
                    "id" => $result['rootId'] , 
                    "hasSub" => 0
                    );

                $m->save($arr);
            }
            if($m->where('id = '. $opt['id'])->delete()){
                $result["data"] = Error::SUCCESS_OK;
            }else{
                $result["data"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }

            $this->ajaxReturn($result,"JSON");
        }
    }

    public function addMenuHandle(){
        
        $opt = I('post.');
        $opt['ecid'] = session('ecid');

        $m = M('Company_menu');
        if($opt['rootId'] == -1){
            //一级菜单不得超过4个字符
            if(mb_strlen( $opt['name'], "UTF8" ) > 4)
                $data["data"] = Error::ERROR_MENU_ROOT_LONG;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_ROOT_LONG);
           

            $subOpt['rootId'] = -1;
            $subOpt['ecid'] = session('ecid');

            $count = $m->where($subOpt)->count();
            if($count >= 3){
                $data["data"] = Error::ERROR_MENU_ROOT_COUNT;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_ROOT_COUNT);

                $this->ajaxReturn($data,"JSON");
            }  
        }
        else{
            //子菜单不得超过7个字符
            if(mb_strlen( $opt['name'], "UTF8" ) > 7)
                $data["data"] = Error::ERROR_MENU_SUB_LONG;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_SUB_LONG);
                

            $subOpt['rootId'] = $opt['rootId'];
            $subOpt['ecid'] = session('ecid');

            $count = $m->where($subOpt)->count();

            if($count >= 5)
                $data["data"] = Error::ERROR_MENU_SUB_COUNT;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_SUB_COUNT);
                
        }
        
        $result=$m->where("ecid = ".$opt['ecid']. " AND name = '".$opt['name']."'" )->find();
        
        if(!$result){

        $time = time();
        $opt['ecid'] = session('ecid');
        $opt['key'] = 'SZ12365_MENUCLICK_' . $time;
        $opt['sort'] = $time;
        
        if($m->add($opt)){
                    $data["data"] = Error::SUCCESS_OK;
                }else{
                    $data["data"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
        }else{
            $data["data"] = Error::ERROR_MENU_NAME_EXIST;
            $data["info"] = Error::getErrMsg(Error::ERROR_MENU_NAME_EXIST);
        }
        if($opt['rootId'] != -1)
            $this->setSubMenuSign($opt['rootId']);

        $this->ajaxReturn($data,"JSON");
    }

    public function editMenuHandle(){

        $opt = I('post.');
        unset($opt['activityName']);

        $opt['informKeyword'] = ltrim(implode(",", I('post.informKeyword')),',');

        if(I('post.view_attr')){
            $opt['view_attr'] = json_encode(I('post.view_attr'));
        }

        $m = M('Company_menu');
        $option['name']=I('post.name');
        $result=$m->where("$option AND id != ".I('post.id'))->find();
        if(!$result){            
            $opt['modifyUserId'] = session($this->_userCfg['UID']);
            $opt['modifyTime'] = date("Y-m-d H:i:s");
            if($m->save($opt)){
                $menu_cache = 'weixin_menu'. $opt['ecid'].$opt['key'];
                S($menu_cache, NULL);
                $data['data'] = Error::SUCCESS_OK;
                $data["info"] = Error::getErrMsg(Error::SUCCESS_OK);
            }
            else{
                $data['data'] = Error::ERROR_EDIT_HANDLE_ERR;
                $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }
        else{
            $data['data'] = Error::ERROR_MENU_NAME_EXIST;
            $data["info"] = Error::getErrMsg(Error::ERROR_MENU_NAME_EXIST);
        }

        $this->ajaxReturn($data,"JSON");
    }

    public function releaseMenuHandle(){
        $token = $this->getAppToken(session('ecid'));

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            $this->ajaxReturn(Error::ERROR_MENU_TOKEN_EMPTY,Error::getErrMsg(Error::ERROR_MENU_TOKEN_EMPTY),0);
        }

        $menu = $this->getMenuSet(session('ecid'));
        $menu_weixin = array('button' => '' );

        for($i=0; $i<count($menu); $i++){
            $menu_weixin['button'][$i] = $this->changeWeixinArray($menu[$i] , $token);

            if($menu[$i]['hasSub'] == 1){
                for($j = 0;$j<count($menu[$i]['subitem']);$j++){
                    if(($menu[$i]['subitem'][$j]['responseType'] == null || $menu[$i]['subitem'][$j]['responseType'] == "text") && $menu[$i]['subitem'][$j]['responseText'] == ""){
                        $data['data'] = ERROR::ERROR_MENU_REPLY_EMPTY;
                        $data['info'] = $menu[$i]['subitem'][$j]['name'] . "：" . Error::getErrMsg(Error::ERROR_MENU_REPLY_EMPTY);
                        $this->ajaxReturn($data , "JSON");
                    }
                }
            }else{
                if(($menu[$i]['responseType'] == "text" || $menu[$i]['responseType'] == null) && $menu[$i]['responseText'] == ""){
                    $data['data'] = ERROR::ERROR_MENU_REPLY_EMPTY;
                    $data['info'] = $menu[$i]['name'] . "：" . Error::getErrMsg(Error::ERROR_MENU_REPLY_EMPTY);
                    $this->ajaxReturn($data , "JSON");
                }
            }
            
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
            if($token['weibo_accessToken']){
                 $weiboObj = new \Org\Weibo\Weibo(array('appsecret' => $token['weibo_AppSecret'],'accesstoken' => $token['weibo_accessToken']));
                 $weiboObj->createMenu($menu_weixin);
             }
            if($tmp = $weObj->createMenu($menu_weixin)){
                $data['data'] = Error::SUCCESS_OK;
                $data['info'] = Error::getErrMsg(Error::SUCCESS_OK);
                $this->ajaxReturn($data,'JSON');
            }
        }
        
    }

    private function changeWeixinArray($menuItem , $token){
        $item = '';
        $item['name'] = $menuItem['name'];
        if($menuItem['hasSub'] == 1){
            $subItem = $menuItem['subitem'];

            for($i=0; $i<count($subItem); $i++){
                $item['sub_button'][$i] = $this->changeWeixinArray($subItem[$i] , $token);
            }
        }
        else{
            switch ($menuItem['responseType']) {
                case 'text':
                case 'news':
                case 'check_prize':
                case 'information':
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'view':
                    $item['type'] = 'view';
                    if($menuItem['view_attr']){
                        $view_attr = json_decode($menuItem['view_attr'],true);
                        switch ($view_attr['type']) {
                            case 'view_define':
                                $item['url'] = $view_attr['news'];
                                break;
                            case 'view_material':
                                $item['url'] = 'http://www.msa12365.com/Index/news/id/'.$view_attr['news'];
                                break;
                            case 'fw_check':
                                $item['url'] = $this->getFwCheckUrl();
                                break;                            
                        }
                    }
                    else{
                        $item['url'] = $menuItem['linkURL'];
                    }
                    break;
                case 'fwCheck':
                    $item['type'] = 'view';
                    $item['url'] = $this->getFwCheckUrl();
                    break;
                case 'service':
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'mall':
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'report':
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'vip':
                    $url = $this->getRedirectUrl($menuItem['vip_keyword'], session( "ecid" ));
                    if($url){
                        $item['type'] = 'view';
                        $item['url'] = $url;
                    }
                    else{
                        $item['type'] = 'click';
                        $item['key'] = $menuItem['key'];
                    }
                    break;
                case 'activity':
                    // $item['type'] = 'view';
                    // $item['url'] = $this->getActivityUrl($menuItem['activityId'] , $token['weixin_AppId']);
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'qrcode':
                    $item['type'] = 'scancode_waitmsg';
                    $item['key'] = $menuItem['key'];
                    break;
                default:
                    break;
            }
        }

        return $item;
    }

    private function getRedirectUrl($type, $ecid){
        $url = \Weixin\Response\WechatConst::REDIRECT_DOMAIN;

        $oauthUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=<-appid->&redirect_uri=<-url->&response_type=code&scope=snsapi_base&state=1#wechat_redirect";

        switch ($type) {
            case 'index':
                $opt['url'] = "index.php";
                $opt['ecid'] = $ecid;
                $opt['type'] = "shop";
                break;
            case 'info':
                $opt['url'] = 'user.php';
                $opt['ecid'] = $ecid;
                $opt['type'] = "shop";
                break;
            case 'order':
                $opt['url'] = 'user.php?act=order_list';
                $opt['ecid'] = $ecid;
                $opt['type'] = "shop";
                break;
            default:
                # code...
                break;
        }

        if($opt){
            $params = http_build_query($opt);
            $url .= $params;

            $url = urlencode($url);

            $oauthUrl = str_replace("<-url->", $url, $oauthUrl);

            $companyInfo = $this->getAppToken($ecid);

            $oauthUrl = str_replace("<-appid->", $companyInfo['weixin_AppId'], $oauthUrl);

            return $oauthUrl;
        }

        return false;
    }

    /**
    * 定义函数
    * getActivityUrl
    * 函数功能描述
    * 获取活动相关的菜单链接
    * @access private
    * @param int $activityId 活动id
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-06 创建函数
    */
    private function getActivityUrl($activityId , $weixin_AppId){
        $m = M('Company_activity');
        $opt['id'] = $activityId;
        $result = $m->where($opt)->find();

        $url = '';

        switch ($result['type']) {
            case 'question':
                $data['ecid'] = $result['ecid'];
                $data['activityId'] = $activityId;
                $data['type'] = $result['type'];

                $url = $this->getQuestionRedirectUrl($data , $weixin_AppId);
                break;
            
            default:
                # code...
                break;
        }

        return $url;
    }

    /**
    * 定义函数
    * getQuestionRedirectUrl
    * 函数功能描述
    * 获取问题活动的链接
    * @access private
    * @param array $data
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-06 创建函数
    */
    private function getQuestionRedirectUrl($data , $weixin_AppId){
        $url = \Weixin\Response\WechatConst::SERVER_DOMAIN."/Api/Redirect";
        $params = http_build_query($data);
        $url .= $params;

        $response = new \Weixin\Response\Response();
        return $response->getOauthRedirectUrl($url , $weixin_AppId);
    }

    private function setSubMenuSign($rootIdID){
        $m = M('Company_menu');
        $opt['id'] = $rootIdID;
        $opt['hasSub'] = 1;
        $m->save($opt);
    }

    private function getMenuSet($ecid){
        $m = M("Company_menu");

        $opt['ecid'] = $ecid;
        $opt['rootId'] = -1;

        $result = $m->where($opt)->order('sort asc')->select();

        for($i=0; $i<count($result); $i++){
            if($result[$i]['hasSub'] == 1){
                $subOpt['rootId'] = $result[$i]['id'];

                $result[$i]['subitem'] = $m->where($subOpt)->order('sort asc')->select();
            }
        }

        return $result;
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }

    public function change() {
        $this->assign( 'userName', session( 'user_name' ) );//用户名

        import( '@.WechatMenu.MenuDbControl' );
        $menuControl = new MenuDbControl( session( "ecid" ) );
        $result = $menuControl->getWeixinMenuArray();
        $material = M( "Company_".session( "ecid" )."_material_group" );
        $materialCount = $material->count();
        $materialPage = ceil( $materialCount/4 );

        $this->setToken();
        $this->assign( 'materialCount', $materialCount );// 素材总数
        $this->assign( 'materialPage', $materialPage );// 素材页数
        $this->assign( 'result', array( "button"=>$result ) );
        $this->display();
    }

    private function getFwCheckUrl(){
        $ecid = session( "ecid" );
        $token = sha1($ecid.'sz12365');
        $url = "http://www.msa12365.com/Fw/index?ecid={$ecid}&token={$token}";

        return $url;
    }
}
?>
