<?php
class MobelExchangeAction extends Action{
  public function index(){
    

    //获取奖品信息
    $result = $this->checkCode($_GET["ecid"] , $_GET["code"] , $_GET["id"]);

    if($result["status"] == Error::SUCCESS_OK){
      $result["output"] = "<fieldset id='luckytable'>
                    <label>姓名：</label>
                    <input type='text' placeholder='请输入真实姓名' id='name'>
                    <label>电话：</label>
                    <input type='text' placeholder='请输入联系电话' id='tel'>
                    <label>地址：</label>
                    <input type='text' placeholder='请输入联系地址' id='address'>
                    <label>&nbsp;</label>
                    <button onclick=\"exchange()\">兑奖</button>
                </fieldset>

              <div id='luckyMsg' style='display:none;text-align:center;'>
                <p class='ui header'>奖品已成功兑换，请等待工作人员联系。</p>
              </div>";
    }else{
      $result["output"] = "";
    }

    //获取奖品介绍
    $msg = $this->getMsg($_GET["ecid"] , $result["id"]);

    $this->assign("action" , $msg);
    $this->assign("result" , $result);
    $this->display();
  }

  public function checkCode($ecid , $code , $id){
    

    $m = M( "Company_".$ecid."_lucky_exchange" );

    //判断防伪码是否为中奖号码
    if ( !$result = $m->where( "fwCode = '".$code."' AND wechatID = '".$id."'" )->find() ) {
      $data = array(
        "status" => Error::ERROR_EXCHANGE_UNLUCK ,
        "info" => "<p class='ui header'>该防伪码未能中奖</p>"
      );
      return $data;
    }

    //中奖号码已兑奖
    if ( $result["name"] != null ) {
      $data = array(
                "status" => Error::ERROR_EXCHANGE_REPERT ,
                "info" => "兑奖信息已提交" ,
                "msg" => "<table class='ui basic table'><tbody>
                <tr><td width='40%' align='right'>姓名：</td><td align='left'>".$result["name"]."</td></tr>
                <tr><td width='40%' align='right'>电话：</td><td align='left'>".$result["tel"]."</td></tr>
                <tr><td width='40%' align='right'>地址：</td><td align='left'>".$result["address"]."</td></tr>
                <tbody>
                </table>"
                
      );
    }else {
      $data = array(
                "status" => Error::SUCCESS_OK ,
                "info" => "恭喜您中奖！！！" ,
                "msg" => "<p class='ui header'>获得" . $result["prize"]."</p>"
      );
    }

    $data["id"] = $result["id"];

    return $data;
  }

  public function exchangeHandle(){
    if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

    

    $setTime = $_POST["timestamp"];
    $now = time();
    
    //判断是否超时
    if(ceil($now - $setTime)/60 > 20){
      $data = array(
        "status" => -1 , 
        "info" => "页面超时，请刷新"
      );
    }else{
      $id = $_POST["id"];
      $_POST["exchangeTime"] = date( "Y-m-d H:i:s" );
      unset($_POST["id"]);

      if(M( "Company_".$_POST["ecid"]."_lucky_exchange" )->data( $_POST )->where( "id = '".$id."'" )->save()){
        $data = array(
          "status" => Error::SUCCESS_OK ,
          "info" => "提交成功！！！"
        );  
      }else{
        $data = array(
          "status" => Error::ERROR_EXCHANGE_UNLUCK , 
          "info" => "提交失败！！！"
          );
      }
    }

    $this->ajaxReturn( $data, 'JSON' );
  }

  public function getMsg($ecid , $id){
    $lucky = M("Company_".$ecid."_lucky_exchange")->where("id = '".$id."'")->find();
    $msg = M("Company_".$ecid."_lucky")->where("id = '".$lucky["luckyID"]."'")->find();
    return $msg;
  }
}
?>
