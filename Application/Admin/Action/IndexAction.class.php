<?php
namespace Admin\Action;
use Org\Error\Error;
class IndexAction extends AdminAction {
    public function index() {
        if(session( $this->_userCfg['DEALERID'] )){
            //redirect( __ROOT__.'/index.php/Location' );
        }

        $this->assign( 'userName', session( $this->_userCfg['NAME'] ) );
        $this->assign( 'ecid', session( $this->_userCfg['ECID'] ) );

        $lastRight = $this->getLastDayStatistics('wechat', 'first', session( $this->_userCfg['ECID'] ));
        $lastRepert = $this->getLastDayStatistics('wechat', 'repert', session( $this->_userCfg['ECID'] ));
        $lastError = $this->getLastDayStatistics('wechat', 'error', session( $this->_userCfg['ECID'] ));

        $this->assign('lastFirst',($lastRight)?$lastRight['total']:'-');
        $this->assign('lastRepert',($lastRepert)?$lastRepert['total']:'-');
        $this->assign('lastError',($lastError)?$lastError['total']:'-');
        $this->assign('companyName',$companyName);
        $this->assign('companyLogo',$companyLogo);

        if(I('get.t'))
            session('theme','nifty');
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }        
        
    }

    public function VerifyStatistics(){
        $startTime=date("Y-m-d",strtotime("-1 week"));
        $endTime=date("Y-m-d",strtotime("-1 day"));

        $this->assign('startTime',$startTime);
        $this->assign('endTime',$endTime);
        $this->display();
    }

    public function subscribeStatistics(){
        $startTime=date("Y-m-d",strtotime("-1 week"));
        $endTime=date("Y-m-d",strtotime("-1 day"));

        $this->assign('startTime',$startTime);
        $this->assign('endTime',$endTime);

        $this->display();
    }

    public function chart(){
    	$ecid = session( $this->_userCfg['ECID'] );

        if($_POST['startTime']==''||$_POST['endTime']==''){
            $startDate = date("Y-m-d",strtotime("-1 week"));
            $endDate = date("Y-m-d",strtotime("-1 day"));
        }else{
            $startDate = $_POST['startTime'];
            $endDate = $_POST['endTime'];
        }
    	

        $type = $_POST['type'];
        $event = $_POST['event'];

        if($_POST['status']=='0'){
            $data = $this->getFwChartData($startDate, $endDate, session($this->_userCfg['ECID']), session($this->_userCfg['ECID']), 'wechat', $type );
            $this->assign('name', $type);
        }else{
            $data = $this->getwxChartData($startDate,$endDate,session($this->_userCfg['ECID']),$event);
            $this->assign('name', $event);
        }
          $this->assign('data', $data);
    	

    	$this->display();
    }

    private function getFwChartData($startTime, $endTime, $wxEcid, $ecid, $type, $fwType){
        $m = new \Think\Model();

        $result = $m->query("
            SELECT
                sDate x,
                total y
            FROM
                sz12365_fw_check_statistics
            WHERE
                weixin_ecid = {$wxEcid}
            AND ecid = {$ecid}
            AND type = '{$type}'
            AND fwType = '{$fwType}'
            AND sDate >= '{$startTime}'
            AND sDate <= '{$endTime}'
            ORDER BY sDate ASC");

        return $result;
    }
    /**
     * 获取表sz12365_fw_company_wx_statistics图表数据
     * @param  [date] $wx_startime 起始日期
     * @param  [date] $wx_endtime  截至日期
     * @param  [int] $wx_ecid     当前ecid
     * @param  [varchar] $event       事件
     * @author 蒋东芸
     * 日期：2014/4/1
     */
    private function getwxChartData($wx_startime,$wx_endtime,$wx_ecid,$event){
        $m = new \Think\Model();

        $result = $m->query("
            SELECT 
                time x,
                total y
            FROM
                sz12365_fw_company_wx_statistics
            WHERE
                ecid = {$wx_ecid}
            AND event = '{$event}'
            AND time >= '{$wx_startime}'
            AND time <= '{$wx_endtime}'
            ORDER BY time ASC");
        return $result;
    }

    private function getLastDayStatistics($type, $fwType, $ecid){
    	$m = M('Check_statistics');

    	$opt['weixin_ecid'] = $ecid;
    	$opt['ecid'] = $ecid;
    	$opt['type'] = $type;
    	$opt['fwType'] = $fwType;
    	$opt['sDate'] = date("Y-m-d",strtotime("-1 day"));

    	$result = $m->where($opt)->find() ;
    	return $result;
    }
}

?>
