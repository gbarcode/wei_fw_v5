<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Admin\Action;
use Org\Error\Error;
use Org\Weibo\SaeTOAuthV2;
use Org\Weibo\SaeTClientV2;

class MaterialLibAction extends AdminAction{
    const REDIRECT_DOMAIN            = "http://jump.msa12365.com?";
//    const HOSTURL ="http://127.0.0.1";
    const HOSTURL ="http://v5.test.msa12365.com";
    const CLIENT_ID = '2513417815';
    const CLIENT_SECRET = '9642a7d2fd760771941c94d1007b5667';
    public function index() {
        //分页
        // 导入分页类
        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $Data = M( "Company_material_group" );
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $groupResult = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $newsArr = null;     //记录数据库数据
        if($groupResult){
            for ( $i = 0;$i<count( $groupResult );$i++ ) {
                $newsArr[$i]["groupId"] = $groupResult[$i]['id'];
                $newsArr[$i]["modifyTime"] = $groupResult[$i]['modifyTime'];
                $newsId = explode( ",", $groupResult[$i]["materialId"] );

                $newsRow = null;
                for ( $j = 0;$j<count( $newsId );$j++ ) {
                    $newsResult = M( "Company_news" )->where( 'id = ' . $newsId[$j] )->find();
                    $url = self::REDIRECT_DOMAIN."ecid=".$newsResult['ecid']."&type=news&form_type=1&newsId=".$newsResult['id'];//用于微博分享
                    $newsRow[$j] = array(
                        'id' => $newsResult['id'],
                        'title' => $newsResult['title'],
                        'description' => $newsResult['description'],
                        'content' => $newsResult['content'],
                        'newsImg' => ( $j==0 )?$newsResult['bigImg']:$newsResult['minImg'],
                        'synWeb' => $newsResult['synWeb'],
                        'url' => $url
                    );
                }
                if(count($newsRow) > 1)
                    $newsArr[$i]['multiple'] = 1;
                else
                    $newsArr[$i]['multiple'] = 0;
                $newsArr[$i]['Result'] = $newsRow;
            }
        }
        if(I('get.tab') != ''){
            $this->assign('tab' , I('get.tab'));
        } 
        $this->assign('ecid',$opt['ecid']);//用于本地上传时获取ecid
        
        $this->assign( 'page', $show );// 赋值分页输出
        $this->setToken();
        $this->assign( "newsArr" , $newsArr );//赋值素材数据
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display("new-index");
        }    

    }
    //打开微博授权回调页
    public function OauthCallback() {
        if (I("get.code")) {
            $o = new SaeTOAuthV2(self::CLIENT_ID, self::CLIENT_SECRET);
            $keys = array();
            $keys['code'] = I("get.code");
            $keys['redirect_uri'] = self::HOSTURL.U('OauthCallback');
            try {
                $token = $o->getAccessToken('code', $keys);
            } catch (OAuthException $e) {
                
            }
        }
        if (!$token) {
            die('授权失败');
        }
        
        $oauth['ecid'] = session($this->_userCfg['ECID']);
        $oauth['access_token'] = $token['access_token'];
        $oauth['remind_in'] = $token['remind_in'];
        $oauth['expires_in'] = $token['expires_in'];
        $oauth['oauthTime'] = strtotime(new \Org\Util\Date());
        $oauth['uid'] = $token['uid'];
        
        $row = M('app_oauth')->where("ecid='".session($this->_userCfg['ECID'])."' AND uid='".$token['uid']."'")->find();

        //保存授权信息
        if($row){
            $oauth['id']=$row['id'];
            M('app_oauth')->save($oauth);
        }else{
            if($oauth['access_token']){
                 M('app_oauth')->add($oauth);
            }
        }
        $this->display('oauthCallback');
    }

    //定时发布前先进行判断 授权是否过期
    public function oauth2Weibo(){
        $oauth = M('app_oauth')->where("ecid='".session($this->_userCfg['ECID'])."'")->find();
        $result['status']= -1;
        $result['expires_in']= NULL;
        if($oauth){
            $c=new SaeTOAuthV2(self::CLIENT_ID, self::CLIENT_SECRET,$oauth['access_token']);
            $tokenInfo = $c->getTokenInfo();
            if(!$tokenInfo['error']){
                $oauth['oauthTime'] = $tokenInfo['create_at'];
                $oauth['expires_in'] = $tokenInfo['expire_in'];
                M('app_oauth')->save($oauth);
                
                $now = strtotime(new \Org\Util\Date());
                if($now < ($oauth['oauthTime'] + $oauth['expires_in'])){
                $result['status']=0;
                $result['expires_in'] = $oauth['expires_in'];
            }
            }
        }
        $this->ajaxReturn( $result , "JSON" );
    }

    public function AddNews() {
        $this->setToken();
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }
    
    public function NewsManage() {
        $this->display();
    }
    
    public function TimedManage() {
        $this->display();
    }
    
    public function timedPublish(){
        $result['type']=$_POST['type'];
        $result['id'] = $_POST['id'];
        $result['expires_in']=$_POST['expires_in'];
        $this->assign( 'result', $result );
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }
    
    //新闻管理list
    public function newsList(){
        $news = M("Company_news");
        $opt['ecid'] = session($this->_userCfg['ECID']);
        
        $count = $news->where($opt)->count();// 查询满足要求的总记录数 
        $page = new \Think\Page($count,15);// 实例化分页类 传入总记录数
        $show = $page->show();// 分页显示输出
        // 进行分页数据查询
         $poiArr = $news->where($opt)->order( 'modifyTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();
         
          for($i=0;$i<count($poiArr);$i++){
             if($poiArr[$i]['description']){
                 $poiArr[$i]['weiboContent'] =  (mb_strlen($poiArr[$i]['description'],"utf-8")<100)?("'".$poiArr[$i]['description']."'"):("'".mb_substr($poiArr[$i]['description'],0,90,"utf-8")."...'");
             }else{
                 $poiArr[$i]['weiboContent'] = (mb_strlen(strip_tags($poiArr[$i]['content']),"utf-8")<100)?("'".strip_tags($poiArr[$i]['content'])."'"):("'".mb_substr(strip_tags($poiArr[$i]['content']),0,90,"utf-8")."...'");          
             }
         }
        
        $this->assign('id',$opt['id']);
        $this->assign('ecid' , session( $this->_userCfg['ECID'] ));
        $this->assign( "list" , $poiArr );//赋值数据
        $this->assign( 'page', $show );// 赋值分页输出
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }  
    }

    /**
    * 函数名：synWebHandle
    * 新闻同步选择和新闻置顶选择改变数据字段内容
    * @access public
    * @param  id       新闻id
    * @return $data    读取的数据
    * @author 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2015-04-07 创建函数
    */
    public function synWebHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
        $m             = M('Company_news');
        $opt['id']     = I('post.id');//获取新闻id
        
        if(I('post.type') == 'synWeb'){
            $opt['synWeb'] = I('post.val');//网站同步
        }else{
            $opt['top']    = I('post.val');//新闻置顶
        }
        
        $result        = $m->save($opt);

        if($result){
            $data['status'] = Error::SUCCESS_OK;
        }else{
            $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
        }

        $this->ajaxReturn($data , "JSON" );
    }    
    
    //定时记录list
    public function timedList(){
        $news = M("Send_crontab_weibo");
        $opt['ecid'] = session($this->_userCfg['ECID']);
        
        $count = $news->where($opt)->count();// 询满足要求的总记录数 $map表示查询条件
        $page = new \Think\Page($count,15);// 实例化分页类 传入总记录数
        $show = $page->show();// 分页显示输出
        // 进行分页数据查询
         $poiArr = $news->where($opt)->order( 'sendTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();
         $now = strtotime(new \Org\Util\Date());
         for($i=0;$i<count($poiArr);$i++){
             if($poiArr[$i]['successTime']){
                 $poiArr[$i]['status'] ='发布成功';
             }else{
                 if($now<$poiArr[$i]['sendTime']){
                     $poiArr[$i]['status'] ='未发布';
                 }else{
                     $poiArr[$i]['status'] ='发布失败';
                 }
             }
             $poiArr[$i]['addTime']= date( "Y-m-d h:m:s",$poiArr[$i]['addTime']);
             $poiArr[$i]['sendTime']= date( "Y-m-d h:m:s",$poiArr[$i]['sendTime']);
             
         }
        
        $this->assign('id',$opt['id']);
        $this->assign('ecid' , session( $this->_userCfg['ECID'] ));
        $this->assign( "list" , $poiArr );//赋值数据
        $this->assign( 'page', $show );// 赋值分页输出
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }

    private function AddNewsGroup( $sort='' ) {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        $data = array(
            'ecid' => session($this->_userCfg['ECID']) , 
            'modifyTime' => date( "Y-m-d H:i:s" ),
            'modifyUserId' => session($this->_userCfg['UID'])
        );

        return M( "Company_material_group" )->data( $data )->add();
    }

    public function AddNewsHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        if ( I('post.items') != '' ) {
            $items = I('post.items');

            for ( $i = 0;$i<count( $items );$i++ ) {
                $opt['ecid'] = session($this->_userCfg['ECID']);
                $opt['title'] = $items[$i]['title'];
                if($this->isDataExist('Company_news' , $opt)){
                    $result['status'] = Error::ERROR_NEWS_NAME_EXIST;
                    $result['info'] = Error::getErrMsg(Error::ERROR_NEWS_NAME_EXIST);
                    $this->ajaxReturn($result , "JSON");
                }
            }
            
            I('post.handleType') == "AddGroup"?$groupID=$this->AddNewsGroup():$groupID=I('post.groupid');

            for ( $i = 0;$i<count( $items );$i++ ) {
                if($items[$i]['title'] == "" || $items[$i]['image'] == "" || $items[$i]['content'] == ""){
                    $result['status'] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result['info'] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                    $this->ajaxReturn($result , "JSON");
                }

                $data = array(
                    'ecid' => session($this->_userCfg['ECID']) , 
                    'title' => $items[$i]['title'],
                    'thumb_media_id' => $items[$i]['media_id'],
                    'modifyTime' => date( "Y-m-d H:i:s" ),
                    'modifyUserId' => session($this->_userCfg['UID']) , 
                    'description' => $items[$i]['description'],
                    'content' => $items[$i]['content'],
                    'bigImg' => $items[$i]['image'],
                    'minImg' => $items[$i]['image'],
                    'addPicture' => I('post.addPicture')
                );
                if ( $newsid = M( "Company_news" )->add( $data ) ) {
                    $this->bindGroup( $newsid , $groupID );
                    $this->setMaterialSession($groupID);
                }
            }
            
            //新增图文素材的同时上传到微信图文素材管理
            $weObj = $this->newWechat();
            if ($weObj) {
                for ($i = 0; $i < count($items); $i++) {
                    $digest = null;
                    if (count($items) == 1) {//单图文时才有digset
                        $digest = $items[$i]['description'];
                    }

                    $articles[$i] = array(
                        'title' => $items[$i]['title'],
                        'thumb_media_id' => $items[$i]['media_id'],
                        'author' => '',
                        'digest' => $digest,
                        'show_cover_pic' => $items[$i]['addPicture'],
                        'content' => $items[$i]['content'],
                        'content_source_url' => '');
                }
                
                $art = $weObj->uploadForeverArticles($articles); //上传图文到微信

                $groupResult = M("Company_material_group")->find($groupID);
                $groupData = array('media_id' => $art['media_id']);
                M("Company_material_group")->data($groupData)->where('id = ' . $groupID)->save(); //把微信数据库的图文media_id保存到V5
            }

            $result['status'] = Error::SUCCESS_OK;
            $this->ajaxReturn( $result , "JSON" );
        }
    }

     private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }
    
    //实例化WeChat 并获取accessToken
     private function newWechat() {
        $token = $this->getAppToken(session($this->_userCfg['ECID']));
        if ($token['weixin_AppId'] != '' && $token['weixin_AppSecret'] != '') {
            $weObj = new \Org\Weixin\Wechat();
            if ($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])) {
                return $weObj;
            }
        }return FALSE;
    }
    
    private function setMaterialSession($materialID){
        $webPath = "http://115.29.37.72";
        $m = M( 'Company_material_group' );
        $opt['id'] = $materialID;
        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $result = $m->where( $opt )->find();
        $arrMaterialID = explode( ',', $result['materialId'] );

        for ( $i=0; $i<count( $arrMaterialID ); $i++ ) {
            $m = M( 'Company_news' );
            $condition['id'] = $arrMaterialID[$i];
            $result = $m->where( $condition )->find();

            $picUrl = ($i==0)?$result['bigImg']:$result['minImg'];

            if(substr($picUrl,0,4) != 'http')
                $picUrl = \Weixin\Response\WechatConst::SERVER_DOMAIN . $picUrl;

            $reply['type'] = "news";
            $reply['content'][$i] = array(
                'Title'=>$result['title'],
                'Description'=>$result['description'],
                'PicUrl'=> $picUrl,
                'Url'=> \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Index/news/id/'.$condition['id'] );
        }

        S("Material_".$materialID , $reply);
    }

    private function bindGroup( $newsid , $groupid ) {
        $groupResult = M( "Company_material_group" )->where( 'id = ' . $groupid )->find();

        //判断是否有materialID存在，如果有则添加逗号
        if ( $groupResult['materialId'] == "" ) {
            $groupData = array(
                'materialId' => $newsid
            );
        }
        else {
            $groupData = array(
                'materialId' => $groupResult['materialId'] .= ",".$newsid
            );
        }

        return M( "Company_material_group" )->data( $groupData )->where( 'id = ' . $groupid )->save();
    }

    public function getAddDiv() {
        $id = I('get.id');
        $key = I('get.key');
        $data['data'] = "<div class='news-list' id='list-$key' style='min-height:100px;' onmouseover=\"showEdit($key)\" onmouseout=\"outEdit($key)\">
                        <div id='title-$key' class='news-title' style='min-height:70px;'>标题</div>
                        <div class='news-img'><span><img id='img-$key' src=''/></span></div>
                        <div class='news-edit' id='news-$key' style='min-height:70px;'><div id='news-a'><a onclick=\"setSelID($key, newsArr, getImgPath(newsArr.items[$id].image, 's'))\"><img src='".__ROOT__."/Public/Image/edit.png' /></a><a href='javascript:delItem();'><img src='".__ROOT__."/Public/Image/delete.png'/></a></div></div>
                        <div style='clear:both'></div></div>";

        $data['status'] = 0;
        $data['key'] = $key;

        $this->ajaxReturn( $data , 'JSON');
    }

    public function deleteNews() {

        if ( M( "company_".session( "ecid" )."_material" )->where( "id=".$_GET["newsid"] )->delete() ) {
            $groupResult = M( "company_".session( "ecid" )."_material_group" )->where( "id=".$_GET["groupid"] )->select();
            $groupid = explode( ",", $groupResult[0]["materialID"] );

            $id = $groupid[0];
            for ( $i = 1;$i < count( $groupid );$i++ ) {
                if ( $groupid[$i] != $_GET["newsid"] ) {
                    $id .= ",".$groupid[$i];
                }
            }
        }

        $data = array(
            "materialID" => $id
        );

        if ( M( "company_".session( "ecid" )."_material_group" )->data( $data )->where( "id=".$_GET["groupid"] )->save() ) {
            $this->success( '修改成功' , 'EditNews?groupid='.$_GET["groupid"] );
        }
        else {
            $this->error( '修改失败' );
        }
    }

    public function EditNews() {
        $groupResult = M( "Company_material_group" )->where( "id=".$_GET["groupid"] )->find();
        $newsIds = explode( ",", $groupResult["materialId"] );
        $newsArr['selID'] = 0;
        $newsArr['groupid'] = $_GET["groupid"];
        $newsArr['total'] = count( $newsIds );
        $newsArr['delIds'] = '';

        for ( $j = 0;$j<count( $newsIds );$j++ ) {
            $newsResult = M( "Company_news" )->where( 'id = ' . $newsIds[$j] )->find();

            $newsArr['items'][$j] = array(
                'id' => $newsResult['id'],
                'key' => $j,
                'title' => $newsResult['title'],
                'modifyTime' => $newsResult['modifyTime'],
                'description' => $newsResult['description'],
                'content' => htmlspecialchars_decode($newsResult['content']),
                'image' => ( $j==0 )?$newsResult['bigImg']:$newsResult['minImg'],
                'update'=> 0,
                'synWeb'=>$newsResult['synWeb'],
                'addPicture'=>$newsResult['addPicture']
            );
        }

        $this->assign('ecid',session($this->_userCfg['ECID']));//用于本地上传时获取ecid
        $this->assign( 'userName', session( 'user_name' ) );//用户名
        $this->assign( "itemArrJson" , json_encode( $newsArr ) );
        $this->assign( "itemArr" , $newsArr );
        $this->assign( "itemTime" , date( "Y-m-d", strtotime( $groupResult['modifyTime'] ) ) );

        $this->setToken();
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }

    public function ModifyHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $items = $_POST["items"];
        
        if ( $items != '' ) {
            for ( $i = 0;$i<count( $items );$i++ ) {
                $opt['ecid'] = session($this->_userCfg['ECID']);
                $opt['title'] = $items[$i]['title'];
                $opt['id'] = array("NEQ" , $items[$i]['id']);
                if($this->isDataExist('Company_news' , $opt)){
                    $result['status'] = Error::ERROR_NEWS_NAME_EXIST;
                    $result['info'] = Error::getErrMsg(Error::ERROR_NEWS_NAME_EXIST);
                    $this->ajaxReturn($result , "JSON");
                }

                if($items[$i]['title'] == "" || $items[$i]['image'] == "" || $items[$i]['content'] == ""){
                    $result['status'] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result['info'] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                    $this->ajaxReturn($result , "JSON");
                }

                if ( $items[$i]['update'] == 1 ) {
                    $oldNews = M('Company_news')->where("id = '".$items[$i]['id']."' AND ecid = '".session($this->_userCfg['ECID'])."'")->find();//获取未修改时的news
                    
                    $img = $items[$i]['image'];

                    $data = array(
                        'id' => $items[$i]['id'],
                        'title' => $items[$i]['title'],
                        'thumb_media_id' =>$items[$i]['media_id']?$items[$i]['media_id']:$oldNews['thumb_media_id'],
                        'modifyTime' => date( "Y-m-d H:i:s" ),
                        'modifyUserId' => session($this->_userCfg['UID']) , 
                        'description' => $items[$i]['description'],
                        'content' => $items[$i]['content'],
                        'bigImg' => $img,
                        'minImg' => $img,
                        // 'synWeb' => $items[$i]['synWeb'],
                        'addPicture'=>$items[$i]['addPicture']
                    );
                    M( "Company_news" )->data( $data )->save();

                }

                if ( $items[$i]['update'] == 2 ) {
                    $data = array(
                        'ecid' => session($this->_userCfg['ECID']) , 
                        'title' => $items[$i]['title'],
                        'thumb_media_id' =>$items[$i]['media_id'],
                        'modifyTime' => date( "Y-m-d H:i:s" ),
                        'modifyUserId' => session($this->_userCfg['UID']) , 
                        'description' => $items[$i]['description'],
                        'content' => $items[$i]['content'],
                        'bigImg' => $items[$i]['image'],
                        'minImg' => $items[$i]['image'],
                        'synWeb' => $items[$i]['synWeb'],
                        'addPicture'=>$items[$i]['addPicture']
                    );
                    $items[$i]['id'] = M( "Company_news" )->data( $data )->add();

                }

                if ( $_POST['delIds'] != '' ) {
                    $arrIds = explode( '-' , $_POST['delIds'] );

                    for ( $i=0; $i<count( $arrIds ); $i++ ) {
                        if ( $arrIds[$i] != '' ) {
                            M( "Company_news" )->delete( $arrIds[$i] );
                        }
                    }
                }
            }
            
            //新增图文素材的同时上传到微信图文素材管理
            $weObj = $this->newWechat();
            if ($weObj) {
                for ($i = 0; $i < count($items); $i++) {
                    $digest = null;
                    if (count($items) == 1) {//单图文时才有digset
                        $digest = $items[$i]['description'];
                    }
                    $oldNews1 = M('Company_news')->where("id = '".$items[$i]['id']."' AND ecid = '".session($this->_userCfg['ECID'])."'")->find();//获取未修改时的news
                    $articles[$i] = array(
                        'title' => $items[$i]['title'],
                        'thumb_media_id' => $items[$i]['media_id'] ? $items[$i]['media_id'] : $oldNews1['thumb_media_id'],
                        'author' => '',
                        'digest' => $digest,
                        'show_cover_pic' => $items[$i]['addPicture'],
                        'content' => $items[$i]['content'],
                        'content_source_url' => '');
                }
                $group = M('Company_material_group')->where("id='".$_POST['groupid']."'AND ecid='".session($this->_userCfg['ECID'])."'")->find();
//                $articles = array('articles' => $articles);
                
                $weObj->delForeverMedia($group['media_id']);//修改接口暂时无效，删除图文，然后再上传
                $art = $weObj->uploadForeverArticles($articles); 
//                $result['info']=$art;

                $groupData = array('media_id' => $art['media_id']);
                M("Company_material_group")->data($groupData)->where('id = ' . $_POST['groupid'])->save(); //把微信数据库的图文media_id保存到V5
//                $result['_msg'] = $art['errcode'].'!!'.$articles['articles'][0]['title'];
                
            }

            $materialIds = $items[0]['id'];
            for ( $i=1; $i<count( $items ); $i++ ) {
                $materialIds .= ',' . $items[$i]['id'];
            }
            $data = array(
                'id' => $_POST['groupid'],
                'ecid' => session($this->_userCfg['ECID']) , 
                'modifyTime' => date( "Y-m-d H:i:s" ),
                'modifyUserId' => session($this->_userCfg['UID']) , 
                'materialId' => $materialIds
            );
            $t = M( "Company_material_group" )->data( $data )->save();
            $this->setMaterialSession($_POST['groupid']);
            $result['status'] = Error::SUCCESS_OK;
            $this->ajaxReturn($result , "JSON");
        }
    }

    public function deleteGroup() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        
        if($row = M("Company_event_response")->where("responseType = 'news' AND ecid = '".session($this->_userCfg['ECID'])."'")->find())
        {
            if($row['responseMaterialId'] == I('post.id')){
                $arr['responseType'] = 'text';
                 M("Company_event_response")->where("id = ".$row['id'])->save($arr);  
            }
        }

        $newsid = M( "Company_material_group" )->where( "id=".$_POST["id"]." AND ecid = '".session($this->_userCfg['ECID'])."'")->select();
        $newsidArr = explode( ",", $newsid[0]["materialId"] );

        for ( $i = 0;$i < count( $newsidArr );$i++ ) {
            M( "Company_news" )->where( "id=".$newsidArr[$i]." AND ecid = '".session($this->_userCfg['ECID'])."'")->delete();
        }

        //找到当前删除图文的media_id，通过接口删除微信端图文
        $news = M( "Company_material_group" )->where( "id=".$_POST["id"]." AND ecid = '".session($this->_userCfg['ECID'])."'")->find();
        $media_id = $news['media_id'];

        if ( M( "Company_material_group" )->where( "id=".$_POST["id"]." AND ecid = '".session($this->_userCfg['ECID'])."'")->delete() ) {
            S("Material_".$_POST["id"] , null);
            
            if($media_id && $weObj = $this->newWechat()){
                 $weObj->delForeverMedia($media_id);//删除永久图文
            }
            
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    private function isDataExist($table, $options){
        $m = M($table);

        return $m->where($options)->find();
    }
    
    //微博定时发布
    public function wbPublishHandle(){
         if(!IS_POST) 
            _404 ('页面不存在' , U('index'));
         
        $newsId = I('post.newsId');

        $result["status"] = Error::ERROR_GENERAL;
        $result["info"] = '发布失败';
        if(I('post.handleType') == "delete"){//删除
             if(M('Send_crontab_weibo')->where("id = '".I('post.id')."'")->delete()){
                    $result["status"] = Error::SUCCESS_OK;
                    $result["info"]='删除成功';
                }else{
                    $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
                }
        }else if(I('post.id')){//修改
            $record = M('Send_crontab_weibo')->where("id='".I('post.id')."'AND ecid='".session($this->_userCfg['ECID'])."'")->find();
             $result["info"] = $record['id'];
            $record['content'] = I('post.content');
            $record['sendTime']=  strtotime(I('post.sendTime'));
            $record['addTime']=  strtotime(date( "Y-m-d H:i:s" ));
             if( M('Send_crontab_weibo')->save($record)){
                    $result['status'] = Error::SUCCESS_OK;
                    $result["info"] = '重新发布成功';
               }
            
        }else{//新增
            if($row = M('Company_news')->where("id='".$newsId."'AND ecid='".session($this->_userCfg['ECID'])."'")->find()){
                $publishContent['ecid'] = session($this->_userCfg['ECID']);
                $publishContent['platform'] = 'weibo' ;
                $publishContent['modifyUserId']= $row['modifyUserId'];
                $publishContent['title']= $row['title'];
                $publishContent['img']=$row['bigImg'];
                $publishContent['content']= I('post.content');
                $publishContent['sendTime']=  strtotime(I('post.sendTime'));
                $publishContent['addTime']=  strtotime(date( "Y-m-d H:i:s" ));
                $publishContent['url']=self::REDIRECT_DOMAIN."ecid=".session($this->_userCfg['ECID'])."&type=news&form_type=1&newsId=".$newsId;

               if( M('Send_crontab_weibo')->add($publishContent)){
                    $result['status'] = Error::SUCCESS_OK;
                    $result["info"] = '发布成功';
               }
            }
        }
        $this->ajaxReturn($result , "JSON");
    }
    
    //定时群发微信
    public function wxGroupSendHandle() {
        if (!IS_POST)
            _404('页面不存在', U('index'));

        $groupId = I('post.groupId');
        $result["status"] = Error::ERROR_GENERAL;
        $result["info"] = '发布失败';
        if (I('post.handleType') == "delete") {//删除
        } else if (I('post.id')) {//修改
        } else {//新增
            if ($row = M('Company_material_group')->where("id='" . $groupId . "'AND ecid='" . session($this->_userCfg['ECID']) . "'")->find()) {
                $arrMaterialID = explode(',', $row['materialId']);
                    $m = M('Company_news');
                    $condition['id'] = $arrMaterialID[0];
                    $result = $m->where($condition)->find();

                $arr['ecid'] = session($this->_userCfg['ECID']);
                $arr['platform'] = 'wechat' ;
                $arr['targetUser'] = 'all';
                $arr['content'] = $row['media_id'];
                $arr['title'] = "【微信群发】".$result['title'];
                $arr['modifyUserId'] = $row['modifyUserId'];
                $arr['sendTime']=  strtotime(I('post.sendTime'));
                $arr['addTime']=  strtotime(date( "Y-m-d H:i:s" ));
                
                if( M('Send_crontab_weibo')->add($arr)){
                    $result['status'] = Error::SUCCESS_OK;
                    $result["info"] = '发布成功';
               }
            }
        }
        
         $this->ajaxReturn($result , "JSON");
    } 
    
    /**
     * 显示解决方案信息
     * @author 蒋东芸
     * 2014/7/8 
     */
    public function solution(){
        $m = M('Web_solution');

        $opt['ecid'] = session( $this->_userCfg['ECID'] );//获取缓存ECID并赋值到opt数组中
        $count      = $m->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 8);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        //查询满足ecid为缓存ecid，并以id顺序排列
        $result = $m->where($opt)->order('id desc')->limit( $page->firstRow.','.$page->listRows )->select();
        //循环赋值给页面
        for($i=0;$i<count($result);$i++){
            $this->assign('title', $result[$i]['title']);
        }
        $this->assign('solution',$result);//将变量result赋值给页面
        $this->assign( 'page', $show );// 赋值分页输出
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }  
    }
    /**
     * 编辑解决方案内容
     * @author 蒋东芸
     * 2014/7/9
     */
    public function solutionEdit(){
        //以JSON格式将getSolution函数中获得id赋值给页面
        $this->assign('solutionInfo', json_encode($this->getSolution(I('get.id'))));
        $this->setToken();
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }
    /**
     * getDealer 获取解决方案信息
     */
    private function getSolution($id){
        /**
         * 判断id是否为添加模式
         * 如果是建立result数组
         * 如果不是以id等于变量为条件，并将操作方式改为编辑。
         * 返回数据。
         */
        if($id == 'add'){
            $result = array(
                "title" => "" ,
                "company" => "" ,
                "description" => "" ,
                "img" => "" ,
                "keyword" => "" ,
                "handleType" => "add" , 
                "content" => "" 
                );
        }else{
            $result = M("Web_solution")->where("id = '".$id."'")->find();
            $result["handleType"] = "edit";
        }

        return $result;
    }
    /**
     * 解决方案内容增、删、改处理
     * @author 蒋东芸
     * 2014/7/9
     */
    public function solutionHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));//判断数据是否是以post方式传值
        $m = M("Web_solution");//实例化数据库
        //将post方式传值赋值给opt数组
        $opt['ecid'] = session($this->_userCfg['ECID'] );//获取缓存ECID，并赋值
        $opt['title'] = I('post.title');
        $opt['company'] = I('post.company');
        $opt['keyword'] = I('post.keyword');
        $opt['description'] = I('post.description');
        $opt['img'] = I('post.img');
        $opt['content'] = I('post.content','','');
        $opt['modifyTime'] = date("Y-m-d H:i:s");//调用date函数并以年月日时分秒形式赋值给opt数组中
        $opt['modifyUserId'] = session($this->_userCfg["UID"]);

        if(I('post.handleType') == "add"){
            //判断标题是否重复，如果重复，赋值提示错误信息，调用错误信息提示语。
            //否则添加数组，添加成功，提示操作成功信息，添加不成功，提示错误信息，调用错误信息提示语
            if($m->where("title = '".I('post.title')."' AND ecid = ".session($this->_userCfg['ECID']))->find()){
                $result["status"] = Error::ERROR_TITLE_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_TITLE_EXIST);
            }else{
                if($m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }else if(I('post.handleType') == "edit"){            
            //判断标题是否重复，如重复，提示错误信息，调用错误信息语。
            if($m->where("title = '".I('post.title')."' AND id != ".I('post.id')." AND ecid = ".session($this->_userCfg['ECID']))->find()){
                $result["status"] = Error::ERROR_TITLE_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_TITLE_EXIST);
            }else{
                if($m->where('id = '.I('post.id'))->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }            
        }else if(I('post.handleType') == "delete"){
            if($m->where("id = '".I('post.id')."'")->delete()){//删除满足id为所传的数据。
                    $result["status"] = Error::SUCCESS_OK;//提示操作成功
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;//操作失败提示信息
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);//调用操作失败信息
                }
        }

        $this->ajaxReturn($result,"JSON");
        //以JSON格式返回数据
    }
    
//    //同步微信端
//    public function SyncData() {
//            $weObj = $this->newWechat();
//            if ($weObj) {
//                $groups = M("Company_material_group")->where("media_id is NULL AND ecid = '" . session($this->_userCfg['ECID']) . "'")->select();
//                for ($i = 0; $i < count($groups); $i++) {
//                    $newsidArr = explode(",", $groups[$i]['materialId']);
//                    for ($j = 0; $j < count($newsidArr); $j++) {
//                        $news = M("Company_news")->where("id=" . $newsidArr[$j] . " AND ecid = '" . session($this->_userCfg['ECID']) . "'")->find();
//
//                        $digest = null;
//                        if (count($newsidArr) == 1) {//单图文时才有digset
//                            $digest = $news['description'];
//                        }
//                        $this->getMediaId();
//                        $articles[$j] = array(
//                            'title' => $news['title'],
//                            'thumb_media_id' => $news['media_id'],
//                            'author' => '',
//                            'digest' => $digest,
//                            'show_cover_pic' => 1,
//                            'content' => $news['content'],
//                            'content_source_url' => '');
//                        dump($articles[$j]);
//                    }
//                    die;
//                    $articles = array('articles' => $articles);
//                    $art = $weObj->uploadForeverArticles($articles); //上传图文到微信
//
////                    $groupResult = M("Company_material_group")->find($groups[$i]['id']);
////                    $groupData = array('media_id' => $art['media_id']);
////                    M("Company_material_group")->data($groupData)->where('id = ' . $groups[$i]['id'])->save(); //把微信数据库的图文media_id保存到V5
//                    $groups[$i]['media_id'] = $art['media_id'];
//                    M("Company_material_group")->save($groups[$i]);
//                }
//            }
//    }
//private function getMediaId(){
//    
//}


}
?>
