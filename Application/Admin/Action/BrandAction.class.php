<?php
namespace Admin\Action;
use Org\Error\Error;
class BrandAction extends AdminAction {
    public function index() {
        //获取厂商信息
        $company = M("Company_info")->select();

        $this->assign("company" , $company);

        if(session($this->_userCfg['ECID']) == '816'){

            if(session('theme')){
                $this->theme('nifty')->display();
            }else{
                $this->display('admin');
            }   
        }else{
            $this->assign('ecid' , session($this->_userCfg['ECID']));
            
            if(session('theme')){
                $this->theme('nifty')->display();
            }else{
                $this->display();
            }   
        }
    }

    public function brand(){
        //分页
        // 导入分页类
        $Data = M('Company_brand');

        $opt['ecid'] = I('post.ecid');

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 8 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $brand = $Data->where($opt)->order( 'modifyTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        if($brand){
            for($j = 0;$j < count($brand);$j++){
                $brand[$j]['keywordArr'] = array();
                $keyword = explode(",", $brand[$j]['keyword']);
                for($i = 0;$i < count($keyword)-1;$i++){
                    $brand[$j]['keywordArr'][$i] = $keyword[$i];
                }
                $brand[$j]['handleType'] = "";
            }

            $brand['add'] = array(
            'name' => '' ,
            'bigImg' => '' ,
            'minImg' => '' ,
            'info' => '' ,
            'keyword' => '' ,
            'keywordArr' => array(),
            'handleType' => 'add' , 
            'ecid' => I('post.ecid')
            );

            $this->assign( 'page', $show );// 赋值分页输出
        }

        $this->assign( 'cId', I('post.cId') );
        $this->assign('rand' , rand());
        $this->assign('ecid' , I('post.ecid'));
        $this->assign("brand" , $brand);
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function product(){
        $brand = M("Company_brand")->where("ecid = '".I('post.ecid')."'")->select();

        $tags = M("Company_tags")->where("ecid = '".I('post.ecid')."'")->select();

        $label=M("Company_fw_label")->where("ecid = '".I('post.ecid')."'")->select();

        $column=M('Company_column')->where("ecid = '".I('post.ecid')."'")->select();

        $this->assign('ecid' , I('post.ecid'));
        $this->assign('rand' , rand());
        $this->assign("label" , $label);
        $this->assign("brand" , $brand);
        $this->assign("tags" , $tags);
        $this->assign('column',$column);
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function tags(){
        //分页
        // 导入分页类
        $Data = M('Company_tags');

        $opt['ecid'] = I('post.ecid');

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 8 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $tags = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        if($tags){
            $this->assign( 'tagsPage', $show );// 赋值分页输出
        }
        
        $this->assign( 'cId', I('post.cId') );
        $this->assign('ecid' , I('post.ecid'));
        $this->assign("tags" , $tags);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    public function getBrandFromID(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        if(I('post.id') != "add"){
            $brand = M("Company_brand")->where("id = '".I('post.id')."'")->find();

            $brand['info'] = htmlspecialchars_decode($brand['info']);
            $brand['keywordArr'] = array();
            $keyword = explode(",", $brand['keyword']);
            for($i = 0;$i < count($keyword)-1;$i++){
                $brand['keywordArr'][$i] = $keyword[$i];
            }
            $brand['handleType'] = "";
        }else{
            $brand = array(
                'name' => '' ,
                'bigImg' => '' ,
                'minImg' => '' ,
                'info' => '' ,
                'keyword' => '' ,
                'keywordArr' => array(),
                'handleType' => 'add' ,
                'ecid' => I('post.ecid')
                );
        }
        
        $this->ajaxReturn($brand , "JSON");
    }

    public function getProductFromBrandID(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $product = M("Company_product")->where("brandId = '".I('post.id')."'")->select();

        for($j = 0;$j < count($product);$j++){
            $product[$j]['keywordArr'] = array();
            $keyword = explode(",", $product[$j]['keyword']);
            for($i = 0;$i < count($keyword)-1;$i++){
                $product[$j]['keywordArr'][$i] = $keyword[$i];
            }

            $product[$j]['tagsArr'] = array();
            $tags = explode(",", $product[$j]['tagId']);
            for($i = 0;$i < count($tags)-1;$i++){
                $tagArr = M("Company_tags")->where("id = '".$tags[$i]."'")->find();
                $product[$j]['tagsArr'][$i] = $tagArr['name'];
            }

            $product[$j]['handleType'] = "";
        }

        $this->ajaxReturn($product , "JSON");
    }

    public function getProductFromID(){
        if(I('post.id') == "add"){
            $product = array(
                'name' => '' ,
                'brandId' => 0 ,
                'bigImg' => '' ,
                'minImg' => '' ,
                'info' => '' ,
                'tagId' => '' ,
                'columnId' => '' ,
                'tagsArr' => array(),
                'columnArr' => array(),
                'keyword' => '' ,
                'keywordArr' => array(),
                'handleType' => 'add' ,
                'ecid' => I('post.ecid')
                );
        }else{
            $product = M("Company_product")->where("id = '".I('post.id')."'")->find();

            $product['keywordArr'] = array();
            $keyword = explode(",", $product['keyword']);
            for($i = 0;$i < count($keyword)-1;$i++){
                $product['keywordArr'][$i] = $keyword[$i];
            }

            $product['tagsArr'] = array();
            $tags = explode(",", $product['tagId']);
            for($i = 0;$i < count($tags)-1;$i++){
                $tagArr = M("Company_tags")->where("id = '".$tags[$i]."'")->find();
                $product['tagsArr'][$i] = $tagArr['name'];
            }

            $product['columnArr'] = array();
            $column = explode(",", $product['columnId']);
            for($i = 0;$i < count($column)-1;$i++){
                $columnArr = M("Company_column")->where("id = '".$column[$i]."'")->find();
                $product['columnArr'][$i] = $columnArr['name'];
            }

            $product['handleType'] = "";
            $product['info'] = htmlspecialchars_decode($product['info']);
        }

        $this->ajaxReturn($product , "JSON");
    }

    public function getTagsFromID(){
        if($_POST["id"] == "add"){
            $tags = array(
                'name' => '' , 
                'handleType' => 'add' ,
                'ecid' => I('post.ecid')
                );
        }else{
            $tags = M("Company_tags")->where("id = '".I('post.id')."'")->find();
            $tags['isChange'] = false;

            $tags['handleType'] = "";
        }

        $this->ajaxReturn($tags , "JSON");
    }

    /**
     * getProductQr获取产品二维码
     * @author:范小宝
     * @return string
     */
    public function getProductQr(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        $m = M("Company_qr_type");
        $opt['ecid'] = I('post.ecid');
        $opt['productId'] = I('post.id');$result = $m->where($opt)->find();

        if($result = $m->where($opt)->find()){
            $data['status'] = Error::SUCCESS_OK;
            $data['info'] = $this->getQrPicUrl($result['ecid'] , $result['scene_id']);
        }else{
            $data['status'] = ERROR_PRODUCT_SCENEID_EMPTY;
            $data['info'] = Error::getErrMsg(Error::ERROR_PRODUCT_SCENEID_EMPTY);
        }

        $this->ajaxReturn($data , "JSON");
    }

    /**
     * createProductQr生成产品二维码
     * @author:范小宝
     * @return string
     */
    public function createProductQr(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("Company_qr_type");
        $opt['ecid'] = I('post.ecid');
        $opt['productId'] = I('post.id');
        $opt['type'] = "product";
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['scene_id'] = $this->getSceneId("product" , $opt['ecid']);

        if($m->add($opt)){
            $data['status'] = Error::SUCCESS_OK;
        }else{
            $data['status'] = ERROR_ADD_HANDLE_ERR;
            $data['info'] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }

        $this->ajaxReturn($data , "JSON");
    }

    private function getSceneId($type , $ecid){
        $m = M('Company_qr_type');

        //取出该类型sceneid范围
        $opt['typeName'] = $type;
        $typeZone = M("Company_qr_zone")->where($opt)->find();

        //查找该ecid最大sceneid值
        $opt = null;
        $opt['ecid'] = $ecid;
        $opt['scene_id'] = array('between' , $typeZone['MinScene'].','.$typeZone['MaxScene']);
        $maxSceneId = $m->where($opt)->max('scene_id');

        if($maxSceneId != ''){
            if($maxSceneId == $typeZone['MaxScene']){
                $sceneId = $typeZone['MinScene'];
            }else{
                $sceneId = $maxSceneId + 1;
            }
        }else{
            $sceneId = $typeZone['MinScene'];
        }

        return $sceneId;
    }

    private function getQrPicUrl($ecid , $sceneId){
        $token = $this->getAppToken($ecid);

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            return;
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
          $qrCode = $weObj->getQRCode($sceneId, 1);
            if($qrCode){
                return $weObj->getQRUrl($qrCode['ticket']);
            }
        }
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }

    /**
     * brandHandle 品牌信息操作
     * 范小宝
     */
    public function brandHandle(){
    	if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        //判断是否重名
        $arr['name']=I('post.name');
        $arr['id']=array('neq' , I('post.id'));
        $arr['ecid'] = session($this->_userCfg['ECID']);

        if(I('post.handleType')!="delete"){
            if($this->checkbrandName($arr,'Company_brand')){
                $result["status"] = Error::ERROR_SAVE;
                $result["info"] = Error::getErrMsg(Error::ERROR_SAVE);
                $this->ajaxReturn($result , "JSON");
            }
        }

        $m = M("Company_brand");
        $opt['ecid'] = session( $this->_userCfg["ECID"] );
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['modifyUserId'] = session( $this->_userCfg["UID"] );
        $opt['name'] = strip_tags(I('post.name'));
        $opt['info'] = strip_tags(I('post.info'));
        $opt['bigImg'] = I('post.bigImg');
        $opt['minImg'] = I('post.minImg');
        $opt['keyword'] = I('post.keyword');
        
        //判断操作类型
        if(I('post.handleType') == "add"){
            if($m->add($opt))
            {
        		$result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }else if(I('post.handleType') == "edit"){
            if($m->where("id = '".I('post.id')."'")->save($opt)){
        		$result["status"] = Error::SUCCESS_OK;
             }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }else if(I('post.handleType') == "delete"){
            if($m->where("id = '".I('post.id')."'")->delete()){
                $result["status"] = Error::SUCCESS_OK;
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }
    public function checkbrandName($arr,$tableName){
        $m=M($tableName);

        if($row = $m->where($arr)->find()){
            return true;
        }
        else{
            return false;
        }
    }

    public function productHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $ecid=session( $this->_userCfg["ECID"]);

        $m = M("Company_product");

        $arr['name']=I('post.name');
        $arr['id']=array('neq' , I('post.id'));
        
        if(I('post.handleType')!="delete"){
            if($this->checkbrandName($arr,'Company_product')){
                $result["status"] = Error::ERROR_product_SAVE;
                $result["info"] = Error::getErrMsg(Error::ERROR_product_SAVE);
                $this->ajaxReturn($result , "JSON");
            }
        }

        $opt['brandId'] = I('post.brandId');
        $opt['name'] = I('post.name');
        $opt['id'] = array("neq" , I('post.id'));

        if($m->where($opt)->find()){
            $result["status"] = Error::ERROR_PRODUCT_NAME_EXIST;
            $result["info"] = Error::getErrMsg(Error::ERROR_PRODUCT_NAME_EXIST);
        }else{
            unset($opt['id']);
            $opt['modifyTime'] = date("Y-m-d H:i:s");
            $opt['modifyUserId'] = session( $this->_userCfg["UID"] );
            $opt['info'] = I('post.info');
            $opt['bigImg'] = I('post.bigImg');
            $opt['minImg'] = I('post.minImg');
            $opt['keyword'] = I('post.keyword');
            $opt['tagId'] = I('post.tagId');
            $opt['columnId'] = I('post.columnId');
            if(I('post.handleType') == "add"){
                if($row = $m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                    $result['info'] = $opt['brandId'];
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }else if(I('post.handleType') == "edit"){
                if($row = $m->where('id = '.I('post.id'))->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                    $result['info'] = $opt['brandId'];
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        if(I('post.handleType') == "delete"){
            if($m->where('id = '.I('post.id'))->delete()){
                $result["status"] = Error::SUCCESS_OK;
                $result['info'] = $opt['brandId'];
            }else{
                $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    public function tagsHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("Company_tags");

        if($m->where("ecid = '".session( $this->_userCfg["ECID"] )."' AND name = '".I('post.name')."'")->find()){
            $result["status"] = Error::ERROR_BRAND_TAGS_NAME_EXIST;
            $result["info"] = Error::getErrMsg(Error::ERROR_BRAND_TAGS_NAME_EXIST);
        }else{
            $opt['name'] = strip_tags(I('post.name'));
            $opt['ecid'] = session( $this->_userCfg["ECID"] );

            if(I('post.handleType') == "add"){
                if($m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }else if(I('post.handleType') == "edit"){
                if($m->where('id = '.I('post.id'))->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    public function column(){
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    public function addcolumn(){
        $opt['group_id'] = I('post.group_id');
        $opt['name'] = I('post.name');
        $opt['ecid']=session('ecid');
        $m = M('Company_column');
        
        if($m->add($opt)){
            $data["data"] = 0;
        }else{
            $data["data"] = Error::ERROR_EDIT_HANDLE_ERR;
            $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        
        $this->ajaxReturn($data,"JSON");
    }

    public function getcolumnSet(){
        $m = M("Company_column");

        $opt['ecid'] = session('ecid');
        $opt['group_id'] = 0;

        $result = $m->where($opt)->select();

        for($i=0; $i<count($result); $i++){
            if($result[$i]['group_id'] == 0){
                $subOpt['group_id'] = $result[$i]['id'];

                $result[$i]['subitem'] = $m->where($subOpt)->select();
            }
        }

        $this->assign('menuArr',$result);
        $this->display();
    }

    public function delColumn(){
        $opt['id']=I('post.id');
        $m=M('Company_column');
        if($m->where($opt)->delete()){
            $result["data"] = Error::SUCCESS_OK;
        }else{
            $result["data"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }

        $this->ajaxReturn($result,"JSON");
    }

    public function editColumnHandle(){
        $opt['id']=I('post.id');
        $opt['name']=I('post.name');
        $m=M('Company_column');
        $res=$m->where("id = ".$opt['id'])->find();
        if($res){
            $result=$m->save($opt);
            if( $result){
                $data['data'] = 0;
                $data["info"] = Error::getErrMsg(Error::SUCCESS_OK);
            }else{
                $data['data'] = Error::ERROR_EDIT_HANDLE_ERR;
                $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
            
        }else{
            $data['data'] = Error::ERROR_EDIT_HANDLE_ERR;
            $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        $this->ajaxReturn($data,"JSON");
    }

    public function showColumnHtml(){
        $opt['id']=I('post.id');
        $arr['ecid']=session($this->_userCfg['UID']);
        $m=M('Company_column');
        $res=$m->where($opt)->find();
        $data['id']=$res['id'];
        if($res['group_id']==0){
            $option['group_id']=$res['id'];
            $result=$m->where($option)->find();
            if($result){
                $data['obj']=0;
                $this->assign('info',$data);
            }else{
                $data['obj']=1;
                $this->assign('info',$data);
            }
            
        }else{
            $data['obj']=1;
            $this->assign('info',$data);
        }
        $this->assign('res',$res);
        $this->display();
    }

    public function addColumnInfo(){
        $opt['id']=I('post.id');
        $option['image']=I('post.image');
        $option['introduce']=I('post.introduce');
        $m=M('Company_column');
        $res=$m->where($opt)->save($option);
        if($res){
            $data["data"] = 0;
        }else{
            $data["data"] = Error::ERROR_EDIT_HANDLE_ERR;
            $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        $this->ajaxReturn($data,"JSON");
    }
}
?>
