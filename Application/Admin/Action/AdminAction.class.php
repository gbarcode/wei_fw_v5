<?php
namespace Admin\Action;
use Think\Action;
use Org\Error\Error;
class AdminAction extends Action{
	protected $_userCfg;    //用户信息变量配置

	public function _initialize() {
		//获取用户信息config
		$this->_userCfg = C( 'LOGIN_USER' );

		//检查是否登录
		$code = $this->isLogin();
		if ( $code != Error::SUCCESS_OK ) {
			$this->error( Error::getErrMsg( $code ) , '/' );
		}
		//检查权限
		/*$code = $this->checkAuth();
		if ( $code != Error::SUCCESS_OK ) {
			$this->error( Error::getErrMsg( $code ) );
		}*/
		$this->assign("ecid" , session( $this->_userCfg['ECID'] ));
		$this->assign("role" , session( $this->_userCfg['ROLE'] ));
	}

	protected function isAdmin() {
		return in_array( session( $this->_userCfg['UID'] ), $this->_userCfg['ADMIN_UID'] );
	}

	private function checkAuth() {
		import( 'ORG.Util.Auth' );//加载权限类库
		

		//临时关闭
		return Error::SUCCESS_OK;

		if ( $this->isAdmin() )
			return Error::SUCCESS_OK;    //如果是管理员，则直接返回真值，不需要进行权限验证

		$auth=new Auth();
		//检查Module权限
		if ( !$auth->check( MODULE_NAME, session( $this->_userCfg['UID'] ) ) ) {
			return Error::ERROR_NOT_MODULE_AUTHORITY;
		}
		//检查Action权限
		if ( !$auth->check( MODULE_NAME.'-'.ACTION_NAME, session( $this->_userCfg['UID'] ) ) ) {
			return Error::ERROR_NOT_ACTION_AUTHORITY;
		}

		return Error::SUCCESS_OK;
	}

	private function isLogin() {
		if ( !session( $this->_userCfg['UID'] )) {
                    
                    
                    if(I("get.ecid") != null)
                        return Error::SUCCESS_OK;
                    
                    return Error::ERROR_NOT_LOGIN;
		}

		if(session( $this->_userCfg['DEALERID'] ))$this->assign('isDealer' , session( $this->_userCfg['DEALERID'] ));

		return Error::SUCCESS_OK;
	}

	public function setToken(){
		$timestamp = time();
		$token = md5($timestamp . C('SAFE_KEY'));

		$this->assign('timestamp',$timestamp);
		$this->assign('token',$token);
	}
}
?>
