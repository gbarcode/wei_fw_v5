<?php
namespace Admin\Action;
use Org\Error\Error;
use Org\Mxt\MxtClient;
class LocationAction extends AdminAction{
    /**
     * index 页面
     */
    public function index() {    
        $uid = session($this->_userCfg['UID']);
        $role = session($this->_userCfg['ROLE']);

        $this->assign('role',$role);
        $this->assign('type',$this->getDeailer());
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display('new-index');
        } 
    }
    public function dealerList(){
        $Data = M( "Company_dealers" );
        $opt['ecid'] = session( $this->_userCfg['ECID'] );

        //判断是否为经销商登陆
        if(session( $this->_userCfg['DEALERID'] )){
            $opt['id|root'] = session( $this->_userCfg['DEALERID'] );
        }
        if(I('post.dealerType') == '-1'){
            $opt['root'] =  array('eq','-1');//选择经销商类型查询
        }
        if(I('post.dealerType') == '0'){
            $opt['root'] = array('neq','-1');
        }
        if(I('post.id')!= 'all'&& I('post.id')!= ''){
            $opt['id'] = I('post.id');//经销商选择查询
        }

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 15);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $poiArr = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        for($i = 0;$i < count($poiArr);$i++){

            $res['dealerId'] =$poiArr[$i]['id'];

            if($this->CheckDealerId($res))
                $poiArr[$i]['QrInfo']= '1';
            else{
                $poiArr[$i]['QrInfo']= '0';
            }
        }

        //获取省份
        $this->getProvince();
        $this->assign('dealerType',I('post.dealerType'));
        $this->assign('id',I('post.id'));
        $this->assign('root',$opt['root']);
        $this->assign('id',$opt['id']);
        $this->assign('ecid' , session( $this->_userCfg['ECID'] ));
        $this->assign( "list" , $poiArr );//赋值数据
        $this->assign( 'page', $show );// 赋值分页输出
        $this->setToken();
        $this->display();
    }
    public function dealer(){
         $Data = M( "Company_dealers" );

        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        //判断是否为经销商登陆
        if(session( $this->_userCfg['DEALERID'] )){
            $opt['id'] = session( $this->_userCfg['DEALERID'] );
            $rootDealer = $Data->where($opt)->select();

            unset($opt['id']);
            $opt['root'] = session( $this->_userCfg['DEALERID'] );
        }else{
            //获取一级经销商
            $opt['root'] = -1;
            $rootDealer = $Data->where($opt)->select();
            unset($opt['root']);
        }
        $this->assign('rootDealer' , $rootDealer);
        $this->assign('dealerInfo', json_encode($this->getDealer(I('get.id'))));
        $this->setToken();
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function zgDealer(){
        $Data = M( "Company_dealers" );

        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $opt['id'] = session( $this->_userCfg['DEALERID'] );
        $rootDealer = $Data->where($opt)->find();

        unset($opt['id']);
        $opt['root'] = session( $this->_userCfg['DEALERID'] );

        $dealerInfo = $this->getDealer($rootDealer['id']);
        if($dealerInfo['info'] != ''){
            $dealerInfo['info'] = $this->changeArray(json_decode($dealerInfo['info']));
        }

        $this->assign('rootDealer' , $rootDealer);
        $this->assign('dealerInfo', $dealerInfo);
        $this->setToken();
        $this->display();
    }

    public function zgproduct(){
        $Data = M( "Company_dealers" );

        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $opt['id'] = session( $this->_userCfg['DEALERID'] );
        $rootDealer = $Data->where($opt)->find();

        unset($opt['id']);
        $opt['root'] = session( $this->_userCfg['DEALERID'] );

        $dealerInfo = $this->getDealer($rootDealer['id']);
        if($dealerInfo['product'] != ''){
            $dealerInfo['product'] = $this->changeArray(json_decode($dealerInfo['product']));
        }

        $this->assign('rootDealer' , $rootDealer);
        $this->assign('dealerInfo', $dealerInfo);
        $this->setToken();
        $this->display();
    }

    public function zgproductupload(){
        $dealerId = session( $this->_userCfg['DEALERID'] );
        
        $isZgAdmin = false;
        if($dealerId != 1157){
            $this->assign('isZgAdmin',false);
        }else{
            $isZgAdmin = true;
            $this->assign('isZgAdmin',true);
        }
        $this->setToken();
        $this->display();
    }

    public function getZgProduct(){
        $Data = M('Company_200113_product');
        $dealerId = session( $this->_userCfg['DEALERID'] );
        
        $isZgAdmin = false;
        if($dealerId != 1157){
            $opt['dealerId'] = $dealerId;
            $this->assign('isZgAdmin',false);
        }else{
            $opt['sign'] = 0;
            $isZgAdmin = true;
            $this->assign('isZgAdmin',true);
        }

        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 5 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign('result',$result);
        $this->assign( 'page', $show );// 赋值分页输出

        if($isZgAdmin){
            $this->display('zgProductReview');
        }else{
            $this->display();
        }
    }

    public function zgProductUploadHandle(){
        $m = M('Company_200113_product');
        $opt = I('post.');
        $opt['dealerId'] = session( $this->_userCfg['DEALERID'] );
        $opt['sign'] = 0;
        $opt['addTime'] = date("Y-m-d H:i:s");

        if($m->add($opt)){
            $result['status'] = 0;
        }else{
            $result['status'] = -1;
        }

        $this->ajaxReturn($result);
    }

    public function zgReviewHandle(){
        $postArr = I('post.');
        $opt['id'] = $postArr['id'];
        if($postArr['type'] == 'ok'){
            $opt['sign'] = 1;
        }else{
            $opt['sign'] = -1;
        }

        if(M('Company_200113_product')->save($opt)){
            $result['status'] = 0;
        }else{
            $result['status'] = -1;
        }
        $this->ajaxReturn($result,'json');
    }

    public function changeArray($object)
    {
        $_array = is_object($object) ? get_object_vars($object) : $object;    //判断数据中是否有对象，有获取对象
     
        foreach ($_array as $key => $value) {
            $value = (is_array($value) || is_object($value)) ? $this->changeArray($value) : $value;
            $array[$key] = $value;
        }
     
        return $array;
    }

    public function changePWD(){
        $oldP = I('post.oldP');
        $newP = I('post.newP');
        $userid = session($this->_userCfg['UID']);

        $oldOpt['id'] = $userid;
        $oldOpt['user_password'] = md5($oldP);
        if(M('User_info')->where($oldOpt)->find()){
            $newOpt['user_password'] = md5($newP);
            M('User_info')->where('id = '.$userid)->save($newOpt);
            $result['status'] = 0;
        }else{
            $result['status'] = -1;
            $result['message'] = '旧密码错误';
        }

        $this->ajaxReturn($result , 'json');
    }

    /**
     * getProvince 获取省份信息
     */
    private function getProvince(){
        $result = M('province')->select();
        $this->assign('province' , $result);
    }

    /**
     * CheckDealerId 检验经销商是否存在二维码
     */
    private function CheckDealerId($res){
        $result=M('Company_qr_type')->where($res)->find();

        return $result;
    }

    /**
     * LoginInfo 经销商登陆账号信息弹窗
     */
    public function LoginInfo(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        if($_POST['id'] == 'add'){
            $result = array(
                "user_name" => "" ,
                "dealerId" => $_POST['dealerId'] ,
                "handleType" => "add"
                );
        }else{
            $m = M("User_info");
            $opt['id'] = $_POST['id'];
            $result = $m->where($opt)->find();
            $result['handleType'] = "edit";
        }

        $this->assign("dealerUser" , $result);
        $this->display();
    }

    /**
     * dealerUserHandle 经销商登陆账号操作
     */
    public function dealerUserHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt['user_name'] = I('post.user_name');
        $m = M("User_info");

        if(I('post.handleType') == "add"){
            $this->checkLoginName($opt);
            
            $opt['company_ecid'] = session( $this->_userCfg['ECID'] );
            $opt['user_password'] = $this->getDefaultPwd();
            $opt['roleId'] = "dealer";
            $opt['dealerId'] = I('post.dealerId');
            if($row = $m->add($opt)){
                $m = M("Company_dealers");
                $opt = null;
                
                $opt['isLoginUser'] = $row;
                $m->where('id = '.I('post.dealerId'))->save($opt);
                $data['status'] = Error::SUCCESS_OK;
            }else{
                $data['status'] = ERROR_ADD_HANDLE_ERR;
                $data['info'] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
            }
        }else{
            $opt['id'] = array('neq' , I('post.id'));
            $this->checkLoginName($opt);
            unset($opt['id']);

            if($m->where('id = '.I('post.id'))->save($opt)){
                $data['status'] = Error::SUCCESS_OK;
            }else{
                $data['status'] = ERROR_EDIT_HANDLE_ERR;
                $data['info'] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }

        $this->ajaxReturn($data , "JSON");
    }

    /**
     * checkLoginName 判断账号名是否重复
     */
    private function checkLoginName($opt){
        $m = M('User_info');
        if($m->where($opt)->find()){
            $result['status'] = ERROR_LOGIN_NAME_EXIST;
            $result['info'] = Error::getErrMsg(Error::ERROR_LOGIN_NAME_EXIST);
            $this->ajaxReturn($result , "JSON");
        }

        return;
    }

    /**
     * getDefaultPwd 获取企业默认密码
     */
    private function getDefaultPwd(){
        $ecid = session($this->_userCfg['ECID']);
        $companyInfo = M('Company_info')->where('company_ecid = '.$ecid)->find();

        if($companyInfo['defaultPwd']){
            return $companyInfo['defaultPwd'];
        }else{
            return C('DEFAULT_PWD');
        }
    }

    /**
     * agent 代理品牌产品标签页
     */
    public function agent(){
        $opt['ecid'] = session($this->_userCfg['ECID']);

        //赋值经销商信息
        if(session($this->_userCfg['DEALERID'])){
            $opt['root|id'] = session($this->_userCfg['DEALERID']);
            $this->assign("dealerItem" , M("Company_dealers")->where($opt)->select());
            unset($opt['root|id']);
        }else{
            $this->assign("dealerItem" , M("Company_dealers")->where($opt)->select());
        }

        $this->assign("cId" , $_POST['cId']);
        $this->assign("type" , $_POST['type']);
        $this->assign("id" , I('post.id'));
        
        //判断当前显示
        if($_POST['type'] == "brand"){
            $this->assign("brand" , $this->getPage("Company_brand" , $opt , 10));
            if(session('theme')){
                $this->theme('nifty')->display("brand");
            }else{
                $this->display("brand");
            } 
        }else if($_POST['type'] == "product"){
            $this->assign("brand" , M("Company_brand")->where($opt)->select());
            if(session('theme')){
                $this->theme('nifty')->display("product");
            }else{
                $this->display("product");
            } 
        }
    }

    /**
     * stock 库存管理标签页
     */
    public function stock(){
        $brandOpt['ecid'] = session($this->_userCfg['ECID']);
        $brand = M("Company_brand")->where($brandOpt)->select();

        $result = array();
        $m = M("Company_".$brandOpt['ecid']."_stock");

        //判断是否为经销商登陆
        if(session($this->_userCfg['DEALERID'])){
            $opt['dealerId'] = session($this->_userCfg['DEALERID']);
            $this->assign("role" , "dealer");
        }else{
            $this->assign("role" , "admin");
        }

        //判断是否有选择经销商
        if(I('post.dealerId') == 'all'){
            $opt['dealerId'] = -1;
        }else{
            $opt['dealerId'] = I('post.dealerId');
        }
        
        $num = 0;
        for($i = 0;$i<count($brand);$i++){
            $productOpt['brandId'] = $brand[$i]['id'];
            if($productInfo = M("Company_product")->where($productOpt)->select()){
                for($j = 0;$j<count($productInfo);$j++){
                    $result[$num]['id'] = $productInfo[$j]['id'];
                    $result[$num]['name'] = $productInfo[$j]['name'];

                    $opt['mark'] = 0;

                    //判断是否有选择产品
                    if(I('post.productId') == 'all'){
                        $opt['productId'] = $productInfo[$j]['id'];
                        $result[$num]['count'] = $m->where($opt)->count();
                    }else{
                        if(I('post.productId') == $productInfo[$j]['id']){
                            $opt['productId'] = $productInfo[$j]['id'];
                            $result[$num]['count'] = $m->where($opt)->count();
                        }
                    }
                    
                    $num++;
                }
            }
        }

        //判断用户权限，获取对应经销商
        $dealerOpt['ecid'] = session($this->_userCfg['ECID']);
        if(session($this->_userCfg['DEALERID'])){
            $dealerOpt['root'] = session($this->_userCfg['DEALERID']);
        }

        $this->assign("dealer" , M('Company_dealers')->where($dealerOpt)->select());
        $this->assign("cId" , I('post.cId'));
        $this->assign("result" , $result);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    /**
     * sell 库存管理标签页
     */
    public function sell(){
        $this->display();
    }

    /**
     * sell 库存管理标签页，产品出货窗口
     */
    public function wlOutHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt['wl_Code'] = I('post.wlCode');
        $m = M('Company_'.session($this->_userCfg['ECID']).'_stock');

        //判断库存是否存在该物流码信息
        if($row = $m->where($opt)->find()){
            //判断物流码是否已经出货
            if($row['mark'] == 0){
                $opt['mark'] = -1;
                $opt['outTime'] = date('Y-m-d H:i:s');

                if($m->where('id = '.$row['id'])->save($opt)){
                    $result['status'] = Error::SUCCESS_OK;
                }else{
                    $result['status'] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }else{
                $result['status'] = Error::ERROR_WLCODE_IS_OUT;
                $result["info"] = Error::getErrMsg(Error::ERROR_WLCODE_IS_OUT);
            }
        }else{
            $result['status'] = Error::ERROR_WLCODE_IS_UNBIND;
            $result["info"] = Error::getErrMsg(Error::ERROR_WLCODE_IS_UNBIND);
        }

        $this->ajaxReturn($result , 'JSON');
    }

    /**
     * getProductFromBrandID 通过品牌ID获取产品信息
     */
    public function getProductFromBrandID(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $product = M("Company_product")->where("brandId = '".$_POST['id']."'")->select();

        $this->ajaxReturn($product , "JSON");
    }

    /**
     * getPage 获取分页
     */
    private function getPage($table , $opt , $pageCount){
        // 导入分页类

        $Data = M( $table );
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , $pageCount );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result = $Data->where($opt)->order('id desc')->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign( 'page', $show );// 赋值分页输出
        return $result;
    }

    /**
     * add 页面
     */
    public function add(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        if($_POST['id'] == 'add'){
            $result = array(
                "name" => "" ,
                "tel" => "" ,
                "info" => "" ,
                "handleType" => "add" , 
                "address" => "" , 
                "longitude" => ""  , 
                "latitude" => ""  , 
                "poiId" => ""
                );
        }else{
            $result = M("Company_dealers")->where("id = '".$_POST['id']."'")->find();
            $result["handleType"] = "edit";
        }

        $this->display();
    }

    /**
     * web 网店信息弹窗
     */
    public function web(){
        if(I('post.id') == "add"){
            $result = array(
                "dealerId" => "" ,
                "name" => "" ,
                "url" => "" , 
                "handleType" => "add"
                );
        }else{
            $result = M("Company_dealers_electricity")->where("id = '".I('post.id')."'")->find();
            $result['handleType'] = "edit";
        }
        //判断是否为经销商登陆
        if(session( $this->_userCfg['DEALERID'] )){
            $option['id|root'] = session( $this->_userCfg['DEALERID'] );
        }
        $option['ecid'] = session( $this->_userCfg['ECID'] ); 
        $dealers = M("Company_dealers")->where($option)->select();

        $this->assign("dealer" , $dealers);
        $this->assign("item" , $result);
        $this->display();
    }


    /**
     * electricity 网店管理标签
     */
    public function electricity(){
        $Model= new \Think\Model();//实例化空模型
        $ecid = session( $this->_userCfg['ECID'] );
        $opt = " dealers.ecid = {$ecid} AND dealers.id=electricity.dealerId";
        //判断是否为经销商登陆
        if(session( $this->_userCfg['DEALERID'] )){
            $id = session( $this->_userCfg['DEALERID'] );
            $root = session( $this->_userCfg['DEALERID'] );
            $opt .= " AND (dealers.id ={$id} OR dealers.root ={$root})";
        }
        $this->assign('bindType',$Model->distinct(true)->field('dealers.id,dealers.name as dealerName')->table(array('sz12365_fw_company_dealers'=>'dealers','sz12365_fw_company_dealers_electricity'=>'electricity'))
            ->where($opt)->select());//获取所有经销商

        
        if(I('post.id')!= 'all'&& I('post.id')!= ''){
            $dealerId = I('post.id');//经销商选择查询
            $opt .= " AND electricity.dealerId = {$dealerId}";
        }
        /**
         * 蒋东芸
         * 2014/6/17
         */
        $count = $Model->field('dealers.name as dealerName,electricity.id,electricity.name as electriName')->table(array('sz12365_fw_company_dealers'=>'dealers','sz12365_fw_company_dealers_electricity'=>'electricity'))
            ->where($opt)->order( 'dealers.id asc' )->limit( $page->firstRow.','.$page->listRows )->count();

        $page       = new \Think\Page( $count , 15 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Model->field('dealers.name as dealerName,electricity.id,electricity.name as electriName')->table(array('sz12365_fw_company_dealers'=>'dealers','sz12365_fw_company_dealers_electricity'=>'electricity'))
            ->where($opt)->order( 'dealers.id asc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign( "item" , $result );//赋值数据        
        $this->assign('page',$show);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }  
    }
    /**
     * order 物流码管理标签
     */
    public function order(){
        // 导入分页类

        $opt["ecid"] = session( $this->_userCfg['ECID'] );

        if($_POST['type'] != "all"){
            $opt['handleType'] = $_POST['type'];
        }

        //是否为经销商登陆
        if(session( $this->_userCfg['DEALERID'] )){
            $opt['modifyUserId'] = session( $this->_userCfg['UID'] );

            //获取二级经销商
            $subDealer = M("Company_dealers")->where("root = " . session( $this->_userCfg['DEALERID'] ))->select();
            $this->assign("subDealer" , $subDealer);
        }else{
            //获取所有经销商
            $subDealer = M("Company_dealers")->where("ecid = " . session( $this->_userCfg['ECID'] ))->select();
            $this->assign("subDealer" , $subDealer);
        }

        //是否有选择经销商
        if($_POST['dealerId'] != '' && $_POST['dealerId'] != 'all'){
            $opt['sellerId'] = $_POST['dealerId'];
        }

        //时间范围
        if(I("post.startTime") != '' && I("post.endTime")!=''){
            $startTime = I("post.startTime");
            $endTime = date("Y-m-d",strtotime(I("post.endTime"))+'86400');//延后一天。
            $opt['modifyTime'] = array('between',array($startTime,$endTime));
        }else{
            $startTime = date("Y-m-d" , strtotime("-1 week"));
            $endTime = date("Y-m-d",strtotime("-1 day")); 
            $this->assign('startTime',$startTime);
            $this->assign('endTime',$endTime);
        }        

        $Data = M( "Company_wl_order" );
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result = M("Company_wl_order")->where($opt)->order('id desc')->limit( $page->firstRow.','.$page->listRows )->select();

        for($i = 0;$i < count($result);$i++){
            //获取订单操作类型
            if($result[$i]['handleType'] == 0){
                $result[$i]['type'] = "绑定产品";

                $bind = M("Company_product")->where("id = '".$result[$i]['productId']."'")->find();
                $result[$i]['bind'] = "<p>绑定产品：".$bind['name']."</p>";
            }
            if($result[$i]['handleType'] == 1){
                $result[$i]['type'] = "绑定经销商";

                $bind = M("Company_dealers")->where("id = '".$result[$i]['sellerId']."'")->find();
                $result[$i]['bind'] = "<p>绑定经销商：".$bind['name']."</p>";
            }
            if($result[$i]['handleType'] == 2){
                $result[$i]['type'] = "经销商发货";

                $bind = M("Company_product")->where("id = '".$result[$i]['productId']."'")->find();
                $result[$i]['bind'] = "<p>绑定产品：".$bind['name']."</p>";
                $bind = M("Company_dealers")->where("id = '".$result[$i]['sellerId']."'")->find();
                $result[$i]['bind'] .= "<p>绑定经销商：".$bind['name']."</p>";
            }

            //获取操作人名称
            $user = M("User_info")->where("id = '".$result[$i]['modifyUserId']."'")->find();
            $result[$i]['user'] = $user['user_name'];

            $result[$i]['wlcode'] = "<span><a onclick=\"showWlcode(".$result[$i]['id'].")\" href='javascript:void(0)'>查看物流码</a></span>";
        }

        $this->assign("dealerId" , session( $this->_userCfg['UID'] ));
        $this->assign("dealerName" , session( $this->_userCfg['NAME'] ));
        $this->assign("ecid" , session( $this->_userCfg['ECID'] ));
        $this->assign("type" , $_POST['type']);
        $this->assign("cId" , $_POST['cId']);
        $this->assign("order" , $result);
        $this->assign( "list" , $poiArr );//赋值数据
        $this->assign( 'page', $show );// 赋值分页输出
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    /**
     * showWlcode 查看物流码弹窗
     */
    public function showWlcode(){
        $result = M("Company_wl_log")->where("orderId = '".$_POST['id']."'")->select();
        $this->assign("wlcode" , $result);
        $this->display();
    }

    /**
     * address 经销商地址页面
     */
    public function address(){
        $result = M("Company_dealers")->where("id = '".$_GET['id']."'")->find();
        $this->assign("poi" , $result);
        $this->setToken();
        $this->display();
    }

    /**
    * orderWLInfo 通过订单号查询物流码
    */
    public function orderWLInfo(){
        $opt['orderNum'] = I('post.order');
        $m = M('Company_'.session($this->_userCfg['ECID']).'_wl_info');

        $result = $m->where($opt)->field('wlCode as wl_Code')->select();

        $this->assign('wlcode' , $result);
        $this->display('showWlcode');
    }

    /**
     * wlcodeInfo 物流码绑定信息弹窗
     */
    public function wlcodeInfo(){
        $code = I('post.code');

        if(strlen($code) == 20){
            $ecid = session($this->_userCfg['ECID']);
            $companyInfo = $this->getCompanyInfo($ecid);
            $options = array(
                'ecid'     => $ecid,
                'api_user' => $companyInfo['mxt_api'],
                'api_psw'  => $companyInfo['mxt_psw'],
                'OpenId'   => '',
                'fwCode'   => $code,
                'type'     => 'web');

            $mxt = new MxtClient( $options );
            $mxt->FwCheck( $code );
            $code = $mxt->getWlcode();
        }

        $result = M("Company_".session( $this->_userCfg['ECID'] )."_wl_info")->where("wlCode = '".$code."'")->find();
        
        if($result['productId'] != ""){
            $result['product'] = M("Company_product")->where("id = '".$result['productId']."'")->getField('name');
        }

        if($result['SellerId'] != ""){
            $result['seller'] = M("Company_dealers")->where("id = '".$result['SellerId']."'")->getField('name');
        }

        //获取操作人名称
        $result['user'] = M("User_info")->where("id = '".$result['modifyUserId']."'")->getField('user_name');

        $this->assign("wlcode" , $code);
        $this->assign('info' , $result);
        $this->display();
    }

    /**
     * getCompanyInfo 通过ecid获取得到授权的经销商
     */
    private function getCompanyInfo($ecid){
        return M('Company_info')->where("company_ecid = '".$ecid."'")->find();
    }

    /**
     * getBindDealer 通过brandId获取得到授权的经销商
     */
    public function getBindDealer(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $strSQL = "
                SELECT
                    sz12365_fw_company_dealers.id ,
                    sz12365_fw_company_dealers.`name`
                FROM
                    sz12365_fw_company_dealers_bind_info
                JOIN
                    sz12365_fw_company_dealers
                ON 
                    sz12365_fw_company_dealers_bind_info.dealerId = sz12365_fw_company_dealers.id
                WHERE ";

        if(I('post.type') == "brand"){
            $opt = "sz12365_fw_company_dealers_bind_info.brandId = " . I('post.brandId') . " AND sz12365_fw_company_dealers_bind_info.ecid = " . session($this->_userCfg['ECID']);
        }else if(I('post.type') == "product"){
            $opt = "
                    ( ( sz12365_fw_company_dealers_bind_info.brandId = " . I('post.brandId') . " AND sz12365_fw_company_dealers_bind_info.productId = " . I('post.productId') . " ) 
                    OR ( sz12365_fw_company_dealers_bind_info.brandId = " . I('post.brandId') . " AND sz12365_fw_company_dealers_bind_info.productId = -1 ) )
                    AND sz12365_fw_company_dealers_bind_info.ecid = " . session($this->_userCfg['ECID']);
        }
        $result['bind'] = M()->query($strSQL.$opt);
        //判断是否为经销商登陆
        if(session( $this->_userCfg['DEALERID'] )){
            $option['id|root'] = session( $this->_userCfg['DEALERID'] );
        }
        $option['ecid'] = session( $this->_userCfg['ECID'] );  

        $dealerInfo = M('Company_dealers')->where($option)->select();
        $result['unbind'] = array();
        for($i = 0;$i<count($dealerInfo);$i++){
            $isUNBind = true;
            for($j = 0;$j<count($result['bind']);$j++){
                if($dealerInfo[$i]['id'] == $result['bind'][$j]['id'])
                    $isUNBind = false;
            }

            if($isUNBind)
                array_push($result['unbind'] , $dealerInfo[$i]);
        }

        $this->ajaxReturn($result , 'JSON');
    }

    /**
     * getIsBindInfo 获取经销商授权品牌产品信息
     */
    public function getIsBindInfo(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        $m = M("Company_dealers_bind_info");
        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $opt['dealerId'] = $_POST['dealerId'];
        $opt['brandId'] = $_POST['brandId'];

        //判断查询授权信息类型为产品
        if($_POST['type'] == "product"){
            $opt['productId'] = array(array("EQ" , $_POST['productId']) , array("EQ" , -1) , "OR");
        }

        if($m->where($opt)->find()){
            $data['status'] = Error::SUCCESS_OK;
        }else{
            $data['status'] =  Error::ERROR_DEALER_BIND_INFO_NULL;
            $data["info"]   =  Error::getErrMsg(Error::ERROR_DEALER_BIND_INFO_NULL);
        }

        $this->ajaxReturn($data , "JSON");
    }

    /**
     * getDealer 获取经销商信息
     */
    private function getDealer($id){
        
        if($id == 'add'){
            $result = array(
                "name" => "" ,
                "tel" => "" ,
                "WeChat" => "",
                "root" => -1 ,
                "info" => "" ,
                "bigImg" => "" ,
                "minImg" => "" ,
                "handleType" => "add" , 
                "address" => "" ,
                "isLoginUser" => 0
                );
        }else{
            $result = M("Company_dealers")->where("id = '".$id."'")->find();
            $result["handleType"] = "edit";
        }

        return $result;
    }

    /**
     * bindInfoHandle
     */
    public function bindInfoHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("Company_dealers_bind_info");
        $dealerArr = explode(',' , I('post.dealerId'));

        for($i = 0;$i<count($dealerArr)-1;$i++){
            $opt['dealerId'] = $dealerArr[$i];
            $opt['brandId'] = I('post.brandId');
            $opt['productId'] = I('post.productId');
            $opt['ecid'] = session( $this->_userCfg['ECID'] );
            $opt['modifyUserId'] = session( $this->_userCfg['UID'] );
            $opt['modifyTime'] = date("Y-m-d H:i:s");
            $m->add($opt);
        }

        $result['status'] = Error::SUCCESS_OK;
        $this->ajaxReturn($result , "JSON");
    }

    /**
     * dealerHandle 经销商信息操作
     */
    public function dealerHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $m = M("Company_dealers");
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['modifyUserId'] = session($this->_userCfg["UID"]);
        $opt['name'] = I('post.name');
        $opt['tel'] = I('post.tel');
        $opt['WeChat'] = I('post.WeChat');
        $opt['bigImg'] = I('post.bigImg');
        $opt['minImg'] = I('post.minImg');
        $opt['info'] = I('post.info','','');
        $opt['product'] = I('post.product','','');
        $opt['root'] = I('post.root');

        // if($opt['root'] == -1){
        //     $opt['provinceId'] = I('post.provinceId');
        // }else{
        //     $opt['provinceId'] = 0;
        // }

        if(session($this->_userCfg['DEALERID'])){
            $opt['root'] = session($this->_userCfg['DEALERID']);
        }

        //经销商数据库操作
        if(I('post.handleType') == "add"){
            //判断经销商名称是否重复
            if($m->where("name = '".I('post.name')."' AND ecid = ".session($this->_userCfg['ECID']))->find()){
                $result["status"] = Error::ERROR_DEALER_NAME_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_DEALER_NAME_EXIST);
            }else{
                //添加经销商
                $opt['ecid'] = session($this->_userCfg['ECID']);
                
                if($m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }else if(I('post.handleType') == "edit"){
            //判断经销商名称是否重复
            if($m->where("name = '".I('post.name')."' AND id != ".I('post.id')." AND ecid = ".session($this->_userCfg['ECID']))->find()){
                $result["status"] = Error::ERROR_DEALER_NAME_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_DEALER_NAME_EXIST);
            }else{
                if($m->where('id = '.I('post.id'))->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }else if(I('post.handleType') == "delete"){
            $opt = null;
            $opt['sellerId'] = I('post.id');
            //检查该经销商是否进行过绑定操作
            if(M("Company_wl_order")->where($opt)->find()){
                $result["status"] = Error::ERROR_DEALER_BIND_EXIST;
                $result["info"] = Error::getErrMsg(Error::ERROR_DEALER_BIND_EXIST);
            }else{
                //删除百度地图POI
                $result = $m->where("id = ".I('post.id'))->find();

                if($result['poiId'] != "" || $result['poiId'] != null){
                    $baiduMap = new \Admin\BaiduMap\BaiduMap( session( $this->_userCfg["ECID"] ) );
                    $baiduMap->delPoi($result['poiId']);
                }

                //删除经销商信息
                if($m->where("id = '".I('post.id')."'")->delete()){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            }
        }

        $this->ajaxReturn($result , "JSON");
    }

    /**
     * updatePoiHandle 更新百度地图
     */
    public function updatePoiHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'poi' ) );
        $baiduMap = new \Admin\BaiduMap\BaiduMap( session( $this->_userCfg["ECID"] ) );

        $data = array(
            "longitude" => I('post.longitude') ,
            "latitude" => I('post.latitude') ,
            "poiId" => I('post.poiId') ,
            "address" => I('post.address') ,
            "title" => I('post.title')
        );

        $opt = array(
            "id" => I('post.id') ,
            "longitude" => I('post.longitude') ,
            "latitude" => I('post.latitude') ,
            "poiId" => I('post.poiId') ,
            "address" => I('post.address') ,
            );

        if(I('post.poiId') == "" || I('post.poiId') == 0){
            $response = $baiduMap->addPoi( $data , $opt );
        }else{
            $response = $baiduMap->modifyPoi( I('post.poiId') , $data , $opt);
        }

        $this->ajaxReturn( $response );
    }

    /**
     * webHandle 网店信息操作
     */
    public function webHandle(){
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'poi' ) );

        $m = M("Company_dealers_electricity");
        $opt['name'] =I('post.name');
        $opt['url'] = I('post.url');
        $opt['dealerId'] = I('post.dealerId');
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['modifyUserId'] = session( $this->_userCfg['UID'] );

        //判断操作类型
        switch(I('post.handleType')){
            case 'add':
                if($m->add($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            break;
            case 'edit':
                if($m->where("id = '".I('post.id')."'")->save($opt)){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            break;
            case 'delete':
                if($m->where("id = '".I('post.id')."'")->delete()){
                    $result["status"] = Error::SUCCESS_OK;
                }else{
                    $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
            break;
        }
        $this->ajaxReturn($result , "JSON");
    }

    /**
     * authorize 授权书管理栏目
     */
    public function authorize(){
        $ecid = session( $this->_userCfg['ECID'] );
        $this->assign('authoriBrand',$this->getAuthorizeBrand($ecid));
        $this->assign('authoritype',$this->getDeailer());
        $this->assign( 'id', assert($_POST['id'])?$_POST['id']:'all' );// 赋值分页输出
        $this->assign("role",session($this->_userCfg['ROLE']));
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }
    public function authorizeList(){
        $opt['ecid'] = session( $this->_userCfg['ECID']);
        $opt['pass'] = 1;
        if(session( $this->_userCfg['DEALERID'])){
            $opt['dealerId'] = session( $this->_userCfg['DEALERID']);
        }
        if(I('post.id') != '' && I('post.id') != 'all'){
            $opt['dealerId'] = $_POST['id'];
        }
        if(I('post.brandId') != '-1' && I('post.brandId') != '' && I('post.brandId') != 'all'){
            $opt['brandId'] = I('post.brandId');
        }
        if(I('post.productId') != '' && I('post.productId') != 'all'){
            $opt['productId'] = I('post.productId');
        }
        $Data = M( "Company_authorize" );

        $count      = $Data->where($opt)->count();

        $page       = new \Think\Page( $count , 9);// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        for($i = 0;$i<count($result);$i++){
            //获取经销商
            $dealer = M("Company_dealers")->where("id = '".$result[$i]["dealerId"]."'")->find();
            $result[$i]["dealer"] = $dealer["name"];

            //判断是否绑定产品
            if($result[$i]['productId'] != -1){
                //获取产品名称
                $product = M("Company_product")->where("id = '".$result[$i]["productId"]."'")->find();
                $result[$i]['product'] = $product['name'];
                $result[$i]['productId'] = $product['id'];
            }else{
                $result[$i]['product'] = "无";
            }

            //判断是否绑定网店，如果有，显示网店名称
            if($result[$i]['webId']){
                $result[$i]['dealer'] .= "（".M("Company_dealers_electricity")->where("id = ".$result[$i]['webId'])->getField('name')."）";
            }

            //获取品牌名称
            $brand = M("Company_brand")->where("id = '".$result[$i]["brandId"]."'")->find();
            $result[$i]["brand"] = $brand["name"];
            $result[$i]["brandId"] = $brand["id"];

            //获取经销商地址
            $dealer = M("Company_dealers_electricity")->where("dealerId = '".$result[$i]["dealerId"]."'")->find();
            $result[$i]["url"] = $dealer["url"];
        }

        $this->setToken();
        $this->assign( "list" , $result );//赋值数据
        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign("role",session($this->_userCfg['ROLE']));
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }
    }

    /**
    * 函数名：authorizeReview
    * 读取经销商授权书信息
    * @access public
    * @return $data    读取的数据
    * @author 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2014-12-29 创建函数
    */
   public function authorizeReview(){
    $opt['ecid'] = session( $this->_userCfg['ECID']);
    $Data = M( "Company_authorize" );

    $count      = $Data->where('ecid='.$opt['ecid']." and pass = 0 and message is null")->count();

    $page       = new \Think\Page( $count , 9);// 实例化分页类 传入总记录数
    $show       = $page->show();// 分页显示输出

    $result = $Data->where('ecid='.$opt['ecid']." and pass = 0 and message is null")->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();
    for($i = 0;$i<count($result);$i++){//获取经销商
        $dealer = M("Company_dealers")->where("id = '".$result[$i]["dealerId"]."'")->find();
        $result[$i]["dealer"] = $dealer["name"];

        //判断是否绑定产品
        if($result[$i]['productId'] != -1){
            //获取产品名称
            $product = M("Company_product")->where("id = '".$result[$i]["productId"]."'")->find();
            $result[$i]['product'] = $product['name'];
            $result[$i]['productId'] = $product['id'];
        }else{
            $result[$i]['product'] = "无";
        }

        //获取品牌名称
        $brand = M("Company_brand")->where("id = '".$result[$i]["brandId"]."'")->find();
        $result[$i]["brand"] = $brand["name"];
        $result[$i]["brandId"] = $brand["id"];        
    }
    $this->assign( "list" , $result );//赋值数据
    $this->assign( 'page', $show );// 赋值分页输出
    if(session('theme')){
        $this->theme('nifty')->display();
    }else{
        $this->display();
    }
}
    /**
     * 蒋东芸
     * 2014/6/18
     * @return 授权书中的所有品牌
     */
    private function getAuthorizeBrand($ecid){
        $res = M()->distinct(true)->field('brand.name,authorize.brandId')->table(array('sz12365_fw_company_brand'=>'brand','sz12365_fw_company_authorize'=>'authorize'))
            ->where("authorize.ecid = {$ecid} AND brand.id = authorize.brandId")->select();

        return $res;
    }

    public function showProductInfo(){
        $brandId = I('post.brandId');

        $m = M('Company_authorize');
        $opt['brandId'] = $brandId;

        $res = $m->distinct(true)->field('productId')->where($opt)->order('endTime desc')->select();

        for($i=0;$i<count($res);$i++){
            if($res[$i]['productId']=='-1'){
                $this->assign('noBindProduct', '-1');
            }else{
                $result[$i]['name'] = M('Company_product')->where('id = '.$res[$i]['productId'])->getField('name');
                $result[$i]['id'] = $res[$i]['productId'];
            }
        }
        $this->assign('productInfo', $result);
        $this->assign('val', I('post.val'));

        $this->display();
    }

    /**
    * 函数名：passHandle
    * 经销商是否通过审核处理
    * @access public
    * @return $data    读取的数据
    * @author 蒋东芸 <jiangdy@gbarcode.com>
    * 修改历史： 1、 蒋东芸 2014-12-29 创建函数
    */
    public function passHandle(){
        $opt = I('post.');

        $m = M('Company_authorize');
        
        if($m->save($opt)){
            $data['status'] = Error::SUCCESS_OK;
        }else{
            $data['status'] = ERROR_EDIT_HANDLE_ERR;
            $data['info'] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        $this->ajaxReturn($data,"JSON");
    }

    /**
     * getDeailer 获取企业经销商
     */
    private function getDeailer(){
        $deal['ecid'] = session( $this->_userCfg['ECID'] );
        if(session( $this->_userCfg['DEALERID']) ){
            $deal['id|root'] = session( $this->_userCfg['DEALERID'] );
        }
        $res=M("Company_dealers")->where($deal)->order( 'id desc' )->select();

        return $res;
    }

    /**
     * addBrandAuthorize 添加品牌授权书
     */
    public function addBrandAuthorize(){
        $opt['ecid'] = session( $this->_userCfg['ECID']);
        //获取品牌
        $brand = M("Company_brand")->where($opt)->select();

        // //获取经销商
        // if(session( $this->_userCfg['DEALERID']) ){
        //     $opt['id|root'] = session( $this->_userCfg['DEALERID'] );
        // }
        // $dealer = M("Company_dealers")->where($opt)->order( 'id desc' )->select();

        $this->assign("brand" , $brand);
        $this->assign("currentDate" , date("Y-m-d"));
        $this->display();
    }

    /**
     * addProductAuthorize 添加产品授权书
     */
    public function addProductAuthorize(){
        /**
         * 获取拥有产品的品牌
         * @author 蒋东芸
         * 修改日期：2014/7/28
         */
        $Model= new \Think\Model();//实例化空模型      
        $ecid = session( $this->_userCfg['ECID']);
        $opt = "brand.ecid = {$ecid} AND brand.id = product.brandId";
        //获取品牌
        $brand = $Model->field("brand.id,brand.name")->table(array('sz12365_fw_company_brand'=>'brand','sz12365_fw_company_product'=>'product'))
            ->where($opt)->group('brand.name')->select();            

        //获取经销商
        if(session( $this->_userCfg['DEALERID']) ){
            $opt['id|root'] = session( $this->_userCfg['DEALERID'] );
        }
        $dealer = M("Company_dealers")->where($opt)->order( 'id desc' )->select();

        $this->assign("brand" , $brand);
        $this->assign("dealerResult" , $dealer);
        $this->assign("dealerSelect" , $dealer);
        $this->assign("todayDate" , date("Y-m-d"));
        $this->display();
    }

    /**
     * addAuthorizeHandle 添加授权书
     */
    public function addAuthorizeHandle(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));

        $dealerArr = explode(',' , I('post.dealerId'));

        for($i = 0;$i<count($dealerArr)-1;$i++){
            $opt['ecid'] = session($this->_userCfg['ECID']);
            $opt['brandId'] = I('post.brandId');
            $opt['productId'] = I('post.productId');
            $opt['dealerId'] = $dealerArr[$i];
            $opt['startTime'] = I('post.startTime');
            $opt['endTime'] = I('post.endTime');

            if(session($this->_userCfg['ROLE']) == 'admin'){
                $opt['pass'] = 1;
            }

            $webArr = $this->checkWebNum($opt['dealerId']);

            if(count($webArr) == 0){
                $fwcode = M("Company_fwcode")->find();//获取防伪码
                $opt['fwCode'] = $fwcode["fwCode"];
                M("Company_authorize")->data($opt)->add();
                M("Company_fwcode")->where("id = '".$fwcode["id"]."'")->delete();//删除已绑定的防伪码
            }else{
                for($j = 0;$j<count($webArr);$j++){
                    $fwcode = M("Company_fwcode")->find();//获取防伪码
                    $opt['fwCode'] = $fwcode["fwCode"];
                    $opt['webId'] = $webArr[$j]['id'];
                    //$opt['name'] .= "（".$webArr[$j]['name']."）";
                    M("Company_authorize")->data($opt)->add();
                    M("Company_fwcode")->where("id = '".$fwcode["id"]."'")->delete();//删除已绑定的防伪码
                }
            }
        }

        $data["status"] = Error::SUCCESS_OK;
        $data["info"] = Error::getErrMsg(Error::SUCCESS_OK);

        $this->ajaxReturn($data , "JSON");
    }

    public function checkWebNum($dealerId){
        $m = M('Company_dealers_electricity');
        $opt['dealerId'] = $dealerId;
        return $m->where($opt)->select();
    }


    /**
     * getDealerList 获取还没获得该品牌授权书的经销商
     */
    public function getDealerList(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));

        $opt['ecid'] = session($this->_userCfg['ECID']);
        //获取经销商
        if(session( $this->_userCfg['DEALERID']) ){
            $opt['id|root'] = session( $this->_userCfg['DEALERID'] );
        }
        $dealerArr = M('Company_dealers')->where($opt)->select();

        $opt['brandId'] = I('post.brandId');
        if(I('post.productId') == -1){
            $opt['productId'] = -1;
        }else{
            $opt['productId'] = array(array('eq',I('post.productId')) , array('eq',-1) , 'OR');
        }
        $opt['endTime'] = array('gt',date("Y-m-d"));
        $opt['pass'] = 1;
        $authorizeArr = M('Company_authorize')->where($opt)->select();

        $result = array();
        //判断经销商是否有绑定授权书
        for($i = 0;$i<count($dealerArr);$i++){
            $isNoBind = true;
            for($j = 0;$j<count($authorizeArr);$j++){
                if($authorizeArr[$j]['dealerId'] == $dealerArr[$i]['id'])
                    $isNoBind = false;
            }

            if($isNoBind){
                array_push($result , $dealerArr[$i]);
            }
        }

        $this->ajaxReturn($result , 'JSON');
    }


    /**
     * getStockInfo 获取产品库存信息
     */
    public function getProductStockCount(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));

        $m = M("Company_".session( $this->_userCfg['ECID'] )."_stock");
        $opt['dealerId'] = session( $this->_userCfg['DEALERID'] );
        $opt['productId'] = I('post.productId');
        if($result = $m->where($opt)->select()){
            $this->ajaxReturn(count($result));
        }else{
            $this->ajaxReturn(0);
        }
    }


    /**
     * getProduct 获取产品
     */
    public function getProduct(){
        if($result = M("Company_product")->where("brandId = '".I('post.id')."'")->select()){
            $this->ajaxReturn($result, "JSON");
        }else{
            $this->ajaxReturn(-1 , "获取失败！！！" , -1);
        }
    }

    /**
     * chart 显示授权书页面
     */
    public function chart(){
        $m = M("Company_authorize");
        $opt['id'] = $_GET['id'];
        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $result = $m->where($opt)->find();

        //获取经销商名称
        $dealer = M("Company_dealers")->where("id = ".$result['dealerId'])->find();
        $result['name'] = $dealer['name'];

        //获取经销商地址
        if($result['webId']){
            $web = M("Company_dealers_electricity")->where("id = ".$result['webId'])->find();
        }else{
            $web = M("Company_dealers_electricity")->where("dealerId = ".$result['dealerId'])->find();
        }


        $result["url"] = $web["url"];
        $result["webname"] = $web["name"];
        $qr = $_GET['qr'];

        if($result['brandId'] == 88){
            $result['brandId'] = "88new";
        }

        $filePath = '';
        //判断是否存在文件
        if($result['productId'] == -1){
            if($qr == 'none'){
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/Public/chart/'.$result['brandId'].'_noqr.grf';
                $result['grf'] = '/Public/chart/'.$result['brandId'].'_noqr.grf';
            }else{
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/Public/chart/'.$result['brandId'].'.grf';
                $result['grf'] = '/Public/chart/'.$result['brandId'].'.grf';
            }
        }else{
            if($qr == 'none'){
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/Public/chart/'.$result['brandId'].'-'.$result['productId'].'_noqr.grf';
                $result['grf'] = '/Public/chart/'.$result['brandId'].'-'.$result['productId'].'_noqr.grf';
            }else{
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/Public/chart/'.$result['brandId'].'-'.$result['productId'].'.grf';
                $result['grf'] = '/Public/chart/'.$result['brandId'].'-'.$result['productId'].'.grf';
            }
        }

        if(!file_exists($filePath)){
            $result['grf'] = '/Public/chart/'.session($this->_userCfg['ECID']).'_null.grf';
        }

        $this->assign("item" , $result);
        $this->assign('qr', $qr);
        $this->display();
    }

    /**
     * delChart 删除授权书
     */
    public function delChart(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        if(M("Company_authorize")->where($_POST)->delete()){
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }

        $this->ajaxReturn($result , "JSON");
    }

    /**
     * 生成二维码
     * @author 蒋东芸
     */
    public function createdealerQr(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        $m = M("Company_qr_type");
        $opt['ecid'] = session($this->_userCfg['ECID']);
        $opt['dealerId'] = $_POST['id'];
        $opt['type']="dealer";
        $opt['modifyUserId'] = session($this->_userCfg['UID']);
        $opt['modifyTime'] = date("Y-m-d H:i:s");
        $opt['scene_id'] = $this->getSceneId("dealer" , $opt['ecid']);

        if($m->add($opt)){
            $data['status'] = Error::SUCCESS_OK;
        }else{
            $data['status'] = ERROR_ADD_HANDLE_ERR;
            $data['info'] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }
        

        $this->ajaxReturn($data , "JSON");
    }

    /**
     * 点击添加经销商动态框中的确定按钮，将相应的信息写入到表Company_qr_type中ecid、scene_id、dealerId、type字段中
     * @author 蒋东芸
     */
    private function getSceneId($type , $ecid){

        $m = M('Company_qr_type');

        //取出该类型sceneid范围
        $opt['typeName'] = $type;
        $typeZone = M("Company_qr_zone")->where($opt)->find();
        unset($opt['typeName']);
        $opt['ecid'] = $ecid;
        $opt['scene_id'] = array('between' , $typeZone['MinScene'].','.$typeZone['MaxScene']);
        
        //查找该ecid最大sceneid值
        $maxSceneId = $m->where($opt)->max('scene_id');

        if($maxSceneId != ''){
          if($maxSceneId == $typeZone['MaxScene']){
            $sceneId = $typeZone['MinScene'];
          }else{
            $sceneId = $maxSceneId + 1;
          }
        }
        else
          $sceneId = $typeZone['MinScene'];


        return $sceneId;
    }

    /**
     * getQrHandle 获取经销商二维码
     */
    public function getQrHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        

        $m = M("Company_qr_type");
        $opt['dealerId'] = $_POST['id'];
        if($result = $m->where($opt)->find()){
            $data['status'] = Error::SUCCESS_OK;
            $data['info'] = $this->getQrPicUrl($result['ecid'] , $result['scene_id']);
        }
        else{
            $data['status'] = ERROR_PRODUCT_SCENEID_EMPTY;
            $data['info'] = Error::getErrMsg(Error::ERROR_PRODUCT_SCENEID_EMPTY);
        }
        $this->ajaxReturn($data , "JSON");
    }

    /**
     * getQrPicUrl 获取二维码路径
     */
    private function getQrPicUrl($ecid , $sceneId){

        $token = $this->getAppToken($ecid);

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            return;
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
          $qrCode = $weObj->getQRCode($sceneId, 1);
            if($qrCode){
                return $weObj->getQRUrl($qrCode['ticket']);
            }
        }
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }

    public function dealerSelect(){
         $m = M( "Company_dealers" );
         $opt['ecid']=session( $this->_userCfg['ECID'] );
    
        if(I('post.dealerType') == '-1'){
            $opt['root'] =  array('eq','-1');//选择经销商类型查询
            $res=$m->where($opt)->select();
        }
        if(I('post.dealerType') == '0'){
            $opt['root'] = array('neq','-1');
            $res=$m->where($opt)->select();
        }
        if(I('post.dealerType') == 'all'){
            $res=$m->where($opt)->select();
        }

        $this->ajaxReturn($res , "JSON");
    }

    public function qrcode(){
        Vendor( 'phpqrcode.phpqrcode' );
        \QRcode::png("http://zg.fw.szqr.co:88/system/Customer/?e=Yuz&u=".$this->dec62(session($this->_userCfg['NAME'])) , false , 'H' , 20);
    }

    private function dec62($n){
        $base = 62;  
        $index = '4IruOsSkvcQj91VMgyGTWPl5FxwoqHdfn28C0ELz6Nea7UARhB3KYJiZbtXpDm';  
        $ret = '';  
        for($t = floor(log10($n) / log10($base)); $t >= 0; $t --) {  
            $a = floor($n / pow($base, $t));  
            $ret .= substr($index, $a, 1);  
            $n -= $a * pow($base, $t);  
        }  
        return $ret;  
    }
}
?>
