<?php
namespace Admin\Action;
use Org\Error\Error;
class AdAction extends AdminAction {
	public function index(){
        if(session( $this->_userCfg['ECID'] ) =='816'){
            $this->assign('admin','admin');
        }

        $this->assign('type',$this->getEnterpriseInfo());
        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
	}

    //获取所有企业信息
    private function getEnterpriseInfo(){       
        $result = M('Company_info')->select();

        return $result;
    }

    
    public function adSelect(){
        if(I('post.ecid') !='-1' && I('post.ecid') != ''){
            $opt['ecid'] = I('post.ecid');
        }
        $result = M('Company_ad')->where($opt)->select();

        $this->assign('name',$result);
        $this->display();
    }
    public function adList(){
        $m = M('Company_ad');

        if(session( $this->_userCfg['ECID'] ) !='816'){
            $opt['ecid'] = session( $this->_userCfg['ECID'] );
        }else{
            $admin = session( $this->_userCfg['ECID'] );
        }
        if(I('post.ecid')!='' && I('post.ecid')!='-1'){
            $opt['ecid'] = I('post.ecid');
        }
        if(I('post.id')!='' && I('post.id') !="-1"){
            $opt['id'] = I('post.id');
        }
        $count      = $m->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出

        $result = $m->where($opt)->order( 'modifyTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();
        $this->assign('admin',$admin);
        $this->assign('adInfo',$result);
        $this->assign( 'page', $show );// 赋值分页输出
        $this->display();
    }
    public function adInfo(){
        if(I('post.id')=='add'){
            $result = array(
                'handleType' => "add",
                'name' => "",
                'value'=> "",
                'type' => "web",
                'description'=>"",
                'width'=>"",
                'height'=>"",
                'maxNum'=>"",
                'ecid' => "-1"
            );
        }else{
            $result = M("Company_ad")->where("id = '".I('post.id')."'")->find();
            $result['handleType'] = 'edit';
        }
        $this->assign("advertisement" , $result);
        $this->assign('enterprise',$this->getEnterpriseInfo());
        $this->display();
    }
    //广告信息增删改处理
    public function adHandle(){
        if(!IS_POST) 
            _404 ('页面不存在' , U('index'));

        $opt['name'] = I('post.name');
        $opt['value'] = I('post.value');
        $opt['type'] = I('post.type');
        $opt['ecid'] = I('post.ecid');
        $opt['description'] = I('post.description');
        $opt['width'] = I('post.width');
        $opt['height'] = I('post.height');
        $opt['maxNum'] = I('post.maxNum');
        $opt['modifyUserId'] = session( $this->_userCfg['UID'] );
        $opt['modifyTime'] = date("Y-m-d H:i:s");

        $m= M('Company_ad');
        switch (I("post.handleType")) {
            case 'add':
                if($this->inputNameHandle(I('post.name'),I('post.value'))){
                    $result =$m->add($opt);
                    if($result){
                            $data["status"] = Error::SUCCESS_OK;
                    }else{
                        $data["status"] = Error::ERROR_ADD_HANDLE_ERR;
                        $data["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
                    }                
                }else{
                    $data["status"] = Error::ERROR_CONTENT_FALSE;
                    $data["info"] = Error::getErrMsg(Error::ERROR_CONTENT_FALSE);
                }
                break;
            case 'edit':         
                $option['name'] = I('post.name');
                $option['id'] = array('neq',I('post.id'));
                $condition['value'] = I('post.value');
                $condition['id'] =array('neq',I('post.id'));       
                if($m->where($option)->find()){                       
                    $data["status"] = Error::ERROR_ADNAME_EXIST;
                    $data["info"] = Error::getErrMsg(Error::ERROR_ADNAME_EXIST);                
                }else if($m->where($condition)->find()){
                    $data["status"] = Error::ERROR_ADVALUE_EXIST;
                    $data["info"] = Error::getErrMsg(Error::ERROR_ADVALUE_EXIST);
                }else{
                    $opt['id'] = I('post.id');
                    if($m->save($opt)){
                        $data["status"] = Error::SUCCESS_OK;
                    }else{
                        $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                        $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                    } 
                }                
                break;
            case 'delete':
                if($m->where("id = '".I('post.id')."'")->delete()){
                    $data["status"] = Error::SUCCESS_OK;
                }else{
                    $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
                break;
            
            default:
                $data["status"] = Error::ERROR_EDIT_HANDLE_ERR;
                $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                break;
        }
            $this->ajaxReturn($data,"JSON");
    }
    //判断广告名或广告位是否重名
    private function inputNameHandle($inputName,$inputValue){
        $m = M('Company_ad');
        $opt['name'] = $inputName;
        $opt['value'] = $inputValue;
        $opt['_logic'] = 'or';
        if(!$m->where($opt)->find()){
            return true;
        }
        else{
            return false;
        }
    }
}
?>