<?php
namespace Admin\Action;
use Org\Error\Error;
class FansAction extends AdminAction {
    public function index() {        
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        }    
    }
    public function fansList(){
        if(I('post.name') != '')
            $opt['nickname'] = array('like','%'. I('post.name') .'%');//模糊查询 

        $opt['mark'] = 0;
       
        $Data = M("Company_".session( $this->_userCfg['ECID'] )."_user_info");
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign('result' , $result);
        $this->assign('name' ,I('post.name'));
        
        $this->display();
    }

    /**
     * 用户信息更新
     * @author 黄浩 
     * 修改历史： 2014/9/2 创建
     * 
     */
    public function updateFans(){
        $ecid = session($this->_userCfg['ECID'] );
        $m = M("Company_".$ecid."_user_info");
        $result = $m->where("nickname IS NULL AND mark != -1")->select();
        $num = count($result);
        $count = iconv("utf-8", "gb2312", "总共需要更新：$num <br>");//中文转码
        echo  $count; 
        
        for($i = 0;$i<$num;$i++){
          $this->updateFansInfo($ecid , $result[$i]['openId']);
          $count = $i+1; 
        }

        $str = iconv("utf-8", "gb2312", "完成更新!");
        echo $str; 
    }

    public function updateFansInfo($ecid,$openId){
        $token = $this->getAppToken($ecid);
        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
            $option = $weObj->getUserInfo($openId);

            $saveArr['nickname'] = $option['nickname'];
            $saveArr['sex'] = $option['sex'];
            $saveArr['language'] = $option['language'];
            $saveArr['city'] = $option['city'];
            $saveArr['province'] = $option['province'];
            $saveArr['country'] = $option['country'];
            $saveArr['headimgurl'] = $option['headimgurl'];
            M("Company_".$ecid."_user_info")->where("openId = '".$openId."'")->save($saveArr);
        }
    }
    
    public function fwFans(){
        //获取防伪查询记录
        if(I('post.productId') && I('post.productId') != 'all'){
            $opt['productId'] = I('post.productId');
        }
        $opt['fwType'] = array('neq','error');
        $opt['fromEcid'] = session($this->_userCfg['ECID']);
        $Data = M("Company_fw_log");
        $count      = count($Data->distinct(true)->field('openId')->where($opt)->select());// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->distinct(true)->field('openId')->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign("result" , $this->getWeixinUser($result , 'openId'));
        $this->assign("cId" , I('post.cId'));
        $this->assign("product" , $this->getCompanyProduct());
        $this->display();
    }

    public function qrFans(){
        $opt['Event'] = 'SCAN';

        if(I('post.type') != '' && I('post.type') != 'all'){
            $opt['EventKey'] = $this->getUserListInfo(I('post.type') , I('post.id'));
            $this->assign('type' , I('post.type'));
            $this->assign('id' , I('post.id'));
        }

        $Data = M('Company_'.session($this->_userCfg['ECID']).'_wx_log');
        $count      = count($Data->distinct(true)->field('FromUserName')->where($opt)->select());// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->distinct(true)->field('FromUserName')->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign("result" , $this->getWeixinUser($result , 'FromUserName'));
        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign("cId" , I('post.cId'));
        $this->display();
    }

    public function userResponse(){
        $ecid = session($this->_userCfg['ECID']);
        $fansInfo = M('Company_'.$ecid.'_user_info')->where('id = '.I('get.id'))->find();

        $openId = $fansInfo['openId'];
        
        //获取防伪查询记录
        $opt['openId'] = $openId;
        $opt['fromEcid'] = $ecid;
        $opt['fwType'] = array('neq', 'error');
        $opt['productId'] = array('neq', '');
        $fwLog = M('Company_fw_log')->distinct(true)->field('productId')->where($opt)->select();
        for($i = 0;$i<count($fwLog);$i++){
            if($fwLog[$i]['productId'] != ''){
                $productInfo[$i] = M('Company_product')->where('id = '.$fwLog[$i]['productId'])->find();
            }
        }

        //获取扫描记录
        $scanLog = M('Qr_scan_log')->distinct(true)->field('sceneId')->where("openid = '".$openId."'")->select();
        $option['ecid'] = session($this->_userCfg['ECID']);

        $num = 0;
        for($i = 0;$i<count($scanLog);$i++){
            $option['scene_id'] = $scanLog[$i]['sceneId'];
            $row = M('Company_qr_type')->where($option)->find();

            switch ($row['type']) {
                case 'activity':
                    $result['activity'][$num] = M('Company_activity')->where('id = '.$row['activityId'])->find();
                    $num++;
                    break;

                case 'fwLabel':
                    $result['fwLabel'][$num] =  M('Company_fw_label')->where('id = '.$row['labelId'])->find();
                    $num++;
                    break;

                case 'product':
                    $result['product'][$num] = M('Company_product')->where('id = '.$row['productId'])->find();
                    $num++;
                    break;

                case 'dealer':
                    $result['dealer'][$num] = M('Company_dealers')->where('id = '.$row['dealerId'])->find();
                    $num++;
                    break;

                case 'employees':
                    $result['employees'][$num] = M('Company_employees')->where('id = '.$row['employeeId'])->find();
                    $num++;
                    break;
            }
        }
        
        $this->assign('result' , $result);
        $this->assign("fwProduct" , $productInfo);
        $this->assign("openId" , $openId);
        $this->assign("fansInfo", $fansInfo);
        if(session('theme')){
            $this->theme('nifty')->display();
        }else{
            $this->display();
        } 
    }

    public function userLog(){
        $opt['MsgType'] = 'text';
        $opt['FromUserName'] = I('post.openId');
        $Data = M('Company_'.session($this->_userCfg['ECID']).'_wx_log');
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign("openId" , I('post.openId'));
        $this->assign('page' , $show);
        $this->assign('result' , $result);
        $this->display();
    }

    public function sendLog(){
        $opt['ecid'] = session($this->_userCfg['ECID']);
        $opt['openId'] = I('post.openId');
        $Data = M('Company_response');
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $result = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign("openId" , I('post.openId'));
        $this->assign('page' , $show);
        $this->assign('result' , $result);
        $this->display();
    }

    //获取微信用户信息
    private function getWeixinUser($arr , $name){
        $opt['mark'] = 0;
        $m = M('Company_'.session($this->_userCfg['ECID']).'_user_info');
        for($i = 0;$i<count($arr);$i++){
            $opt['openId'] = $arr[$i][$name];
            $result[$i] = $m->where($opt)->find();
        }
        return $result;
    }

    //获取企业产品
    private function getCompanyProduct(){
        $brandOpt['ecid'] = session($this->_userCfg['ECID']);
        $brand = M("Company_brand")->where($brandOpt)->select();
        $product = array();
        $num = 0;
        //获取产品
        for($i = 0;$i<count($brand);$i++){
            $productOpt['brandId'] = $brand[$i]['id'];
            if($productInfo = M("Company_product")->where($productOpt)->select()){
                for($j = 0;$j<count($productInfo);$j++){
                    $product[$num]['id'] = $productInfo[$j]['id'];
                    $product[$num]['name'] = $productInfo[$j]['name'];
                    $num++;
                }
            }
        }
        return $product;
    }

    public function updateUserInfo(){
        $token = $this->getAppToken(session($this->_userCfg['ECID']));
        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
            $option = $weObj->getUserInfo(I('post.openId'));
            $saveArr['nickname'] = $option['nickname'];
            $saveArr['sex'] = $option['sex'];
            $saveArr['language'] = $option['language'];
            $saveArr['city'] = $option['city'];
            $saveArr['province'] = $option['province'];
            $saveArr['country'] = $option['country'];
            $saveArr['headimgurl'] = $option['headimgurl'];
            M("Company_".session($this->_userCfg['ECID'])."_user_info")->where("openId = '".I('post.openId')."'")->save($saveArr);
        }
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }

    //获取不同类型二维码数据
    public function getQrTypeInfo(){
        $type = I('post.type');

        $opt['ecid'] = session($this->_userCfg['ECID']);
        switch ($type) {
            case 'activity':
                $m = M('View_company_activity');
                $opt['_string'] = "type != 'fwLucky' AND type != 'general'";
                break;

            case 'fwLabel':
                $m = M('Company_fw_label');
                break;

            case 'product':
                $this->ajaxReturn($this->getCompanyProduct() , "JSON");
                break;

            case 'dealer':
                $m = M('Company_dealers');
                break;

            case 'employees':
                $m = M('Company_employees');
                break;
        }

        $this->ajaxReturn($m->where($opt)->select() , "JSON");
    }

    //获取对应用户信息
    public function getUserListInfo($type , $id){
        $m = M('Company_qr_type');
        $opt['type'] = $type;
        switch ($type) {
            case 'activity':
                $opt['activityId'] = $id;
                break;

            case 'fwLabel':
                $opt['labelId'] = $id;
                break;

            case 'product':
                $opt['productId'] = $id;
                break;

            case 'dealer':
                $opt['dealerId'] = $id;
                break;

            case 'employees':
                $opt['employeeId'] = $id;
                break;
        }

        $sceneId = $m->where($opt)->getField('scene_id');

        return $sceneId;
    }

    //发送客服消息
    public function sendCustomMessage(){
        $token = $this->getAppToken(session($this->_userCfg['ECID']));

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            $this->ajaxReturn(Error::ERROR_MENU_TOKEN_EMPTY,Error::getErrMsg(Error::ERROR_MENU_TOKEN_EMPTY),0);
        }

        $data['touser'] = I('post.touser');
        $data['msgtype'] = I('post.msgtype');
        $data['text'] = I('post.text','','');

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
            $result = $weObj->sendCustomMessage($data);
            if($result['errcode'] == 0){
                $opt['ecid'] = session($this->_userCfg['ECID']);
                $opt['openId'] = $data['touser'];
                $opt['responseType'] = 'text';
                $opt['content'] = $data['text']['content'];
                $opt['time'] = date("Y-m-d H:i:s");
                M('Company_response')->add($opt);
            }
            $this->ajaxReturn($result);
        }
    }

    //获取分页信息
    private function getPageInfo($table , $option){

    }
}

?>
