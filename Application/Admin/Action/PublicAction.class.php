<?php
namespace Admin\Action;
use Org\Error\Error;
use Think\Action;
class PublicAction extends Action{
    public function _initialize() {
        import( "ORG.Error.Error" );
        $code = $this->checkToken() ;
        if ( $code != Error::SUCCESS_OK ) {
            $this->ajaxReturn( $code, Error::getErrMsg( $code ), $code );
        }
    }

    public function fwcodetxt(){
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     20480000 ;// 设置附件上传大小
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg' , 'txt');// 设置附件上传类型
        $upload->rootPath  =     './Public/fwcode/200113/'; // 设置附件上传根目录
        $upload->saveName  =     'upfile';
        $upload->autoSub   = false;

        $info = $upload->upload(); // 上传文件 
        if(!$info) {// 上传错误提示错误信息
            $req['code'] = -1;
            $this->ajaxReturn( $req );
        }else{// 上传成功
            $req['code'] = 1;
            $this->ajaxReturn( $req );
        }
    }

    public function file() {
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     1048576 ;// 设置附件上传大小
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath  =     './Public/uploads/'; // 设置附件上传根目录 
        $upload->autoSub = false;

        $info = $upload->upload(); // 上传文件 
        if(!$info) {// 上传错误提示错误信息
            $req['code'] = -1;
            $this->ajaxReturn( $req );
        }else{// 上传成功
            $width = I('post.width');
            $height = I('post.height');

            $oldFile = I('post.oldFile');
            $ecid = I('post.ecid');
            
            //将文件上传到OSS
            Vendor( 'Aliyun.Oss' );
            $oss = new \Oss("wx-img");
            $client = $oss->createClient();
            $req = $oss->uploadFile($client, $info['Filedata'], $oldFile);
                     
            $media = $this->uploadToWechat($ecid,$info);//把素材上传到微信素材库
            $req['media_id'] = $media['media_id'];//把该素材在微信素材库的media_id传回上传页，保存图文时使用
         
            unlink('./Public/uploads/'.$info['Filedata']['savename']);

            $this->ajaxReturn( $req );
        }
    }
    
    //把素材上传到微信素材库
     private function uploadToWechat($ecid,$info) {
        $token = $this->getAppToken($ecid);
        if ($token['weixin_AppId'] != '' && $token['weixin_AppSecret'] != '') {
            $weObj = new \Org\Weixin\Wechat();
            if ($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])) {
                $item = array(
                    'media' => '@'.dirname(dirname(dirname(dirname(__FILE__)))) . '/Public/uploads/' . $info['Filedata']['savename']
                );
                $media = $weObj->uploadForeverMedia($item, 'image');
                return $media;
            }
        }
        
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }
    
    public function fileXls() {
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     1048576 ;// 设置附件上传大小
        $upload->exts      =     array('xlsx','xls');// 设置附件上传类型
        $upload->rootPath  =     './Public/uploads/'; // 设置附件上传根目录 
        $upload->autoSub = false;

        $info = $upload->upload(); // 上传文件 
        if(!$info) {// 上传错误提示错误信息
            $req['code'] = -1;
            $this->ajaxReturn( $req );
        }else{// 上传成功
            $this->ajaxReturn( $info );
        }
    }

    private function imgCut($imgPath, $width, $height){
        $image = new \Think\Image(); 
        $image->open($imgPath);
        // 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.jpg
        $image->thumb($width, $height)->save($imgPath.'.png');
    }

    protected function checkToken() {
        if ( I('post.timestamp' ) == null )
            return Error::ERROR_TIMESTAMP_NULL;
        if ( I('post.token' ) == null )
            return Error::ERROR_TOKEN_NULL;

        if ( md5( I('post.timestamp' ) . C( 'SAFE_KEY' ) ) != I('post.token' ) )
            return Error::ERROR_TOKEN_WRONG;

        return Error::SUCCESS_OK;
    }
}
?>
