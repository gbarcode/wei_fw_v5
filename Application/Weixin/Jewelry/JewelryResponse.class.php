<?php
namespace Weixin\Jewelry;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class JewelryResponse extends Response {
    private $employeeInfo;

    public function __construct( $companyInfo, $wechatMsg ) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    /**
     * getResponse获取证书信息
     * @author:范小宝
     * @return array
     */
    public function getResponseFromJewelryNum($jewelryNum){
        //获取大图
        $img = M('Company_fw_reply')->where('ecid = 816')->getField('rightImg');
        if(substr($img,0,4) != 'http')
            $img = \Weixin\Response\WechatConst::SERVER_DOMAIN . $img;
        else
            $img = $img;

        if(strtoupper(substr($jewelryNum,0,2)) == 'SZ'){
            $orderArr = M("Exdata",'sz12365_fw_',C('JEWELRY_CHECK_DB_LINK'))->where("test_no = '".$jewelryNum."'")->find();
            $webUrl = C('CLIENT_WEB_NAME.'.$this->companyInfo['ecid']).'?nofw='.$orderArr['fwcode'];
        }else{
            $orderArr = M($this->companyInfo['ecid'].C('JEWELRY_CHECK_TABLE_NAME'),'sz12365_fw_',C('JEWELRY_CHECK_DB_LINK'))->where("testNo = '".$jewelryNum."'")->find();
            $webUrl = C('CLIENT_WEB_NAME.'.$this->companyInfo['ecid']).'?fw='.$orderArr['fwCode'];
        }

        if(!$orderArr){
            $reply['type'] = WechatConst::RESPONSE_TYPE_TEXT;
            $reply['content'] = "证书不存在！";
        }else{
            $reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
            $reply['content'] = array(
                array(
                    'Title'=>'微防伪证书验证结果——验证通过',
                    'Description'=>"",
                    'PicUrl'=> $img,
                    'Url'=> $webUrl ),
                array(
                    'Title'=>'证书编号：'.$jewelryNum.'，点击查看证书详情',
                    'PicUrl'=> \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Public/Image/logo.jpg',
                    'Url'=> $webUrl )
                );
        }

        return $reply;
    }
}
?>