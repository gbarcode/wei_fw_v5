<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Weixin\Event;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class EventResponse extends Response{

    public function __construct( $companyInfo, $wechatMsg ) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    public function getResponse() {
        $response = null;
        switch ( $this->wechatMsg['Event'] ) {
        case 'subscribe':
            $this->changeUserMark(0);
            $response = $this->subscribeEvent();
            break;
        case 'CLICK':
            $response = $this->companyMenuResponse();
            break;
        case 'LOCATION':
            $this->saveUserLocation();
            break;
        case 'SCAN':
            $response = $this->scanResponse();
            break;
        case 'unsubscribe':
            $this->changeUserMark(-1);
            break;
        case 'scancode_waitmsg':
            $array = array();
            if (!is_array($this->wechatMsg['SendPicsInfo'])) {
                $array=(array)$this->wechatMsg['ScanCodeInfo'];
                $this->wechatMsg['ScanCodeInfo']=$array;
            }else {
                $array=$this->wechatMsg['ScanCodeInfo'];
            }

            if($this->companyInfo['ecid'] == 200004){
                $scan = new \Weixin\Scan\ScanResponse($this->companyInfo, $this->wechatMsg,$this->wechatMsg['ScanCodeInfo']['ScanResult']);
                
                $response['type'] = 'text';
                $response['content'] = $scan->codeScanResponse();
            }else{
                $response['type'] = 'text';
                $response['content'] = $array['ScanResult'];
            }
            
            break;
        default:
            break;
        }
        return $response;
    }

    private function scanResponse(){
        $response['type'] = news;
        $response['content'] = $this->qrSceneEvent(false);

        return $response;
    }

    private function subscribeEvent() {
        $response = $this->getWelcomeResponse();

        $bSubItem = true;
        if($response == ''){
            $response['type'] = 'news';
            $response['content'] = array();
            $bSubItem = false;
        }

        if($this->wechatMsg['EventKey']){
            $response['ext'] = $this->qrSceneEvent(false);
        }


        return $response;
    }

    private function qrSceneEvent($bSubItem){
        //保存扫描事件
        $this->saveScanLog();
        
        $scene = explode('_', $this->wechatMsg['EventKey']);

        if(count($scene) > 1)
            $sceneId = $scene[1];
        else
            $sceneId = $scene[0];

        $m = M('Company_qr_type');
        $opt = array(
            'ecid'     => $this->companyInfo['ecid'],
            'scene_id' => $sceneId);

        $result = $m->where($opt)->find();

        $addResponse = '';
        if($result){
            switch ($result['type']) {
                case 'activity':
                    $activity = new \Weixin\Activity\ActivityResponse($this->companyInfo, $this->wechatMsg);
                    $activity->responseScan($result['activityId'], $bSubItem);
                    $addResponse = $activity->getResponse();
                    break;

                case 'fwLabel':
                    $label = new \Weixin\Label\LabelResponse($this->companyInfo, $this->wechatMsg);
                    $addResponse = $label->getResponse($result['labelId']);
                    break;

                case 'product':
                    $product = new \Weixin\Product\ProductResponse($this->companyInfo, $this->wechatMsg);
                    $addResponse = $product->getResponse($result['productId'] , 'all');
                    break;

                case 'employees':
                    $employee = new \Weixin\Employee\EmployeeResponse($this->companyInfo, $this->wechatMsg);
                    $addResponse = $employee->getResponseWithId($result['employeeId']);
                    break;

                case 'dealer':
                    $dealer = new \Weixin\Dealer\DealerResponse($this->companyInfo, $this->wechatMsg);
                    $addResponse = $dealer->getResponseWithId($result['dealerId'] , 'all');
                    break;
            }
        }

        if($addResponse)
            return $addResponse;
    }

    private function getWelcomeResponse(){
        $m = M( 'Company_event_response' );
        $condition['ecid']  = $this->companyInfo['ecid'];
        $condition['event'] = $this->wechatMsg['Event'];

        $result = $m->where($condition)->find();

        if ( $result != '' ) {
            $reply = array(
                'type' => $result['responseType'],
                'content' => $result['responseText'],
                'materialID' => $result['responseMaterialId']
                );

            $response =  $this->getWechatResponse( $reply );

            return $response;
        }
    }

    private function saveUserLocation(){
        $m = M('Company_'.$this->companyInfo['ecid'].'_user_info');

        $opt = array(
            'openId' => $this->wechatMsg['FromUserName']);

        $saveOpt = array(
            'longitude' => $this->wechatMsg['Longitude'],
            'latitude' => $this->wechatMsg['Latitude'],
            'locationUpdate' => date( "Y-m-d H:i:s" ));

        $m->where($opt)->data($saveOpt)->save();
    }

    private function companyMenuResponse() {
        $menu_cache = 'weixin_menu'. $this->companyInfo['ecid'].$this->wechatMsg['EventKey'];
        $response = '';

        $table            = 'Company_menu';
        $m                = M( $table );
        $condition['key'] = $this->wechatMsg['EventKey'];
        $result           = $m->where( $condition )->find();

        if ( $result != '' ) {
            $reply['type'] = $result['responseType'];

            switch ( $reply['type'] ) {
            case WechatConst::RESPONSE_TYPE_TEXT:
                $reply['content'] = $result['responseText'];
                $response = $this->getWechatResponse( $reply );
                break;
            case WechatConst::RESPONSE_TYPE_NEWS:
                $reply['materialID'] = $result['responseMaterialId'];
                $response = $this->getWechatResponse( $reply );
                break;
            case WechatConst::RESPONSE_TYPE_INFORMATION:
                $response = $this->getNewsResponse($result['informKeyword']);
                break;
            case WechatConst::RESPONSE_TYPE_CHECK_PRIZE:
                $activityId = $result['informKeyword'];
                $response = $this->getCheckPrizeResponse($activityId);
                break;
            case WechatConst::RESPONSE_TYPE_SERVICE:
                $response = $this->getServiceResponse($this->companyInfo['ecid']);
                break;
            case WechatConst::RESPONSE_TYPE_MALL:
                $response = $this->getMallResponse($result['mall_keyword']);
                break;
            case WechatConst::RESPONSE_TYPE_VIP:
                $response = $this->getVipResponse($result['vip_keyword']);
                break;
            case WechatConst::RESPONSE_TYPE_REPORT:
                $response = $this->getReportResponse();
                break;
            case WechatConst::RESPONSE_TYPE_ACTIVITY:
                $activity = new \Weixin\Activity\ActivityResponse($this->companyInfo, $this->wechatMsg);
                $response = $activity->getActivityResponse($result['activityId']);
            default :
                break;
            }
        }

        if($response){
            return $response;
        }
    }

    private function getActivityInfo($activityId){
        $m = M('Company_activity');

        $result = $m->find($activityId);

        return $result;
    }

    /*
     * 函数：saveScanLog
     * 作用：保存扫描信息
     * @author:范小宝
     */
    private function saveScanLog(){
        $scene = explode('_', $this->wechatMsg['EventKey']);

        if(count($scene) > 1)
            $sceneId = $scene[1];
        else
            $sceneId = $scene[0];

        $m = M("Qr_scan_log");
        $opt['ecid'] = $this->companyInfo['ecid'];
        $opt['openid'] = $this->wechatMsg['FromUserName'];
        $opt['sceneId'] = $sceneId;
        $opt['scanTime'] = date("Y-m-d H:i:s");
        $m->add($opt);
    }

    /*
     * 函数：changeUserMark
     * 作用：修改微信粉丝标识
     * @author:范小宝
     */
    private function changeUserMark($mark){
        $opt['mark'] = $mark;
        M('Company_'.$this->companyInfo['ecid'].'_user_info')->where("openId = '".$this->wechatMsg['FromUserName']."'")->save($opt);
    }
}
?>
