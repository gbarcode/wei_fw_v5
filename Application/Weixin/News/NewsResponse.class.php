<?php
namespace Weixin\News;
class NewsResponse {
	private $keyword;   //关键词
	private $ecid;  //厂商编号
	private $companyName;  //厂商名称
	private $companyInfo;

	const NEW_TYPE_KEYWORD = "<-new->";   //搜索最新新闻，无关键词
	const SORT_TYPE_KEYWORD = "<-sort->";  //区分类别

	const NEWS_SERVER_DOMAIN = "http://www.sz12365.net";   //域名

	public function __construct( $keyword, $companyInfo ) {
		$this->keyword = $keyword;
		$this->companyInfo = $companyInfo;
	}


	public function getNewsResponse() {
		if ( $this->companyInfo['ecid'] !='816' && $this->companyInfo['company_name'] == null )
			return;

		//显示最新的新闻
		if ( $this->keyword == self::NEW_TYPE_KEYWORD ) {
			return $this->getNewReply();
		}

		//按类型的新闻
		if ( substr( $this->keyword, 0, strlen( self::SORT_TYPE_KEYWORD ) ) == self::SORT_TYPE_KEYWORD ) {
			return $this->getSortReply();
		}

		//按关键词搜索
		return $this->getKeywordReply();
	}

	private function getKeywordReply() {
		$news   = M( 'sz12365.Newsinfo', 'sz12365_ecms_' );
		$param  = ( $this->companyInfo['ecid']=='816' )?'':"name = '" . $this->companyInfo['company_name'] . "' AND ";
		$param  = $param . "(find_in_set('". $this->keyword ."',keyboard) OR title LIKE '%" .$this->keyword. "%')";
		$result = $news->where( $param )->order( 'newspath desc' )->limit( 6 )->select();
		return $this->weixinFormat( $result );
	}

	private function getNewReply() {
		$news   = M( 'sz12365.Newsinfo', 'sz12365_ecms_' );

		$param = null;
		if ( $this->companyInfo['company_name'] != null )
			$param['name'] = $this->companyInfo['company_name'];
		$result = $news->where( $param )->order( 'newspath desc' )->limit( 6 )->select();
		return $this->weixinFormat( $result );
	}

	private function getSortReply() {
		$news   = M( 'sz12365.Newsinfo', 'sz12365_ecms_' );

		$param = null;
		if ( $this->companyInfo['company_name'] != null )
			$param['companyName'] = $this->companyInfo['company_name'];

		$param['WeixinSort'] = substr( $this->keyword, strlen( self::SORT_TYPE_KEYWORD ) );
		$result = $news->where( $param )->order( 'newspath desc' )->limit( 6 )->select();

		return $this->weixinFormat( $result );
	}

	private function weixinFormat( $result ) {
		if ( $result ) {
			for ( $i=0; $i<count( $result ); $i++ ) {
				$newsArr[$i]['Title']       = $result[$i]['title'];
				$newsArr[$i]['Description'] = $result[$i]['title'];
				$newsArr[$i]['PicUrl']      = self::NEWS_SERVER_DOMAIN .$result[$i]['titlepic'];
				$newsArr[$i]['Url']         = self::NEWS_SERVER_DOMAIN . "/fangweimachaxun/chaxunjieguo/weizhuye/weimenhuzixun/index.php?id=" . $result[$i]['id'];
			}

			return array( 'type' => 'news',
				'content' => $newsArr );
		}
	}
}
?>
