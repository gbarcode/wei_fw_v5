<?php
namespace Weixin\Activity;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class ActivityResponse extends Response{
	private $activity;
	private $responseInfo;
    private $fwParams;
	private $url = 'http://www.msa12365.com/index.php/Home/Index/activityNews/id/';

    private $luckyReply = '感谢您参与《{-活动名称-}》。恭喜您获得《{-奖品类型-}，奖品：{-奖品名称-}》，请点击输入个人信息。';
    private $unluckyReply = '感谢您参与《{-活动名称-}》,您未中奖。';
    private $hasluckyReply = '感谢您参与《{-活动名称-}》,您已中奖。请点击输入个人信息';

	public function __construct( $companyInfo, $wechatMsg) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    public function hasActivity(){
    	$m = M('View_company_activity');

        $date = date( "Y-m-d H:i:s" );

        $opt['ecid'] = $this->companyInfo['ecid'];
        $opt['_string'] = "(timeType='limit' AND startTime <= '$date' AND endTime >= '$date') OR timeType='unlimit'";
        
        $result = $m -> where($opt) -> select();

        if($result){
            $this->activity = $result;
            return true;
        }
        else
            return false;
    }

    public function getResponse(){
        if($this->responseInfo != '')
            return $this->responseInfo;

        return false;
    }

    public function hasFwLucky($opt){
        $this->fwParams = $opt;
    	$activity = $this->getBinProductActivicy($opt);
        if($activity){
            $this->responseFwLucky($activity);
            return true;
        }
        else{
            $activity = $this->getBinBrandActivicy($opt);
            if($activity){
                $this->responseFwLucky($activity);
                return true;
            }
            else{
                $activity = $this->getAllBrandActivicy($opt);
                if($activity){
                    $this->responseFwLucky($activity);
                    return true;
                }
            }
        }

		return false;
    }

    public function hasGeneralActivity(){
        foreach ($this->activity as $item)
        {
            if($item['type'] == 'general'){
                $this->responseGeneral($item);
                return true;
            }
        }

        return false;
    }

    public function responseScan($activityId, $bSubItem = true){
        if(!$activityId)
            return false;

        $m = M('View_company_activity');
        $opt['id'] = $activityId;
        $result = $m->where($opt)->find();

        if($result){
            switch ($result['type']) {
                case 'scanLucky':
                    $this->responseScanLucky($result, $bSubItem);
                    break;
                case 'sign':
                    $this->responseSign($result, $bSubItem);
                    break;
                case 'share':
                    $this->responseShare($result, $bSubItem);
                default:
                    # code...
                    break;
            }
        }
    }

    /**
    * 定义函数
    * getActivityResponse
    * 函数功能描述
    * 获取活动相关的菜单回复
    * @access public
    * @param int $activityId 活动id
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-11 创建函数
    */
    public function getActivityResponse($activityId){
        $m = M('Company_activity');
        $opt['id'] = $activityId;
        $result = $m->where($opt)->find();

        $this->responseInfo['type'] = WechatConst::RESPONSE_TYPE_NEWS;
        $this->responseInfo['content'] = array(
            array(
            'Title'  => $result['name'],
            'PicUrl' => $this->getImgUrl($result['bigImg']),
            'Url'    => $this->url.$result['id'])
        );

        switch ($result['type']) {
            case 'question':
                $data['ecid'] = $result['ecid'];
                $data['activityId'] = $activityId;
                $data['type'] = $result['type'];

                $this->responseInfo['content'][1] = array(
                    'Title'  => '点击参与活动' ,
                    'PicUrl' => $this->getImgUrl($result['minImg']),
                    'Url'    => $this->getQuestionRedirectUrl($data , $this->getAppId($result['ecid']))
                    );
                break;

            case 'scanLucky':
                $this->responseScan($activityId, false);
                break;

            case 'meeting':
                $url = \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Home/Activity/meeting/openId/'.$this->wechatMsg['FromUserName'].'/id/'.$activityId;
                $this->responseInfo['content'][1] = array(
                    'Title'  => '点击进行会议报名' ,
                    'PicUrl' => $this->getImgUrl($result['minImg']),
                    'Url'    => $url
                    );
                break;
            
            default:
                # code...
                break;
        }

        return $this->responseInfo;
    }

    private function saveJoinUser($ecid , $activityId , $isLucky){
        //获取需要存储的数据
        $opt['userId'] = M('Company_'.$ecid.'_user_info')->where("openId = '".$this->wechatMsg['FromUserName']."'")->getField('id');
        $opt['ecid'] = $ecid;
        $opt['joinTime'] = date( "Y-m-d H:i:s" );
        $opt['activityId'] = $activityId;
        $opt['hasLuckyChange'] = $isLucky;

        //将获取到的数据添加到activity_join表中
        M('Company_activity_join')->add($opt);
    }

    /**
    * 定义函数
    * getAppId
    * 函数功能描述
    * 获取企业微信appid
    * @access private
    * @param int $ecid 企业id
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-11 创建函数
    */
    private function getAppId($ecid){
        $m = M('Company_info');
        $opt['company_ecid'] = $ecid;
        return $m->where($opt)->getField('weixin_AppId');
    }

    /**
    * 定义函数
    * getQuestionRedirectUrl
    * 函数功能描述
    * 获取问题活动的链接
    * @access private
    * @param array $data
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-11 创建函数
    */
    private function getQuestionRedirectUrl($data , $weixin_AppId){
        $url = \Weixin\Response\WechatConst::REDIRECT_DOMAIN;
        $params = http_build_query($data);
        $url .= $params;

        $response = new \Weixin\Response\Response();
        return $response->getOauthRedirectUrl($url , $weixin_AppId);
    }

    private function responseSign($activityInfo, $bSubItem = true){
        $sign = new \Weixin\SignIn\SignInResponse($activityInfo, $this->wechatMsg['FromUserName'], $this->companyInfo['ecid']);

        $this->responseInfo = $sign->signIn()->response();
    }

    private function responseShare($activityInfo, $bSubItem = true){
        $config = json_decode($activityInfo['configure'], true);

        $date=date("Y-m-d");
        if(($date>=$activityInfo['startTime'] && $date<=$activityInfo['endTime']) || $activityInfo['timeType'] == 'unlimit'){
            $params['type'] = 'news';
            $params['materialID'] = $config['materialId'];

            $response = $this->getWechatResponse($params);

            $this->responseInfo = $response['content'];
        }
    }

    private function getExchageCode($activityId , $openId){
        $m = M('Company_lucky_user');
        $opt['activityId'] = $activityId;
        $opt['userId'] = $openId;
        return $m->where($opt)->getField('exchangeCode');
    }

    private function responseScanLucky($activityInfo, $bSubItem = true){
        $isLucky = false;
        if($activityInfo){
            $date=date("Y-m-d");
            if(($date>=$activityInfo['startTime'] && $date<=$activityInfo['endTime']) || $activityInfo['timeType'] == 'unlimit'){
                if($activityInfo['luckyReply'])
                    $this->luckyReply = $activityInfo['luckyReply'];

                if($activityInfo['unluckyReply'])
                    $this->unluckyReply = $activityInfo['unluckyReply'];

                $sceneArr = explode('_', $this->wechatMsg['EventKey']);

                if(count($sceneArr) > 1)
                    $sceneId = $sceneArr[1];
                else
                    $sceneId = $sceneArr[0];

                if($this->hasScanQrCode($sceneId) && $this->checkUserJoin($activityInfo['id'] , 100002)){
                    if($activityInfo['id'] == 251)
                        $title = '感谢您参与《' . $activityInfo['name']. '》。您已参加过本次活动！您的兑奖码是'.$this->getExchageCode($activityInfo['id'] , $this->wechatMsg['FromUserName']);
                    else
                        $title = '感谢您参与《' . $activityInfo['name']. '》。您已参加过本次活动！';
                }
                else{
                    $activityInfo['userCount'] += 1;
                    $isLucky = false;
                    if($activityInfo['userCount'] >= $activityInfo['luckyNum']){
                        $prize = $this->getPrize($activityInfo['id']);

                        if($prize){
                            $this->saveJoinUser($this->companyInfo['ecid'] , $activityInfo['id'] , 1);
                            if($activityInfo['userCount'] > $activityInfo['luckyNum'])
                                $activityInfo['luckyNum'] = $activityInfo['userCount'];
                        
                            $activityInfo['luckyNum'] += $activityInfo['baseNumber'];

                            if($activityInfo['id'] == 264){
                                $title = "感谢您参与《{-活动名称-}》。恭喜您获得《{-奖品类型-}，奖品：{-奖品名称-}》。";
                            }else{
                                $title = $this->luckyReply;
                            }

                            $title = str_replace( '{-活动名称-}', $activityInfo['name'], $title);
                            $title = str_replace( '{-奖品类型-}', $prize['prizeType'], $title);
                            $title = str_replace( '{-奖品名称-}', htmlspecialchars_decode($prize['prizeName']), $title);

                            $isLucky = true;
                        }
                        else{
                            $this->saveJoinUser($this->companyInfo['ecid'] , $activityInfo['id'] , 0);
                            $title = $this->unluckyReply;
                            $title = str_replace( '{-活动名称-}', $activityInfo['name'], $title);
                        }
                    }
                    else{
                        $this->saveJoinUser($this->companyInfo['ecid'] , $activityInfo['id'] , 0);
                        $title = $this->unluckyReply;
                        $title = str_replace( '{-活动名称-}', $activityInfo['name'], $title);
                    }

                    $data = M('Company_activity');
                    unset($activityInfo['typeName']);
                    $data->save($activityInfo);
                }

                if($bSubItem){
                    $this->responseInfo = array(array(
                        'Title'  => $title,
                        'PicUrl' => $this->getImgUrl($activityInfo['minImg'])
                        ));
                }
                else{
                    if($activityInfo['id'] == 251 && $isLucky){
                        $title = str_replace( '一等奖，', '', $title);
                        $title = str_replace( '二等奖，', '', $title);
                        $title = str_replace( '三等奖，', '', $title);
                        $title = $title." 您的兑奖码是".$this->getExchageCode($activityInfo['id'] , $this->wechatMsg['FromUserName']);
                    }

                    $this->responseInfo = array(
                        array(
                        'Title'  => $activityInfo['name'],
                        'PicUrl' => $this->getImgUrl($activityInfo['bigImg']),
                        'Url'    => $this->url.$activityInfo['id']),
                        array(
                        'Title'  => $title,
                        'PicUrl' => $this->getImgUrl($activityInfo['minImg'])
                        ));

                    if($isLucky){
                        $this->responseInfo[1]['Url'] = WechatConst::SERVER_DOMAIN . "/index.php/Activity/index/activityId/{$activityInfo['id']}/openId/{$this->wechatMsg['FromUserName']}";
                    }
                }
            }
        }
    }

    private function checkUserJoin($activityId , $ecid){
        if($ecid == 100002){
            $m = M('Company_activity_join');
            $opt['ecid'] = $ecid;
            $opt['activityId'] = $activityId;
            $opt['userId'] = M('Company_'.$ecid.'_user_info')->where("openId = '".$this->wechatMsg['FromUserName']."'")->getField('id');
            if($m->where($opt)->find()){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    private function getBinProresponseScanctActivicy($opt){
        foreach ($this->activity as $item)
        {
            if($item['type'] == 'fwLucky'){
                if($item['bindProductId'] == $opt['ProductId'] && $item['bindBrandId'] == $opt['BrandId'])
                    return $item;
            }
        }
        return false;
    }

    private function getBinProductActivicy($opt){
        foreach ($this->activity as $item)
        {
            if($item['type'] == 'fwLucky'){
                if($item['bindProductId'] == $opt['ProductId'] && $item['bindBrandId'] == $opt['BrandId'])
                    return $item;
            }
        }
        return false;
    }

    private function getBinBrandActivicy($opt){
        foreach ($this->activity as $item)
        {
            if($item['type'] == 'fwLucky'){
                if($item['bindProductId'] == -1 && $item['bindBrandId'] == $opt['BrandId'])
                    return $item;
            }
        }
        return false;
    }

    private function hasScanQrCode($sceneId){
        $m = M('Company_'.$this->companyInfo['ecid'].'_user_info');
        $opt = array(
            'openId' => $this->wechatMsg['FromUserName']);

        $result = $m->where($opt)->find();

        if($result){
            $sceneArr = explode(',', $result['scanQrScene']);

            if(array_search($sceneId, $sceneArr) !== false){
                return true;
            }
            else{
                if($result['scanQrScene'])
                    $result['scanQrScene'] .= ',' . $sceneId;
                else
                    $result['scanQrScene'] = $sceneId;

                $m->save($result);

                return false;
            }
        }
    }

    private function getAllBrandActivicy(){
        foreach ($this->activity as $item)
        {
            if($item['type'] == 'fwLucky'){
                if($item['bindProductId'] == -1 && $item['bindBrandId'] == -1)
                    return $item;
            }
        }
        return false;
    }

    private function responseGeneral($activity){
        $this->responseInfo = array(array(
            'Title' => $activity['name'],
            'PicUrl' => WechatConst::SERVER_DOMAIN .$activity['minImg'],
            'Url' => $activity['replyType']=='url'?$activity['url']:$this->url));
    }

    private function responseFwLucky($activity){
        //获取参与防伪活动用户的openId，用来取出当前用户的id
        $Data = M('Company_'.$activity['ecid'].'_user_info');
        $condition['openId'] = $this->wechatMsg['FromUserName'];

        //获取需要存储的数据
        $opt['userId'] = $Data->where($condition)->getField('id');
        $opt['ecid'] = $activity['ecid'];
        $opt['joinTime'] = date( "Y-m-d H:i:s" );
        $opt['activityId'] = $activity['id'];
        $opt['hasLuckyChange'] = 1;

        //将获取到的数据添加到activity_join表中
        M('Company_activity_join')->add($opt);

        $isLucky = false;
        if($this->checkUserLucky($activity['id'], $this->wechatMsg['FromUserName'])){
            $title = $this->hasluckyReply;
            $title = str_replace( '{-活动名称-}', $activity['name'], $title);
        }
        else{
            unset($activity['typeName']);
            if($activity['luckyReply'])
                $this->luckyReply = $activity['luckyReply'];

            if($activity['unluckyReply'])
                $this->unluckyReply = $activity['unluckyReply'];

            if($this->fwParams['CheckCount'] == 0)
                $activity['userCount'] += 1;
            
            if($activity['userCount'] == $activity['luckyNum'] && $this->fwParams['CheckCount'] == 0){
                $prize = $this->getPrize($activity['id']);

                if($prize){
                    $activity['luckyNum'] += $activity['baseNumber'];

                    $title = $this->luckyReply;
                    $isLucky = true;
                    $title = str_replace( '{-活动名称-}', $activity['name'], $title);
                    $title = str_replace( '{-奖品类型-}', $prize['prizeType'], $title);
                    $title = str_replace( '{-奖品名称-}', $prize['prizeName'], $title);
                }
                else{
                    $title = $this->unluckyReply;
                    $title = str_replace( '{-活动名称-}', $activity['name'], $title);
                }
            }
            else{
                $title = $this->unluckyReply;
                $title = str_replace( '{-活动名称-}', $activity['name'], $title);
            }

            $m = M('Company_activity');
            $m->startTrans();
            $m->save($activity);
            $m->commit();
        }

        $this->responseInfo = array(array(
            'Title'  => $title,
            'PicUrl' => $this->getImgUrl($activity['minImg'])));

        if($isLucky){
            $this->responseInfo[0]['Url'] = WechatConst::SERVER_DOMAIN . "/index.php/Activity/index/activityId/{$activity['id']}/openId/{$this->wechatMsg['FromUserName']}";
        }
    }

    private function checkUserLucky($activityId, $openId){
        $m = M('Company_lucky_user');

        $opt['activityId'] = $activityId;
        $opt['userId'] = $openId;

        $result = $m->where($opt)->find();

        if($result)
            return true;

        return false;
    }

    private function getPrize($activityId){
    	$m = M('Company_lucky_prize');

    	$opt['activityId'] = $activityId;
        $arrPrize = array();

        $result = $m->where($opt)->select();

        foreach ($result as $prize){
            for($i=0; $i<($prize['num']-$prize['luckyCount']); $i++){
                $item['id'] = $prize['id'];
                $item['prizeType'] = $prize['prizeType'];
                $item['prizeName'] = $prize['prizeName'];
                array_push($arrPrize, $item);
            }
        }

        shuffle($arrPrize);
        shuffle($arrPrize);

        $luckyPrize = $arrPrize[rand(0, count($arrPrize))];
        if($this->saveLuckyUser($activityId, $luckyPrize['id']))
            return $luckyPrize;
        else
            return false;
    }

    private function saveLuckyUser($activityId, $prizeId){
        $m = M('Company_lucky_prize');
        $opt['id'] = $prizeId;
        $opt['luckyCount'] = array('exp','luckyCount+1');

        $result = $m->save($opt);

        if($result){
            $data = M('Company_lucky_user');

            $option['activityId'] = $activityId;
            $option['prizeId'] = $prizeId;
            $option['userId'] = $this->wechatMsg['FromUserName'];
            $option['fwCode'] = $this->wechatMsg['Content'];
            $option['luckyTime'] = date( "Y-m-d H:i:s" );

            //判断是否为安琪马拉松活动
            if($activityId == 251){
                $count = $data->where('activityId = '.$activityId)->count();
                $option['exchangeCode'] = $count+1;
            }else{
                $option['exchangeCode'] = md5($option['userId']);
            }

            if($data->add($option))
                return true;
        }

        return flase;
    }
}
?>