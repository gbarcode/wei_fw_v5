<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Weixin\Keyword;
use Weixin\Response\Response;
use Org\Mxt\MxtClient;
class KeywordResponse extends Response{

    public function __construct( $companyInfo, $wechatMsg) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    /**
     * getResponse获取回复信息
     *
     * @return Array 回复信息
     */
    public function getResponse() {
        $keyword = assert($this->wechatMsg['Content'])?$this->wechatMsg['Content']:$this->wechatMsg['Recognition'];

        //是否是绑定微信帐号指令
        if($keyword == \Weixin\Response\WechatConst::WEIXIN_BIND_ACCESS_KEYWORD){
            return $this->bindWeiXin();
        }

        //是否为活动评论 ，是则保存
        if($info = S("sign_".$this->wechatMsg['FromUserName'])){
            $this->saveUserMsg($info);
        }

        //如果为查询员工，则返回员工信息
        if(strpos($keyword, 'e') === 0 || strpos($keyword, 'E') === 0){
            $employeeName = substr($keyword, 1);
            return $this->getEmployeeResponse($employeeName);
        }

        //如果为证书查询，则返回证书信息
        if((strpos($keyword, 'n') === 0 || strpos($keyword, 'N') === 0) && $this->companyInfo['ecid'] != '816'){
            $jewelryNum = substr($keyword, 1);
            return $this->getJewelryResponse($jewelryNum);
        }

        //如果是数字，且长度大于13，则调用防伪验证
        if ( is_numeric( $keyword ) && mb_strlen( $keyword, "UTF8" ) > 13 ) {

            //ecid是否为防伪企业
            if(intval($this->companyInfo['ecid']) < 200000 && intval($this->companyInfo['ecid']) != '816'){
                return;
            }

            $result = $this->fwCheck($keyword);

            return $result;
        }

        //查找企业指定的关键词回复
        return $this->getKeywordResponse($keyword);
    }

    private function getEmployeeResponse($employeeName){
        $employee = new \Weixin\Employee\EmployeeResponse($this->companyInfo, $this->wechatMsg);

        $response['type'] = 'news';
        $response['content'] = $employee->getResponseWithName($employeeName);

        return $response;
    }

    /*
     *获取证书信息
     */
    private function getJewelryResponse($jewelryNum){
        $jewelry = new \Weixin\Jewelry\JewelryResponse($this->companyInfo, $this->wechatMsg);

        return $jewelry->getResponseFromJewelryNum($jewelryNum);
    }

    private function getKeywordResponse($keyword) {
        $m = M('Company_keyword_response');

        $result = $m->where( "ecid = " .$this->companyInfo['ecid']. " AND find_in_set('". $keyword ."',keyword)" )->find();

        if ( $result != '' ) {
            $reply['type'] = $result['responseType'];

            $options = array(
                'type'=>$result['responseType'],
                'content'=>$result['textContent'],
                'materialID'=>$result['materialId']);
            
            return $this->getWechatResponse( $options );
        }
        else{
            

            if($this->companyInfo['ecid'] == 200122){
                $reply['type'] = "text";
                $reply['content'] = "如没有及时回复，请致电400 096 5899联系我们。谢谢您对鼎易的支持！";
            
                return $reply;
            }

            if($this->companyInfo['ecid'] == 200136){
                $reply['type'] = "text";
                $reply['content'] = "感谢亲的关注，小编正在赶来回复的路上！亲还可以猛戳下方菜单试试哦~";
            
                return $reply;
            }

            //if($this->companyInfo['ecid'] == 200129){
                $result['type'] = 'transfer_customer_service';
                $result['content'] = $keyword;
                return $result;
            //}

            $result['type'] = 'text';
            $result['content'] = '';
            return $result;
            if($result = $this->getNewsResponse($keyword)){
                return $reuslt;
            }
        }
    }

    /**
     * 防伪验证
     *
     * @return Array
     */
    private function fwCheck($fwCode) {
        $options = array(
            'ecid'     => $this->companyInfo['ecid'],
            'api_user' => $this->companyInfo['mxt_api'],
            'api_psw'  => $this->companyInfo['mxt_psw'],
            'OpenId'   => $this->wechatMsg['FromUserName'],
            'fwCode'   => $fwCode,
            'type'     => 'wechat',
            'isJewelryCheck' => $this->isJewelryCheckCompany());

        $mxt = new MxtClient( $options );
        $result = $mxt->FwCheck( $fwCode );

        $fwParams = $mxt->getFwParams();
        $addResponse = '';

        if(!$mxt->getJewelryCheck() && $fwParams['CheckCount'] >= 0){
            $activity = new \Weixin\Activity\ActivityResponse($this->companyInfo, $this->wechatMsg);

            if($activity->hasActivity()){
                
                if($activity->hasFwLucky($fwParams)){

                    $addResponse= $activity->getResponse();
                }

                if($addResponse == ''){
                    if($activity->hasGeneralActivity()){
                        $addResponse= $activity->getResponse();
                    }
                }
            }
        }

        if($addResponse)
            $result['content'] = array_merge( $result['content'], $addResponse );

        //查询是否需要添加产品广告

        return $result;
    }

    private function isJewelryCheckCompany(){
        $companyArr = C('JEWELRY_COMPANY_ECID');
        for($i = 0;$i<count($companyArr);$i++){
            if($this->companyInfo['ecid'] == $companyArr[$i]){
                return true;
            }
        }

        return false;
    }

    private function bindWeiXin(){
        $params = '';
        if(I('get.platform') =='weibo'){
             //绑定微博号
        if($this->companyInfo['weibo_access'] != null && 
            $this->companyInfo['weibo_access'] == $this->wechatMsg['ToUserName']){
            $params['content'] = '已绑定，无需再次绑定！!';
        }
        if($this->companyInfo['weibo_access'] == null){
            $m = M('Company_info');
            $opt['company_ecid'] = $this->companyInfo['ecid'];
            $opt['weibo_access'] = $this->wechatMsg['ToUserName'];
            if($m->save($opt)){
                $params['content'] = '绑定成功，微防伪V5为您的微博粉丝平台服务！';
            }
        }
        }else{
            if($this->companyInfo['weixin_access'] != null && 
            $this->companyInfo['weixin_access'] == $this->wechatMsg['ToUserName']){
            $params['content'] = '已绑定，无需再次绑定！';
        }
        if($this->companyInfo['weixin_access'] == null){
            $m = M('Company_info');
            $opt['company_ecid'] = $this->companyInfo['ecid'];
            $opt['weixin_access'] = $this->wechatMsg['ToUserName'];
            if($m->save($opt)){
                $params['content'] = '绑定成功，微防伪V4为您的微信公众平台服务！';
            }
        }
        }
        
        
        if($params['content']){
            $params['type'] = \Weixin\Response\WechatConst::RESPONSE_TYPE_TEXT;

            return $this->getWechatResponse($params);
        }
    }

    /**
     * saveUserMsg保存用户评论
     * @author:范小宝
     * @return null
     */
    private function saveUserMsg($info){
        $opt['id'] = $info['activityId'];
        $activity = M("Company_activity")->where($opt)->find();
        $date = date("Y-m-d");
        //判断当前是否在活动时间内，是则保存评论
        if($date >= $activity['startTime'] && $date <= $activity['endTime']){
            $m = M("Company_activity_msg");

            $data = array(
                "activityId" => $info['activityId'] , 
                "openId" => $info['openid'] , 
                "msg" => $this->wechatMsg['Content'] , 
                "comform" => 0 ,
                "addTime" => date("Y-m-d H:i:s")
                );

            $m->add($data);
        }
    }
}
?>
