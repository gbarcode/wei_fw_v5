<?php
namespace Weixin\Label;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class LabelResponse extends Response {
    private $responseInfo;

    public function __construct( $companyInfo, $wechatMsg ) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    /**
     * getResponse获取标签回复
     * @author:范小宝
     * @return array
     */
    public function getResponse($labelId){
        //通过labelId获取标签信息
        $m = M("Company_fw_label");
        $opt['id'] = $labelId;
        $result = $m->where($opt)->find();

        //获取企业LOGO
        $companyOpt['company_ecid'] = $result['ecid'];
        $company = M("Company_info")->where($companyOpt)->find();

        $this->responseInfo = array(array(
                'Title' => $result['name'] , 
                'PicUrl' => $this->getImgUrl($result['image']) , 
                'Description' => "" ,
                'Url' => $company['company_web'] , 
            ) , array(
                'Title'=>'点击查看该防伪标签使用教程' ,
                'PicUrl' => $this->getImgUrl($company['company_logo']) ,
                'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/label/id/{$labelId}"
            ));

        return $this->responseInfo;
    }
}
?>