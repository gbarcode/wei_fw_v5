<?php
namespace Weixin\Product;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class ProductResponse extends Response {
    private $productInfo;
    private $brandInfo;
    private $response = array();

    public function __construct( $companyInfo, $wechatMsg ) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    /**
     * getResponse获取产品回复
     * @author:范小宝
     * @return array
     */
    public function getResponse($productId , $type){
        //通过productId获取产品信息
        $m = M("Company_product");
        $opt['id'] = $productId;
        $this->productInfo = $m->where($opt)->find();

        array_push($this->response , array(
                'Title' => $this->productInfo['name'] , 
                'PicUrl' => $this->getImgUrl($this->productInfo['bigImg']) , 
                'Description' => "" ,
                'Url' => WechatConst::SERVER_DOMAIN . "/MobileShow/product/id/".$this->productInfo['id']
            ));

        switch ($type) {
            case 'all':
                $this->getBrandInfo();
                $this->getLabelInfo();
                break;

            case 'brand':
                $this->getBrandInfo();
                break;

            case 'label':
                $this->getLabelInfo();
                break;
        }

        return $this->response;
    }

    /**
     * getResponse获取品牌回复
     * @author:范小宝
     * @return array
     */
    public function getBrandInfo(){
        //获取所属品牌信息
        $m = M("Company_brand");
        $opt['id'] = $this->productInfo['brandId'];
        $this->brandInfo = $m->where($opt)->find();

        array_push($this->response , array(
                'Title' => "所属品牌：" . $this->brandInfo['name'] ,
                'PicUrl' => $this->getImgUrl($this->brandInfo['minImg']) , 
                'Url' => WechatConst::SERVER_DOMAIN . "/MobileShow/brand/id/".$this->brandInfo['id']
            ));
        return;
    }

    /**
     * getLabelInfo获取防伪标签信息
     * @author:范小宝
     * @return array
     */
    public function getLabelInfo(){
        //获取所使用的防伪标签
        $m = M("Company_fw_label");
        $opt['id'] = $this->productInfo['labelId'];
        $labelInfo = $m->where($opt)->find();

        if($labelInfo){
            array_push($this->response , array(
                'Title' => "所使用防伪标签：" . $labelInfo['name'] ,
                'PicUrl' => $this->getImgUrl($labelInfo['image']) , 
                'Url' => WechatConst::SERVER_DOMAIN . "/MobileShow/label/index/id/".$labelInfo['id']
            ));
        }
        return;
    }
}
?>