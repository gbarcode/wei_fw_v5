<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Weixin\Scan;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
use Org\Mxt\MxtClient;
class ScanResponse extends Response{

    public function __construct( $companyInfo,$wechatMsg, $scanInfo ) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
        $this->scanInfo = $scanInfo;
    }

    public function codeScanResponse(){
        $key = explode("?" , $this->scanInfo);

        Vendor( 'NuSoap.nusoap' );
        $webSoap = new \nusoap_client( "http://203.91.45.207/key.asmx?wsdl", true, false, false, false, false, 0, 30 );
        $webSoap->soap_defencoding = 'UTF-8';
        $webSoap->decode_utf8 = false;
        $webSoap->xml_encoding = 'utf-8';
        $Parms = array( "key" => $key[1] );

        $data = $webSoap->call( 'getFwData', $Parms );
        //$fwCode = "15516147199677990078";
        $options = array(
            'ecid'     => $this->companyInfo['ecid'],
            'api_user' => $this->companyInfo['mxt_api'],
            'api_psw'  => $this->companyInfo['mxt_psw'],
            'OpenId'   => $this->wechatMsg['FromUserName'],
            'fwCode'   => $data['getFwDataResult']['fwCode'],
            'type'     => 'web',
            'isJewelryCheck' => false);

        $mxt = new MxtClient( $options );
        $result = $mxt->FwCheck( $data['getFwDataResult']['fwCode'] );
        return "防伪码：\n".$data['getFwDataResult']['fwCode']."\n物流码：\n".$result['wlcode']."\n查询次数：".$result['checkCount']."次";
    }

    private function from62($num) {  
        $from = 62;  
        $num = strval($num);  
        $dict = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';  
        $len = strlen($num);  
        $dec = 0;  
        for($i = 0; $i < $len; $i++) {  
            $pos = strpos($dict, $num[$i]);  
            $dec = bcadd(bcmul(bcpow($from, $len - $i - 1), $pos), $dec);  
        }  
        return $dec;  
    }

}
?>
