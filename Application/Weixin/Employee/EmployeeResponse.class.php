<?php
namespace Weixin\Employee;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class EmployeeResponse extends Response {
    private $employeeInfo;

    public function __construct( $companyInfo, $wechatMsg ) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    /**
     * getResponse获取标签回复
     * @author:范小宝
     * @return array
     */
    public function getResponseWithId($employeeId){
        //通过employeeId获取员工信息
        $m = M("Company_employees");
        $opt['id'] = $employeeId;
        $result = $m->where($opt)->find();

        if($result){
            $this->employeeInfo = $result;
            //判断员工是否扫描过
            $m = M("Company_employees_sn");
            $opt = null;
            $opt['ecid'] = $this->companyInfo['ecid'];
            $opt['employeeId'] = $employeeId;
            $opt['openId'] = $this->wechatMsg['FromUserName'];
            //如果员工未被扫描，则添加扫描信息
            if(!$m->where($opt)->find()){
                $this->saveEmployeeInfo();
            }

            return $this->setResponse();
        }
    }

    public function getResponseWithName($name){
        //通过employeeId获取员工信息
        $m = M("Company_employees");
        $opt['name'] = $name;
        $opt['ecid'] = $this->companyInfo['ecid'];
        $result = $m->where($opt)->find();

        if($result){
            $this->employeeInfo = $result;
            return $this->setResponse();
        }
    }

    private function setResponse(){
        $m = M('Company_info');
        $opt['company_ecid'] = $this->companyInfo['ecid'];
        $companyArr = $m->where($opt)->find();

        $response = array(array(
            'Title' => $this->employeeInfo['name'] . $this->employeeInfo['position'] ."为您服务" , 
            'PicUrl' => $this->getImgUrl($companyArr['company_logo']) , 
            'Description' => "" ,
            'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/employee/id/{$this->employeeInfo['id']}" 
            ) , array(
            'Title' => "点击联系我" ,
            'PicUrl' => $this->getImgUrl($this->employeeInfo['img']) , 
            'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/employee/id/{$this->employeeInfo['id']}" 
            ));

        return $response;
    }

    /**
     * saveEmployeeInfo添加扫描员工信息
     * @author:范小宝
     * @return null
     */
    private function saveEmployeeInfo(){
        $m = M("Company_employees_sn");
        $opt['ecid'] = $this->companyInfo['ecid'];
        $opt['openId'] = $this->wechatMsg['FromUserName'];
        $opt['employeeId'] = $this->employeeInfo['id'];
        $opt['snTime'] = date("Y-m-d H:i:s");
        $m->add($opt);
    }
}
?>