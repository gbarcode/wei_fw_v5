<?php
namespace Weixin\Response;
class WechatConst{
	const SERVER_DOMAIN              = "http://www.msa12365.com";
	const REDIRECT_DOMAIN            = "http://jump.msa12365.com?";
	const ADMIN_ECID                 = "816";
	const RESPONSE_TYPE_TEXT         = 'text';
	const RESPONSE_TYPE_NEWS         = 'news';
	const RESPONSE_TYPE_TMALL        = 'tmall';
	const RESPONSE_TYPE_ACTIVITY     = 'activity';
	const RESPONSE_TYPE_INFORMATION  = 'information';
	const RESPONSE_TYPE_CHECK_PRIZE  = 'check_prize';
	const RESPONSE_TYPE_SERVICE      = 'service';
	const RESPONSE_TYPE_MALL         = 'mall';
	const RESPONSE_TYPE_VIP          = 'vip';
	const RESPONSE_TYPE_REPORT       = 'report';
	const WEIXIN_BIND_ACCESS_KEYWORD = '微防伪为您服务';
	const OAUTH_REDIRECT_URL         = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=<appid>&redirect_uri=<url>&response_type=code&scope=snsapi_base&state=0#wechat_redirect";
}

?>
