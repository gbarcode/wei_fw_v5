<?php
namespace Weixin\Response;
use Weixin\Response\WechatConst;
use Org\Error\Error;
class Response {
	protected $companyInfo;
	protected $wechatMsg;
	private $webSoap;
	private $shopUrl;
	private $activityUrl = 'http://www.msa12365.com/index.php/Home/Index/activityNews/id/';

	/*
     * 函数：getWechatResponse
     * 作用：将回复的内容转化为微信所需的参数
     * @Param:  当text时   $Param['type'], $Param['Content']
     *          当news时   $Param['type'], $Param['materialID']
     */
	protected function getWechatResponse( $Param, $bSubItem = false ) {
		//如果是TEXT，直接返回即可
		if ( $Param['type'] == WechatConst::RESPONSE_TYPE_TEXT )
			return $Param;

		//如果是news,则要从素材库里获取
		if ( $Param['type'] == WechatConst::RESPONSE_TYPE_NEWS ) {
			//$reply = S("Material_".$Param['materialID']);
			if(!$reply){
				$m = M( 'Company_material_group' );
				$opt['id'] = $Param['materialID'];
				$opt['ecid'] = $this->companyInfo['ecid'];
				$result = $m->where( $opt )->find();
				$arrMaterialID = explode( ',', $result['materialId'] );

				for ( $i=0; $i<count( $arrMaterialID ); $i++ ) {
					$m = M( 'Company_news' );
					$condition['id'] = $arrMaterialID[$i];
					$result = $m->where( $condition )->find();

					$picUrl = ($i==0)?$result['bigImg']:$result['minImg'];

					if(substr($picUrl,0,4) != 'http')
						$picUrl = WechatConst::SERVER_DOMAIN . $picUrl;

					$url = WechatConst::REDIRECT_DOMAIN . "ecid={$this->companyInfo['ecid']}&type=news&from_type=1&newsId={$condition['id']}";

					if($bSubItem){
						
					}else{
						$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
						$reply['content'][$i] = array(
						'Title'=>$result['title'],
						'Description'=>$result['description'],
						'PicUrl'=> $picUrl,
						'Url'=> $this->getOauthRedirectUrl($url));
					}
				}

				//S("Material_".$Param['materialID'], $reply);
			}

			return $reply;
		}
	}

	public function getOauthRedirectUrl($url, $appid = ''){
            return $url;	//不进入微信授权认证
            if($appid == '')
			$appid = $this->companyInfo['weixin_AppId'];
		
		$base_url = WechatConst::OAUTH_REDIRECT_URL;

		$base_url = str_replace("<url>", urlencode($url), $base_url);
		$base_url = str_replace("<appid>", $appid, $base_url);

		return $base_url;
	}

	protected function getTmallResponse() {
		$m = M( 'Company_info' );

		$result = $m->find( $this->companyInfo['ecid'] );

		if ( $result != null ) {
			$response['type'] = WechatConst::RESPONSE_TYPE_NEWS;
			$response['content'][0] = array(
				'Title'=>'欢迎关临' . $result['company_tmallName'],
				'Description'=>'欢迎关临' . $result['company_tmallName'],
				'PicUrl'=> WechatConst::SERVER_DOMAIN . '/Public/Image/tmall.jpg',
				'Url'=> $result['company_tmallUrl'] );

			return $response;
		}
	}

	protected function getNewsResponse($keyword) {
		$news = new \Weixin\News\NewsResponse( $keyword , $this->companyInfo );

		return $news->getNewsResponse();
	}

	protected function getProductAd() {
		$m = M( 'Company_product_detail' );

		$data['ecid'] = $this->companyInfo['ecid'];
		$data['fwSign'] = 1;

		$result = $m->where( $data )->find();

		if ( $result != null ) {
			$response[0] = array(
				'Title'=> ( $result['adSign'] == 1 )?$result['ads']:$result['name'],
				'Description'=>'',
				'PicUrl'=> WechatConst::SERVER_DOMAIN . $result['smallImg'],
				'Url'=> ( $result['linkSign']==1 )?$result['link']:WechatConst::SERVER_DOMAIN );

			return $response;
		}
	}

	/**
    * 定义函数
    * getActivityInfo
    * 函数功能描述
    * 获取活动信息
    * @access private
    * @param int $activityId 活动id
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-20 创建函数
    */
	private function getActivityInfo($activityId){
        $m = M('Company_activity');

        $result = $m->find($activityId);

        return $result;
    }

    /**
    * 定义函数
    * getCheckPrizeResponse
    * 函数功能描述
    * 获取查询中奖回复
    * @access protected
    * @param string $activityItem 活动id
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-20 创建函数
    */
	protected function getCheckPrizeResponse($activityItem){
		$activityArr = split(',',$activityItem);

		$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;

		for($i = 0;$i<count($activityArr);$i++){
			$reply['content'][$i] = $this->checkLucky($this->getActivityInfo($activityArr[$i]));
		}

        return $reply;
	}

	/**
    * 定义函数
    * checkLucky
    * 函数功能描述
    * 查询是否中奖
    * @access private
    * @param array $activityInfo 活动信息
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-20 创建函数
    */
	private function checkLucky($activityInfo = null) {
        $result = M()->query("
            SELECT 
			luckyUser.fwCode,
			luckyUser.luckyTime,
			luckyUser.userId,
			luckyUser.prizeId,
			luckyUser.activityId,
			prize.prizeName,
			prize.prizeType,
			userInfo.nickname,
			userInfo.realName,
			userInfo.address,
			userInfo.tel
			FROM 
			sz12365_fw_company_lucky_user AS luckyUser 
			JOIN 
			sz12365_fw_company_lucky_prize AS prize 
			ON luckyUser.prizeId = prize.id
			LEFT OUTER JOIN
			sz12365_fw_company_{$this->companyInfo['ecid']}_user_info AS userInfo
			ON userInfo.openId = luckyUser.userId
			WHERE luckyUser.userId = '{$this->wechatMsg['FromUserName']}' AND luckyUser.activityId = {$activityInfo['id']}
            ");

        if ( $result != null ) {
        	$picUrl = $activityInfo['bigImg'];

			if(substr($picUrl,0,4) != 'http')
				$picUrl = WechatConst::SERVER_DOMAIN . $picUrl;

            $exchangeText = ( $result[0]['realName']=='' )?"您仍未登记信息，请点击登记地址信息。":"{$result[0]['realName']},您已登记地址信息,我们将尽快发放奖品！";

        	$reply = array(
                'Title'=> "活动：《{$activityInfo['name']}》，恭喜您有一条中奖记录，奖品是：《{$result[0]['prizeType']}:{$result[0]['prizeName']}》.{$exchangeText}",
                'PicUrl'=> $picUrl,
                'Url'=> WechatConst::SERVER_DOMAIN . "/Home/Activity/index/activityId/{$activityInfo['id']}/openId/{$this->wechatMsg['FromUserName']}");
        }
        else {
        	$picUrl = $activityInfo['minImg'];

			if(substr($picUrl,0,4) != 'http')
				$picUrl = WechatConst::SERVER_DOMAIN . $picUrl;

        	$reply = array(
                'Title'=>"活动：《{$activityInfo['name']}》，未查询到中奖记录！",
                'PicUrl'=> $picUrl,
                'Url'=>$this->activityUrl.$activityInfo['id']);
        }

        return $reply;
    }

    /*
     * 函数：getServiceResponse
     * 作用：获取企业客服信息
     * @Param:  
     */
	protected function getServiceResponse( $ecid ) {
		//获取企业信息
		$m = M("Company_info");
		$opt = null;
		$opt['company_ecid'] = $ecid;
		$company = $m->where($opt)->find();

		$response[0] = array(
			'Title'=> $company['company_name'] ,
			'Description'=>'',
			'PicUrl'=> WechatConst::SERVER_DOMAIN . $company['company_logo'] ,
			'Url'=> $company['company_web']
			);

		//获取员工信息，优先选择已扫描的员工
        $result = M()->query("
            SELECT 
            sz12365_fw_company_employees.id ,
            sz12365_fw_company_employees.`name` ,
            sz12365_fw_company_employees.`img` ,
            sz12365_fw_company_employees.sex ,
            sz12365_fw_company_employees_sn.openId
            FROM
            sz12365_fw_company_employees
            JOIN
            sz12365_fw_company_employees_sn
            ON
            sz12365_fw_company_employees.id = sz12365_fw_company_employees_sn.employeeId AND sz12365_fw_company_employees_sn.openId = '{$this->wechatMsg['FromUserName']}'");
        
        for($i = 0;$i < count($result);$i++){
        	$picUrl = $result[$i]['img'];

			if(substr($picUrl,0,4) != 'http')
				$picUrl = WechatConst::SERVER_DOMAIN . $picUrl;

        	array_push($response , array(
        		'Title' => $result[$i]['name'] . $result[$i]['position'] . "为您服务" , 
                'PicUrl' => $picUrl , 
                'Url' => WechatConst::SERVER_DOMAIN . "/Home/MobileShow/employee/id/{$result[$i]['id']}"
        		));

        	if($i == 7)break;
        }

		$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
		$reply['content'] = $response;
		
		return $reply;
	}

	/*
     * 函数：getShopWebSoap
     * 作用：新建websoap
     * @Param:  
     */
	private function getShopWebSoap(){
		//获取web接口链接
		$this->shopUrl = M('Company_info')->where('company_ecid = '.$this->companyInfo['ecid'])->getField('mall_domain');
		$WebServiceURL = $this->shopUrl . "/weixin/webservice.php?wsdl";
		Vendor( 'NuSoap.nusoap' );
        $this->webSoap = new \nusoap_client( $WebServiceURL, true, false, false, false, false, 0, 30 );
        $this->webSoap->soap_defencoding = 'UTF-8';
        $this->webSoap->decode_utf8 = false;
        $this->webSoap->xml_encoding = 'utf-8';
	}

	/*
     * 函数：getServiceResponse
     * 作用：获取企业客服信息
     * @Param:  
     */
	protected function getMallResponse( $type ) {
		$this->getShopWebSoap();

		//获取企业LOGO
		$logo = M('Company_info')->where('company_ecid = '.$this->companyInfo['ecid'])->getField('company_logo');

        //获取商品信息
        $opt['type'] = $type;
		$opt['wxId'] = $this->wechatMsg['FromUserName'];
		$opt['timestamp'] = time();
		$opt['token'] = $this->getShopToken($opt['timestamp']);
		$result = $this->webSoap->call( 'getGoodList', $opt );
        
        //设置首条信息
        $reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
        $firstReply = $this->skipMallIndex();
        $response[0] = $firstReply['content'][0];

        //判断是否有商品信息，并设置回复
        if($result['Code'] == 0){
        	for($i = 0;$i<count($result['Items']);$i++){
        		if($i == 6)
        			return;

        		$ecuid = $this->checkUserEcuid($this->wechatMsg['FromUserName']);

        		$response[$i+1] = array(
        			'Title' => $result['Items'][$i]['name'] , 
	                'PicUrl' => $this->shopUrl."/".$result['Items'][$i]['goods_img'] , 
	                'Url' => $this->getShopLoginUrl($result['Items'][$i]['url'], $ecuid));
        	}
        	
			$reply['content'] = $response;
        }else{
        	$reply['type'] = WechatConst::RESPONSE_TYPE_TEXT;
        	$reply['content'] = "暂未设置商品信息";
        }

        return $reply;
	}

	/*
     * 函数：getReportResponse
     * 作用：申诉举报页面
     * @Param:  
     */
	protected function getReportResponse(){
		$url = \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Home/MobileShow/userReport/openId/'.$this->wechatMsg['FromUserName'].'/ecid/'.$this->companyInfo['ecid'];
		$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
		$reply['content'] = array(
				array(
					'Title' => '点击提交申诉举报内容',
					//'PicUrl'=> $picUrl,
					'Url' => $url
				)
			);

		return $reply;
	}

	/*
     * 函数：getVipResponse
     * 作用：获取会员信息
     * @Param:  
     */
	protected function getVipResponse( $type ) {
		$this->getShopWebSoap();
		$timestamp = time();

		switch ($type) {
			case 'index':
				return $this->skipMallIndex();
				break;
			case 'info':
				return $this->getVipInfo($this->getShopToken($timestamp) , $timestamp);
				break;
			case 'order':
				return $this->getVipOrder($this->getShopToken($timestamp) , $timestamp);
				break;
			case 'bind':
				return $this->VipBindHandle($this->getShopToken($timestamp) , $timestamp);
				break;
			case 'unbind':
				$save['ecuid'] = 0;
				M('Company_'.$this->companyInfo['ecid'].'_user_info')->where("openId = '".$this->wechatMsg['FromUserName']."'")->save($save);
				$result['Code'] = Error::SUCCESS_OK;
				break;
		}

		$reply['type'] = WechatConst::RESPONSE_TYPE_TEXT;
		if($result['Code'] == 0){
        	$reply['content'] = "操作成功";
		}else{
			$reply['content'] = $result['Message'];
		}

		return $reply;
	}

	/*
     * 函数：getVipInfo
     * 作用：获取会员信息
     * @Param:  
     */
	private function getVipInfo($token , $timestamp){
		//获取企业LOGO
		$logo = M('Company_info')->where('company_ecid = '.$this->companyInfo['ecid'])->getField('company_logo');

		$m = M('Company_'.$this->companyInfo['ecid'].'_user_info');
		$opt['openId'] = $this->wechatMsg['FromUserName'];

		$row = $m->where($opt)->find();
		$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
		if($row['ecuid'] <= 0){
			$reply['content'] = array(
				array(
					'Title' => '点击绑定会员' ,
					'PicUrl' => $logo ,
					'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/vipBind/id/".$this->wechatMsg['FromUserName']."/ecid/".$this->companyInfo['ecid']
					)
				);
		}else{
			$reply['content'] = array(
				array(
					'Title' => '点击查看会员信息' ,
					'PicUrl' => $logo ,
					'Url' => $this->getShopLoginUrl('user.php' , $row['ecuid'])
					)
				);
		}

		return $reply;
	}

	/*
     * 函数：VipBindHandle
     * 作用：会员绑定操作
     * @Param:  
     */
	private function VipBindHandle($token , $timestamp){
		//获取企业LOGO
		$logo = M('Company_info')->where('company_ecid = '.$this->companyInfo['ecid'])->getField('company_logo');

		$m = M('Company_'.$this->companyInfo['ecid'].'_user_info');
		$opt['openId'] = $this->wechatMsg['FromUserName'];
		$opt['ecuid'] = 0;
		if($m->where($opt)->find()){
			$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
			$reply['content'] = array(
				array(
					'Title' => '点击绑定会员' ,
					'PicUrl' => $logo ,
					'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/vipBind/id/".$this->wechatMsg['FromUserName']."/ecid/".$this->companyInfo['ecid']
					)
				);
		}else{
			$reply['type'] = WechatConst::RESPONSE_TYPE_TEXT;
			$reply['content'] = '用户已绑定！';
		}

		return $reply;
	}

	/*
     * 函数：getVipOrder
     * 作用：获取会员订单信息
     * @Param:  
     */
	private function getVipOrder($token , $timestamp){
		$opt['wxId'] = $this->wechatMsg['FromUserName'];
		$opt['token'] = $token;
		$opt['ecuid'] = $this->checkUserEcuid($this->wechatMsg['FromUserName']);
		$opt['timestamp'] = $timestamp;
		// $result = $this->webSoap->call('checkUserOrder' , $opt);

		$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;

		if($opt['ecuid'] <= 0){
			$reply['content'] = array(
				array(
					'Title' => '点击绑定会员' ,
					'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/vipBind/id/".$this->wechatMsg['FromUserName']."/ecid/".$this->companyInfo['ecid']
					)
				);
		}else{
			$reply['content'] = array(
				0 => array(
					'Title' => "点击查看订单信息",
					'Url' => $this->getShopLoginUrl("user.php%3Fact%3Dorder_list", $opt['ecuid'])
					)
				);
		}

		return $reply;
	}

	/*
     * 函数：getShopToken
     * 作用：生成商城token
     * @Param:  
     */
	private function getShopToken($timestamp){
		return sha1('sz12365'.'\0\0\0\0\0\0'.$this->wechatMsg['FromUserName'].$timestamp);
	}

	protected function getImgUrl($imgFile){
		if(substr($imgFile,0,4) != 'http')
			return WechatConst::SERVER_DOMAIN . $imgFile;

		return $imgFile;
	}

	protected function checkUserEcuid($wxid){
		$m = M("Company_{$this->companyInfo['ecid']}_user_info");

		$opt['openId'] = $wxid;

		$result = $m->where($opt)->find();

		return $result['ecuid'];
	}

	/**
	 * 函数：skipMallIndex  跳转至商城首页
	 * @return array 微信回复数组
	 * @author 范小宝 <fanxb@gbarcode.com>  2014-07-21
	 */
	private function skipMallIndex(){
		$logo = M('Company_info')->where('company_ecid = '.$this->companyInfo['ecid'])->getField('company_logo');

		$m = M('Company_'.$this->companyInfo['ecid'].'_user_info');
		$opt['openId'] = $this->wechatMsg['FromUserName'];
		$row = $m->where($opt)->find();

		$reply['type'] = WechatConst::RESPONSE_TYPE_NEWS;
		if($row['ecuid'] > 0){
			$reply['content'] = array(
				array(
        			'Title' => "点击进入商城首页" , 
	                'PicUrl' => $logo , 
	                'Url' => $this->getShopLoginUrl('index.php' , $row['ecuid'])
	                ));
		}else{
			$reply['content'] = array(
				array(
					'Title' => '点击绑定会员' ,
					'PicUrl' => $logo , 
					'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/vipBind/id/".$this->wechatMsg['FromUserName']."/ecid/".$this->companyInfo['ecid']
					));
		}

		return $reply;
	}

	/**
	 * 函数：getShopLoginUrl  获取商城自动登录网址
	 * @param string $url 跳转地址
	 * @param int $ecuid 商城用户ID
	 * @return string 认证地址
	 * @author 李少年 <lisn@gbarcode.com>  2014-07-21
	 */
	protected function getShopLoginUrl($url, $ecuid){
		$timestamp = time();

		$token = $this->getShopToken($timestamp);

		return "{$this->shopUrl}/weixin/redirect.php?token={$token}&t={$timestamp}&wxid={$this->wechatMsg['FromUserName']}&url={$url}&ecuid={$ecuid}";
	}
}
?>
