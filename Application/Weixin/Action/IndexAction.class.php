<?php

// 本类由系统自动生成，仅供测试用途

namespace Weixin\Action;

use Think\Action;
use Org\Error\Error;
use Org\Weixin\Wechat;
use Org\Weibo\Weibo;

class IndexAction extends Action {
    //企业信息
    private $companyInfo;
    private $wechatMsg;

    public function index() {
        $weObj = new Wechat($this->getOption());

        //验证合法性
        $weObj->valid();
        //获取微信参数

        $this->wechatMsg = $weObj->getRev()->getRevData();

        //回复内容
        $response = '';

        // //检查是否具有调用权限
        $code = $this->checkCompany($weObj->getRevTo(), $weObj->getRevContent());
        if ($code != Error::SUCCESS_OK) {
            echo Error::getErrMsg($code);
            return;
        }

        //添加用户
        $this->saveUserInfo($this->companyInfo['ecid'], $this->wechatMsg['FromUserName']);

        //处理收到的信息
        switch ($this->wechatMsg['MsgType']) {
            case Wechat::MSGTYPE_TEXT://收到文本信息
                $keywordResponse = new \Weixin\Keyword\KeywordResponse($this->companyInfo, $this->wechatMsg);
                $response = $keywordResponse->getResponse();
                break;
            case Wechat::MSGTYPE_EVENT://收到事件信息
                $eventResponse = new \Weixin\Event\EventResponse($this->companyInfo, $this->wechatMsg);
                $response = $eventResponse->getResponse();
                break;
            case Wechat::MSGTYPE_LOCATION://收到地理信息
                $locationResponse = new \Weixin\Location\LocationResponse($this->companyInfo, $this->wechatMsg);
                $response = $locationResponse->getResponse();
                break;
            case Wechat::MSGTYPE_IMAGE://回复图片信息
                break;
            case Wechat::MSGTYPE_VOICE://处理语音信息
                if (assert($this->wechatMsg['Recognition'])) {
                    $keywordResponse = new \Weixin\Keyword\KeywordResponse($this->companyInfo, $this->wechatMsg);
                    $response = $keywordResponse->getResponse();
                }
                break;
            default:
                break;
        }

        // 响应回复
        switch ($response['type']) {
            case Wechat::MSGTYPE_TEXT:
                $weObj->text($response["content"])->reply();
                break;
            case 'transfer_customer_service':
                $weObj->transfer_customer_service()->reply();
                break;
            case Wechat::MSGTYPE_NEWS:
                $weObj->news($response['content'])->reply();
            default:
                break;
        }

        if ($response['ext'] != null) {
            $this->sendCustomMessage($response['ext'], $this->wechatMsg['FromUserName']);
        }

        // 记录日志
        $wechatMsg['Content'] = htmlspecialchars_decode($wechatMsg['Content']); //过滤html和实体
        $wechatMsg['Content'] = strip_tags($wechatMsg['Content']);
        \Weixin\Log\ResponseLog::wxReceiveLog($this->companyInfo['ecid'], $this->wechatMsg);
    }

    public function weibo() {
        $ecid = I('get.ecid');
        $companyInfo = M('company_info')->where(array('company_ecid'=> I('get.ecid')))->find();
        if(!$companyInfo){
            return FALSE;
        }
        $options = array('appsecret' => $companyInfo['weibo_AppSecret']);
        //初始化SDK
        $weObj = new Weibo($options);
        //验证合法性
        $weObj->valid();
        //获取微博参数
//        $wechatMsg  = array(//测试用
//            'FromUserName' => '3077455061',//ecid:200105
//            'ToUserName' => '2828512894',
//            'MsgType' => 'text',
//            'Content' => '56435345345466733524');

        $this->wechatMsg = $weObj->getRev()->getRevData();
//        $this->wechatMsg = $weObj->getRev($wechatMsg)->getRevData();//测试用
//        回复内容
        $response = '';

        // //检查是否具有调用权限
        $code = $this->checkCompany($weObj->getRevTo(), $weObj->getRevContent());
        if ($code != Error::SUCCESS_OK) {
            echo Error::getErrMsg($code);
            return;
        }

        //添加用户
        $this->saveUserInfo($this->companyInfo['ecid'], $this->wechatMsg['FromUserName']);
        //处理收到的信息
        switch ($this->wechatMsg['MsgType']) {
            case Wechat::MSGTYPE_TEXT://收到文本信息
                $keywordResponse = new \Weixin\Keyword\KeywordResponse($this->companyInfo, $this->wechatMsg);
                $response = $keywordResponse->getResponse();
                break;
            case Wechat::MSGTYPE_EVENT://收到事件信息
                $eventResponse = new \Weixin\Event\EventResponse($this->companyInfo, $this->wechatMsg);
                $response = $eventResponse->getResponse();
                break;
            case Wechat::MSGTYPE_LOCATION://收到地理信息
                $locationResponse = new \Weixin\Location\LocationResponse($this->companyInfo, $this->wechatMsg);
                $response = $locationResponse->getResponse();
                break;
            case Wechat::MSGTYPE_IMAGE://回复图片信息
                break;
            case Wechat::MSGTYPE_VOICE://处理语音信息
                if (assert($this->wechatMsg['Recognition'])) {
                    $keywordResponse = new \Weixin\Keyword\KeywordResponse($this->companyInfo, $this->wechatMsg);
                    $response = $keywordResponse->getResponse();
                }
                break;
            default:
                break;
        }

        // 响应回复
        switch ($response['type']) {
            case Wechat::MSGTYPE_TEXT:
                $weObj->text($response["content"])->reply();
                break;
            case Wechat::MSGTYPE_NEWS:
//            $weObj->text( $response["type"] )->reply();
                $weObj->news($response['content'])->reply();
                break;
            default:
                break;
        }

//        if($response['ext'] != null){
//            $this->sendCustomMessage($response['ext'], $this->wechatMsg['FromUserName']);
//        }
//
//        // 记录日志
//        $wechatMsg['Content']=htmlspecialchars_decode($wechatMsg['Content']);//过滤html和实体
//        $wechatMsg['Content'] = strip_tags($wechatMsg['Content']);
//        \Weixin\Log\ResponseLog::wxReceiveLog( $this->companyInfo['ecid'],  $this->wechatMsg);
    }

    private function getOption() {
        return array(
            'token' => md5(I('get.ecid') . I('get.token')) //填写你设定的key
        );
    }

    private function sendCustomMessage($msg, $openId) {
        for ($i = 0; $i < count($msg); $i++) {
            $msg[$i] = array_change_key_case($msg[$i], CASE_LOWER);
        }

        $msgArr = array(
            'touser' => $openId,
            'msgtype' => 'news',
            'news' => array(
                'articles' => $msg));

        $weObj = new Wechat(); //引用微信SDK
        if ($weObj->checkAuth($this->companyInfo['weixin_AppId'], $this->companyInfo['weixin_AppSecret'])) {
            $weObj->sendCustomMessage($msgArr);
        }
    }

    private function saveUserInfo($ecid, $openId) {
        $user_cache = 'user_info' . $ecid . $openId;
        $result = S($user_cache);

        if (I('get.platform') == "weibo") {
            $ecid = $ecid . "_weibo";
        }
        $m = M('Company_' . $ecid . '_user_info');
        if (!$result) {
            $opt['openId'] = $openId;
            $result = $m->where($opt)->find();
        }

        if (!$result) {
            if ($option = $this->getWechatUserInfo($openId)) {
                $option['userUpdate'] = date('Y-m-d H:i:s');
            }
            $saveArr['openId'] = $openId;
            $saveArr['nickname'] = $option['nickname'];
            $saveArr['sex'] = $option['sex'];
            $saveArr['language'] = $option['language'];
            $saveArr['city'] = $option['city'];
            $saveArr['province'] = $option['province'];
            $saveArr['country'] = $option['country'];
            $saveArr['headimgurl'] = $option['headimgurl'];
            $saveArr['subscribe_time'] = date('Y-m-d H:i:s');
            $saveArr['userUpdate'] = $option['userUpdate'];
            $saveArr['mark'] = 0;
            $m->add($saveArr);
            $opt['openId'] = $openId;
            $result = $m->where($opt)->find();

            $this->createMallUser($ecid, $openId);

            S($user_cache, $result);
        } else {
            $time = date('Y-m-d H:i:s');
            $updateTime = $result['userUpdate'];

            if ($result['ecuid'] == 0)
                $this->createMallUser($ecid, $openId);

            if ($updateTime == null || $this->DateDiff($time, $updateTime) > 7) {

                if ($option = $this->getWechatUserInfo($openId)) {

                    $saveArr['nickname'] = $option['nickname'];
                    $saveArr['sex'] = $option['sex'];
                    $saveArr['language'] = $option['language'];
                    $saveArr['city'] = $option['city'];
                    $saveArr['province'] = $option['province'];
                    $saveArr['country'] = $option['country'];
                    $saveArr['headimgurl'] = $option['headimgurl'];
                    $saveArr['userUpdate'] = date('Y-m-d H:i:s');
                    $saveArr['mark'] = 0;
                    if ($result['subscribe_time'] == "0000-00-00 00:00:00") {
                        $saveArr['subscribe_time'] = date('Y-m-d H:i:s');
                    }

                    $m->where("id = " . $result['id'])->save($saveArr);
                    $opt['openId'] = $openId;
                    $result = $m->where($opt)->find();
                }
            }

            S($user_cache, $result);
        }
    }

    private function createMallUser($ecid, $openId) {
        $opt['timestamp'] = time();
        $opt['wxId'] = $openId;
        $opt['token'] = sha1('sz12365' . '\0\0\0\0\0\0' . $opt['wxId'] . $opt['timestamp']);

        //获取web接口链接
        $url = $this->companyInfo['mall_domain'];

        if ($url == '')
            return false;

        $WebServiceURL = $url . "/weixin/webservice.php?wsdl";
        Vendor('NuSoap.nusoap');
        $webSoap = new \nusoap_client($WebServiceURL, true, false, false, false, false, 0, 30);
        $webSoap->soap_defencoding = 'UTF-8';
        $webSoap->decode_utf8 = false;
        $webSoap->xml_encoding = 'utf-8';

        $result = $webSoap->call('createUserHandle', $opt);

        if ($result['Code'] > 0) {
            $save['ecuid'] = $result['Code'];
            $m = M('Company_' . $ecid . '_user_info');
            $m->where("openId = '" . $openId . "'")->save($save);

            return true;
        }

        return false;
    }

    private function DateDiff($date1, $date2, $unit = "") { //时间比较函数，返回两个日期相差几秒、几分钟、几小时或几天 
        switch ($unit) {
            case 's':
                $dividend = 1;
                break;
            case 'i':
                $dividend = 60; //oSPHP.COM.CN 
                break;
            case 'h':
                $dividend = 3600;
                break;
            case 'd':
                $dividend = 86400;
                break; //开源OSPhP.COM.CN 
            default:
                $dividend = 86400;
        }
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        if ($time1 && $time2)
            return (float) ($time1 - $time2) / $dividend;
        return false;
    }

    private function getWechatUserInfo($openId) {
        //如果开通的不是高级版微防伪，直接返回
        if ($this->companyInfo['serviceId'] != 2)
            return false;

        //如果接口信息不完整，直接返回
        if (I('get.platform') == 'weibo') {
            $weObj1 = new Weibo();
            $userInfo = $weObj1->getUserInfo($openId);
            $userInfo['CreateTime'] = date("Y-m-d H:i:s", $userInfo['CreateTime']);

            if ($userInfo) {
                return $userInfo;
            }
        } else {
            if ($this->companyInfo['weixin_AppId'] == '' || $this->companyInfo['weixin_AppSecret'] == '') {
                return false;
                $weObj = new Wechat();
                if ($weObj->checkAuth($this->companyInfo['weixin_AppId'], $this->companyInfo['weixin_AppSecret'])) {
                    $userInfo = $weObj->getUserInfo($openId);
                    $userInfo['CreateTime'] = date("Y-m-d H:i:s", $userInfo['CreateTime']);

                    if ($userInfo) {
                        return $userInfo;
                    }
                }
            }
        }


        return false;
    }

    private function checkCompany($wxID, $keyword = '') {
//        $wxID = "gh_8e7c58fbae78";//测试用
        if ($ecid = I('get.ecid')) {
            $ecid_cache = 'company_info' . I('get.ecid');
            if ($rs = S($ecid_cache)) {
                $this->companyInfo = $rs;
            } else {
                $m = M('View_company_weixin_services');
                $opt['ecid'] = I('get.ecid');
                $opt['startTime'] = array('ELT', date("Y-m-d"));  //当前时间要大于服务开始时间
                $opt['endTime'] = array('EGT', date("Y-m-d"));    //当前时间要小于服务结束时间
                $result = $m->where($opt)->find();

                if ($result) {
                    $this->companyInfo = $result;
                    S($ecid_cache, $result);
                } else {
                    return Error::ERROR_GENERAL;
                }
            }

            //找到对应服务企业
            if ($this->companyInfo != null) {
                if (I('get.platform') == "weibo") {
                    //如果没有找到微信号，要判断是不是绑定微信号关键词
                    if ($this->companyInfo['weibo_access'] == null) {
                        if ($keyword == \Weixin\Response\WechatConst::WEIXIN_BIND_ACCESS_KEYWORD) {   //如果是绑定微信指定，返回OK
                            S($ecid_cache, NULL);
                            return Error::SUCCESS_OK;
                        }

                        return Error::ERROR_COMPANY_NOT_BIND_ACCESS;
                    }
                    if ($wxID != $this->companyInfo['weibo_access']) {
                        return Error::ERROR_COMPANY_WRONG_ACCESS;
                    } else {
                        return Error::SUCCESS_OK;
                    }
                } else {
                    //如果没有找到微信号，要判断是不是绑定微信号关键词
                    if ($this->companyInfo['weixin_access'] == null) {
                        if ($keyword == \Weixin\Response\WechatConst::WEIXIN_BIND_ACCESS_KEYWORD) {   //如果是绑定微信指定，返回OK
                            S($ecid_cache, NULL);
                            return Error::SUCCESS_OK;
                        }

                        return Error::ERROR_COMPANY_NOT_BIND_ACCESS;
                    }
                    //如果微信号不同，则非法
                    if ($wxID != $this->companyInfo['weixin_access'])
                        return Error::ERROR_COMPANY_WRONG_ACCESS;

                    return Error::SUCCESS_OK;
                }
            }
            else {
                return Error::ERROR_COMPANY_NOTEXIST;
            }
        } else {
            return Error::ERROR_GENERAL;
        }
    }

    public function test() {
        $this->checkCompany('To', 'test');

        $wechatMsg = array(
            'FromUserName' => '1111111', //ecid:200105
            'ToUserName' => 'To',
            'MsgType' => 'text',
            'Event' => 'CLICK',
            'EventKey' => 'SZ12365_MENUCLICK_1412694593',
            'Content' => '男女标准体重'
            );

//        $wechatMsg  = array(
//            'FromUserName' => 'ogETojuLhjNU5ds2EOB3vGCqhwh0',
//            'ToUserName' => 'To',
//            'MsgType' => 'event',
//            'Content' => '13199046020170742476',
//            'Event' => 'scancode_waitmsg',
//            'ScanResult' => 'SZ12365_MENUCLICK_1398826195');
//        $keywordResponse = new \Weixin\Event\EventResponse( $this->companyInfo, $wechatMsg);
        $keywordResponse = new \Weixin\Keyword\KeywordResponse($this->companyInfo, $wechatMsg);
        $response = $keywordResponse->getResponse();

        dump($response);
    }

    private function current_time() {
        list($mic, $sec) = explode(" ", microtime());
        return floatval($mic) + floatval($sec);
    }

    private function txt($s) {
        $fh = fopen('text.txt', "a");
        fwrite($fh, $s . "\r\n");    // 输出：6
        fclose($fh);
    }

}
