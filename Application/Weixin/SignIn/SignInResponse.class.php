<?php
namespace Weixin\SignIn;
use Org\Error\Error;
use Weixin\Response\WechatConst;
use Weixin\Response\Response;
class SignInResponse extends Response{
	private $activityInfo;
    private $UserOpenId;
    private $bindUserId;
    private $ecid;

    private $activityUserId;

    private $userInfo;

    private $responseInfo;

	public function __construct( $activityInfo, $UserOpenId, $ecid) {
        $this->activityInfo = $activityInfo;
        $this->UserOpenId   = $UserOpenId;
        $this->ecid = $ecid;
    }

    public function signIn(){
        if($this->activityInfo['id'] == ''){
            return Error::ERROR_ACTIVITY_INFO_EMPTY; 
        }

        $this->setResponse();

        return $this;
    }

    public function response(){
        return $this->responseInfo;
    }

    private function getUserInfo($openId){
        $m = M('Company_'.$this->ecid.'_user_info');
        $opt['openId'] = $openId;

        $result = $m->where($opt)->find();

        if($result){
            $this->userInfo = $result;
        }
    }
    //签到判断后的流程
    private function setResponse(){
        import( '@.Response.WechatConst' );

        if(!$this->userInfo)
            $this->getUserInfo($this->UserOpenId);

        $date=date("Y-m-d");
        if(($date>=$this->activityInfo['startTime'] && $date<=$this->activityInfo['endTime']) || $this->activityInfo['timeType'] == 'unlimit'){
            $this->responseInfo[0] = array(
                'Title' => $this->activityInfo['name'],
                'Description' => '',
                'Url' => WechatConst::SERVER_DOMAIN,
                'PicUrl' => $this->getImgUrl($this->activityInfo['bigImg'])
                );

            
            $title = '';
            //如果有记录 则签过到
            if($result = $this->hasSignIn()){
                $title = '您已完成签到操作！';
            }
            else{//没有记录 没签到
                $title = "欢迎您，{$this->userInfo['nickname']}，请点击验证个人身份完成签到！";
            }
            //链接
            $url = WechatConst::SERVER_DOMAIN . "/User/activity/activityId/{$this->activityInfo['id']}/openId/{$this->UserOpenId}/ecid/{$this->ecid}";

            $this->responseInfo[1] = array(
                'Title' => $title,
                'Description' => '',
                'Url' => $url,
                'PicUrl' => $this->getImgUrl($this->activityInfo['minImg'])
                );
        }
    }
    //签到过程判断 判断是否签过到
    private function hasSignIn(){
        $m = M('Company_'.$this->ecid.'_activity_user');
        $opt['activityId']=$this->activityInfo['id'];
        $opt['openId'] = $this->UserOpenId;

        return $m->where($opt)->find();  
    }
}
?>