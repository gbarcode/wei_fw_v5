<?php

namespace Plan\Controller;

use Think\Controller;
use Org\Weibo\SaeTClientV2;
use Org\Weibo\SaeTOAuthV2;
use Org\Weixin\Wechat;

class IndexController extends Controller {

//定时发送微博
    public function index() {
        $now = strtotime(new \Org\Util\Date());

        $condition['sendTime'] = array('between', ($now - 60) . "," . ($now + 240));
        $condition['successTime'] = NULL || 0;

//        $condition['platform'] = "weibo";
        $sends = M('view_weibo_send_crontab')->where($condition)->select();

        if ($sends) {
            for ($i = 0; $i < count($sends); $i++) {
                if ($sends[$i]['platform'] == 'weibo') {
                    $this->sendWeibo($sends[$i]);
                } else if ($sends[$i]['platform'] == 'wechat') {
                    $this->wxGroupSend($sends[$i]);
                }
            }
        }else{
            echo 'no sends';
        }
    }

    function sendWeibo($msg) {
        $c = new SaeTClientV2($client_id, $client_secret, $msg['access_token']);
//                $d = new SaeTOAuthV2($client_id, $client_secret, $msg['access_token']);
//                dump($d->getTokenInfo());die;
        $text = "【" . $msg['title'] . "】" . $msg['content'] . $msg['url'];

        $ret = $c->upload($text, $msg['img']);
        if (isset($ret['error_code']) && $ret['error_code'] > 0) {
            echo "<p>发送失败，错误：{$ret['error_code']}:{$ret['error']}</p>";
        } else {
            $data = array('successTime' => $now);
            M('send_crontab_weibo')->where("id='" . $msg['id'] . "'")->setField($data);
            echo "<p>发送成功</p>";
        }
    }

    /*
     * @通过curl方式获取指定的图片到本地
     * @ 完整的图片地址
     * @ 要存储的文件名
     */

    public function getImg($url = "", $filename = "") {
        //去除URL连接上面可能的引号
        //$url = preg_replace( '/(?:^['"]+|['"/]+$)/', '', $url );
        $hander = curl_init();
        $fp = fopen($filename, 'wb');
        curl_setopt($hander, CURLOPT_URL, $url);
        curl_setopt($hander, CURLOPT_FILE, $fp);
        curl_setopt($hander, CURLOPT_HEADER, 0);
        curl_setopt($hander, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($hander,CURLOPT_RETURNTRANSFER,false);//以数据流的方式返回数据,当为false是直接显示出来
        curl_setopt($hander, CURLOPT_TIMEOUT, 60);
        curl_exec($hander);
        curl_close($hander);
        fclose($fp);
        return $filename;
    }

    //定时群发微信
    public function wxGroupSend($msg) {
        $company = M("Company_info")->where("company_ecid='" . $msg['ecid'] . "'")->find();
        $w = new Wechat();
        $accessToken = $w->checkAuth($company['weixin_AppId'], $company['weixin_AppSecret']);
        dump($accessToken);

        //获取group的news
        $row = M('Company_material_group')->where("media_id='" . $msg['content'] . "'AND ecid='" . $msg['ecid'] . "'")->find();
        $arrMaterialID = explode(',', $row['materialId']);

        $dir = dirname(dirname(dirname(dirname(__FILE__)))) . '/Public/uploads/';

        //封装每条news 转换成可上传的群发图文格式
        for ($i = 0; $i < count($arrMaterialID); $i++) {

            $m = M('Company_news');
            $result = $m->find($arrMaterialID[$i]);

            $type = explode('.', $result['bigImg']);
            $file = $this->getImg($result['bigImg'], $dir . round(strtotime(New \Org\Util\Date())) . "." . $type[3]);

            $item = array(
                'media' => '@' . $file
            );
            $thumb_media = $w->uploadMedia($item, 'image'); //上传每个news的图片素材，群发微信暂时只支持临时素材
            unlink($file);

            $articles[$i] = array(
                'title' => $result['title'],
                'thumb_media_id' => $thumb_media['media_id'],
                'author' => '',
                'digest' => $result['description'],
                'show_cover_pic' => $result['addPicture'],
                'content' => $result['content'],
                'content_source_url' => '');
        }
        $articles = array('articles' => $articles);
        $media = $w->uploadArticles($articles);

//        $articles[0] = array(
//                        'title' => 'test',
//                        'thumb_media_id' => "xXbh1rAJXIjiqrBB8PH-8QiSclpdVryZBTT0pE4PGCT7naVLBfiIjQLBAcs93-sZ",
//                        'author' => '',
//                        'digest' => "",
//                        'show_cover_pic' => "1",
//                        'content' => "测试",
//                        'content_source_url' => '');
//        $articles = array('articles'=>$articles);
//        $media = $w->uploadArticles($articles);
//        dump($media);die;

        $data['filter'] = array('is_to_all' => false, 'group_id' => "136");
        $data['mpnews'] = array('media_id' => $media['media_id']);
        $data['msgtype'] = 'mpnews';
        $ret = $w->sendGroupMassMessage($data);
        if (isset($ret['error_code']) && $ret['error_code'] > 0) {
            echo "<p>发送失败，错误：{$ret['error_code']}:{$ret['error']}</p>";
        } else {
            $data = array('successTime' => $now);
            M('send_crontab_weibo')->where("id='" . $msg['id'] . "'")->setField($data);
            echo "<p>发送成功</p>";
        }
    }

}
