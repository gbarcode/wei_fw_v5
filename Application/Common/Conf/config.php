<?php
return array(
	'MODULE_ALLOW_LIST'     =>  array('Home','Admin', 'Weixin', 'Api', 'Mobile', 'Web', 'Fw'), // 配置你原来的分组列表
	'DEFAULT_MODULE'        =>  'Home', // 配置你原来的默认分组
    //'配置项'=>'配置值'
    'URL_CASE_INSENSITIVE' =>false,
    'SHOW_ERROR_MSG'        =>  true,
    'TMPL_L_DELIM'    =>'<{', //修改左定界符
    'TMPL_R_DELIM'    =>'}>', //修改右定界符

    //数据库配置信息
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => 'gbarcode.mysql.rds.aliyuncs.com', // 服务器地址
    'DB_NAME'   => 'db_fw', // 数据库名
    'DB_USER'   => 'gbarcode', // 用户名
    'DB_PWD'    => 'tytm2011', // 密码
    'DB_PORT'   => 3306, // 端口
    'DB_CHARSET'=> 'utf8', // 字符集
   // 'DB_DSN'          =>'mysql://root:@127.0.0.1/db_fw', //使用DSN方式配置数据库信息

    'DB_PREFIX'       =>'sz12365_fw_',  //设置表前缀
    'SESSION_PREFIX'  =>'sz12365_fw',
    //'SHOW_PAGE_TRACE' =>true, //开启页面Trace
    //
    'URL_MODEL'          => '2',
    'APP_SUB_DOMAIN_DEPLOY'   =>    1, // 开启子域名配置
    'APP_SUB_DOMAIN_RULES'    =>    array(   
        'jump.msa12365.com'        => 'Api/Redirect',  // admin子域名指向Admin模块
        '*.web'   => array('Web','ecid=*'),  //
    ),

    'TMPL_PARSE_STRING'=>array(           //添加自己的模板变量规则
        '__CSS__'   =>__ROOT__.'/Public/Css',
        '__JS__'    =>__ROOT__.'/Public/Js',
        '__IMAGE__' =>__ROOT__.'/Public/Image',
        '__LOGIN__' =>__ROOT__.'/index.php/Index/Login',
        '__INDEX__' =>__ROOT__.'/index.php',
    ),

    //权限验证相关配置
    'AUTH_CONFIG'=>array(
        'AUTH_ON'           => true, //认证开关
        'AUTH_TYPE'         => 1, // 认证方式，1为时时认证；2为登录认证。
        'AUTH_GROUP'        => 'sz12365_weixin_auth_group', //用户组数据表名
        'AUTH_GROUP_ACCESS' => 'sz12365_weixin_auth_group_access', //用户组明细表
        'AUTH_RULE'         => 'sz12365_weixin_auth_rule', //权限规则表
        'AUTH_USER'         => 'sz12365_weixin_user_info'//用户信息表
    ),

    //登录用户相关配置
    'LOGIN_USER' => array(
        'ADMIN_UID' =>array( '14' ), //管理员用户ID
        'NAME'      => 'user_name',  //登录session记录用户名
        'UID'       => 'user_uid',  //登录session记录用户ID
        'ECID'      => 'ecid'    ,   //登录sessiong记录ecid
        'DEALERID'  => 'dealerId' ,  //登录sessiong记录经销商id
        'ROLE'      => 'role'       //登录sessiong记录用户权限
    ),

    //用户默认密码
    'DEFAULT_PWD' => md5('tytm123'),

    //配置U方法生成链接格式
    'URL_HTML_SUFFIX' => '',

    'SAFE_KEY' => 'sz12365',

    'WECHAT_ADMIN_API' => array(
        'APPID' => 'wx4a29f7f580e3e5e8',
        'APPSECRET' => '05a54008d13e70a464ba6f6c17d4777d'
        ),

    'MAIL_ADDRESS'=>'wx@sz12365.net', // 邮箱地址
    'MAIL_SMTP'=>'smtp.exmail.qq.com', // 邮箱SMTP服务器
    'MAIL_LOGINNAME'=>'wx@sz12365.net', // 邮箱登录帐号
    'MAIL_PASSWORD'=>'tytm123', // 邮箱密码
    'MAIL_CHARSET'=>'UTF-8',//编码
    'MAIL_AUTH'=>true,//邮箱认证
    'MAIL_HTML'=>true,//true HTML格式 false TXT格式

    //证书验证数据库连接
    'JEWELRY_CHECK_DB_LINK'     =>  'mysql://root:@127.0.0.1/client_data',
    'JEWELRY_CHECK_TABLE_NAME'  =>  '_exdata' ,
    //证书验证企业编号
    'JEWELRY_COMPANY_ECID'=>array(
        '200113'
        ),
    'CLIENT_WEB_NAME' => array(
        '200113' => 'http://zg.fw.msa12365.com' ,
        ),
);
?>
